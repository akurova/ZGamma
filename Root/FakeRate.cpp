#include "EToGamReader.h"
#include "CalculationFakeRate.h"
#include "FakeRatePlotter.h"

void EToGamReader::getFakeRate(StrV input, double lumi){


	Double_t Nee=0;
	Double_t Negam=0;
	Double_t eeBKG=0;
	Double_t eyBKG=0;
	Double_t dNee=0;
	Double_t dNegam=0;
	Double_t deeBKG=0;
	Double_t deyBKG=0;
	Double_t err=0;

	Config conf("../ZGamma.cfg");

	if (conf.getBool("bkgSyst")||conf.getBool("Calculate")||conf.getBool("Plots")){
		if (conf.getBool("MassWindow")) fatal("you can't calculate mass window variation while calculating f-r from data");

		double scale = (conf.getNum("MEL")+conf.getNum("MER"))/(conf.getNum("BELL")-conf.getNum("BELR")+conf.getNum("BERR")-conf.getNum("BERL"));
		StrV data;
		data = input;

		conf.setValue("Central", "NO");
		conf.setValue("HighPt", "NO");
		auto FakeRateForw = CalculationFakeRate(data,"data_forw",lumi,conf);

		conf.setValue("Central", "YES");
		conf.setValue("HighPt", "NO");
		auto FakeRateCentLE = CalculationFakeRate(data,"data_cent_le",lumi,conf);

		conf.setValue("Central", "YES");
		conf.setValue("HighPt", "YES");
		auto FakeRateCentHE = CalculationFakeRate(data,"data_cent_he",lumi,conf);

		if (conf.getStr("Region")=="B" || conf.getStr("Region")=="C" || conf.getStr("Region")=="D" || conf.getStr("Region")=="B-E" || conf.getStr("Region")=="D-F" || conf.getStr("Region")=="E" || conf.getStr("Region")=="F"){

			FakeRateForw+=FakeRateCentLE;
			FakeRateForw+=FakeRateCentHE;

			if (conf.getBool("bkgSyst")||conf.getBool("bkgFit")){
				NumV result = bkgFitSyst(FakeRateForw.hists,conf);
				params["fr_forw"]=result[0];
				params["fr_forw_stat"]=result[1];
				cout<<"forward fake rate: "<<params["fr_forw"]<<"+-"<<params["fr_forw_stat"]<<endl;
				params["fr_cent_lt250"]=result[0];
				params["fr_cent_lt250_stat"]=result[1];
				cout<<"central pT<250 GeV fake rate: "<<params["fr_cent_lt250"]<<"+-"<<params["fr_cent_lt250_stat"]<<endl;
				params["fr_cent_gt250"]=result[0];
				params["fr_cent_gt250_stat"]=result[1];
				cout<<"central pT<250 GeV fake rate: "<<params["fr_cent_gt250"]<<"+-"<<params["fr_cent_gt250_stat"]<<endl;
			}
		}else{
			if (conf.getBool("bkgSyst")||conf.getBool("bkgFit")){
				conf.setValue("Central", "NO");
				conf.setValue("HighPt", "NO");				
				NumV result = bkgFitSyst(FakeRateForw.hists,conf);
				params["fr_forw"]=result[0];
				params["fr_forw_stat"]=result[1];
				cout<<"forward fake rate: "<<params["fr_forw"]<<"+-"<<params["fr_forw_stat"]<<endl;
			}
			else{

				Nee=FakeRateForw.hists["M_yee"]->IntegralAndError(1,FakeRateForw.hists["M_yee"]->GetNbinsX(),dNee);
				Negam=FakeRateForw.hists["M_yey"]->IntegralAndError(1,FakeRateForw.hists["M_yey"]->GetNbinsX(),dNegam);
				eeBKG=FakeRateForw.hists["M_yee_b"]->IntegralAndError(1,FakeRateForw.hists["M_yee_b"]->GetNbinsX(),deeBKG);
				eyBKG=FakeRateForw.hists["M_yey_b"]->IntegralAndError(1,FakeRateForw.hists["M_yey_b"]->GetNbinsX(),deyBKG);

				params["fr_forw"]=(Negam-eyBKG*scale)/(Nee-eeBKG*scale);
				params["fr_forw_stat"]=sqrt(dNegam*dNegam/(Nee-scale*eeBKG)/(Nee-scale*eeBKG)+\
			  	scale*scale*deyBKG*deyBKG/(Nee-scale*eeBKG)/(Nee-scale*eeBKG)+\
			  	scale*scale*dNee*dNee*(Negam-scale*eyBKG)*(Negam-scale*eyBKG)/(Nee-scale*eeBKG)/(Nee-scale*eeBKG)/(Nee-scale*eeBKG)/(Nee-scale*eeBKG)+\
			  	scale*scale*deeBKG*deeBKG*(Negam-scale*eyBKG)*(Negam-scale*eyBKG)/(Nee-scale*eeBKG)/(Nee-scale*eeBKG)/(Nee-scale*eeBKG)/(Nee-scale*eeBKG));
			  
			  	cout<<"forward\n";
				cout<<"N ee ="<<Nee<<"-"<<eeBKG<<"*"<<scale<<"="<<Nee-eeBKG*scale<<endl;
				cout<<"N e gam ="<<Negam<<"-"<<eyBKG<<"*"<<scale<<"="<<Negam-eyBKG*scale<<endl;
				cout<<"fake rate: "<<params["fr_forw"]<<"+-"<<params["fr_forw_stat"]<<endl;
			}
			if (conf.getBool("Plots")||conf.getBool("PlotsMass") && !conf.getBool("bkgSyst")) {
				FakeRatePlotter p_forw = FakeRatePlotter(FakeRateForw.hists, conf);
			}



			if (conf.getBool("bkgSyst")||conf.getBool("bkgFit")){
				conf.setValue("Central", "YES");
				conf.setValue("HighPt", "NO");
				NumV result = bkgFitSyst(FakeRateCentLE.hists,conf);
				params["fr_cent_lt250"]=result[0];
				params["fr_cent_lt250_stat"]=result[1];
				cout<<"central pT<250 GeV fake rate: "<<params["fr_cent_lt250"]<<"+-"<<params["fr_cent_lt250_stat"]<<endl;
			}
			else{

				Nee=FakeRateCentLE.hists["M_yee"]->IntegralAndError(1,FakeRateCentLE.hists["M_yee"]->GetNbinsX(),dNee);
				Negam=FakeRateCentLE.hists["M_yey"]->IntegralAndError(1,FakeRateCentLE.hists["M_yey"]->GetNbinsX(),dNegam);
				eeBKG=FakeRateCentLE.hists["M_yee_b"]->IntegralAndError(1,FakeRateCentLE.hists["M_yee_b"]->GetNbinsX(),deeBKG);
				eyBKG=FakeRateCentLE.hists["M_yey_b"]->IntegralAndError(1,FakeRateCentLE.hists["M_yey_b"]->GetNbinsX(),deyBKG);

				params["fr_cent_lt250"]=(Negam-eyBKG*scale)/(Nee-eeBKG*scale);
				params["fr_cent_lt250_stat"]=sqrt(dNegam*dNegam/(Nee-scale*eeBKG)/(Nee-scale*eeBKG)+\
			  	scale*scale*deyBKG*deyBKG/(Nee-scale*eeBKG)/(Nee-scale*eeBKG)+\
			  	scale*scale*dNee*dNee*(Negam-scale*eyBKG)*(Negam-scale*eyBKG)/(Nee-scale*eeBKG)/(Nee-scale*eeBKG)/(Nee-scale*eeBKG)/(Nee-scale*eeBKG)+\
			  	scale*scale*deeBKG*deeBKG*(Negam-scale*eyBKG)*(Negam-scale*eyBKG)/(Nee-scale*eeBKG)/(Nee-scale*eeBKG)/(Nee-scale*eeBKG)/(Nee-scale*eeBKG));
			  
			  	cout<<"central pT<250 GeV\n";
				cout<<"N ee ="<<Nee<<"-"<<eeBKG<<"*"<<scale<<"="<<Nee-eeBKG*scale<<endl;
				cout<<"N e gam ="<<Negam<<"-"<<eyBKG<<"*"<<scale<<"="<<Negam-eyBKG*scale<<endl;
				cout<<"fake rate: "<<params["fr_cent_lt250"]<<"+-"<<params["fr_cent_lt250_stat"]<<endl;

			}
			if (conf.getBool("Plots")||conf.getBool("PlotsMass")&& !conf.getBool("bkgSyst")) {
				FakeRatePlotter p_cent_le = FakeRatePlotter(FakeRateCentLE.hists, conf);
			}



			if (conf.getBool("bkgSyst")||conf.getBool("bkgFit")){
				conf.setValue("Central", "YES");
				conf.setValue("HighPt", "YES");
				NumV result = bkgFitSyst(FakeRateCentHE.hists,conf);
				params["fr_cent_gt250"]=result[0];
				params["fr_cent_gt250_stat"]=result[1];
				cout<<"central pT>250 GeV fake rate: "<<params["fr_cent_gt250"]<<"+-"<<params["fr_cent_gt250_stat"]<<endl;
			}
			else{

				Nee=FakeRateCentHE.hists["M_yee"]->IntegralAndError(1,FakeRateCentHE.hists["M_yee"]->GetNbinsX(),dNee);
				Negam=FakeRateCentHE.hists["M_yey"]->IntegralAndError(1,FakeRateCentHE.hists["M_yey"]->GetNbinsX(),dNegam);
				eeBKG=FakeRateCentHE.hists["M_yee_b"]->IntegralAndError(1,FakeRateCentHE.hists["M_yee_b"]->GetNbinsX(),deeBKG);
				eyBKG=FakeRateCentHE.hists["M_yey_b"]->IntegralAndError(1,FakeRateCentHE.hists["M_yey_b"]->GetNbinsX(),deyBKG);

				params["fr_cent_gt250"]=(Negam-eyBKG*scale)/(Nee-eeBKG*scale);
				params["fr_cent_gt250_stat"]=sqrt(dNegam*dNegam/(Nee-scale*eeBKG)/(Nee-scale*eeBKG)+\
			  	scale*scale*deyBKG*deyBKG/(Nee-scale*eeBKG)/(Nee-scale*eeBKG)+\
			  	scale*scale*dNee*dNee*(Negam-scale*eyBKG)*(Negam-scale*eyBKG)/(Nee-scale*eeBKG)/(Nee-scale*eeBKG)/(Nee-scale*eeBKG)/(Nee-scale*eeBKG)+\
			  	scale*scale*deeBKG*deeBKG*(Negam-scale*eyBKG)*(Negam-scale*eyBKG)/(Nee-scale*eeBKG)/(Nee-scale*eeBKG)/(Nee-scale*eeBKG)/(Nee-scale*eeBKG));
			  
			  	cout<<"central pT>250 GeV\n";
				cout<<"N ee ="<<Nee<<"-"<<eeBKG<<"*"<<scale<<"="<<Nee-eeBKG*scale<<endl;
				cout<<"N e gam ="<<Negam<<"-"<<eyBKG<<"*"<<scale<<"="<<Negam-eyBKG*scale<<endl;
				cout<<"fake rate: "<<params["fr_cent_gt250"]<<"+-"<<params["fr_cent_gt250_stat"]<<endl;

			}
			if (conf.getBool("Plots")||conf.getBool("PlotsMass") && !conf.getBool("bkgSyst")) {
				FakeRatePlotter p_cent_he = FakeRatePlotter(FakeRateCentHE.hists, conf);
			}
		}
	} else {

		params["fr_forw"]=conf.getNum("fr_forw");
		params["fr_forw_stat"]=conf.getNum("fr_forw_stat");
		params["fr_cent_lt250"]=conf.getNum("fr_cent_lt250");
		params["fr_cent_lt250_stat"]=conf.getNum("fr_cent_lt250_stat");
		params["fr_cent_gt250"]=conf.getNum("fr_cent_gt250");
		params["fr_cent_gt250_stat"]=conf.getNum("fr_cent_gt250_stat");

	}

}

