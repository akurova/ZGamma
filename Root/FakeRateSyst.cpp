#include "EToGamReader.h"
#include "CalculationFakeRate.h"
#include "CalculationWenu.h"
#include "FakeRatePlotter.h"
#include "DirectoryReader.h"
#include "TMath.h"
void EToGamReader::getFakeRateSyst(){
	Config conf("../ZGamma.cfg");

params["var_Zee_MC_forw"]=0;
params["var_Zee_MC_cent_lt250"]=0;
params["var_Zee_MC_cent_gt250"]=0;

if (conf.getBool("RealFR")){ 

	std::cout<<"Hello there Systematics!\n";

	Double_t Nee=0;
	Double_t Negam=0;
	Double_t eeBKG=0;
	Double_t eyBKG=0;
	Double_t dNee=0;
	Double_t dNegam=0;
	Double_t dNeet=0;
	Double_t dNegamt=0;
	Double_t deeBKG=0;
	Double_t deyBKG=0;
	Double_t err=0;
	Double_t eMET=0;
	Double_t gammaMET=0;
	Double_t deMET=0;
	Double_t dgammaMET=0;


	double scale = (conf.getNum("MEL")+conf.getNum("MER"))/(conf.getNum("BELL")-conf.getNum("BELR")+conf.getNum("BERR")-conf.getNum("BERL"));
	StrV data;
	data = {filename};
	double edge=conf.getNum("MEL");
	double MEL=conf.getNum("MEL");
	double Mz=conf.getNum("Mz");
	double lumi1, lumi2, lumi3, lumi4, lumi; 
	lumi1 = conf.getNum("Lumi2015");
	lumi2 = conf.getNum("Lumi2016");
	lumi3 = conf.getNum("Lumi2017");
	lumi4 = conf.getNum("Lumi2018");
	lumi = lumi1+lumi2+lumi3+lumi4;  
	// printf("%.6f\n",lumi);
	DirectoryReader *dir1=0;
	map<TString,StrV> mc16a,mc16d,mc16e;
	StrV cath = {"Zee","Wenu_"};

    if (conf.isDefined("MC16aDir")) {
      dir1 = new DirectoryReader(conf.getStr("MC16aDir"),cath);
      mc16a = dir1->GetFilesList();
    }
    
    std::cout<<"making sample cathegorization for MC16a\n\n";
    for (auto p: mc16a){
      std::cout<<p.first<<":\n";
      for (auto p2: p.second){
        std::cout<<p2<<"\n";
      }
    }


    if (conf.isDefined("MC16dDir")) {
      dir1 = new DirectoryReader(conf.getStr("MC16dDir"),cath);
      mc16d = dir1->GetFilesList();
    }
    
    std::cout<<"making sample cathegorization for MC16d\n\n";
    for (auto p: mc16d){
      std::cout<<p.first<<":\n";
      for (auto p2: p.second){
        std::cout<<p2<<"\n";
      }
    }



    if (conf.isDefined("MC16eDir")) {
      dir1 = new DirectoryReader(conf.getStr("MC16eDir"),cath);
      mc16e = dir1->GetFilesList();
    }
    
    std::cout<<"making sample cathegorization for MC16e\n\n";
    for (auto p: mc16e){
      std::cout<<p.first<<":\n";
      for (auto p2: p.second){
        std::cout<<p2<<"\n";
      }
    }

    
	cout<<"\nFake rate from Zee MC:\n";
	conf.setValue("FromMC", "YES");

			conf.setValue("Central", "NO");
			conf.setValue("HighPt", "NO");
			auto FakeRateZee_forw = CalculationFakeRate(mc16a["Zee"],"Zee_mc16a_forw",lumi1+lumi2,conf);
			auto FakeRateZee_mc16d_forw = CalculationFakeRate(mc16d["Zee"],"Zee_mc16d_forw",lumi3,conf);
			FakeRateZee_forw+=FakeRateZee_mc16d_forw;
			auto FakeRateZee_mc16e_forw = CalculationFakeRate(mc16e["Zee"],"Zee_mc16e_forw",lumi4,conf);
			FakeRateZee_forw+=FakeRateZee_mc16e_forw;

			if (!conf.getBool("MassWindow")){
				Nee=FakeRateZee_forw.hists["M_yee"]->IntegralAndError(1,FakeRateZee_forw.hists["M_yee"]->GetNbinsX(),dNee);
				Negam=FakeRateZee_forw.hists["M_yey"]->IntegralAndError(1,FakeRateZee_forw.hists["M_yey"]->GetNbinsX(),dNegam);


				params["fr_forw_Zee"]=Negam/Nee;
				params["fr_forw_Zee_stat"]=sqrt(dNegam*dNegam/Nee/Nee+dNee*dNee*Negam*Negam/Nee/Nee/Nee/Nee);
			  	cout<<"forward\n";
				cout<<"N ee ="<<Nee<<endl;
				cout<<"N e gam ="<<Negam<<endl;
				printf("fake rate: %.6f±%.6f\n",params["fr_forw_Zee"],params["fr_forw_Zee_stat"]);
			}

			if (conf.getBool("PlotsMass")) {
				FakeRatePlotter p_forw = FakeRatePlotter(FakeRateZee_forw.hists, conf);
			}

			conf.setValue("Central", "YES");
			conf.setValue("HighPt", "NO");
			auto FakeRateZee_CentLE = CalculationFakeRate(mc16a["Zee"],"Zee_mc16a_CentLE",lumi1+lumi2,conf);
			auto FakeRateZee_mc16d_CentLE = CalculationFakeRate(mc16d["Zee"],"Zee_mc16d_CentLE",lumi3,conf);
			FakeRateZee_CentLE+=FakeRateZee_mc16d_CentLE;
			auto FakeRateZee_mc16e_CentLE = CalculationFakeRate(mc16e["Zee"],"Zee_mc16e_CentLE",lumi4,conf);
			FakeRateZee_CentLE+=FakeRateZee_mc16e_CentLE;
			
			if (!conf.getBool("MassWindow")){
				Nee=FakeRateZee_CentLE.hists["M_yee"]->IntegralAndError(1,FakeRateZee_CentLE.hists["M_yee"]->GetNbinsX(),dNee);
				Negam=FakeRateZee_CentLE.hists["M_yey"]->IntegralAndError(1,FakeRateZee_CentLE.hists["M_yey"]->GetNbinsX(),dNegam);

				params["fr_cent_lt250_Zee"]=Negam/Nee;
				params["fr_cent_lt250_Zee_stat"]=sqrt(dNegam*dNegam/Nee/Nee+dNee*dNee*Negam*Negam/Nee/Nee/Nee/Nee);
			  
			  	cout<<"central pT<250 GeV\n";
				cout<<"N ee ="<<Nee<<endl;
				cout<<"N e gam ="<<Negam<<endl;
				printf("fake rate: %.6f±%.6f\n",params["fr_cent_lt250_Zee"],params["fr_cent_lt250_Zee_stat"]);
			}

			if (conf.getBool("PlotsMass")) {
				FakeRatePlotter p_cent_le = FakeRatePlotter(FakeRateZee_CentLE.hists, conf);
			}

			conf.setValue("Central", "YES");
			conf.setValue("HighPt", "YES");
			auto FakeRateZee_CentHE = CalculationFakeRate(mc16a["Zee"],"Zee_mc16a_CentHE",lumi1+lumi2,conf);
			auto FakeRateZee_mc16d_CentHE = CalculationFakeRate(mc16d["Zee"],"Zee_mc16d_CentHE",lumi3,conf);
			FakeRateZee_CentHE+=FakeRateZee_mc16d_CentHE;
			auto FakeRateZee_mc16e_CentHE = CalculationFakeRate(mc16e["Zee"],"Zee_mc16e_CentHE",lumi4,conf);
			FakeRateZee_CentHE+=FakeRateZee_mc16e_CentHE;

			if (!conf.getBool("MassWindow")){
				Nee=FakeRateZee_CentHE.hists["M_yee"]->IntegralAndError(1,FakeRateZee_CentHE.hists["M_yee"]->GetNbinsX(),dNee);
				Negam=FakeRateZee_CentHE.hists["M_yey"]->IntegralAndError(1,FakeRateZee_CentHE.hists["M_yey"]->GetNbinsX(),dNegam);

				params["fr_cent_gt250_Zee"]=Negam/Nee;
				params["fr_cent_gt250_Zee_stat"]=sqrt(dNegam*dNegam/Nee/Nee+dNee*dNee*Negam*Negam/Nee/Nee/Nee/Nee);
			  
			  	cout<<"central pT>250 GeV\n";
				cout<<"N ee ="<<Nee<<endl;
				cout<<"N e gam ="<<Negam<<endl;
				printf("fake rate: %.6f±%.6f\n",params["fr_cent_gt250_Zee"],params["fr_cent_gt250_Zee_stat"]);
			}

			if (conf.getBool("PlotsMass")) {
				FakeRatePlotter p_cent_he = FakeRatePlotter(FakeRateZee_CentHE.hists, conf);
			}

			if (conf.getBool("MassWindow")){

				cout<<"\nForward\n";
				Nee=FakeRateZee_forw.hists["M_yee"]->IntegralAndError(FakeRateZee_forw.hists["M_yee"]->GetXaxis()->FindBin(Mz-MEL),FakeRateZee_forw.hists["M_yee"]->GetXaxis()->FindBin(Mz+MEL),dNee);
				Negam=FakeRateZee_forw.hists["M_yey"]->IntegralAndError(FakeRateZee_forw.hists["M_yey"]->GetXaxis()->FindBin(Mz-MEL),FakeRateZee_forw.hists["M_yee"]->GetXaxis()->FindBin(Mz+MEL),dNegam);
				cout<<"nominal ee "<<Nee<<"±"<<dNee<<" nominal egam "<<Negam<<"±"<<dNegam<<" fake-rate "<<Negam/Nee<<"±"<<sqrt(dNegam*dNegam/Nee/Nee+dNee*dNee*Negam*Negam/Nee/Nee/Nee/Nee)<<"\n";
				Double_t ee=Nee; Double_t egam=Negam; 
				cout<<"increasing window\n";
				while (fabs(Nee-ee)<dNee || fabs(Negam-egam)<dNegam){
					edge+=0.2;
					ee=FakeRateZee_forw.hists["M_yee"]->IntegralAndError(FakeRateZee_forw.hists["M_yee"]->GetXaxis()->FindBin(Mz-edge),FakeRateZee_forw.hists["M_yee"]->GetXaxis()->FindBin(Mz+edge),dNeet);
					egam=FakeRateZee_forw.hists["M_yey"]->IntegralAndError(FakeRateZee_forw.hists["M_yey"]->GetXaxis()->FindBin(Mz-edge),FakeRateZee_forw.hists["M_yee"]->GetXaxis()->FindBin(Mz+edge),dNegamt);
					cout<<"edge "<<edge<<" ee "<<ee<<" diff "<<ee-Nee<<" egam "<<egam<<" diff "<<egam-Negam<<" "<<egam/ee<<"±"<<sqrt(dNegamt*dNegamt/ee/ee+dNeet*dNeet*egam*egam/ee/ee/ee/ee)<<"\n";
				} edge=MEL; Double_t varI=egam/ee; 
				cout<<"fake rate "<<varI<<"±"<<sqrt(dNegamt*dNegamt/ee/ee+dNeet*dNeet*egam*egam/ee/ee/ee/ee)<<"\n";
				ee=Nee; egam=Negam; 
				cout<<"decreasing window\n";
				while (fabs(Nee-ee)<dNee || fabs(Negam-egam)<dNegam){
					edge-=0.2;
					ee=FakeRateZee_forw.hists["M_yee"]->IntegralAndError(FakeRateZee_forw.hists["M_yee"]->GetXaxis()->FindBin(Mz-edge),FakeRateZee_forw.hists["M_yee"]->GetXaxis()->FindBin(Mz+edge),dNeet);
					egam=FakeRateZee_forw.hists["M_yey"]->IntegralAndError(FakeRateZee_forw.hists["M_yey"]->GetXaxis()->FindBin(Mz-edge),FakeRateZee_forw.hists["M_yee"]->GetXaxis()->FindBin(Mz+edge),dNegamt);
					cout<<"edge "<<edge<<" ee "<<ee<<" diff "<<fabs(ee-Nee)<<" egam "<<egam<<" diff "<<fabs(egam-Negam)<<" "<<egam/ee<<"±"<<sqrt(dNegamt*dNegamt/ee/ee+dNeet*dNeet*egam*egam/ee/ee/ee/ee)<<"\n";
				} Double_t varD=egam/ee;		
				cout<<"fake rate "<<varD<<"±"<<sqrt(dNegamt*dNegamt/ee/ee+dNeet*dNeet*egam*egam/ee/ee/ee/ee)<<"\n";
				printf("largest variation: %.6f%%\n",max(fabs(Negam/Nee-varD)/Negam*Nee*100,fabs(Negam/Nee-varI)/Negam*Nee*100));

				cout<<"\ncentral pT<250 GeV\n";
				Nee=FakeRateZee_CentLE.hists["M_yee"]->IntegralAndError(FakeRateZee_CentLE.hists["M_yee"]->GetXaxis()->FindBin(Mz-MEL),FakeRateZee_CentLE.hists["M_yee"]->GetXaxis()->FindBin(Mz+MEL),dNee);
				Negam=FakeRateZee_CentLE.hists["M_yey"]->IntegralAndError(FakeRateZee_CentLE.hists["M_yey"]->GetXaxis()->FindBin(Mz-MEL),FakeRateZee_CentLE.hists["M_yee"]->GetXaxis()->FindBin(Mz+MEL),dNegam);
				cout<<"nominal ee "<<Nee<<"±"<<dNee<<" nominal egam "<<Negam<<"±"<<dNegam<<" fake-rate "<<Negam/Nee<<"±"<<sqrt(dNegam*dNegam/Nee/Nee+dNee*dNee*Negam*Negam/Nee/Nee/Nee/Nee)<<"\n";
				ee=Nee; egam=Negam; edge=MEL;
				cout<<"increasing window\n";
				while (fabs(Nee-ee)<dNee || fabs(Negam-egam)<dNegam){
					edge+=0.2;
					ee=FakeRateZee_CentLE.hists["M_yee"]->IntegralAndError(FakeRateZee_CentLE.hists["M_yee"]->GetXaxis()->FindBin(Mz-edge),FakeRateZee_CentLE.hists["M_yee"]->GetXaxis()->FindBin(Mz+edge),dNeet);
					egam=FakeRateZee_CentLE.hists["M_yey"]->IntegralAndError(FakeRateZee_CentLE.hists["M_yey"]->GetXaxis()->FindBin(Mz-edge),FakeRateZee_CentLE.hists["M_yee"]->GetXaxis()->FindBin(Mz+edge),dNegamt);
					cout<<"edge "<<edge<<" ee "<<ee<<" diff "<<ee-Nee<<" egam "<<egam<<" diff "<<egam-Negam<<" "<<egam/ee<<"±"<<sqrt(dNegamt*dNegamt/ee/ee+dNeet*dNeet*egam*egam/ee/ee/ee/ee)<<"\n";
				} edge=MEL; varI=egam/ee; 
				cout<<"fake rate "<<varI<<"±"<<sqrt(dNegamt*dNegamt/ee/ee+dNeet*dNeet*egam*egam/ee/ee/ee/ee)<<"\n";
				ee=Nee; egam=Negam; 
				cout<<"decreasing window\n";
				while (fabs(Nee-ee)<dNee || fabs(Negam-egam)<dNegam){
					edge-=0.2;
					ee=FakeRateZee_CentLE.hists["M_yee"]->IntegralAndError(FakeRateZee_CentLE.hists["M_yee"]->GetXaxis()->FindBin(Mz-edge),FakeRateZee_CentLE.hists["M_yee"]->GetXaxis()->FindBin(Mz+edge),dNeet);
					egam=FakeRateZee_CentLE.hists["M_yey"]->IntegralAndError(FakeRateZee_CentLE.hists["M_yey"]->GetXaxis()->FindBin(Mz-edge),FakeRateZee_CentLE.hists["M_yee"]->GetXaxis()->FindBin(Mz+edge),dNegamt);
					cout<<"edge "<<edge<<" ee "<<ee<<" diff "<<fabs(ee-Nee)<<" egam "<<egam<<" diff "<<fabs(egam-Negam)<<" "<<egam/ee<<"±"<<dNegamt*dNegamt/ee/ee+dNeet*dNeet*egam*egam/ee/ee/ee/ee<<"\n";
				} varD=egam/ee;		
				cout<<"fake rate "<<varD<<"±"<<sqrt(dNegamt*dNegamt/ee/ee+dNeet*dNeet*egam*egam/ee/ee/ee/ee)<<"\n";
				printf("largest variation: %.6f%%\n",max(fabs(Negam/Nee-varD)/Negam*Nee*100,fabs(Negam/Nee-varI)/Negam*Nee*100));

				cout<<"\ncentral pT>250 GeV\n";
				Nee=FakeRateZee_CentHE.hists["M_yee"]->IntegralAndError(FakeRateZee_CentHE.hists["M_yee"]->GetXaxis()->FindBin(Mz-MEL),FakeRateZee_CentHE.hists["M_yee"]->GetXaxis()->FindBin(Mz+MEL),dNee);
				Negam=FakeRateZee_CentHE.hists["M_yey"]->IntegralAndError(FakeRateZee_CentHE.hists["M_yey"]->GetXaxis()->FindBin(Mz-MEL),FakeRateZee_CentHE.hists["M_yee"]->GetXaxis()->FindBin(Mz+MEL),dNegam);
				cout<<"nominal ee "<<Nee<<"±"<<dNee<<" nominal egam "<<Negam<<"±"<<dNegam<<" fake-rate "<<Negam/Nee<<"±"<<sqrt(dNegam*dNegam/Nee/Nee+dNee*dNee*Negam*Negam/Nee/Nee/Nee/Nee)<<"\n";
				ee=Nee; egam=Negam; edge=MEL;
				cout<<"increasing window\n";
				while (fabs(Nee-ee)<dNee || fabs(Negam-egam)<dNegam){
					edge+=0.2;
					ee=FakeRateZee_CentHE.hists["M_yee"]->IntegralAndError(FakeRateZee_CentHE.hists["M_yee"]->GetXaxis()->FindBin(Mz-edge),FakeRateZee_CentHE.hists["M_yee"]->GetXaxis()->FindBin(Mz+edge),dNeet);
					egam=FakeRateZee_CentHE.hists["M_yey"]->IntegralAndError(FakeRateZee_CentHE.hists["M_yey"]->GetXaxis()->FindBin(Mz-edge),FakeRateZee_CentHE.hists["M_yee"]->GetXaxis()->FindBin(Mz+edge),dNegamt);
					cout<<"edge "<<edge<<" ee "<<ee<<" diff "<<ee-Nee<<" egam "<<egam<<" diff "<<egam-Negam<<" "<<egam/ee<<"±"<<sqrt(dNegamt*dNegamt/ee/ee+dNeet*dNeet*egam*egam/ee/ee/ee/ee)<<"\n";
				} edge=MEL; varI=egam/ee; 
				cout<<"fake rate "<<varI<<"±"<<sqrt(dNegamt*dNegamt/ee/ee+dNeet*dNeet*egam*egam/ee/ee/ee/ee)<<"\n";
				ee=Nee; egam=Negam; 
				cout<<"decreasing window\n";
				while (fabs(Nee-ee)<dNee || fabs(Negam-egam)<dNegam){
					edge-=0.2;
					ee=FakeRateZee_CentHE.hists["M_yee"]->IntegralAndError(FakeRateZee_CentHE.hists["M_yee"]->GetXaxis()->FindBin(Mz-edge),FakeRateZee_CentHE.hists["M_yee"]->GetXaxis()->FindBin(Mz+edge),dNeet);
					egam=FakeRateZee_CentHE.hists["M_yey"]->IntegralAndError(FakeRateZee_CentHE.hists["M_yey"]->GetXaxis()->FindBin(Mz-edge),FakeRateZee_CentHE.hists["M_yee"]->GetXaxis()->FindBin(Mz+edge),dNegamt);
					cout<<"edge "<<edge<<" ee "<<ee<<" diff "<<fabs(ee-Nee)<<" egam "<<egam<<" diff "<<fabs(egam-Negam)<<" "<<egam/ee<<"±"<<sqrt(dNegamt*dNegamt/ee/ee+dNeet*dNeet*egam*egam/ee/ee/ee/ee)<<"\n";
				} varD=egam/ee;		
				cout<<"fake rate "<<varD<<"±"<<sqrt(dNegamt*dNegamt/ee/ee+dNeet*dNeet*egam*egam/ee/ee/ee/ee)<<"\n";
				printf("largest variation: %.6f%%\n",max(fabs(Negam/Nee-varD)/Negam*Nee*100,fabs(Negam/Nee-varI)/Negam*Nee*100));

				return;
	    	}


	cout<<"\nReal fake rate from Zee MC:\n";
	conf.setValue("FromMC", "YES");
	conf.setValue("Truth", "YES");

			conf.setValue("Central", "NO");
			conf.setValue("HighPt", "NO");
			auto FakeRateZee_Truth_forw = CalculationFakeRate(mc16a["Zee"],"Zee_Truth_mc16a_forw",lumi1+lumi2,conf);
			auto FakeRateZee_Truth_mc16d_forw = CalculationFakeRate(mc16d["Zee"],"Zee_Truth_mc16d_forw",lumi3,conf);
			FakeRateZee_Truth_forw+=FakeRateZee_Truth_mc16d_forw;
			auto FakeRateZee_Truth_mc16e_forw = CalculationFakeRate(mc16e["Zee"],"Zee_Truth_mc16e_forw",lumi4,conf);
			FakeRateZee_Truth_forw+=FakeRateZee_Truth_mc16e_forw;

			Nee=FakeRateZee_Truth_forw.hists["M_yee"]->IntegralAndError(1,FakeRateZee_Truth_forw.hists["M_yee"]->GetNbinsX(),dNee);
			Negam=FakeRateZee_Truth_forw.hists["M_yey"]->IntegralAndError(1,FakeRateZee_Truth_forw.hists["M_yey"]->GetNbinsX(),dNegam);


				params["fr_forw_Zee_Truth"]=Negam/Nee;
				params["fr_forw_Zee_Truth_stat"]=sqrt(dNegam*dNegam/Nee/Nee+dNee*dNee*Negam*Negam/Nee/Nee/Nee/Nee);
			  	cout<<"forward\n";
				cout<<"N ee ="<<Nee<<endl;
				cout<<"N e gam ="<<Negam<<endl;
				printf("fake rate: %.6f±%.6f\n",params["fr_forw_Zee_Truth"],params["fr_forw_Zee_Truth_stat"]);

				printf("systematics from truth - tag-n-probe difference: %.6f %%\n",fabs(params["fr_forw_Zee_Truth"]-params["fr_forw_Zee"])/min(params["fr_forw_Zee_Truth"],params["fr_forw_Zee"])*100);


			conf.setValue("Central", "YES");
			conf.setValue("HighPt", "NO");
			auto FakeRateZee_Truth_CentLE = CalculationFakeRate(mc16a["Zee"],"Zee_Truth_mc16a_CentLE",lumi1+lumi2,conf);
			auto FakeRateZee_Truth_mc16d_CentLE = CalculationFakeRate(mc16d["Zee"],"Zee_Truth_mc16d_CentLE",lumi3,conf);
			FakeRateZee_Truth_CentLE+=FakeRateZee_Truth_mc16d_CentLE;
			auto FakeRateZee_Truth_mc16e_CentLE = CalculationFakeRate(mc16e["Zee"],"Zee_Truth_mc16e_CentLE",lumi4,conf);
			FakeRateZee_Truth_CentLE+=FakeRateZee_Truth_mc16e_CentLE;

			Nee=FakeRateZee_Truth_CentLE.hists["M_yee"]->IntegralAndError(1,FakeRateZee_Truth_CentLE.hists["M_yee"]->GetNbinsX(),dNee);
			Negam=FakeRateZee_Truth_CentLE.hists["M_yey"]->IntegralAndError(1,FakeRateZee_Truth_CentLE.hists["M_yey"]->GetNbinsX(),dNegam);

				params["fr_cent_lt250_Zee_Truth"]=Negam/Nee;
				params["fr_cent_lt250_Zee_Truth_stat"]=sqrt(dNegam*dNegam/Nee/Nee+dNee*dNee*Negam*Negam/Nee/Nee/Nee/Nee);
			  
			  	cout<<"central pT<250 GeV\n";
				cout<<"N ee ="<<Nee<<endl;
				cout<<"N e gam ="<<Negam<<endl;
				printf("fake rate: %.6f±%.6f\n",params["fr_cent_lt250_Zee_Truth"],params["fr_cent_lt250_Zee_Truth_stat"]);

				printf("systematics from truth - tag-n-probe difference: %.6f %%\n",fabs(params["fr_cent_lt250_Zee_Truth"]-params["fr_cent_lt250_Zee"])/min(params["fr_cent_lt250_Zee_Truth"],params["fr_cent_lt250_Zee"])*100);

			conf.setValue("Central", "YES");
			conf.setValue("HighPt", "YES");
			auto FakeRateZee_Truth_CentHE = CalculationFakeRate(mc16a["Zee"],"Zee_Truth_mc16a_CentHE",lumi1+lumi2,conf);
			auto FakeRateZee_Truth_mc16d_CentHE = CalculationFakeRate(mc16d["Zee"],"Zee_Truth_mc16d_CentHE",lumi3,conf);
			FakeRateZee_Truth_CentHE+=FakeRateZee_Truth_mc16d_CentHE;
			auto FakeRateZee_Truth_mc16e_CentHE = CalculationFakeRate(mc16e["Zee"],"Zee_Truth_mc16e_CentHE",lumi4,conf);
			FakeRateZee_Truth_CentHE+=FakeRateZee_Truth_mc16e_CentHE;

			Nee=FakeRateZee_Truth_CentHE.hists["M_yee"]->IntegralAndError(1,FakeRateZee_Truth_CentHE.hists["M_yee"]->GetNbinsX(),dNee);
			Negam=FakeRateZee_Truth_CentHE.hists["M_yey"]->IntegralAndError(1,FakeRateZee_Truth_CentHE.hists["M_yey"]->GetNbinsX(),dNegam);

				params["fr_cent_gt250_Zee_Truth"]=Negam/Nee;
				params["fr_cent_gt250_Zee_Truth_stat"]=sqrt(dNegam*dNegam/Nee/Nee+dNee*dNee*Negam*Negam/Nee/Nee/Nee/Nee);
			  
			  	cout<<"central pT>250 GeV\n";
				cout<<"N ee ="<<Nee<<endl;
				cout<<"N e gam ="<<Negam<<endl;
				printf("fake rate: %.6f±%.6f\n",params["fr_cent_gt250_Zee_Truth"],params["fr_cent_gt250_Zee_Truth_stat"]);
				printf("systematics from truth - tag-n-probe difference: %.6f %%\n",fabs(params["fr_cent_gt250_Zee_Truth"]-params["fr_cent_gt250_Zee"])/min(params["fr_cent_gt250_Zee_Truth"],params["fr_cent_gt250_Zee"])*100);

	cout<<"\nReal fake rate from Wenu MC (currently not taken into account, if needed uncomment in FakeRateSyst.cpp)\n";

				// conf.setValue("mjj","0");
				// conf.setValue("GamCentrality","0");
				// conf.setValue("angular","NO");
				// conf.setValue("VBSjets","NO");
				// conf.setValue("CR2","NO");
				// auto WenuSR = CalculationWenu(mc16a["Wenu_"],"WenuSR",lumi1+lumi2,conf);
				// auto WenuSR_mc16d = CalculationWenu(mc16d["Wenu_"],"WenuSR_mc16d",lumi3,conf);
				// WenuSR+=WenuSR_mc16d;
				// auto WenuSR_mc16e = CalculationWenu(mc16e["Wenu_"],"WenuSR_mc16e",lumi4,conf);
				// WenuSR+=WenuSR_mc16e;			

				// conf.setValue("CR2","YES");
				// auto WenuCR2 = CalculationWenu(mc16a["Wenu_"],"WenuCR2",lumi1+lumi2,conf);
				// auto WenuCR2_mc16d = CalculationWenu(mc16d["Wenu_"],"WenuCR2_mc16d",lumi3,conf);
				// WenuCR2+=WenuCR2_mc16d;
				// auto WenuCR2_mc16e = CalculationWenu(mc16e["Wenu_"],"WenuCR2_mc16e",lumi4,conf);
				// WenuCR2+=WenuCR2_mc16e;			

			 //  	cout<<"forward\n";
			 //  	gammaMET=WenuSR.hists["n_ph_forw"]->IntegralAndError(1,WenuSR.hists["n_ph_forw"]->GetNbinsX(),dgammaMET);
			 //  	eMET=WenuCR2.hists["n_ph_forw"]->IntegralAndError(1,WenuCR2.hists["n_ph_forw"]->GetNbinsX(),deMET);
			 //  	params["fr_forw_Wenu_Truth"]=gammaMET/eMET;
			 //  	params["fr_forw_Wenu_Truth_stat"]=sqrt(dgammaMET*dgammaMET/eMET/eMET+deMET*deMET*gammaMET*gammaMET/eMET/eMET/eMET/eMET);
			 //  	cout<<"gamma+MET = "<<gammaMET<<endl;
			 //  	cout<<"e+MET = "<<eMET<<endl;
				// printf("fake rate: %.6f±%.6f\n",params["fr_forw_Wenu_Truth"],params["fr_forw_Wenu_Truth_stat"]);
				// printf("systematics from truth Zee and Wenu difference: %.6f %%\n",fabs(params["fr_forw_Zee_Truth"]-params["fr_forw_Wenu_Truth"])/min(params["fr_forw_Zee_Truth"],params["fr_forw_Wenu_Truth"])*100);
			  	
			 //  	cout<<"central pT<250 GeV\n";
			 //  	gammaMET=WenuSR.hists["n_ph_cent_lt250"]->IntegralAndError(1,WenuSR.hists["n_ph_cent_lt250"]->GetNbinsX(),dgammaMET);
			 //  	eMET=WenuCR2.hists["n_ph_cent_lt250"]->IntegralAndError(1,WenuCR2.hists["n_ph_cent_lt250"]->GetNbinsX(),deMET);
			 //  	params["fr_cent_lt250_Wenu_Truth"]=gammaMET/eMET;
			 //  	params["fr_cent_lt250_Wenu_Truth_stat"]=sqrt(dgammaMET*dgammaMET/eMET/eMET+deMET*deMET*gammaMET*gammaMET/eMET/eMET/eMET/eMET);
			 //  	cout<<"gamma+MET = "<<gammaMET<<endl;
			 //  	cout<<"e+MET = "<<eMET<<endl;
				// printf("fake rate: %.6f±%.6f\n",params["fr_cent_lt250_Wenu_Truth"],params["fr_cent_lt250_Wenu_Truth_stat"]);
				// printf("systematics from truth Zee and Wenu difference: %.6f %%\n",fabs(params["fr_cent_lt250_Zee_Truth"]-params["fr_cent_lt250_Wenu_Truth"])/min(params["fr_cent_lt250_Zee_Truth"],params["fr_cent_lt250_Wenu_Truth"])*100);
			  	
			 //  	cout<<"central pT>250 GeV\n";
			 //  	gammaMET=WenuSR.hists["n_ph_cent_gt250"]->IntegralAndError(1,WenuSR.hists["n_ph_cent_gt250"]->GetNbinsX(),dgammaMET);
			 //  	eMET=WenuCR2.hists["n_ph_cent_gt250"]->IntegralAndError(1,WenuCR2.hists["n_ph_cent_gt250"]->GetNbinsX(),deMET);
			 //  	params["fr_cent_gt250_Wenu_Truth"]=gammaMET/eMET;
			 //  	params["fr_cent_gt250_Wenu_Truth_stat"]=sqrt(dgammaMET*dgammaMET/eMET/eMET+deMET*deMET*gammaMET*gammaMET/eMET/eMET/eMET/eMET);
			 //  	cout<<"gamma+MET = "<<gammaMET<<endl;
			 //  	cout<<"e+MET = "<<eMET<<endl;
				// printf("fake rate: %.6f±%.6f\n",params["fr_cent_gt250_Wenu_Truth"],params["fr_cent_gt250_Wenu_Truth_stat"]);
				// printf("systematics from truth Zee and Wenu difference: %.6f %%\n",fabs(params["fr_cent_gt250_Zee_Truth"]-params["fr_cent_gt250_Wenu_Truth"])/min(params["fr_cent_gt250_Zee_Truth"],params["fr_cent_gt250_Wenu_Truth"])*100);
			  	
			 //  	printf("Other components\n");
			 //  	printf("Mass window var: %.6f%% %.6f%% %.6f%%\n",conf.getNum("var_forw"),conf.getNum("var_cent_lt250"),conf.getNum("var_cent_gt250"));
			 //  	printf("Bkg under Z peak var: %.6f%% %.6f%% %.6f%%\n",conf.getNum("var_bkg_forw"),conf.getNum("var_bkg_cent_lt250"),conf.getNum("var_bkg_cent_gt250"));
			params["var_Zee_MC_forw"]=fabs(params["fr_forw_Zee_Truth"]-params["fr_forw_Zee"])/min(params["fr_forw_Zee_Truth"],params["fr_forw_Zee"]);
			params["var_Zee_MC_cent_lt250"]=fabs(params["fr_cent_lt250_Zee_Truth"]-params["fr_cent_lt250_Zee"])/min(params["fr_cent_lt250_Zee_Truth"],params["fr_cent_lt250_Zee"]);
			params["var_Zee_MC_cent_gt250"]=fabs(params["fr_cent_gt250_Zee_Truth"]-params["fr_cent_gt250_Zee"])/min(params["fr_cent_gt250_Zee_Truth"],params["fr_cent_gt250_Zee"]);
} else {
			params["var_Zee_MC_forw"]=conf.getNum("real_syst_forw");
			params["var_Zee_MC_cent_lt250"]=conf.getNum("real_syst_cent_le");
			params["var_Zee_MC_cent_gt250"]=conf.getNum("real_syst_cent_he");
}				

			  	cout<<"\nTotal uncertainty on fake rate\n";
			  	cout<<"\n"<<params["var_Zee_MC_cent_lt250"]<<"\n";

			  	params["fr_forw_sys_total_rel"]=sqrt(params["var_Zee_MC_forw"]*params["var_Zee_MC_forw"]+\
			  		//fabs(params["fr_forw_Zee_Truth"]-params["fr_forw_Wenu_Truth"])/min(params["fr_forw_Zee_Truth"],params["fr_forw_Wenu_Truth"])*fabs(params["fr_forw_Zee_Truth"]-params["fr_forw_Wenu_Truth"])/min(params["fr_forw_Zee_Truth"],params["fr_forw_Wenu_Truth"])+
		  			conf.getNum("var_forw")*conf.getNum("var_forw")*0.0001+\
		  			conf.getNum("var_bkg_forw")*conf.getNum("var_bkg_forw")*0.0001);//\
			  	// +0.409327*0.409327); //additional uncertainty on binning vs eta
			  	params["fr_cent_lt250_sys_total_rel"]=sqrt(params["var_Zee_MC_cent_lt250"]*params["var_Zee_MC_cent_lt250"]+\
			  		//fabs(params["fr_cent_lt250_Zee_Truth"]-params["fr_cent_lt250_Wenu_Truth"])/min(params["fr_cent_lt250_Zee_Truth"],params["fr_cent_lt250_Wenu_Truth"])*fabs(params["fr_cent_lt250_Zee_Truth"]-params["fr_cent_lt250_Wenu_Truth"])/min(params["fr_cent_lt250_Zee_Truth"],params["fr_cent_lt250_Wenu_Truth"])+
			  		conf.getNum("var_cent_lt250")*conf.getNum("var_cent_lt250")*0.0001+\
			  		conf.getNum("var_bkg_cent_lt250")*conf.getNum("var_bkg_cent_lt250")*0.0001);//\
			  	// +0.311559*0.311559); //additional uncertainty on binning vs eta
			  	params["fr_cent_gt250_sys_total_rel"]=sqrt(params["var_Zee_MC_cent_gt250"]*params["var_Zee_MC_cent_gt250"]+\
			  		//fabs(params["fr_cent_gt250_Zee_Truth"]-params["fr_cent_gt250_Wenu_Truth"])/min(params["fr_cent_gt250_Zee_Truth"],params["fr_cent_gt250_Wenu_Truth"])*fabs(params["fr_cent_gt250_Zee_Truth"]-params["fr_cent_gt250_Wenu_Truth"])/min(params["fr_cent_gt250_Zee_Truth"],params["fr_cent_gt250_Wenu_Truth"])+
			  		conf.getNum("var_cent_gt250")*conf.getNum("var_cent_gt250")*0.0001+\
			  		conf.getNum("var_bkg_cent_gt250")*conf.getNum("var_bkg_cent_gt250")*0.0001);//\
			  	// +0.421439*0.421439); //additional uncertainty on binning vs eta


			  	printf("forward region: %.4f±%.4f±%.4f %.6f%%\n",conf.getNum("fr_forw"),conf.getNum("fr_forw_stat"),conf.getNum("fr_forw")*params["fr_forw_sys_total_rel"],params["fr_forw_sys_total_rel"]*100);
			  	printf("central pT<250 GeV region: %.4f±%.4f±%.4f %.6f%%\n",conf.getNum("fr_cent_lt250"),conf.getNum("fr_cent_lt250_stat"),conf.getNum("fr_cent_lt250")*params["fr_cent_lt250_sys_total_rel"],params["fr_cent_lt250_sys_total_rel"]*100);
			  	printf("central pT>250 GeV region: %.4f±%.4f±%.4f %.6f%%\n",conf.getNum("fr_cent_gt250"),conf.getNum("fr_cent_gt250_stat"),conf.getNum("fr_cent_gt250")*params["fr_cent_gt250_sys_total_rel"],params["fr_cent_gt250_sys_total_rel"]*100);
	
}

