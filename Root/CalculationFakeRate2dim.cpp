#include "CalculationFakeRate2dim.h"
#include "FakeRateReader2dim.h"

CalculationFakeRate2dim::CalculationFakeRate2dim(StrV files, TString name, double lumi, Config conf){
	filelist=files;
	SName = name;
	FakeRateReader2dim *nominal = 0;
	hists=nominal->getInitialMap(SName);
  	for (auto p: filelist){
      // std::cout<<"processing sample "<<p<<"\n";
      nominal = new FakeRateReader2dim(p);
      nominal->setParams(lumi, conf);
      hists+=nominal->getHistograms(SName);
      // std::cout<<"file is read. \n";
      // nominal=0;
      // std::cout<<"finished \n";
    }
}