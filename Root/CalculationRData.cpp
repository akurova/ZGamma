#include "CalculationRData.h"
#include "RDataReader.h"

#include <TString.h>
#include "Common.h"
#include <iostream>

CalculationRData::CalculationRData(StrV files, TString name, double lumi){
	filelist=files;
	// TString histname = name;
	SName = name;
	std::cout<<name<<std::endl;
	RDataReader *nominal = 0;
	hists=nominal->getInitialMap(SName);
  	for (auto p: filelist){
      // std::cout<<"processing sample "<<p<<"\n";
      nominal = new RDataReader(p);
      nominal->setParams(lumi);
      hists+=nominal->getHistograms(SName);
      // std::cout<<"file is read. \n";
      // nominal=0;
      // std::cout<<"finished \n";
    }
};
