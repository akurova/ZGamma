#include "EToGamReader.h"
#include "Plotter.h"
#include <TF1.h>
#include <TFitResult.h>
#include "TMath.h"

NumV EToGamReader::bkgFitSyst(HistMap h, Config conf){

	Double_t par0,par1,par2,Nee,Negam,NeeMaxBkg,dNeeMaxBkg,NeeMinBkg,dNeeMinBkg,NeeAvBkg,dNeeAvBkg,NegamMaxBkg,dNegamMaxBkg,NegamMinBkg,dNegamMinBkg,NegamAvBkg,dNegamAvBkg;

	Nee=h["M_yee"]->Integral();

	TF1  *f1 = new TF1("f1","expo",30,60);
	TF1  *f2 = new TF1("f2","expo",120,200);
	TFitResultPtr fr1 = h["M_yee_b"]->Fit(f1,"LIR0QS");
	TFitResultPtr fr2 = h["M_yee_b"]->Fit(f2,"LIR0QS+");
	Plotter *p=0;
	p->fit_plotter(h["M_yee_b"],f1,f2,conf);
	// Double_t *params=0;
	// params=h["M_yee_b"]->GetFunction("f1")->GetParameters();
	// params=h["M_yee_b"]->GetFunction("f1")->GetParErrors();
	// f1->SetParErrors(params);
	// printf("fitted params %.6f %.6f\n",par0,par1);
	// printf("set params %.6f %.6f\n",f1->GetParameter(0),f1->GetParameter(1));
	// printf("\nIntegral from f1: %.0f\n",f1->Integral(conf.getNum("Mz")-10,conf.getNum("Mz")+10)/h["M_yee_b"]->GetXaxis()->GetBinWidth(1));
	// params=h["M_yee_b"]->GetFunction("f2")->GetParameters();
	// params=h["M_yee_b"]->GetFunction("f2")->GetParErrors();
	// f2->SetParErrors(params);

	// f1->SetParameters(fr1->GetParams());
	// f1->SetRange(conf.getNum("Mz")-10,conf.getNum("Mz")+10);
	// f2->SetParameters(fr2->GetParams());
	// f2->SetRange(conf.getNum("Mz")-10,conf.getNum("Mz")+10);
	// printf("\nIntegral from f2: %.0f\n",f2->Integral(conf.getNum("Mz")-10,conf.getNum("Mz")+10)/h["M_yee_b"]->GetXaxis()->GetBinWidth(1));

	if (f1->Integral(conf.getNum("Mz")-10,conf.getNum("Mz")+10)>f2->Integral(conf.getNum("Mz")-10,conf.getNum("Mz")+10)){
		NeeMaxBkg=f1->Integral(conf.getNum("Mz")-10,conf.getNum("Mz")+10)/h["M_yee_b"]->GetXaxis()->GetBinWidth(1);
		NeeMinBkg=f2->Integral(conf.getNum("Mz")-10,conf.getNum("Mz")+10)/h["M_yee_b"]->GetXaxis()->GetBinWidth(1);
		dNeeMaxBkg=f2->IntegralError(conf.getNum("Mz")-10,conf.getNum("Mz")+10,fr2->GetParams(),fr2->GetCovarianceMatrix().GetMatrixArray())/h["M_yee_b"]->GetXaxis()->GetBinWidth(1);
		dNeeMinBkg=f1->IntegralError(conf.getNum("Mz")-10,conf.getNum("Mz")+10,fr1->GetParams(),fr1->GetCovarianceMatrix().GetMatrixArray())/h["M_yee_b"]->GetXaxis()->GetBinWidth(1);
	}
	else {
		NeeMaxBkg=f2->Integral(conf.getNum("Mz")-10,conf.getNum("Mz")+10)/h["M_yee_b"]->GetXaxis()->GetBinWidth(1);
		NeeMinBkg=f1->Integral(conf.getNum("Mz")-10,conf.getNum("Mz")+10)/h["M_yee_b"]->GetXaxis()->GetBinWidth(1);	
		dNeeMaxBkg=f1->IntegralError(conf.getNum("Mz")-10,conf.getNum("Mz")+10,fr1->GetParams(),fr1->GetCovarianceMatrix().GetMatrixArray())/h["M_yee_b"]->GetXaxis()->GetBinWidth(1);
		dNeeMinBkg=f2->IntegralError(conf.getNum("Mz")-10,conf.getNum("Mz")+10,fr2->GetParams(),fr2->GetCovarianceMatrix().GetMatrixArray())/h["M_yee_b"]->GetXaxis()->GetBinWidth(1);
	}
	// NeeMaxBkg=max(f1->Integral(conf.getNum("Mz")-10,conf.getNum("Mz")+10),f2->Integral(conf.getNum("Mz")-10,conf.getNum("Mz")+10))/h["M_yee_b"]->GetXaxis()->GetBinWidth(1);
	// NeeMinBkg=min(f1->Integral(conf.getNum("Mz")-10,conf.getNum("Mz")+10),f2->Integral(conf.getNum("Mz")-10,conf.getNum("Mz")+10))/h["M_yee_b"]->GetXaxis()->GetBinWidth(1);
	NeeAvBkg=(NeeMaxBkg+NeeMinBkg)*0.5;
	dNeeAvBkg=sqrt((dNeeMaxBkg/NeeMaxBkg)*(dNeeMaxBkg/NeeMaxBkg)+(dNeeMinBkg/NeeMinBkg)*(dNeeMinBkg/NeeMinBkg))*NeeAvBkg;
	printf("\nNee %.0f; Bkg max %.0f±%.0f, min %.0f±%.0f, av. %.0f±%.0f\n",Nee,NeeMaxBkg,dNeeMaxBkg,NeeMinBkg,dNeeMinBkg,NeeAvBkg,dNeeAvBkg);

	auto fmy = [](Double_t *x, Double_t *par) {

		    if (x[0] > 70 && x[0] < 110) {
		      TF1::RejectPoint();
		      return 0.;
		   }
		   return exp(par[0]+par[1]*x[0]+par[2]*x[0]*x[0]);
	};
	TF1 *f3 = new TF1("f3",fmy,25,200,3);
	Negam=h["M_yey"]->Integral();
	// f3->SetParLimits(2,-1000,-0.1);
	// if (conf.getBool("Central")) h["M_yey_b"]->Rebin(2);
	TFitResultPtr fr3 = h["M_yey_b"]->Fit(f3,"LIR0QS");
	TF1  *f4 = new TF1("f3","exp([0]+[1]*x+[2]*x*x)",25,200);

	// par0=h["M_yey_b"]->GetFunction("f3")->GetParameter(0);
	// par1=h["M_yey_b"]->GetFunction("f3")->GetParameter(1);
	// par2=h["M_yey_b"]->GetFunction("f3")->GetParameter(2);
	// f4->SetParameters(par0,par1,par2);
	// params=h["M_yey_b"]->GetFunction("f3")->GetParameters();
	f4->SetFitResult(*fr3);
	// f4->SetParameters(fr3->GetParams());
	// const Double_t *params=h["M_yey_b"]->GetFunction("f3")->GetParErrors();
	// printArrSq<Double_t>(params,h["M_yey_b"]->GetFunction("f3")->GetNpar());
	// f4->SetParErrors(params);
	// cout<<"parameters set \n";
	// printf("fitted params %.6f %.6f %.6f\n",par0,par1,par2);
	// p->hist_plotter<TF1>(f4);
	// cout<<"plot made \n";
	p->fit_plotter(h["M_yey_b"],f4,conf);
	// cout<<"plot made \n";
	// f4->SetRange(conf.getNum("Mz")-10,conf.getNum("Mz")+10);
	// NegamMaxBkg=max(f3->Integral(conf.getNum("Mz")-10,conf.getNum("Mz")+10),f4->Integral(conf.getNum("Mz")-10,conf.getNum("Mz")+10));
	// NegamMinBkg=min(f3->Integral(conf.getNum("Mz")-10,conf.getNum("Mz")+10),f4->Integral(conf.getNum("Mz")-10,conf.getNum("Mz")+10));
	// NegamAvBkg=(NegamMaxBkg+NegamMinBkg)*0.5;
	// fr3->GetCovarianceMatrix().Print();
	NegamAvBkg=f4->Integral(conf.getNum("Mz")-10,conf.getNum("Mz")+10)/h["M_yey_b"]->GetXaxis()->GetBinWidth(1);
	dNegamAvBkg=f4->IntegralError(conf.getNum("Mz")-10,conf.getNum("Mz")+10,fr3->GetParams(),fr3->GetCovarianceMatrix().GetMatrixArray())/h["M_yey_b"]->GetXaxis()->GetBinWidth(1);
	// printf("\nNegam %.0f; Bkg max %.0f, min %.0f\n",Negam,NegamMaxBkg,NegamMinBkg);
	printf("\nNegam %.0f; Bkg av %.0f±%.0f\n",Negam,NegamAvBkg,dNegamAvBkg);
	NumV result;
	Double_t unc = sqrt(Negam/(Nee-NeeAvBkg)/(Nee-NeeAvBkg)+dNegamAvBkg*dNegamAvBkg/(Nee-NeeAvBkg)/(Nee-NeeAvBkg)+\
		  	Nee*(Negam-NegamAvBkg)*(Negam-NegamAvBkg)/(Nee-NeeAvBkg)/(Nee-NeeAvBkg)/(Nee-NeeAvBkg)/(Nee-NeeAvBkg)+\
		  	dNeeAvBkg*dNeeAvBkg*(Negam-NegamAvBkg)*(Negam-NegamAvBkg)/(Nee-NeeAvBkg)/(Nee-NeeAvBkg)/(Nee-NeeAvBkg)/(Nee-NeeAvBkg));
	result.push_back((Negam-NegamAvBkg)/(Nee-NeeAvBkg));
	result.push_back(unc);
	printf("\nfake rate %.6f±%.6f\n",(Negam-NegamAvBkg)/(Nee-NeeAvBkg),unc);
	printf("\nfake rate w/o subt %.6f±%.6f difference %.3f\n",Negam/Nee,Negam/Nee*sqrt(1/Negam+1/Nee),fabs(Negam/Nee-(Negam-NegamAvBkg)/(Nee-NeeAvBkg))/max(Negam/Nee,(Negam-NegamAvBkg)/(Nee-NeeAvBkg)));
	// printf("systematics %.2f%%", (Negam-NegamAvBkg)/(Nee-NeeAvBkg)-result[0]);
	if (conf.getBool("bkgSyst")){
		printf("var from dNeeMaxBkg: %.6f%%\n",fabs((Negam-NegamAvBkg)/(Nee-NeeMaxBkg)-result[0])/result[0]*100);
		printf("var from dNeeMinBkg: %.6f%%\n",fabs((Negam-NegamAvBkg)/(Nee-NeeMinBkg)-result[0])/result[0]*100);
		// printf("var from dNegamMaxBkg: %.6f%%\n",fabs((Negam-NegamAvBkg-dNegamAvBkg)/(Nee-NeeAvBkg)-result[0])/result[0]*100);
		// printf("var from dNegamMinBkg: %.6f%%\n",fabs((Negam-NegamAvBkg+dNegamAvBkg)/(Nee-NeeAvBkg)-result[0])/result[0]*100);

		for (int i=0; i<h["M_yey_b"]->GetFunction("f3")->GetNpar(); i++){
			double err = f3->GetParError(i);
			double par = f3->GetParameter(i);
			// printArr(f4->GetParameters(),f4->GetNpar());
			f4->SetParameter(i,par+err);
			// printArr(f4->GetParameters(),f4->GetNpar());
			double integr;
			integr=f4->Integral(conf.getNum("Mz")-10,conf.getNum("Mz")+10)/h["M_yey_b"]->GetXaxis()->GetBinWidth(1);
			printf("var from par %.0i positive: %.6f%%\n",i,fabs((Negam-integr)/(Nee-NeeAvBkg)-result[0])/result[0]*100);
			f4->SetParameter(i,par-err);
			// printArr(f4->GetParameters(),f4->GetNpar());
			integr=f4->Integral(conf.getNum("Mz")-10,conf.getNum("Mz")+10)/h["M_yey_b"]->GetXaxis()->GetBinWidth(1);
			printf("var from par %.i negative: %.6f%%\n",i,fabs((Negam-integr)/(Nee-NeeAvBkg)-result[0])/result[0]*100);
			f4->SetParameter(i,par);
		}
	}printf("sum largest variations from ee and eg in quadrature\n");

	return result;
}