#include "Plotter.h"

void Plotter::fit_plotter(TH1D* hist, TF1* hist2, TF1* hist3, Config conf)
{

	TLatex *   tex;
	gROOT->LoadMacro("$HOME/atlasrootstyle/AtlasLabels.C");
	gROOT->LoadMacro("$HOME/atlasrootstyle/AtlasUtils.C");


	SetAtlasStyle();
	gStyle->SetErrorX(0.5);

	TCanvas *c1=new TCanvas(hist->GetName(),hist->GetName());
	hist->Draw();
	hist->GetXaxis()->SetRangeUser(0,200);
	hist2->SetRange(30,conf.getNum("Mz")+10);
	hist3->SetRange(conf.getNum("Mz")-10,200);
	hist2->SetLineColor(kRed);
	hist3->SetLineColor(kRed);
	hist2->Draw("same");
	hist3->Draw("same");
	tex = new TLatex(0.6,0.85,"#sqrt{s}=13 TeV, "+conf.getStr("TotalLumi")+" fb^{-1}");
	tex->SetNDC();
	tex->SetTextFont(42);
	tex->SetLineWidth(2);
	tex->Draw("same");
	ATLASLabel(0.23,0.85,"Internal");
	if (conf.getBool("Central")) tex = new TLatex(0.23,0.8,"|#eta|<1.37");
	else tex = new TLatex(0.23,0.8,"1.52<|#eta|<2.37");
	tex->SetNDC();
	tex->SetTextFont(42);
	tex->SetLineWidth(2);
	if (conf.getStr("Region")!="B"&&conf.getStr("Region")!="C"&&conf.getStr("Region")!="D") tex->Draw("same");
	if (conf.getBool("Central")){
		if (conf.getBool("HighPt")) tex = new TLatex(0.23,0.75,"p^{#gamma}_{T}>250 GeV");
		else tex = new TLatex(0.23,0.75,"p^{#gamma}_{T}<250 GeV");
		tex->SetNDC();
		tex->SetTextFont(42);
		tex->SetLineWidth(2);
		if (conf.getStr("Region")!="B"&&conf.getStr("Region")!="C"&&conf.getStr("Region")!="D") tex->Draw("same");
	}
	tex = new TLatex(0.23,0.2,Form("#chi^{2}/NDF = %.3f",hist2->GetChisquare()/hist2->GetNDF()));
	tex->SetNDC();
	tex->SetTextFont(42);
	tex->SetLineWidth(2);	
	tex->Draw("same");
	tex = new TLatex(0.7,0.2,Form("#chi^{2}/NDF = %.3f",hist3->GetChisquare()/hist3->GetNDF()));
	tex->SetNDC();
	tex->SetTextFont(42);
	tex->SetLineWidth(2);	
	tex->Draw("same");

	  TLine *l=0;
	  l=new TLine(conf.getNum("Mz")-10,0,conf.getNum("Mz")-10,hist->GetMaximum());
      l->SetLineColor(kBlue);
      l->SetLineStyle(9);
      l->Draw("same");	  
      l=new TLine(conf.getNum("Mz")+10,0,conf.getNum("Mz")+10,hist->GetMaximum());
      l->SetLineColor(kBlue);
      l->SetLineStyle(9);
      l->Draw("same");

	c1->UseCurrentStyle();
	c1->SetLogy();
	c1->Update();

	TString names;
	if (conf.getBool("SavePlots")&&!conf.getBool("Plots")&&!conf.getBool("PlotsMass")){

        names="../../plots/fake-rate/"+TString(c1->GetTitle())+conf.getStr("Tag")+"_fit_result.png";
        c1->SaveAs(names.Data());
        names="../../plots/fake-rate/"+TString(c1->GetTitle())+conf.getStr("Tag")+"_fit_result.C";
        c1->SaveAs(names.Data());
        names="../../plots/fake-rate/"+TString(c1->GetTitle())+conf.getStr("Tag")+"_fit_result.pdf";
        c1->SaveAs(names.Data());	    	

    }

};

void Plotter::fit_plotter(TH1D* hist, TF1* hist2, Config conf)
{

	TLatex *   tex;
	gROOT->LoadMacro("$HOME/atlasrootstyle/AtlasLabels.C");
	gROOT->LoadMacro("$HOME/atlasrootstyle/AtlasUtils.C");


	SetAtlasStyle();
	gStyle->SetErrorX(0.5);
	TCanvas *c1=new TCanvas(hist->GetName(),hist->GetName());
	hist->Draw();
	hist->GetXaxis()->SetRangeUser(0,200);
	// hist2->SetRange(conf.getNum("Mz")-10,200);
	hist2->SetLineColor(kRed);
	hist2->Draw("same");
	tex = new TLatex(0.6,0.85,"#sqrt{s}=13 TeV, "+conf.getStr("TotalLumi")+" fb^{-1}");
	tex->SetNDC();
	tex->SetTextFont(42);
	tex->SetLineWidth(2);
	tex->Draw("same");
	ATLASLabel(0.23,0.85,"Internal");
	if (conf.getBool("Central")) tex = new TLatex(0.23,0.8,"|#eta|<1.37");
	else tex = new TLatex(0.23,0.8,"1.52<|#eta|<2.37");
	tex->SetNDC();
	tex->SetTextFont(42);
	tex->SetLineWidth(2);
	if (conf.getStr("Region")!="B"&&conf.getStr("Region")!="C"&&conf.getStr("Region")!="D") tex->Draw("same");
	if (conf.getBool("Central")){
		if (conf.getBool("HighPt")) tex = new TLatex(0.23,0.75,"p^{#gamma}_{T}>250 GeV");
		else tex = new TLatex(0.23,0.75,"p^{#gamma}_{T}<250 GeV");
		tex->SetNDC();
		tex->SetTextFont(42);
		tex->SetLineWidth(2);
		if (conf.getStr("Region")!="B"&&conf.getStr("Region")!="C"&&conf.getStr("Region")!="D") tex->Draw("same");
	}

	tex = new TLatex(0.7,0.2,Form("#chi^{2}/NDF = %.3f",hist2->GetChisquare()/hist2->GetNDF()));
	tex->SetNDC();
	tex->SetTextFont(42);
	tex->SetLineWidth(2);	
	tex->Draw("same");

	  TLine *l=0;
	  l=new TLine(conf.getNum("Mz")-10,0,conf.getNum("Mz")-10,hist->GetMaximum());
      l->SetLineColor(kBlue);
      l->SetLineStyle(9);
      l->Draw("same");	  
      l=new TLine(conf.getNum("Mz")+10,0,conf.getNum("Mz")+10,hist->GetMaximum());
      l->SetLineColor(kBlue);
      l->SetLineStyle(9);
      l->Draw("same");

	c1->UseCurrentStyle();
	c1->SetLogy();
	c1->Update();
	
		TString names;
		if (conf.getBool("SavePlots")&&!conf.getBool("Plots")&&!conf.getBool("PlotsMass")){

	        names="../../plots/fake-rate/"+TString(c1->GetTitle())+conf.getStr("Tag")+"_fit_result.png";
	        c1->SaveAs(names.Data());
	        names="../../plots/fake-rate/"+TString(c1->GetTitle())+conf.getStr("Tag")+"_fit_result.C";
	        c1->SaveAs(names.Data());
	        names="../../plots/fake-rate/"+TString(c1->GetTitle())+conf.getStr("Tag")+"_fit_result.pdf";
	        c1->SaveAs(names.Data());	    	

	    }

};