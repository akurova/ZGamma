#include "CalculationFakeRate.h"
#include "FakeRateReader.h"

CalculationFakeRate::CalculationFakeRate(StrV files, TString name, double lumi, Config conf){
	filelist=files;
	SName = name;
	FakeRateReader nominal = FakeRateReader();
  nominal.setParams(lumi, conf);
	hists=nominal.getInitialMap(SName);
  	for (auto p: filelist){
      // std::cout<<"processing sample "<<p<<"\n";
      nominal = FakeRateReader(p);
      nominal.setParams(lumi, conf);
      hists+=nominal.getHistograms(SName);
      // std::cout<<"file is read. \n";
      // nominal=0;
      // std::cout<<"finished \n";
    }
}