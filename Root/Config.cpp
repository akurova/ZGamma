#include "TSystem.h"
#include "THashList.h"

// Local include(s):
#include "Config.h"


  Config::Config()
    : m_env("env")
  {
    // Must have no pointer initialization, for CINT
    m_env.IgnoreDuplicates(true);
  }

  Config::Config(const Config &config)
    : Config()
  {
    config.m_env.Copy(m_env);
    copyTable(config.m_env);
  }

  Config::Config(TString fileName)
    : Config()
  {
    addFile(fileName);
  }

  /*
  Config::Config(TEnv *env) : Config() {
    m_env->Copy(env);
  }
   */

  void Config::ensureDefined(TString key)
  {
    if (!isDefined(key)) { fatal("Config no value found for " + key); }
  }

  bool Config::isDefined(TString key)
  {
    return m_env.Defined(key);
  }

  TString Config::getStr(TString key)
  {
    ensureDefined(key);
    return m_env.GetValue(key, "");
  }

  TString Config::getStr(TString key, TString dflt)
  {
    return m_env.GetValue(key, dflt.Data());
  }

  int Config::getInt(TString key)
  {
    ensureDefined(key);
    return m_env.GetValue(key, -99);
  }

  int Config::getInt(TString key, int dflt)
  {
    return m_env.GetValue(key, dflt);
  }

  bool Config::getBool(TString key, bool dflt)
  {
    return m_env.GetValue(key, dflt);
  }

  bool Config::getBool(TString key)
  {
    ensureDefined(key);
    return getBool(key, false);
  }

  double Config::getNum(TString key)
  {
    ensureDefined(key);
    return m_env.GetValue(key, -99.0);
  }

  double Config::getNum(TString key, double dflt)
  {
    return m_env.GetValue(key, dflt);
  }

  StdStrV Config::getStdStrV(TString key)
  {
    ensureDefined(key);
    StrV vec = vectorize(m_env.GetValue(key, ""), " \t");
    StdStrV stdvec;

    for (TString str : vec) { stdvec.push_back(str.Data()); }

    return stdvec;
  }

  StdStrV Config::getStdStrV(TString key, StdStrV dflt)
  {
    if (isDefined(key)) {
      StrV vec = vectorize(m_env.GetValue(key, ""), " \t");
      StdStrV stdvec;

      for (TString str : vec) { stdvec.push_back(str.Data()); }

      return stdvec;
    }

    return dflt;
  }

  StrV Config::getStrV(TString key)
  {
    ensureDefined(key);
    return vectorize(m_env.GetValue(key, ""), " \t");
  }

  StrV Config::getStrV(TString key, StrV dflt)
  {
    if (isDefined(key)) {
      return vectorize(m_env.GetValue(key, ""), " \t");
    }

    return dflt;
  }

  NumV Config::getNumV(TString key)
  {
    ensureDefined(key);
    return vectorizeNum(m_env.GetValue(key, ""), " \t");
  }

  NumV Config::getNumV(TString key, NumV dflt)
  {
    if (isDefined(key)) {
      return vectorizeNum(m_env.GetValue(key, ""), " \t");
    }

    return dflt;
  }

  void Config::printDB()
  {
    TIter next(m_env.GetTable());

    while (TEnvRec *er = (TEnvRec *) next()) {
      printf("  %-60s%s\n", Form("%s:", er->GetName()), er->GetValue());
    }
  }

  void Config::addFile(TString fileName)
  {
    TString path(fileName);

    if (!fileExist(path) || path == "") { fatal("Cannot find settings file " + fileName + "\n"); }

    // settings read in by files should not overwrite values set by setValue()
    TEnv env;
    int status = env.ReadFile(path.Data(), EEnvLevel(0));

    if (status != 0) { fatal("Cannot read settings file " + fileName); }

    TIter next(env.GetTable());

    while (TEnvRec *er = (TEnvRec *) next()) {
      if (!isDefined(er->GetName())) { setValue(er->GetName(), er->GetValue()); }
    }
  }

