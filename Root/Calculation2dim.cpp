#include "Calculation2dim.h"
// #include "TreeReader.h"
#include "TreeReader2dim.h"

#include <TString.h>
#include "Common.h"
#include <iostream>

Calculation2dim::Calculation2dim(StrV files, TString name, double lumi){
	filelist=files;
	// TString histname = name;
	SName = name;
	TreeReader2dim *nominal = 0;
	hists=nominal->getInitialMap(SName);
  	for (auto p: filelist){
      // std::cout<<"processing sample "<<p<<"\n";
      nominal = new TreeReader2dim(p);
      nominal->setParams(lumi);
      hists+=nominal->getHistograms(SName);
      // std::cout<<"file is read. \n";
      // nominal=0;
      // std::cout<<"finished \n";
    }
};

// void Calculation::read(TString filename){
    
// }

void Calculation2dim::operator+=(const Calculation2dim &c2){
	// this->SName=c2.SName;
    this->hists+=c2.hists;
};

void Calculation2dim::operator-=(const Calculation2dim &c2){
  this->hists-=c2.hists;
};

void Calculation2dim::operator=(const Calculation2dim &c2){
	// this->SName=c2.SName;
	// std::cout<<"initial name "<<c2.SName<<std::endl;
  this->hists=c2.hists;
}

