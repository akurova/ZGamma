#include "EToGamReader.h"
#include "Config.h"
#include <vector>
#include <map>
#include <TString.h>

#include <TTree.h>
#include <TFile.h>
#include <iostream>
#include <TMath.h>
#include <TH1.h>
#include <TLorentzVector.h>

using namespace std;

EToGamReader::EToGamReader(){

};

EToGamReader::EToGamReader(TString s){
    if (s.Contains("MC")) filename = s+"/user.akurova.MxAOD.root";
    else if (s.Contains("data")||s.Contains("Data")) filename=s;
    // cout<<"Opening file "<<filename<<endl;
};

void EToGamReader::setParams(double lumi, Config conf){
    //TODO avoid double initialization of config (now it's done both here and in main programm)
    // Config con("../ZGamma.cfg");
        params["CR"]=conf.getBool("CR");
        params["CR2"]=1; //always true for etogam data-driven background
        params["CR3"]=conf.getBool("CR3");
        params["veto"]=conf.getBool("veto");
        params["tauveto"]=conf.getBool("tauveto");
        params["angular"]=conf.getBool("angular");
        // params["metjet"]=par[5];
        // params["metgam"]=par[6];
        params["nojets"]=conf.getBool("nojets");
        params["VBSjets"]=conf.getBool("VBSjets");
        params["METsign"]=conf.getNum("METSignificance");
        params["METsoft"]=conf.getNum("SoftTerm");
        params["DPhiMetGam"]=conf.getNum("DPhiMETGam");
        params["metjet2"]=conf.getNum("DPhiSubleadJetMET");
        params["metjet"]=conf.getNum("DPhiMETjet");
        params["gCent"]=conf.getNum("GamCentrality");
        params["DYjj"]=conf.getNum("DYjj");
        params["MET"]=conf.getNum("MET");
        params["mjj"]=conf.getNum("mjj");
        params["lumi"]=lumi;
        params["norm"]=conf.getBool("normalize");
        params["Write"]=conf.getBool("WriteToTree");
        params["Central"]=conf.getBool("Central");
        params["HighPt"]=conf.getBool("HighPt");
        params["etaSyst"]=conf.getBool("EtaSyst");
        for (auto p: params){
            std::cout<<p.first<<":"<<p.second<<"\n";
        }
};

HistMap EToGamReader::getInitialMap(TString name){
    HistMap h;
    double N_jets[5]={-0.5,0.5,1.5,2.5,7.5};
    // double N_jets[6]={-0.5,0.5,1.5,2.5,3.5,7.5};
    // double pt[6]={150,200,250,350,450,600}; //for e to gam CR
    // double pt[6]={150,200,250,350,600,1100}; //for e to gam CR
    // double pt[6]={150,220,300,420,600,1100}; //for e to gam CR
    double pt[7]={150,200,250,350,450,600,1100}; //for e to gam CR
    // double pt[11]={150,180,210,240,270,300,340,380,430,510,600}; //for e to gam CR
    double pt_j[8]={50,100,150,250,350,450,600,1100}; //for e to gam CR
    // double pt[5]={150,250,400,600,1100}; //for e to gam CR
    double mtZg[6]={250,500,750,1000,1500,2500}; //for e to gam CR
    double ptZg[5]={0,100,250,550,1100}; //for e to gam CR
    double mjj[7]={0, 150, 300, 500, 850, 1600, 3500}; 
    double ptmiss[7]={130,200,250,350,450,600,1100}; //for e to gam CR
    // double ptmiss[5]={130,250,400,600,1100}; //for e to gam CR
    // double mjj[8]={0, 150, 300, 550, 800, 1100, 1500, 3500}; 
    // double mjj[8]={0, 150, 300, 450, 700, 1100, 1500, 3500}; //for e to gam CR
    h["n_el"]=new TH1D("n_el_"+name,";N_{e};Events/bin",6,-0.5,5.5);
    h["n_el"]->Sumw2();
    h["e_eta"]=new TH1D("e_eta_"+name,";#eta_{e-probe};Events/bin",6,-3,3);
    h["e_eta"]->Sumw2();
    h["y_eta"]=new TH1D("gamma_eta_"+name,";#eta_{#gamma};Events/bin",10,-2.5,2.5);
    h["y_eta"]->Sumw2();
    h["pT_Zy"]=new TH1D("pT_Zy_"+name,";p_{T}^{p^{miss}_{T}#gamma} [GeV];Events/bin",4,ptZg);//33 ,0,1650);//,237,150,4890);
    h["pT_Zy"]->Sumw2();
    h["n_mu"]=new TH1D("n_mu_"+name,";N_{#mu};Events/bin",6,-0.5,5.5);
    h["n_mu"]->Sumw2();
    h["n_ph"]=new TH1D("n_ph_"+name,";N_{#gamma};Events/bin",6,-0.5,5.5);
    h["n_ph"]->Sumw2();
    h["pT_lead_y"]=new TH1D("pT_lead_y_"+name,";E_{T}^{e-probe} [GeV];Events/bin",6,pt);//33 ,0,1650);//,237,150,4890);
    h["pT_lead_y"]->Sumw2();
    h["n_jet"]=new TH1D("n_jet_"+name,";N_{jets};Events/bin",4,N_jets);
    h["n_jet"]->Sumw2();
    h["pT_MET"]=new TH1D("pT_MET_"+name,";E_{T}^{miss} [GeV];Events/bin",6,ptmiss);
    h["pT_MET"]->Sumw2();
    h["gCent"]=new TH1D("gCent_"+name,";#gamma-centrality;Events/bin",12,0,6);
    h["gCent"]->Sumw2();
    h["dPhi_jet_MET"]=new TH1D("dPhi_jet_MET_"+name,";#Delta#phi(jet,p^{miss}_{T}) [rad];Events/bin",7,0.4,3.2);
    h["dPhi_jet_MET"]->Sumw2();
    h["dPhi_y_MET"]=new TH1D("dPhi_y_MET_"+name,";#Delta#phi(e-probe,p^{miss}_{T}) [rad];Events/bin",13,0.6,3.2);
    h["dPhi_y_MET"]->Sumw2();
    h["dPhi_y_jet"]=new TH1D("dPhi_y_jet_"+name,";#Delta#phi(e-probe,jet) [rad];Events/bin",13,0.6,3.2);
    h["dPhi_y_jet"]->Sumw2();
    h["mjj"]=new TH1D("mjj_"+name,";m_{jj};Events/bin",6,mjj);
    // h["mjj"]=new TH1D("mjj_"+name,";m_{jj};Events/bin",100,0,3500);
    h["mjj"]->Sumw2();
    h["pT_j1"]=new TH1D("pT_j1_"+name,";p_{T}^{j_{1}} [GeV];Events/bin",7,pt_j);//33 ,0,1650);//,237,150,4890);
    h["pT_j1"]->Sumw2();
    h["pT_j2"]=new TH1D("pT_j2_"+name,";p_{T}^{j_{2}} [GeV];Events/bin",7,pt_j);//33 ,0,1650);//,237,150,4890);
    h["pT_j2"]->Sumw2();
    h["mT_Zy"]=new TH1D("mT_Zy_"+name,";m_{T}^{p^{miss}_{T}#gamma} [GeV];Events/bin",5,mtZg);//33 ,0,1650);//,237,150,4890);
    h["mT_Zy"]->Sumw2();
    h["dPhi_jj"]=new TH1D("dPhi_jj_"+name,";|#Delta#phi(j,j)| [rad];Events/bin",13,0,3.25);
    h["dPhi_jj"]->Sumw2();
    h["dPhi_Zj"]=new TH1D("dPhi_Zj_"+name,";|#Delta#phi(p^{miss}_{T},j)| [rad];Events/bin",12,0.25,3.25);
    h["dPhi_Zj"]->Sumw2();
    h["dR_jy"]=new TH1D("dR_jy_"+name,";#Delta R(j,#gamma) [rad];Events/bin",6,0,6);
    h["dR_jy"]->Sumw2();
    if (params["etaSyst"]){
        Double_t eta_x[15]={-2.5,-2,-1.75,-1.5,-1.25,-1,-0.5,0,0.5,1,1.25,1.5,1.75,2,2.5};
        // Double_t eta_x[23]={-2.5,-2,-1.75,-1.5,-1.4,-1.2,-1,-0.8,-0.6,-0.4,-0.2,0,0.2,0.4,0.6,0.8,1,1.2,1.4,1.5,1.75,2,2.5};
        if (!params["HighPt"])
         h["eta_yey"]=new TH1D("eta_yey_"+name,";#eta_{e-probe};Events/bin",14,eta_x);
        else 
        h["eta_yey"]=new TH1D("eta_yey_"+name,";#eta_{e-probe};Events/bin",10,-2.5,2.5); 
        h["eta_yey"]->Sumw2();
    } 
    return h;
};

HistMap EToGamReader::getHistograms(TString name, Config conf){
    HistMap h; 
    TFile *file = new TFile(filename);
    h = getInitialMap(name);
    // Double_t metjet=0.4;
    Double_t metgam=TMath::Pi()*0.5;;    
    // bool ph_tight, loosePrime3, loosePrime4;
    int n_tau_loose;
    unsigned int n_ph, n_e_looseBL, n_e_medium, n_e_Tight, n_mu, n_jet, n_jet20, n_jet25, n_jet30, n_jet35, n_jet40, n_jet45, N_f_l, N_f_h, N_c_l, N_c_h, run;//, event;
    unsigned long long event;
    double  jet_pt, jet_eta, jet_phi, jet_E, jet2_pt, jet2_eta, jet2_phi, jet2_E, jet3_pt, jet3_eta, jet3_phi, jet3_E, metTST_pt, metTST_phi, metTST_Rho, metTST_VarL, \
    e_lead_pt, e_lead_eta, e_lead_phi, e_lead_E, weight,  jet_sum_pt, fake_weight, metTSTsignif, soft_term_pt, topoetcone20, ptvarcone20;
    int  mc_el_type, mc_el_origin;
    bool jet_third_nTrkqg, jet_third_BDTqg, jet_sublead_nTrkqg, jet_sublead_BDTqg, jet_lead_nTrkqg, jet_lead_BDTqg, PIDTight;
    TLorentzVector met, ph, jet, jet2, el;
    Double_t sum_of_weights, sum_of_weights_bk_xAOD, koef,
    sum_of_weights_bk_DxAOD, sumw, sum1, sum2, sum3;
    Long64_t N=0;
    TTree *norm_tree = (TTree*)file->Get("norm_tree");
    if (norm_tree!=0){
        norm_tree->SetBranchAddress("koef",&koef);
        norm_tree->GetEntry(0);
    }
    if (norm_tree==0) {
        koef = 1;
        checker.initialize();
    }
    else koef*=params["lumi"]; 
    // getFakeRate(vectorize(filename),params["lumi"]);
    // std::cout<<"fake rates "<<params["fr_forw"]<<" "<<params["fr_cent_gt250"]<<" "<<params["fr_cent_lt250"]<<"\n";
    TTree *tree_MC_sw = (TTree*)file->Get("output_tree_sw");
    // tree_MC_sw->SetBranchAddress("sum_of_weights",&sum_of_weights);
    // tree_MC_sw->SetBranchAddress("sum_of_weights_bk_DxAOD",&sum_of_weights_bk_DxAOD);
    tree_MC_sw->SetBranchAddress("sum_of_weights_bk_xAOD",&sum_of_weights_bk_xAOD);

    Int_t entry = (Int_t)tree_MC_sw->GetEntries();
    sumw=0;sum1=0;sum2=0;sum3=0;

    for (Int_t i=0; i<entry; i++)
    { 
        tree_MC_sw->GetEntry(i);
        // sum1=sum1+sum_of_weights;
        // sum2=sum2+sum_of_weights_bk_DxAOD;
        sum3=sum3+sum_of_weights_bk_xAOD;
    }

    // sumw=sum1*sum3/sum2;
    sumw=sum3;
    if (tree_MC_sw==0 || koef==1) sumw=1;
    
    TTree *tree;
    
    tree=(TTree*)file->Get("output_tree_emet"); std::cout<<"using emet tree\n";

    
    N = tree->GetEntries();
    
tree->SetBranchAddress("n_ph", &n_ph); //number of photons in event
tree->SetBranchAddress("n_jet", &n_jet); //number of jets in event
// tree->SetBranchAddress("n_jet20", &n_jet20); //number of jets in event
// tree->SetBranchAddress("n_jet25", &n_jet25); //number of jets in event
// tree->SetBranchAddress("n_jet30", &n_jet30); //number of jets in event
// tree->SetBranchAddress("n_jet35", &n_jet35); //number of jets in event
// tree->SetBranchAddress("n_jet40", &n_jet40); //number of jets in event
// tree->SetBranchAddress("n_jet45", &n_jet45); //number of jets in event
tree->SetBranchAddress("n_e_looseBL", &n_e_looseBL);  //number of loose electrons in event
tree->SetBranchAddress("n_tau_loose", &n_tau_loose);  //number of loose tau in event
tree->SetBranchAddress("n_mu", &n_mu); //number of muons in event
tree->SetBranchAddress("n_e_Tight", &n_e_Tight);  //number of Tight electrons in event


    tree->SetBranchAddress("el_pt", &e_lead_pt);  //leading leading electron p_x
    tree->SetBranchAddress("el_eta", &e_lead_eta);  //p_y
    tree->SetBranchAddress("el_phi", &e_lead_phi);  //p_z
    tree->SetBranchAddress("el_E", &e_lead_E);    //E
    tree->SetBranchAddress("PIDTight", &PIDTight);    //E
    tree->SetBranchAddress("topoetcone20", &topoetcone20);    //E
    tree->SetBranchAddress("ptvarcone20_TightTTVA_pt1000", &ptvarcone20);    //E


// tree->SetBranchAddress("jet_sum_pt", &jet_sum_pt); 

tree->SetBranchAddress("jet_lead_pt", &jet_pt);  //leading jet p_x
tree->SetBranchAddress("jet_lead_eta", &jet_eta);  //p_y 
tree->SetBranchAddress("jet_lead_phi", &jet_phi);  //p_z
tree->SetBranchAddress("jet_lead_E", &jet_E);    //E

tree->SetBranchAddress("jet_lead_BDTqg", &jet_lead_BDTqg);
tree->SetBranchAddress("jet_lead_nTrkqg", &jet_lead_nTrkqg);

    tree->SetBranchAddress("jet_sublead_pt", &jet2_pt);  //leading jet p_x
    tree->SetBranchAddress("jet_sublead_eta", &jet2_eta);  //p_y 
    tree->SetBranchAddress("jet_sublead_phi", &jet2_phi);  //p_z
    tree->SetBranchAddress("jet_sublead_E", &jet2_E);    //E

tree->SetBranchAddress("jet_sublead_BDTqg", &jet_sublead_BDTqg);
tree->SetBranchAddress("jet_sublead_nTrkqg", &jet_sublead_nTrkqg);


    tree->SetBranchAddress("jet_third_pt", &jet3_pt);  //leading jet p_x
    tree->SetBranchAddress("jet_third_eta", &jet3_eta);  //p_y 
    tree->SetBranchAddress("jet_third_phi", &jet3_phi);  //p_z
    tree->SetBranchAddress("jet_third_E", &jet3_E);    //E

tree->SetBranchAddress("jet_third_BDTqg", &jet_third_BDTqg);
tree->SetBranchAddress("jet_third_nTrkqg", &jet_third_nTrkqg);


tree->SetBranchAddress("metTST_pt", &metTST_pt);  //MET p_x
tree->SetBranchAddress("metTST_phi", &metTST_phi);  //p_y  
tree->SetBranchAddress("metTST_Rho", &metTST_Rho);  //p_y  
tree->SetBranchAddress("metTST_VarL", &metTST_VarL);  //p_y  
tree->SetBranchAddress("metTSTsignif", &metTSTsignif);  //MET significance  
tree->SetBranchAddress("soft_term_pt", &soft_term_pt);  //MET soft term 
if (koef==1){
    tree->SetBranchAddress("RunNumber",&run);
    tree->SetBranchAddress("EventNumber",&event);
}
if (koef!=1) tree->SetBranchAddress("weight", &weight);    //weight


weight=1;

HistMap h_forw = getInitialMap("forw");
HistMap h_cent_lt250 = getInitialMap("h_cent_lt250");
HistMap h_cent_gt250 = getInitialMap("h_cent_gt250");
double forw=0; double cent_lt250=0; double cent_gt250=0; double whats_left=0;

std::cout<<"name check "<<h_forw["dPhi_y_jet"]->GetName()<<"\n";

// Config conf("../ZGamma.cfg");
TFile *new_file;
TTree *etogam_tree;

if (params["Write"]){
    std::cout<<"creating the tree\n";
    new_file = TFile::Open(conf.getStr("FileName"),"recreate");
    etogam_tree = new TTree("etogam", "data-driven e to gamma background");
    etogam_tree->Branch("n_ph", &n_ph); //number of photons in event
    etogam_tree->Branch("n_jet", &n_jet); //number of jets in event
    // etogam_tree->Branch("n_jet20", &n_jet20); //number of jets in event
    // etogam_tree->Branch("n_jet25", &n_jet25); //number of jets in event
    // etogam_tree->Branch("n_jet30", &n_jet30); //number of jets in event
    // etogam_tree->Branch("n_jet35", &n_jet35); //number of jets in event
    // etogam_tree->Branch("n_jet40", &n_jet40); //number of jets in event
    // etogam_tree->Branch("n_jet45", &n_jet45); //number of jets in event
    etogam_tree->Branch("n_e_looseBL", &n_e_looseBL);  //number of loose electrons in event
    etogam_tree->Branch("n_tau_loose", &n_tau_loose);  //number of loose tau in event
    etogam_tree->Branch("n_mu", &n_mu); //number of muons in event

    etogam_tree->Branch("ph_pt", &e_lead_pt);  //leading leading electron p_x
    etogam_tree->Branch("ph_eta", &e_lead_eta);  //p_y
    etogam_tree->Branch("ph_phi", &e_lead_phi);  //p_z
    etogam_tree->Branch("ph_E", &e_lead_E);    //E

    etogam_tree->Branch("jet_sum_pt", &jet_sum_pt); 

    etogam_tree->Branch("jet_lead_pt", &jet_pt);  //leading jet p_x
    etogam_tree->Branch("jet_lead_eta", &jet_eta);  //p_y 
    etogam_tree->Branch("jet_lead_phi", &jet_phi);  //p_z
    etogam_tree->Branch("jet_lead_E", &jet_E);    //E

    etogam_tree->Branch("jet_sublead_pt", &jet2_pt);  //subleading jet p_x
    etogam_tree->Branch("jet_sublead_eta", &jet2_eta);  //p_y 
    etogam_tree->Branch("jet_sublead_phi", &jet2_phi);  //p_z
    etogam_tree->Branch("jet_sublead_E", &jet2_E);    //E

    etogam_tree->Branch("jet_third_pt", &jet3_pt);  //leading jet p_x
    etogam_tree->Branch("jet_third_eta", &jet3_eta);  //p_y 
    etogam_tree->Branch("jet_third_phi", &jet3_phi);  //p_z
    etogam_tree->Branch("jet_third_E", &jet3_E);    //E

    etogam_tree->Branch("metTST_pt", &metTST_pt);  //MET p_x
    etogam_tree->Branch("metTST_phi", &metTST_phi);  //p_y  
    etogam_tree->Branch("metTSTsignif", &metTSTsignif);  //MET significance  
    etogam_tree->Branch("soft_term_pt", &soft_term_pt);  //MET significance  
    etogam_tree->Branch("metTST_Rho", &metTST_Rho);  //p_y  
    etogam_tree->Branch("metTST_VarL", &metTST_VarL);  //p_y  

    
    etogam_tree->Branch("jet_lead_BDTqg", &jet_lead_BDTqg);
    etogam_tree->Branch("jet_lead_nTrkqg", &jet_lead_nTrkqg);
    
    etogam_tree->Branch("jet_sublead_BDTqg", &jet_sublead_BDTqg);
    etogam_tree->Branch("jet_sublead_nTrkqg", &jet_sublead_nTrkqg);

    etogam_tree->Branch("jet_third_BDTqg", &jet_third_BDTqg);
    etogam_tree->Branch("jet_third_nTrkqg", &jet_third_nTrkqg);

    etogam_tree->Branch("weight", &fake_weight);    //weight

}
int counter=0;
    for (Int_t i=0; i<N; i++)
    {
        tree->GetEntry(i);
        // if (koef==1 && (run==280673||run==332303||run==333380||run==334413||run==349263)) {
        //     // std::cout<<"found run "<<run<<"\n";
        //     if (checker.isDuplicate(run,event)) {std::cout<<"duplicated: "<<run<<" "<<event<<"\n"; counter++;}
        // }
            //koef==1&&(run==280673||run==332303||run==333380||run==334413||run==349263)&&
        if (fabs(weight)>100) continue;
        jet.SetPtEtaPhiE(jet_pt,jet_eta,jet_phi,jet_E);
        jet2.SetPtEtaPhiE(jet2_pt,jet2_eta,jet2_phi,jet2_E);
        met.SetPtEtaPhiM(metTST_pt,0,metTST_phi,0);
        ph.SetPtEtaPhiE(e_lead_pt,e_lead_eta,e_lead_phi,e_lead_E);
        if (n_ph!=0) continue;
        // if (n_e_looseBL!=1 && params["veto"]) continue;
        if (n_e_Tight!=1 && !params["CR"]) continue;
        if (n_e_Tight<1 && params["CR"]) continue;
        if (!PIDTight && filename.Contains("data-15-18-reproc-12-08-2020-MET100GeV_checked.root")) continue;
        if (n_e_looseBL!=1 && params["veto"] && !params["CR"]) continue;
        if (n_e_looseBL<1 && !params["veto"] && !params["CR"]) continue;
        if (n_tau_loose!=0 && params["tauveto"] && !params["CR"]) continue;
        if (n_mu!=0 && params["veto"] && !params["CR"]) continue;
        if (n_jet!=0 && params["nojets"]) continue;
        if (params["VBSjets"] && n_jet<2) continue;
        if (params["VBSjets"] && (jet+jet2).M()<params["mjj"]) continue;

        if (met.Pt()<params["MET"]) continue; 
        if (ph.Pt()<150) continue;
        // if (met.Pt()/sqrt(jet_sum_pt+ph.Pt())<params["METsign"] && !params["CR3"] && params["angular"]) continue;
        // if (met.Pt()/sqrt(jet_sum_pt+ph.Pt())>=params["METsign"] && params["CR3"] && params["angular"]) continue;
        if (soft_term_pt>params["METsoft"] && params["angular"]) continue;
        if (metTSTsignif<params["METsign"] && params["angular"] && !params["CR3"]) continue;
        if (metTSTsignif>params["METsign"] && params["angular"] && params["CR3"]) continue;
        if ((n_mu+n_e_looseBL!=2) && params["CR"] && params["veto"]) continue; //Wgam CR
        if ((n_mu+n_e_looseBL<2) && params["CR"] && !params["veto"]) continue; //Wgam CR
        if (fabs(ph.DeltaPhi(met))<params["DPhiMetGam"] && params["angular"]) continue;
        if (n_jet!=0){  
                if (fabs(met.DeltaPhi(jet))<params["metjet"] && params["angular"]) continue; 
                if (n_jet>1 && fabs(met.DeltaPhi(jet2))<params["metjet2"] && params["angular"]) continue;
                if (n_jet>1 && fabs((ph.Rapidity()-(jet.Rapidity()+jet2.Rapidity())*0.5)/(jet.Rapidity()-jet2.Rapidity()))>params["gCent"] && params["angular"]) continue;
                if (n_jet>1 && fabs(jet.Rapidity()-jet2.Rapidity())<params["DYjj"] && params["angular"]) continue;
        }
    // std::cout<<"basic selection passed\n";
        if (params["Write"]){n_ph=1; n_e_looseBL=n_e_looseBL-1;}
        if (fabs(ph.Eta())>1.5){


            if (n_jet!=0){  
                h_forw["dPhi_jet_MET"]->Fill(fabs(met.DeltaPhi(jet)),weight);
                h_forw["dPhi_y_jet"]->Fill(fabs(ph.DeltaPhi(jet)),weight);
                h_forw["dPhi_Zj"]->Fill(fabs(met.DeltaPhi(jet)),weight);
                h_forw["pT_j1"]->Fill(jet.Pt(),weight);
                h_forw["dR_jy"]->Fill(ph.DeltaR(jet),weight);

            }
                forw+=weight;
                // forw+=params["fr_forw"];
                fake_weight=params["fr_forw"];
                h_forw["dPhi_y_MET"]->Fill(ph.DeltaPhi(met),weight);        
                h_forw["pT_MET"]->Fill(met.Pt(),weight);
                h_forw["pT_lead_y"]->Fill(ph.Pt(),weight);
                h_forw["n_el"]->Fill(n_e_looseBL,weight);
                h_forw["n_ph"]->Fill(n_ph,weight);
                h_forw["n_mu"]->Fill(n_mu,weight);
                h_forw["n_jet"]->Fill(n_jet,weight);
                h_forw["e_eta"]->Fill(ph.Eta(),weight);
                h_forw["y_eta"]->Fill(ph.Eta(),weight);
                if (params["etaSyst"]) h_forw["eta_yey"]->Fill(ph.Eta(),weight);
                h_forw["pT_Zy"]->Fill((ph+met).Pt(),weight);
                h_forw["mT_Zy"]->Fill((ph+met).Mt(),weight);
                // std::cout<<"forward\n";
                if (params["Write"]) etogam_tree->Fill();
                if (n_jet>1) {
                    h_forw["mjj"]->Fill((jet+jet2).M(),weight);
                    h_forw["gCent"]->Fill(fabs((ph.Rapidity()-(jet.Rapidity()+jet2.Rapidity())*0.5)/(jet.Rapidity()-jet2.Rapidity())),weight);
                    h_forw["pT_j2"]->Fill(jet2.Pt(),weight);
                    h_forw["dPhi_jj"]->Fill(fabs(jet.DeltaPhi(jet2)),weight);

                }

        } else if (fabs(ph.Eta())<1.5){

            if (ph.Pt()>250){

                if (n_jet!=0){  
                    h_cent_gt250["dPhi_jet_MET"]->Fill(fabs(met.DeltaPhi(jet)),weight);
                    h_cent_gt250["dPhi_y_jet"]->Fill(fabs(ph.DeltaPhi(jet)),weight);
                    h_cent_gt250["dPhi_Zj"]->Fill(fabs(met.DeltaPhi(jet)),weight);
                    h_cent_gt250["pT_j1"]->Fill(jet.Pt(),weight);
                    h_cent_gt250["dR_jy"]->Fill(ph.DeltaR(jet),weight);
                }

                cent_gt250+=weight;
                // cent_gt250+=params["fr_cent_gt250"];
                fake_weight=params["fr_cent_gt250"];
                h_cent_gt250["dPhi_y_MET"]->Fill(ph.DeltaPhi(met),weight);        
                h_cent_gt250["pT_MET"]->Fill(met.Pt(),weight);
                h_cent_gt250["pT_lead_y"]->Fill(ph.Pt(),weight);
                h_cent_gt250["n_el"]->Fill(n_e_looseBL,weight);
                h_cent_gt250["n_ph"]->Fill(n_ph,weight);
                h_cent_gt250["n_mu"]->Fill(n_mu,weight);
                h_cent_gt250["n_jet"]->Fill(n_jet,weight);
                h_cent_gt250["e_eta"]->Fill(ph.Eta(),weight);
                if (params["etaSyst"]) h_cent_gt250["eta_yey"]->Fill(ph.Eta(),weight);
                h_cent_gt250["y_eta"]->Fill(ph.Eta(),weight);
                h_cent_gt250["pT_Zy"]->Fill((ph+met).Pt(),weight);
                h_cent_gt250["mT_Zy"]->Fill((ph+met).Mt(),weight);
                // std::cout<<"central >250\n";
                if (params["Write"]) etogam_tree->Fill();
                if (n_jet>1) {
                    h_cent_gt250["mjj"]->Fill((jet+jet2).M(),weight);
                    h_cent_gt250["gCent"]->Fill(fabs((ph.Rapidity()-(jet.Rapidity()+jet2.Rapidity())*0.5)/(jet.Rapidity()-jet2.Rapidity())),weight);
                    h_cent_gt250["pT_j2"]->Fill(jet2.Pt(),weight);
                    h_cent_gt250["dPhi_jj"]->Fill(fabs(jet.DeltaPhi(jet2)),weight);                    
                }
            } else if (ph.Pt()<=250){


                if (n_jet!=0){  
                    h_cent_lt250["dPhi_jet_MET"]->Fill(fabs(met.DeltaPhi(jet)),weight);            
                    h_cent_lt250["dPhi_y_jet"]->Fill(fabs(ph.DeltaPhi(jet)),weight);
                    h_cent_lt250["dPhi_Zj"]->Fill(fabs(met.DeltaPhi(jet)),weight);
                    h_cent_lt250["pT_j1"]->Fill(jet.Pt(),weight);
                    h_cent_lt250["dR_jy"]->Fill(ph.DeltaR(jet),weight);
                }

                cent_lt250+=weight;
                // cent_lt250+=params["fr_cent_lt250"];
                fake_weight=params["fr_cent_lt250"];
                    h_cent_lt250["dPhi_y_MET"]->Fill(ph.DeltaPhi(met),weight);
                    h_cent_lt250["pT_MET"]->Fill(met.Pt(),weight);
                    h_cent_lt250["pT_lead_y"]->Fill(ph.Pt(),weight);
                    h_cent_lt250["n_el"]->Fill(n_e_looseBL,weight);
                    h_cent_lt250["n_ph"]->Fill(n_ph,weight);
                    h_cent_lt250["n_mu"]->Fill(n_mu,weight);
                    h_cent_lt250["n_jet"]->Fill(n_jet,weight);
                    h_cent_lt250["e_eta"]->Fill(ph.Eta(),weight);
                    h_cent_lt250["y_eta"]->Fill(ph.Eta(),weight);
                    if (params["etaSyst"]) h_cent_lt250["eta_yey"]->Fill(ph.Eta(),weight);
                    h_cent_lt250["pT_Zy"]->Fill((ph+met).Pt(),weight);
                    h_cent_lt250["mT_Zy"]->Fill((ph+met).Mt(),weight);
                    // std::cout<<"central <250\n";
                    if (params["Write"]) etogam_tree->Fill();
                    if (n_jet>1) {
                        h_cent_lt250["mjj"]->Fill((jet+jet2).M(),weight);
                        h_cent_lt250["gCent"]->Fill(fabs((ph.Rapidity()-(jet.Rapidity()+jet2.Rapidity())*0.5)/(jet.Rapidity()-jet2.Rapidity())),weight);
                        h_cent_lt250["pT_j2"]->Fill(jet2.Pt(),weight);
                        h_cent_lt250["dPhi_jj"]->Fill(fabs(jet.DeltaPhi(jet2)),weight);                    
                    }
            }
        } 
        else {
            whats_left+=weight;
            std::cout<<"out of range "<<ph.Eta()<<" "<<ph.Pt()<<"\n";
        }


    }
    std::cout<<"count of duplications "<<counter<<"\n";
    if (!conf.getBool("EtaSyst")){
        getFakeRate(vectorize(filename),params["lumi"]);
        std::cout<<"calculationg total background\n";
        h_forw.Norm(params["fr_forw"]);
        h_cent_lt250.Norm(params["fr_cent_lt250"]);
        h_cent_gt250.Norm(params["fr_cent_gt250"]); 
        // h_forw.AddError(params["fr_forw_stat"]/params["fr_forw"]);
        // h_cent_lt250.AddError(params["fr_cent_lt250_stat"]/params["fr_cent_lt250"]);
        // h_cent_gt250.AddError(params["fr_cent_gt250_stat"]/params["fr_cent_gt250"]);
        h+=h_forw;
        h+=h_cent_gt250;
        h+=h_cent_lt250;

        printf("e-probe CR forward %.2f\n",forw);
        printf("e-probe CR central high %.2f\n",cent_gt250);
        printf("e-probe CR central low %.2f\n",cent_lt250);
        printf("forward %.2f rel. unc. s.in q.: f-r %.2f%% region %.2f%%\n",forw*params["fr_forw"],params["fr_forw_stat"]/params["fr_forw"]*100,1/sqrt(forw)*100);
        printf("central high %.2f rel. unc. s.in q.: f-r %.2f%% region %.2f%%\n",cent_gt250*params["fr_cent_gt250"],params["fr_cent_gt250_stat"]/params["fr_cent_gt250"]*100,1/sqrt(cent_gt250)*100);
        printf("central low %.2f rel. unc. s.in q.: f-r %.2f%% region %.2f%%\n",cent_lt250*params["fr_cent_lt250"],params["fr_cent_lt250_stat"]/params["fr_cent_lt250"]*100,1/sqrt(cent_lt250)*100);
        // std::cout<<"forward "<<forw*params["fr_forw"]<<"\n";
        // std::cout<<"central high "<<cent_gt250*params["fr_cent_gt250"]<<"\n";
        // std::cout<<"central low "<<cent_lt250*params["fr_cent_lt250"]<<"\n";

        double total, totalerr;
        total=0; totalerr=0;
        if (params["fr_forw"]>=0) {total+=forw*params["fr_forw"]; totalerr+=forw*params["fr_forw"]*params["fr_forw"]/**/;}
        if (params["fr_cent_lt250"]>=0) {total+=cent_lt250*params["fr_cent_lt250"]; totalerr+=cent_lt250*params["fr_cent_lt250"]*params["fr_cent_lt250"]/**/;}
        if (params["fr_cent_gt250"]>=0) {total+=cent_gt250*params["fr_cent_gt250"]; totalerr+=cent_gt250*params["fr_cent_gt250"]*params["fr_cent_gt250"]/**/;}
        if (conf.getBool("Systematics")) {
            getFakeRateSyst();
            double inpurity=0.0; //inputity for e-probe region, depends on region, please correct accordingly
            inpurity=conf.getNum("Inpurity");
            double syst = sqrt(forw*forw*params["fr_forw"]*params["fr_forw"]*params["fr_forw_sys_total_rel"]*params["fr_forw_sys_total_rel"]+cent_lt250*params["fr_cent_lt250"]*cent_lt250*\
                params["fr_cent_lt250"]*params["fr_cent_lt250_sys_total_rel"]*params["fr_cent_lt250_sys_total_rel"]+cent_gt250*params["fr_cent_gt250"]*cent_gt250*params["fr_cent_gt250"]*\
                params["fr_cent_gt250_sys_total_rel"]*params["fr_cent_gt250_sys_total_rel"]+inpurity*inpurity*total*total+\
                // 0.0227542*0.0227542*total*total+\//eta binning
                0.0232418*0.0232418*total*total+\
                params["fr_forw_stat"]*params["fr_forw_stat"]*forw*forw+params["fr_cent_lt250_stat"]*params["fr_cent_lt250_stat"]*cent_lt250*cent_lt250+params["fr_cent_gt250_stat"]*params["fr_cent_gt250_stat"]*cent_gt250*cent_gt250); //stat from fake-rate
            printf("background %.2f±%.2f±%.2f\n",total,sqrt(totalerr),syst);
        } else std::cout<<round(total)<<"±"<<round(sqrt(totalerr))<<"\n";
        // std::cout<<forw*params["fr_forw"]+cent_lt250*params["fr_cent_lt250"]+cent_gt250*params["fr_cent_gt250"]<<"+-";

        // std::cout<< sqrt(forw*params["fr_forw"]*params["fr_forw"]+params["fr_forw_stat"]*params["fr_forw_stat"]*forw*forw+\
        //             cent_lt250*params["fr_cent_lt250"]*params["fr_cent_lt250"]+params["fr_cent_lt250_stat"]*params["fr_cent_lt250_stat"]*cent_lt250*cent_lt250+\
        //             cent_gt250*params["fr_cent_gt250"]*params["fr_cent_gt250"]+params["fr_cent_gt250_stat"]*params["fr_cent_gt250_stat"]*cent_gt250*cent_gt250)<<"\n";

        std::cout<<"e+MET CR: "<<forw+cent_gt250+cent_lt250<<"\n";
        double err=0;
        std::cout<<"forward "<<h_forw["n_el"]->IntegralAndError(1,h_forw["n_el"]->GetNbinsX(),err)<<"+-"<<err<<"\n";
        std::cout<<"central high "<<h_cent_gt250["n_el"]->IntegralAndError(1,h_cent_gt250["n_el"]->GetNbinsX(),err)<<"+-"<<err<<"\n";
        std::cout<<"central low "<<h_cent_lt250["n_el"]->IntegralAndError(1,h_cent_lt250["n_el"]->GetNbinsX(),err)<<"+-"<<err<<"\n";
        std::cout<<"electrons which does not lie into the regions "<<whats_left<<"\n";
        // cout<<"integral="<<h["n_el"]->Integral()<<endl;

        
        if (params["Write"]) {
            std::cout<<"writing the tree\n";
            // etogam_tree->Print();
            etogam_tree->Write();
            new_file->Close();
        }
        h.includeOverflow();
        return h;
    } else if (conf.getBool("EtaSyst")){
        if (!conf.getBool("Central")) return h_forw;
        else if (conf.getBool("HighPt")) return h_cent_gt250;
        else return h_cent_lt250;
    }
    file->Close();

};