#include "ControlPlots.h"
#include "Config.h"
//#include <boost/range/adaptor/reversed.hpp>
#include <THStack.h>
#include <TColor.h>

void ControlPlots::ControlPlot(Calculation::CalcV vec, TString histname){
	Config conf("../ZGamma.cfg");
	
	gROOT->LoadMacro("$HOME/atlasrootstyle/AtlasLabels.C");
	gROOT->LoadMacro("$HOME/atlasrootstyle/AtlasUtils.C");

	SetAtlasStyle();

	gStyle->SetErrorX(0.5);

	TLegend *leg5;
	if (histname.Contains("eta")||histname.Contains("dPhi")||histname.Contains("dR")) leg5=new TLegend(0.187,0.602,0.418,0.916);
	else leg5=new TLegend(0.692,0.602,0.923,0.916);
	leg5->SetBorderSize(0);
	leg5->SetFillStyle(0);
	THStack *hs = new THStack("hs","Stacked 1D histograms");
	Color_t colorL,colorF;
	TLatex *tex;

	if (vec[0].hists.find(histname)) std::cout<<"histogram "<<histname<<" found\n";
	std::cout<<"list of objects: \n";
	int num = vec.size();
	if (!vec[num-1].SName.Contains("Data")&&(conf.getBool("CR2")||conf.getBool("CR")||conf.getBool("CR3"))) fatal("The data should be added as the last entry to vector for plotting!");
	Calculation c2;
	c2 = vec[0];
	double err=0;
	double sum=0;
	if (conf.getBool("CR2")||conf.getBool("CR")||conf.getBool("CR3")){num--;}
	if (!vec[0].SName.Contains("Wenu") || !conf.getBool("CR2")) 
		// sum=vec[0].hists[histname]->IntegralAndError(1,vec[0].hists[histname]->GetNbinsX(),err);
		sum=vec[0].hists[histname]->GetBinContent(2);
	double sumsq=0;
	if (!vec[0].SName.Contains("Wenu") || !conf.getBool("CR2")) sumsq=vec[0].hists[histname]->GetBinError(2)*vec[0].hists[histname]->GetBinError(2);
	// sumsq=err*err;
	std::cout<<vec[0].SName<<" integral "<<vec[0].hists[histname]->IntegralAndError(1,vec[0].hists[histname]->GetNbinsX(),err)<<"+-"<<err<<" name "<<vec[0].SName<<"\n";
	for (int i=1; i<num; i++){
		if (!vec[i].SName.Contains("Wenu") || !conf.getBool("CR2")) sum+=vec[i].hists[histname]->GetBinContent(2);
			// sum+=vec[i].hists[histname]->Integral();
		std::cout<<vec[i].SName<<" integral "<<vec[i].hists[histname]->IntegralAndError(1,vec[i].hists[histname]->GetNbinsX(),err)<<"+-"<<err<<"\n";
		if (!vec[i].SName.Contains("Wenu") || !conf.getBool("CR2")) sumsq+=vec[i].hists[histname]->GetBinError(2)*vec[i].hists[histname]->GetBinError(2);
			// sumsq+=err*err;
		c2+=vec[i];
		vec[i]=c2;
		vec[i].hists[histname]->SetName(histname+"_"+vec[i].SName);
		std::cout<<"stacked\n";
	}
	std::cout<<"sum 2nd bin "<<sum<<std::endl;
	std::cout<<"unc 2nd bin "<<sqrt(sumsq)<<std::endl;

	// std::cout<<"Total sum "<<sum<<std::endl;
	// std::cout<<"Total sum of sq "<<sumsq<<std::endl;
	if (conf.getBool("CR2")||conf.getBool("CR")||conf.getBool("CR3")){	
	std::cout<<vec[num].SName<<" "<<vec[num].hists[histname]->Integral()<<"\n";
	double data=vec[num].hists[histname]->Integral();
	std::cout<<"Purity "<<1-sum/data<<"±"<<sqrt(sum*sum/data/data/data+sumsq/data/data)<<endl;
	std::cout<<"Impurity "<<sum/data<<"±"<<sqrt(sum*sum/data/data/data+sumsq/data/data)<<endl;
	}
	// if (conf.getBool("CR2")) {
	// 	double Wenu=vec[0].hists[histname]->IntegralAndError(1,vec[0].hists[histname]->GetNbinsX(),err);
	// 	std::cout<<"e+MET "<<Wenu<<"±"<<err<<std::endl;
	// 	std::cout<<"Purity "<<Wenu/data<<"±"<<sqrt(Wenu*Wenu/data/data/data+err*err/data/data)<<endl;
	// }

	//data draw settings
	if (conf.getBool("CR2")||conf.getBool("CR")||conf.getBool("CR3")){
		vec[num].hists[histname]->SetMarkerColor(1);
		vec[num].hists[histname]->SetLineColor(1);
		vec[num].hists[histname]->SetMarkerStyle(20);
		leg5->AddEntry(vec[num].hists[histname],"Data","P");
	}

	int Signal; TString tit;
	int i=1;
	for (int h=num-i; h>=0; h--){
		std::cout<<"here "<<vec[h].SName<<std::endl;
		if (vec[h].SName.Contains("nunugam") && vec[h].SName.Contains("EWK")) {Signal=h; colorF=kWhite; colorL=kAzure+3; tit="Z(#nu#nu)#gamma EWK";}
		if (vec[h].SName.Contains("nunugam") && !vec[h].SName.Contains("EWK")) {colorF=kPink-1; colorL=kPink-1; tit="Z(#nu#nu)#gamma";}
		if (vec[h].SName.Contains("SP")) {colorF=kGreen+3; colorL=kGreen+3; tit="#gamma+j";}
		if (vec[h].SName.Contains("Znunu")) {colorF=kCyan+1; colorL=kCyan+1; tit="j#rightarrow#gamma";} //if (conf.getBool("CR2")) tit="j#rightarrow e";}
		if (vec[h].SName.Contains("Wenu")) {colorF=kSpring-5; colorL=kSpring-5; tit="e#rightarrow#gamma"; if (conf.getBool("CR2")) tit="real e+E^{miss}_{T}";}//tit="W(e#nu), W(#tau#nu), top, t#bar{t}";}
		if (vec[h].SName.Contains("llgam")) {colorF=kAzure + 3; colorL=kAzure + 3; tit="Z(ll)#gamma";}
		if (vec[h].SName.Contains("tta")) {colorF=kViolet - 6; colorL=kViolet - 6; tit="tt#gamma";}
		// if (vec[h].SName.Contains("tta_gt1lep")) {colorF=kMagenta - 4; colorL=kMagenta - 4; tit="tt#gamma, #geq1l";}
		if (vec[h].SName.Contains("Wgam") && vec[h].SName.Contains("EWK")) {colorF=kOrange-9; colorL=kOrange-9; tit="W#gamma EWK";}
		if (vec[h].SName.Contains("Wgam") && !vec[h].SName.Contains("EWK")) {colorF=kOrange+1; colorL=kOrange+1; tit="W#gamma";}
		if (vec[h].SName.Contains("other")) {colorF=kViolet - 6; colorL=kViolet - 6; tit="fake e+E^{miss}_{T}";}
		// if (vec[h].SName.Contains("llgam")) {color=2; tit="l^{+}l^{-}#gamma";} //if you need Zllgam separately
		vec[h].hists[histname]->SetLineColor(colorL);
		vec[h].hists[histname]->SetFillColor(colorF);
		vec[h].hists[histname]->SetMarkerColor(colorL);
		if (h==Signal) vec[h].hists[histname]->SetLineWidth(2);
		hs->Add(vec[h].hists[histname]);
		leg5->AddEntry(vec[h].hists[histname],tit,"F");
	}
	std::cout<<"finished coloring"<<std::endl;

	double koef=1.15;
	TCanvas *c1;
	if (conf.getBool("PlotRatio")) {
		c1 = new TCanvas(histname.Data(),histname.Data(),0.,0.,600,600);
		TPad *pad1 = new TPad("pad1","pad1",0,0.3,1,1);
		pad1->SetBottomMargin(0.02);
	    pad1->Draw();
	    pad1->cd();	
	    if (histname.Contains("pT") || histname.Contains("mT") || histname.Contains("n_jet")) {pad1->SetLogy(); koef=10;} 
	}
	else {
		c1 = new TCanvas(histname.Data(),histname.Data(),1); 
		if (histname.Contains("pT") || histname.Contains("mT") || histname.Contains("n_jet")) {c1->SetLogy(); koef=10;}
	}
	    
	std::cout<<vec[num-1].hists[histname]->GetXaxis()->GetTitle()<<"\n";
	hs->Draw("nostack hist");

	
	if (histname.Contains("dPhi_y_jet") || histname.Contains("e_eta") || histname.Contains("y_eta") || histname.Contains("gCent") || histname.Contains("mjj")) koef = 1.55;
	if (histname.Contains("dPhi_Zj") || histname.Contains("dPhi_jj")) koef = 1.55;
	double data_max=vec[num-1].hists[histname]->GetBinContent(vec[num-1].hists[histname]->GetMaximumBin());
	double stack_max=hs->GetMaximum("nostack");
	std::cout<<"maximuma "<<data_max<<" "<<stack_max<<"\n";
	if (stack_max>data_max) hs->SetMaximum(stack_max*koef);
	else hs->SetMaximum(data_max*koef);
	hs->GetXaxis()->SetTitle(vec[num-1].hists[histname]->GetXaxis()->GetTitle());
	hs->GetYaxis()->SetTitle(vec[num-1].hists[histname]->GetYaxis()->GetTitle());

	// if (histname.Contains("n_jet")) hs->GetXaxis()->SetRangeUser(1.5,vec[num-1].hists[histname]->GetXaxis()->GetXmax());

	std::cout<<"debug"<<std::endl;
	if (conf.getBool("PlotRatio")) hs->GetXaxis()->SetLabelOffset(0.015);
	// if (!conf.getBool("CR2")&&!conf.getBool("CR")){num++;}
	TGraphErrors* unc = DrawStatErrors(vec[num-1].hists[histname]);
	// TGraphErrors* unc = DrawStatErrors(vec[Signal].hists[histname]);
	std::cout<<"debug"<<std::endl;
	if (conf.getBool("CR2")||conf.getBool("CR")||conf.getBool("CR3")) vec[num].hists[histname]->Draw("Epsame");	//Draw data only for control regions
std::cout<<"debug"<<std::endl;
	leg5->AddEntry(unc,"uncertainty","F");
	leg5->Draw();
	Float_t x=0.43;
	if (histname.Contains("dR")) x=0.6;
	ATLASLabel(x,0.863,"Internal");
	//TODO luminosity from config
	tex = new TLatex(x,0.78,"#sqrt{s}=13 TeV, "+conf.getStr("TotalLumi")+" fb^{-1}");
	// tex = new TLatex(0.43,0.77,"#sqrt{s}=13 TeV, 36.1 fb^{-1}");
	tex->SetNDC();
	tex->SetTextFont(42);
	tex->SetLineWidth(2);
	tex->Draw("same");
	tex = new TLatex(x,0.70,"SR");
	if (conf.getBool("CR2")&&conf.getNum("mjj")==300) tex = new TLatex(x,0.70,"e-probe CR");
	if (conf.getBool("CR2")&&conf.getNum("mjj")==0) tex = new TLatex(x,0.70,"incl. e-probe CR");
	if (conf.getBool("CR")) tex = new TLatex(x,0.70,"W#gamma CR");
	if (conf.getBool("CR3")) tex = new TLatex(x,0.70,"#gamma+j CR");
	tex->SetNDC();
	tex->SetTextFont(42);
	tex->SetLineWidth(2);
	tex->Draw("same");
if (conf.getBool("CR2")||conf.getBool("CR")||conf.getBool("CR3")){
	if (conf.getBool("PlotRatio")) {
	c1->cd();
	TPad *pad2 = new TPad("pad2","pad2",0,0,1,0.3);
	pad2->SetTopMargin(0.02);
	pad2->Draw();
	pad2->cd();
	pad2->SetBottomMargin(0.33);
	vec[num].hists[histname]->GetYaxis()->SetTitle("Data/Pred.");
	DrawRatioSubPlot("splitError", vec[num].hists[histname], vec[num-1].hists[histname], vec[num].hists[histname]->Integral(), vec[num-1].hists[histname]->Integral(), 0, 0, 0, 0);
	// DrawRatioSubPlot("splitError", vec[num-1].hists[histname], vec[num-2].hists[histname], vec[num-1].hists[histname]->Integral(), vec[num-2].hists[histname]->Integral(), 0.3, 1.7, 0, 0);
	// DrawRatioSubPlot("splitError", vec[num-1].hists[histname], vec[num-2].hists[histname], vec[num-1].hists[histname]->Integral(), vec[num-2].hists[histname]->Integral(), 0.5, 1.5, 0, 0);

	}
}
	c1->Update();
	TString names;
	TString tag="";
	tag = conf.getStr("Tag");
	if (conf.getBool("SavePlots")){
		if (conf.getBool("PlotRatio")){
	        names="../../plots/Control/"+TString(c1->GetTitle())+"_ratio"+tag+".png";
	        c1->SaveAs(names.Data());
	        names="../../plots/Control/"+TString(c1->GetTitle())+"_ratio"+tag+".C";
	        c1->SaveAs(names.Data());
	        names="../../plots/Control/"+TString(c1->GetTitle())+"_ratio"+tag+".pdf";
	        c1->SaveAs(names.Data());
	    }
	    else {
	        names="../plots/Control/"+TString(c1->GetTitle())+tag+".png";
	        c1->SaveAs(names.Data());
	        names="../../plots/Control/"+TString(c1->GetTitle())+tag+".C";
	        c1->SaveAs(names.Data());
	        names="../../plots/Control/"+TString(c1->GetTitle())+tag+".pdf";
	        c1->SaveAs(names.Data());	    	
	    }
    }

};
