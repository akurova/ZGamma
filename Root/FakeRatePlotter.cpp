#include "FakeRatePlotter.h"

FakeRatePlotter::FakeRatePlotter(HistMap map, Config conf){

	gROOT->LoadMacro("$HOME/atlasrootstyle/AtlasLabels.C");
	gROOT->LoadMacro("$HOME/atlasrootstyle/AtlasUtils.C");


	SetAtlasStyle();
	gStyle->SetErrorX(0.5);

	// Config conf("../ZGamma.cfg");
	TLatex *   tex;
	if (conf.getBool("Plots")){
		TCanvas *can2= new TCanvas(map["pT_yey"]->GetName(),map["pT_yey"]->GetName(),1);
		TH1F *rate_pt = (TH1F*)map["pT_yey"]->Clone();
		rate_pt->Divide(map["pT_yee"]);
		rate_pt->GetXaxis()->SetTitle("E^{#gamma}_{T} [GeV]");
		rate_pt->GetYaxis()->SetTitle("Fake rate");
		// rate_pt->GetYaxis()->SetRangeUser(0,0.11);
		rate_pt->GetYaxis()->SetRangeUser(0,2*rate_pt->GetMaximum());
		// rate_pt->GetYaxis()->SetRangeUser(0,0.08);
		rate_pt->Draw();
		tex = new TLatex(0.6,0.85,"#sqrt{s}=13 TeV, "+conf.getStr("TotalLumi")+ " fb^{-1}");
		tex->SetNDC();
		tex->SetTextFont(42);
		tex->SetLineWidth(2);
		tex->Draw("same");
		ATLASLabel(0.25,0.85,"Internal");
		if (TString(can2->GetTitle()).Contains("cent")) tex = new TLatex(0.25,0.8,"|#eta|<1.37");
		else tex = new TLatex(0.25,0.8,"1.52<|#eta|<2.37");
		tex->SetNDC();
		tex->SetTextFont(42);
		tex->SetLineWidth(2);
		tex->Draw("same");
		can2->UseCurrentStyle();
		can2->Update();


		TCanvas *can3= new TCanvas(map["eta_yey"]->GetName(),map["eta_yey"]->GetName(),1);
		TH1F *rate_eta = (TH1F*)map["eta_yey"]->Clone();
		rate_eta->Divide(map["eta_yee"]);
		rate_eta->GetXaxis()->SetTitle("#eta^{#gamma}");
		rate_eta->GetYaxis()->SetTitle("Fake rate");
		// rate_eta->GetYaxis()->SetRangeUser(0,0.11);
		// cout<<map["eta_yey"]->GetName()<<" maximum "<<rate_eta->GetMaximum()<<" minimum "<<rate_eta->GetMinimum(0.00001)<<" average "<<(rate_eta->GetMaximum()+rate_eta->GetMinimum(0.00001))*0.5<<"±"<<(rate_eta->GetMaximum()-rate_eta->GetMinimum(0.00001))*0.5<<" rel. unc. "<<(rate_eta->GetMaximum()-rate_eta->GetMinimum(0.00001))/(rate_eta->GetMaximum()+rate_eta->GetMinimum(0.00001))<<endl;
		rate_eta->GetYaxis()->SetRangeUser(0.0,1.3*rate_eta->GetMaximum());
		rate_eta->Draw();
		tex = new TLatex(0.6,0.85,"#sqrt{s}=13 TeV, "+conf.getStr("TotalLumi")+" fb^{-1}");
		tex->SetNDC();
		tex->SetTextFont(42);
		tex->SetLineWidth(2);
		tex->Draw("same");
		ATLASLabel(0.23,0.85,"Internal");
		if (TString(can3->GetTitle()).Contains("cent")){
			rate_eta->GetYaxis()->SetRangeUser(0.0,1.5*rate_eta->GetMaximum());
			if (TString(can3->GetTitle()).Contains("cent_he")) tex = new TLatex(0.23,0.75,"p^{#gamma}_{T}>250 GeV");
			else tex = new TLatex(0.23,0.75,"p^{#gamma}_{T}<250 GeV");
			tex->SetNDC();
			tex->SetTextFont(42);
			tex->SetLineWidth(2);
			tex->Draw("same");
		}
		can3->UseCurrentStyle();
		can3->Update();
		TString names;
		if (conf.getBool("SavePlots")){

	        names="../../plots/fake-rate/"+TString(can3->GetTitle())+conf.getStr("Tag")+".png";
	        can3->SaveAs(names.Data());
	        names="../../plots/fake-rate/"+TString(can3->GetTitle())+conf.getStr("Tag")+".C";
	        can3->SaveAs(names.Data());
	        names="../../plots/fake-rate/"+TString(can3->GetTitle())+conf.getStr("Tag")+".pdf";
	        can3->SaveAs(names.Data());	    	

	        names="../../plots/fake-rate/"+TString(can2->GetTitle())+conf.getStr("Tag")+".png";
	        can2->SaveAs(names.Data());
	        names="../../plots/fake-rate/"+TString(can2->GetTitle())+conf.getStr("Tag")+".C";
	        can2->SaveAs(names.Data());
	        names="../../plots/fake-rate/"+TString(can2->GetTitle())+conf.getStr("Tag")+".pdf";
	        can2->SaveAs(names.Data());	    	

	    }
	}

	if (conf.getBool("PlotsMass")){
		TCanvas *can4= new TCanvas(map["M_yee"]->GetName(),map["M_yee"]->GetName(),1);
		map["M_yee"]->GetXaxis()->SetTitle("M_{ee} [GeV]");
		map["M_yee"]->GetYaxis()->SetTitle("Entries");
		map["M_yee"]->GetXaxis()->SetRangeUser(0,200);
		map["M_yee"]->Draw("pE");
		tex = new TLatex(0.6,0.85,"#sqrt{s}=13 TeV, "+conf.getStr("TotalLumi")+" fb^{-1}");
		tex->SetNDC();
		tex->SetTextFont(42);
		tex->SetLineWidth(2);
		tex->Draw("same");
		ATLASLabel(0.23,0.85,"Internal");
		if (TString(can4->GetTitle()).Contains("cent")) tex = new TLatex(0.23,0.8,"|#eta|<1.37");
		else tex = new TLatex(0.23,0.8,"1.52<|#eta|<2.37");
		tex->SetNDC();
		tex->SetTextFont(42);
		tex->SetLineWidth(2);
		tex->Draw("same");
		if (TString(can4->GetTitle()).Contains("cent")){
			if (TString(can4->GetTitle()).Contains("cent_he")) tex = new TLatex(0.23,0.75,"p^{#gamma}_{T}>250 GeV");
			else tex = new TLatex(0.23,0.75,"p^{#gamma}_{T}<250 GeV");
			tex->SetNDC();
			tex->SetTextFont(42);
			tex->SetLineWidth(2);
			tex->Draw("same");
		}
		can4->UseCurrentStyle();
		can4->SetLogy();
		map["M_yee"]->GetYaxis()->SetRangeUser(0.01,100*map["M_yee"]->GetMaximum());
		can4->Update();

		TCanvas *can5= new TCanvas(map["M_yey"]->GetName(),map["M_yey"]->GetName(),1);
		map["M_yey"]->GetXaxis()->SetTitle("M_{e#gamma} [GeV]");
		map["M_yey"]->GetXaxis()->SetRangeUser(0,200);
		map["M_yey"]->GetYaxis()->SetTitle("Entries");
		map["M_yey"]->Draw("pE");
		tex = new TLatex(0.6,0.85,"#sqrt{s}=13 TeV, "+conf.getStr("TotalLumi")+" fb^{-1}");
		tex->SetNDC();
		tex->SetTextFont(42);
		tex->SetLineWidth(2);
		tex->Draw("same");
		ATLASLabel(0.23,0.85,"Internal");
		if (TString(can5->GetTitle()).Contains("cent")) tex = new TLatex(0.23,0.8,"|#eta|<1.37");
		else tex = new TLatex(0.23,0.8,"1.52<|#eta|<2.37");
		tex->SetNDC();
		tex->SetTextFont(42);
		tex->SetLineWidth(2);
		tex->Draw("same");
		if (TString(can5->GetTitle()).Contains("cent")){
			if (TString(can5->GetTitle()).Contains("cent_he")) tex = new TLatex(0.23,0.75,"p^{#gamma}_{T}>250 GeV");
			else tex = new TLatex(0.23,0.75,"p^{#gamma}_{T}<250 GeV");
			tex->SetNDC();
			tex->SetTextFont(42);
			tex->SetLineWidth(2);
			tex->Draw("same");
		}
		can5->UseCurrentStyle();
		can5->SetLogy();
		map["M_yey"]->GetYaxis()->SetRangeUser(0.01,100*map["M_yey"]->GetMaximum());
		can5->Update();

		TString names;
		if (conf.getBool("SavePlots")){

	        names="../../plots/fake-rate/"+TString(can4->GetTitle())+conf.getStr("Tag")+".png";
	        can4->SaveAs(names.Data());
	        names="../../plots/fake-rate/"+TString(can4->GetTitle())+conf.getStr("Tag")+".C";
	        can4->SaveAs(names.Data());
	        names="../../plots/fake-rate/"+TString(can4->GetTitle())+conf.getStr("Tag")+".pdf";
	        can4->SaveAs(names.Data());	    	

	        names="../../plots/fake-rate/"+TString(can5->GetTitle())+conf.getStr("Tag")+".png";
	        can5->SaveAs(names.Data());
	        names="../../plots/fake-rate/"+TString(can5->GetTitle())+conf.getStr("Tag")+".C";
	        can5->SaveAs(names.Data());
	        names="../../plots/fake-rate/"+TString(can5->GetTitle())+conf.getStr("Tag")+".pdf";
	        can5->SaveAs(names.Data());	    	

	    }

	}

};

void FakeRatePlotter::FakeRatePlotterWenu(HistMap SR, HistMap CR2, Config conf){

	gROOT->LoadMacro("$HOME/atlasrootstyle/AtlasLabels.C");
	gROOT->LoadMacro("$HOME/atlasrootstyle/AtlasUtils.C");


	SetAtlasStyle();
	gStyle->SetErrorX(0.5);

	// Config conf("../ZGamma.cfg");
	TLatex *   tex;
		TCanvas *can1= new TCanvas("can1","Wenu_fr_vs_pT_cent",1);
		TH1F *rate_pt_cent = (TH1F*)SR["ph_pT_cent"]->Clone();
		rate_pt_cent->Divide(CR2["ph_pT_cent"]);
		rate_pt_cent->GetXaxis()->SetTitle("E^{#gamma}_{T} [GeV]");
		rate_pt_cent->GetYaxis()->SetTitle("Fake rate");
		rate_pt_cent->GetYaxis()->SetRangeUser(0,1.2*rate_pt_cent->GetMaximum());
		// rate_pt->GetYaxis()->SetRangeUser(0,0.08);
		rate_pt_cent->Draw();
		tex = new TLatex(0.6,0.85,"#sqrt{s}=13 TeV, "+conf.getStr("TotalLumi")+ " fb^{-1}");
		tex->SetNDC();
		tex->SetTextFont(42);
		tex->SetLineWidth(2);
		tex->Draw("same");
		ATLASLabel(0.25,0.85,"Internal");
		tex = new TLatex(0.25,0.8,"|#eta|<1.37");
		tex->SetNDC();
		tex->SetTextFont(42);
		tex->SetLineWidth(2);
		tex->Draw("same");
		can1->UseCurrentStyle();
		can1->Update();

		TCanvas *can2= new TCanvas("can2","Wenu_fr_vs_pT_forw",1);
		TH1F *rate_pt_forw = (TH1F*)SR["ph_pT_forw"]->Clone();
		rate_pt_forw->Divide(CR2["ph_pT_forw"]);
		rate_pt_forw->GetXaxis()->SetTitle("E^{#gamma}_{T} [GeV]");
		rate_pt_forw->GetYaxis()->SetTitle("Fake rate");
		rate_pt_forw->GetYaxis()->SetRangeUser(0,1.2*rate_pt_forw->GetMaximum());
		// rate_pt->GetYaxis()->SetRangeUser(0,0.08);
		rate_pt_forw->Draw();
		tex = new TLatex(0.6,0.85,"#sqrt{s}=13 TeV, "+conf.getStr("TotalLumi")+ " fb^{-1}");
		tex->SetNDC();
		tex->SetTextFont(42);
		tex->SetLineWidth(2);
		tex->Draw("same");
		ATLASLabel(0.25,0.85,"Internal");
		tex = new TLatex(0.23,0.8,"1.52<|#eta|<2.37");
		tex->SetNDC();
		tex->SetTextFont(42);
		tex->SetLineWidth(2);
		tex->Draw("same");
		can2->UseCurrentStyle();
		can2->Update();


		TCanvas *can3= new TCanvas("can3","Wenu_fr_vs_eta",1);
		TH1F *rate_eta = (TH1F*)SR["ph_eta"]->Clone();
		rate_eta->Divide(CR2["ph_eta"]);
		rate_eta->GetXaxis()->SetTitle("#eta^{#gamma}");
		rate_eta->GetYaxis()->SetTitle("Fake rate");
		rate_eta->GetYaxis()->SetRangeUser(0,1.2*rate_eta->GetMaximum());
		rate_eta->Draw();
		tex = new TLatex(0.6,0.85,"#sqrt{s}=13 TeV, "+conf.getStr("TotalLumi")+" fb^{-1}");
		tex->SetNDC();
		tex->SetTextFont(42);
		tex->SetLineWidth(2);
		tex->Draw("same");
		ATLASLabel(0.23,0.85,"Internal");
		can3->UseCurrentStyle();
		can3->Update();
		TString names;
		if (conf.getBool("SavePlots")){

	        names="../../plots/fake-rate/"+TString(can3->GetTitle())+conf.getStr("Tag")+".png";
	        can3->SaveAs(names.Data());
	        names="../../plots/fake-rate/"+TString(can3->GetTitle())+conf.getStr("Tag")+".C";
	        can3->SaveAs(names.Data());
	        names="../../plots/fake-rate/"+TString(can3->GetTitle())+conf.getStr("Tag")+".pdf";
	        can3->SaveAs(names.Data());	    	

	        names="../../plots/fake-rate/"+TString(can2->GetTitle())+conf.getStr("Tag")+".png";
	        can2->SaveAs(names.Data());
	        names="../../plots/fake-rate/"+TString(can2->GetTitle())+conf.getStr("Tag")+".C";
	        can2->SaveAs(names.Data());
	        names="../../plots/fake-rate/"+TString(can2->GetTitle())+conf.getStr("Tag")+".pdf";
	        can2->SaveAs(names.Data());	    	

	        names="../../plots/fake-rate/"+TString(can1->GetTitle())+conf.getStr("Tag")+".png";
	        can1->SaveAs(names.Data());
	        names="../../plots/fake-rate/"+TString(can1->GetTitle())+conf.getStr("Tag")+".C";
	        can1->SaveAs(names.Data());
	        names="../../plots/fake-rate/"+TString(can1->GetTitle())+conf.getStr("Tag")+".pdf";
	        can1->SaveAs(names.Data());	    	

	    }
};

void FakeRatePlotter::FakeRateComp(HistMap Zee_cent, HistMap Zee_forw, HistMap SR, HistMap CR2, Config conf){


	gROOT->LoadMacro("$HOME/atlasrootstyle/AtlasLabels.C");
	gROOT->LoadMacro("$HOME/atlasrootstyle/AtlasUtils.C");


	SetAtlasStyle();
	gStyle->SetErrorX(0.5);

	// Config conf("../ZGamma.cfg");
	TLatex *   tex;
		TCanvas *can1= new TCanvas("can1","Wenu_fr_vs_pT_cent",1);
		TH1F *rate_pt_cent = (TH1F*)CR2["ph_pT_cent"]->Clone();
		// rate_pt_cent->Add(SR["ph_pT_forw"]);
		// TH1F *rate_pt_denom = (TH1F*)CR2["ph_pT_cent"]->Clone();
		// rate_pt_denom->Add(CR2["ph_pT_forw"]);
		// rate_pt_cent->Divide(rate_pt_denom);
		// rate_pt_cent->Divide(CR2["ph_pT_cent"]);
		rate_pt_cent->GetXaxis()->SetTitle("E^{e}_{T} [GeV]");
		// rate_pt_cent->GetYaxis()->SetTitle("Fake rate");
		rate_pt_cent->GetYaxis()->SetTitle("Events/bin");
		rate_pt_cent->GetYaxis()->SetRangeUser(0,1.2*rate_pt_cent->GetMaximum());

		TH1F *rate_pt_cent_Zee = (TH1F*)Zee_cent["pT_yee"]->Clone();
		// rate_pt_cent_Zee->Divide(Zee_cent["pT_yee"]);

		// rate_pt->GetYaxis()->SetRangeUser(0,0.08);
		rate_pt_cent->Draw();
		rate_pt_cent_Zee->Draw("same");
		tex = new TLatex(0.6,0.85,"#sqrt{s}=13 TeV, "+conf.getStr("TotalLumi")+ " fb^{-1}");
		tex->SetNDC();
		tex->SetTextFont(42);
		tex->SetLineWidth(2);
		tex->Draw("same");
		ATLASLabel(0.25,0.85,"Internal");
		tex = new TLatex(0.25,0.8,"|#eta|<1.37");
		tex->SetNDC();
		tex->SetTextFont(42);
		tex->SetLineWidth(2);
		tex->Draw("same");

		TLegend *leg1=new TLegend(0.65,0.5,0.9,0.6);
		leg1->SetBorderSize(0);
		leg1->SetFillStyle(0);
		leg1->AddEntry(rate_pt_cent,"Wenu truth","P");
		leg1->AddEntry(rate_pt_cent_Zee,"Zee truth","P");
		leg1->Draw("same");
		can1->UseCurrentStyle();
		rate_pt_cent_Zee->SetMarkerColor(kRed);
		rate_pt_cent_Zee->SetLineColor(kRed);
		can1->Update();

		TCanvas *can2= new TCanvas("can2","Wenu_fr_vs_pT_forw",1);
		TH1F *rate_pt_forw = (TH1F*)CR2["ph_pT_forw"]->Clone();
		// rate_pt_forw->Divide(CR2["ph_pT_forw"]);
		rate_pt_forw->GetXaxis()->SetTitle("E^{e}_{T} [GeV]");
		// rate_pt_forw->GetYaxis()->SetTitle("Fake rate");
		rate_pt_forw->GetYaxis()->SetTitle("Events/bin");
		rate_pt_forw->GetYaxis()->SetRangeUser(0,1.2*rate_pt_forw->GetMaximum());
		// rate_pt->GetYaxis()->SetRangeUser(0,0.08);
		TH1F *rate_pt_forw_Zee = (TH1F*)Zee_forw["pT_yee"]->Clone();
		// rate_pt_forw_Zee->Divide(Zee_forw["pT_yee"]);

		// rate_pt->GetYaxis()->SetRangeUser(0,0.08);
		rate_pt_forw->Draw();
		rate_pt_forw_Zee->Draw("same");
		tex = new TLatex(0.6,0.85,"#sqrt{s}=13 TeV, "+conf.getStr("TotalLumi")+ " fb^{-1}");
		tex->SetNDC();
		tex->SetTextFont(42);
		tex->SetLineWidth(2);
		tex->Draw("same");
		ATLASLabel(0.25,0.85,"Internal");
		tex = new TLatex(0.23,0.8,"1.52<|#eta|<2.37");
		tex->SetNDC();
		tex->SetTextFont(42);
		tex->SetLineWidth(2);
		tex->Draw("same");
		TLegend *leg2=new TLegend(0.65,0.5,0.9,0.6);
		leg2->SetBorderSize(0);
		leg2->SetFillStyle(0);
		leg2->AddEntry(rate_pt_forw,"Wenu truth","P");
		leg2->AddEntry(rate_pt_forw_Zee,"Zee truth","P");
		leg2->Draw("same");

		can2->UseCurrentStyle();
		rate_pt_forw_Zee->SetMarkerColor(kRed);
		rate_pt_forw_Zee->SetLineColor(kRed);
		can2->Update();


		TCanvas *can3 = new TCanvas("can3","Wenu_fr_vs_eta",1);
		TH1F *rate_eta = (TH1F*)CR2["ph_eta"]->Clone();
		// rate_eta->Divide(CR2["ph_eta"]);
		rate_eta->GetXaxis()->SetTitle("#eta^{e}");
		// rate_eta->GetYaxis()->SetTitle("Fake rate");
		rate_eta->GetYaxis()->SetTitle("Events/bin");
		rate_eta->GetYaxis()->SetRangeUser(0,1.2*rate_eta->GetMaximum());
		TH1F *rate_eta_Zee = (TH1F*)Zee_forw["eta_yee"]->Clone();
		// rate_eta_Zee->Divide(Zee_forw["eta_yee"]);

		// rate_pt->GetYaxis()->SetRangeUser(0,0.08);
		rate_eta->Draw();
		rate_eta_Zee->Draw("same");
		tex = new TLatex(0.6,0.85,"#sqrt{s}=13 TeV, "+conf.getStr("TotalLumi")+" fb^{-1}");
		tex->SetNDC();
		tex->SetTextFont(42);
		tex->SetLineWidth(2);
		tex->Draw("same");
		ATLASLabel(0.23,0.85,"Internal");
		TLegend *leg3=new TLegend(0.65,0.5,0.9,0.6);
		leg3->SetBorderSize(0);
		leg3->SetFillStyle(0);
		leg3->AddEntry(rate_eta,"Wenu truth","P");
		leg3->AddEntry(rate_eta_Zee,"Zee truth","P");
		leg3->Draw("same");

		can3->UseCurrentStyle();
		rate_eta_Zee->SetMarkerColor(kRed);
		rate_eta_Zee->SetLineColor(kRed);
		can3->Update();
		TString names;
		if (conf.getBool("SavePlots")){

	        names="../../plots/fake-rate/"+TString(can3->GetTitle())+conf.getStr("Tag")+".png";
	        can3->SaveAs(names.Data());
	        names="../../plots/fake-rate/"+TString(can3->GetTitle())+conf.getStr("Tag")+".C";
	        can3->SaveAs(names.Data());
	        names="../../plots/fake-rate/"+TString(can3->GetTitle())+conf.getStr("Tag")+".pdf";
	        can3->SaveAs(names.Data());	    	

	        names="../../plots/fake-rate/"+TString(can2->GetTitle())+conf.getStr("Tag")+".png";
	        can2->SaveAs(names.Data());
	        names="../../plots/fake-rate/"+TString(can2->GetTitle())+conf.getStr("Tag")+".C";
	        can2->SaveAs(names.Data());
	        names="../../plots/fake-rate/"+TString(can2->GetTitle())+conf.getStr("Tag")+".pdf";
	        can2->SaveAs(names.Data());	    	

	        names="../../plots/fake-rate/"+TString(can1->GetTitle())+conf.getStr("Tag")+".png";
	        can1->SaveAs(names.Data());
	        names="../../plots/fake-rate/"+TString(can1->GetTitle())+conf.getStr("Tag")+".C";
	        can1->SaveAs(names.Data());
	        names="../../plots/fake-rate/"+TString(can1->GetTitle())+conf.getStr("Tag")+".pdf";
	        can1->SaveAs(names.Data());	    	

	    }


};