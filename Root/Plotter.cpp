#include "Plotter.h"

#include <TCanvas.h>
#include <TLine.h>

#include <TApplication.h>
#include <TROOT.h>
#include <TH1F.h>
#include <TH2F.h>
#include "TMath.h"


TGraphErrors* Plotter::DrawStatErrors(TH1* hist)
{
  TGraphErrors* grStatError = new TGraphErrors(hist);
  grStatError->SetLineColor(kWhite);
  grStatError->SetFillColor(kBlack);
  grStatError->SetFillStyle(3004);
  grStatError->Draw("2");

  return grStatError;
};


void Plotter::DrawRatioSubPlot(TString errorsType, TH1* histData, TH1* histMC, float integralData = 0., float integralMC = 0., float setRangeA_Y = 0., float setRangeB_Y = 0., float setRangeA_X = 0., float setRangeB_X = 0.)
{
  Config conf("../ZGamma.cfg");
  gStyle->SetErrorX(0.5);

  if(!histData){
    cout<<"DrawRatioSubPlot:: Fatal error. Pointer histData = 0x0"<<endl;
    exit(1);
  }
  if(!histMC){
    cout<<"DrawRatioSubPlot:: Fatal error. Pointer histMC = 0x0"<<endl;
    exit(1);
  }
  if(!integralMC) {
    cout<<"DrawRatioSubPlot:: Fatal error. integralMC = 0"<<endl;
    exit(1);
  }

    TH1F* h_ratio = (TH1F*)histData->Clone("dataClone");
    if (conf.getBool("UnScaled")||errorsType.Contains("splitError")) {h_ratio->Divide(histData, histMC); std::cout<<"Unscaled\n";}
    else h_ratio->Divide(histData, histMC, 1/integralData, 1/integralMC);
    TH1F* h_ratio_MC = (TH1F*)histData->Clone("MC");
    // h_ratio_MC->Divide(histMC, histMC);

    // TString errorsType = ""; // "union"

    if(errorsType.Contains("splitError")) {
      std::cout<<"spliting errors\n";
      float rel_uncert_from_data_histo, rel_uncert_from_MC_histo, rel_uncert_overall;
      for (int i=1;i<=(h_ratio->GetNbinsX());i++) 
      {
        rel_uncert_from_data_histo = (histData->GetBinContent(i) == 0)?0.:histData->GetBinError(i)/histData->GetBinContent(i);
        h_ratio->SetBinError(i,rel_uncert_from_data_histo*h_ratio->GetBinContent(i));
        rel_uncert_from_MC_histo   = (histMC->GetBinContent(i) == 0)?0.:histMC->GetBinError(i)/histMC->GetBinContent(i);
        h_ratio_MC->SetBinContent(i,1);
        h_ratio_MC->SetBinError(i,rel_uncert_from_MC_histo*h_ratio_MC->GetBinContent(i));
      }

    }  else {
      // We don't use h_ratio_MC here at all
      std::cout<<"union errors\n";
      float rel_uncert_from_data_histo, rel_uncert_from_MC_histo, rel_uncert_overall;
      for (int i=1;i<=(h_ratio->GetNbinsX());i++) 
      {
        rel_uncert_from_data_histo = (histData->GetBinContent(i) == 0)?0.:histData->GetBinError(i)/histData->GetBinContent(i);
        rel_uncert_from_MC_histo   = (histMC->GetBinContent(i) == 0)?0.:histMC->GetBinError(i)/*/(integralMC)*//histMC->GetBinContent(i);
        rel_uncert_overall = sqrt(pow(rel_uncert_from_data_histo,2)+pow(rel_uncert_from_MC_histo,2));
        h_ratio->SetBinError(i,rel_uncert_overall*h_ratio->GetBinContent(i));
      }
    }


    
    h_ratio->GetXaxis()->SetTitle(histData->GetXaxis()->GetTitle());
    h_ratio->GetYaxis()->SetTitle(histData->GetYaxis()->GetTitle());
    h_ratio->GetYaxis()->SetTitleSize(0.12);
    h_ratio->GetYaxis()->SetTitleOffset(0.4);
    h_ratio->GetXaxis()->SetTickLength(0.06);
    h_ratio->GetXaxis()->SetTitleSize(0.12);
    h_ratio->GetXaxis()->SetTitleOffset(1.);
    h_ratio->GetYaxis()->SetTitleOffset(0.4);
    h_ratio->GetXaxis()->SetTickLength(0.06);
    h_ratio->GetXaxis()->SetTitleSize(0.12);
    h_ratio->GetXaxis()->SetTitleOffset(1.3);
    h_ratio->GetXaxis()->SetLabelSize(0.115); 
    h_ratio->GetYaxis()->SetLabelSize(0.115); 


    if(setRangeA_Y!=setRangeB_Y) { // set specific range for Y axis
      h_ratio->GetYaxis()->SetRangeUser(setRangeA_Y,setRangeB_Y);
    }
    else { // set maximum as maximum Y point +10% of full width,
           // set minimum as minimum Y point -10% of full width
      float mindValue = h_ratio->GetBinError(h_ratio->GetMinimumBin());
      float minValue = h_ratio->GetBinContent(h_ratio->GetMinimumBin());
      float maxdValue = h_ratio->GetBinError(h_ratio->GetMaximumBin());
      float maxValue = h_ratio->GetBinContent(h_ratio->GetMaximumBin());

      float width = fabs(maxValue+maxdValue-minValue-mindValue);

      float c = 2; // 100%
      width=max(fabs(1-(maxValue+maxdValue)),fabs(1-(minValue-mindValue)));
      // std::cout<<"width "<<width<<"\n";
      // h_ratio->GetYaxis()->SetRangeUser(minValue-c*width,maxValue+c*width);
      // h_ratio->GetYaxis()->SetRangeUser(minValue-c*mindValue,maxValue+c*maxdValue);
      h_ratio->GetYaxis()->SetRangeUser(1-width*c,1+width*c);
      // h_ratio->GetYaxis()->SetRangeUser(-c*width,c*width);
    }

    if(errorsType.Contains("splitError")) {
      h_ratio->SetMarkerStyle(20);
      h_ratio->Draw("Ep");
      DrawStatErrors(h_ratio_MC);
    } else {
      h_ratio->SetMarkerStyle(1);
      h_ratio->Draw("E");
    }
  
    // TF1* fit = new TF1("fitOfRatio","[0]",0,3);
    // fit->SetParameter(0,2);
    // fit->SetLineColor(kRed);
    // h_ratio->Fit(fit,"R");

    // Draw line
    if(setRangeA_X==setRangeB_X) {
      float minX = h_ratio->GetXaxis()->GetXmin();
      float maxX = h_ratio->GetXaxis()->GetXmax();
      TLine *l=new TLine(minX, 1., maxX, 1.);
      l->SetLineColor(kRed);
      l->SetLineStyle(9);
      l->Draw();
    }
    else {
      TLine *l=new TLine(setRangeA_X, 1., setRangeB_X, 1.);
      l->SetLineColor(kRed);
      l->SetLineStyle(9);
      l->Draw();
      l= new TLine(29.45,setRangeA_Y,29.45,setRangeB_Y);
      l->SetLineStyle(2);
      // l->Draw();
    }
}