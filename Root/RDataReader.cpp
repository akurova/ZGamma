#include "RDataReader.h"
#include "Config.h"
#include <vector>
#include <map>
#include <TString.h>

#include <TTree.h>
#include <TFile.h>
#include <iostream>
#include <TMath.h>
#include <TH1.h>
#include <TLorentzVector.h>

using namespace std;

RDataReader::RDataReader(TString s){
    if (s.Contains("MC")) filename = s+"/user.akurova.MxAOD.root";
    else if (s.Contains("data")) filename=s;
    cout<<"Opening file "<<filename<<endl;
};

void RDataReader::setParams(double lumi){
    //TODO avoid double initialization of config (now it's done both here and in main programm)
    Config con("../ZGamma.cfg");
        params["CR"]=con.getBool("CR");
        params["CR2"]=con.getBool("CR2");
        params["CR3"]=con.getBool("CR3");
        params["veto"]=con.getBool("veto");
        params["angular"]=con.getBool("angular");
        // params["metjet"]=par[5];
        // params["metgam"]=par[6];
        params["tauveto"]=con.getBool("tauveto");
        params["nojets"]=con.getBool("nojets");
        params["IsoInversion"]=con.getBool("IsoInversion");
        params["METsoft"]=con.getNum("SoftTerm");
        params["lumi"]=lumi;
        params["IsoLoose"]=con.getBool("IsolationLoose");
        params["Loose'"]=con.getInt("LoosePrime");
        params["CaloOnly"]=con.getBool("IsoCaloOnly");
        params["norm"]=con.getBool("normalize");
        params["METsign"]=con.getNum("METSignificance");
        params["DPhiMetGam"]=con.getNum("DPhiMETGam");
        params["metjet"]=con.getNum("DPhiMETjet");
        params["MET"]=con.getNum("MET");
        params["VBSjets"]=con.getBool("VBSjets");
        params["MinCut"]=con.getNum("MinCut");
        params["MediumCut"]=con.getNum("MediumCut");
        // std::cout<<"Configuration: \n";
        // for (auto p: params){
        //     std::cout<<p.first<<":"<<p.second<<"\n";
        // }
};

HistMap RDataReader::getInitialMap(TString name){
    HistMap h;
    double N_jets[6]={-0.5,0.5,1.5,2.5,7.5,10.5};
    double pt[5]={150,250,400,600,1100}; //for e to gam CR
    h["B_region_ph_eta"]=new TH1D("B_region_ph_eta_"+name,"",10,-5,5);
    h["B_region_ph_eta"]->Sumw2();
    h["D_region_ph_eta"]=new TH1D("D_region_ph_eta_"+name,"",10,-5,5);
    h["D_region_ph_eta"]->Sumw2();
    h["E_region_ph_eta"]=new TH1D("E_region_ph_eta_"+name,"",10,-5,5);
    h["E_region_ph_eta"]->Sumw2();
    h["F_region_ph_eta"]=new TH1D("F_region_ph_eta_"+name,"",10,-5,5);
    h["F_region_ph_eta"]->Sumw2();
    h["ph_iso_et_tight"]=new TH1D("h_ph_iso_et_tight_"+name,"",50,-30,70);
    h["ph_iso_et_tight"]->Sumw2();
    h["ph_iso_et_antitight"]=new TH1D("h_ph_iso_et_antitight_"+name,"",50,-30,70);
    h["ph_iso_et_antitight"]->Sumw2();
    return h;
};

HistMap RDataReader::getHistograms(TString name){
    if (params["CR2"]) fatal("you can't calculate R Factor for e+MET CR");
    HistMap h;
    TFile *file = new TFile(filename);
    h = getInitialMap(name);
    // Double_t metjet=0.4;
    Double_t metgam=TMath::Pi()*0.5;;    
    // bool ph_tight, loosePrime3, loosePrime4;
    unsigned int n_ph, n_e_looseBL, n_e_medium, n_mu, n_jet, N_f_l, N_f_h, N_c_l, N_c_h;
    double ph_pt, ph_eta, ph_phi, ph_iso_et, ph_iso_pt, jet_pt, jet_eta, jet_phi, jet_E, jet2_pt, jet2_eta, jet2_phi, jet2_E, metTST_pt, metTST_phi,\
    e_lead_pt, e_lead_eta, e_lead_phi, e_lead_E, weight, ph_zp, jet_sum_pt, metTSTsignif, soft_term_pt;
    unsigned int ph_isem;
    int mc_ph_type, mc_ph_origin, mc_el_type, mc_el_origin, n_tau_loose;
    TLorentzVector met, ph, jet, jet2, el;
    Double_t sum_of_weights, sum_of_weights_bk_xAOD, koef,
    sum_of_weights_bk_DxAOD, sumw, sum1, sum2, sum3;
    Long64_t N=0;
    TTree *norm_tree = (TTree*)file->Get("norm_tree");
    if (norm_tree!=0){
        norm_tree->SetBranchAddress("koef",&koef);
        norm_tree->GetEntry(0);
    }
    if (filename.Contains("545839")) koef=0.27410;
    cout<<"koef="<<koef<<endl;
    if (norm_tree==0) {
        koef = 1; 
        checker.initialize();
    }
    else koef*=params["lumi"]; 
    // if (filename.Contains("jets_JZ8")) {
    //     koef*=1000; 
    //     // std::cout<<"\n\n\n\ncorrecting jets_JZ8 cross section\n\n\n\n";
    // }

    TTree *tree_MC_sw = (TTree*)file->Get("output_tree_sw");
    // tree_MC_sw->SetBranchAddress("sum_of_weights",&sum_of_weights);
    // tree_MC_sw->SetBranchAddress("sum_of_weights_bk_DxAOD",&sum_of_weights_bk_DxAOD);
    tree_MC_sw->SetBranchAddress("sum_of_weights_bk_xAOD",&sum_of_weights_bk_xAOD);

    Int_t entry = (Int_t)tree_MC_sw->GetEntries();
    sumw=0;sum1=0;sum2=0;sum3=0;

    for (Int_t i=0; i<entry; i++)
    { 
        tree_MC_sw->GetEntry(i);
        // sum1=sum1+sum_of_weights;
        // sum2=sum2+sum_of_weights_bk_DxAOD;
        sum3=sum3+sum_of_weights_bk_xAOD;
    }
    // std::cout<<"sum1 "<<sum1<<endl;
    // std::cout<<"sum2 "<<sum2<<endl;
    // std::cout<<"sum3 "<<sum3<<endl;
    // sumw=sum1*sum3/sum2;
    sumw=sum3;
    if (tree_MC_sw==0 || koef==1) sumw=1;
    
    TTree *tree;
    if (!params["CR2"]) {tree=(TTree*)file->Get("output_tree");}
    if (params["CR2"]) {tree=(TTree*)file->Get("output_tree_emet");}

    
    N = tree->GetEntries();
    
tree->SetBranchAddress("n_ph", &n_ph); //number of photons in event
tree->SetBranchAddress("n_jet", &n_jet); //number of jets in event
tree->SetBranchAddress("n_e_looseBL", &n_e_looseBL);  //number of loose electrons in event
tree->SetBranchAddress("n_tau_loose", &n_tau_loose);  //number of loose tau in event
tree->SetBranchAddress("n_mu", &n_mu); //number of muons in event

if (params["CR2"]){
    tree->SetBranchAddress("el_pt", &e_lead_pt);  //leading leading electron p_x
    tree->SetBranchAddress("el_eta", &e_lead_eta);  //p_y
    tree->SetBranchAddress("el_phi", &e_lead_phi);  //p_z
    tree->SetBranchAddress("el_E", &e_lead_E);    //E
}

tree->SetBranchAddress("jet_sum_pt", &jet_sum_pt); 
if (!params["CR2"]){
    if (params["IsoLoose"]) tree->SetBranchAddress("ph_iso_et20", &ph_iso_et);    //E //FixedCutLoose
    else tree->SetBranchAddress("ph_iso_et40", &ph_iso_et);    //E //FixedCutTight
    tree->SetBranchAddress("ph_iso_pt", &ph_iso_pt);    //E
    tree->SetBranchAddress("ph_isem", &ph_isem);  //leading ph tighness
    tree->SetBranchAddress("ph_pt", &ph_pt);  //leading ph p_x
    tree->SetBranchAddress("ph_eta", &ph_eta);  //p_y
    tree->SetBranchAddress("ph_phi", &ph_phi);  //p_z
    tree->SetBranchAddress("mc_ph_type", &mc_ph_type);  //p_z
    tree->SetBranchAddress("mc_ph_origin", &mc_ph_origin);  //p_z
}
tree->SetBranchAddress("jet_lead_pt", &jet_pt);  //leading jet p_x
tree->SetBranchAddress("jet_lead_eta", &jet_eta);  //p_y 
tree->SetBranchAddress("jet_lead_phi", &jet_phi);  //p_z
tree->SetBranchAddress("jet_lead_E", &jet_E);    //E

    tree->SetBranchAddress("jet_sublead_pt", &jet2_pt);  //leading jet p_x
    tree->SetBranchAddress("jet_sublead_eta", &jet2_eta);  //p_y 
    tree->SetBranchAddress("jet_sublead_phi", &jet2_phi);  //p_z
    tree->SetBranchAddress("jet_sublead_E", &jet2_E);    //E


tree->SetBranchAddress("metTST_pt", &metTST_pt);  //MET p_x
tree->SetBranchAddress("metTST_phi", &metTST_phi);  //p_y  
tree->SetBranchAddress("soft_term_pt", &soft_term_pt);  //MET soft term 
tree->SetBranchAddress("metTSTsignif", &metTSTsignif);  //MET significance  

if (koef!=1) tree->SetBranchAddress("weight", &weight);    //weight

if (!params["CR2"])   tree->SetBranchAddress("ph_z_point",&ph_zp);

weight=1;

    for (Int_t i=0; i<N; i++)
    {
        tree->GetEntry(i);
        // if (fabs(weight)>100) continue;
        jet.SetPtEtaPhiE(jet_pt,jet_eta,jet_phi,jet_E);
        jet2.SetPtEtaPhiE(jet2_pt,jet2_eta,jet2_phi,jet2_E);
        met.SetPtEtaPhiM(metTST_pt,0,metTST_phi,0);
        if (params["CR2"]) ph.SetPtEtaPhiE(e_lead_pt,e_lead_eta,e_lead_phi,e_lead_E);
        else ph.SetPtEtaPhiM(ph_pt,ph_eta,ph_phi,0);

        if (n_ph!=1 && !params["CR2"]) continue;
        if (n_ph!=0 && params["CR2"]) continue;
        if (n_e_looseBL!=1 && params["CR2"] && !params["CR"] && params["veto"]) continue;
        // if (n_e_Tight!=1 && params["CR2"] && !params["CR"] && params["veto"]) continue;
        // if (!PIDTight && params["CR2"] && (!filename.Contains("pTV_mjj_merging") && !filename.Contains("old_merging") && !filename.Contains("12-08-2020") && !filename.Contains("qg_tagger"))) continue;
        // if ((topoetcone20*0.001>0.06*ph.Pt() || ptvarcone20*0.001>0.06*ph.Pt()) && params["CR2"] && (!filename.Contains("pTV_mjj_merging") && !filename.Contains("old_merging"))) continue;
        if (n_e_looseBL<1 && params["CR2"] && !params["veto"]) continue;
        if (n_mu!=0 && params["CR2"] && params["veto"]) continue;
        if (n_jet!=0 && params["nojets"]) continue;
        if (params["VBSjets"] && n_jet<2) continue;
        if (params["VBSjets"] && (jet+jet2).M()<params["mjj"]) continue;

        if (met.Pt()<params["MET"]) continue; 
        if (ph.Pt()<150) continue;
        if (!params["CR2"] && fabs(ph_zp)>250) continue;
        // if (met.Pt()/sqrt(jet_sum_pt+ph.Pt())<params["METsign"] && !params["CR3"] && params["angular"]) continue;
        // if (met.Pt()/sqrt(jet_sum_pt+ph.Pt())>=params["METsign"] && params["CR3"] && params["angular"]) continue;
        if (soft_term_pt>params["METsoft"] && params["angular"]) continue;
        if (metTSTsignif<params["METsign"] && params["angular"] && !params["CR3"]) continue;
        if (metTSTsignif>params["METsign"] && params["angular"] && params["CR3"]) continue;
        if ((n_mu+n_e_looseBL<1) && params["CR"] && !params["CR2"]) continue; //Wgam CR
        // if ((n_mu+n_e_looseBL!=2) && params["CR"] && params["veto"] && params["CR2"]) continue; //Wgam CR
        if ((n_mu+n_e_looseBL<2) && params["CR"] && params["CR2"]) continue; //Wgam CR
        if ((n_e_looseBL!=0 || n_mu!=0) && params["veto"] && !params["CR"] && !params["CR2"]) continue; //lepton veto
        if (n_tau_loose!=0 && params["tauveto"]) continue;//tau veto
        if (fabs(ph.DeltaPhi(met))<params["DPhiMetGam"] && params["angular"]) continue;

        if (n_jet!=0){  
            if (fabs(met.DeltaPhi(jet))<params["metjet"] && params["angular"]) continue; 
            if (n_jet>1 && fabs(met.DeltaPhi(jet2))<params["metjet2"] && params["angular"]) continue;
            if (n_jet>1 && fabs((ph.Rapidity()-(jet.Rapidity()+jet2.Rapidity())*0.5)/(jet.Rapidity()-jet2.Rapidity()))>params["gCent"] && params["angular"]) continue;
            if (n_jet>1 && fabs(jet.Rapidity()-jet2.Rapidity())<params["DYjj"] && params["angular"]) continue;
            // if (fabs(jet.Eta())<2.5 || fabs(jet2.Eta())<2.5) continue;

        }
        if (fabs(weight)>100 && !(filename.Contains("410470") || filename.Contains("700") || filename.Contains("5046"))) {std::cout<<"large weight "<<weight<<"\n"; continue;/*weight =1;*/}
        
        bool iso = false; bool noniso = false; float ratio=0;
        if (!params["IsoLoose"]){
            if (params["CaloOnly"]){
                // std::cout<<"this is TightCaloOnly\n";
                if (ph_iso_et>11.45+0.022*ph.Pt() && ph_iso_et<29.45+0.022*ph.Pt()) iso = true;
                if (ph_iso_et>5.45+0.022*ph.Pt() && ph_iso_et<11.45+0.022*ph.Pt()) noniso = true;
            }
            else {
                // std::cout<<"this is Tight\n";
                if (ph_iso_et>11.45+0.022*ph.Pt() && ph_iso_et<0.022*ph.Pt()+29.45 && ph_iso_pt/ph.Pt()<0.05) iso = true;
                if (ph_iso_et>5.45+0.022*ph.Pt() && ph_iso_et<11.45+0.022*ph.Pt() && ph_iso_pt/ph.Pt()<0.05) noniso = true;
            }
        }
        else if (params["IsoLoose"]){
            ratio=0.065;

            // std::cout<<"this is Loose\n";
            // if (ph_iso_et>0.065*ph.Pt()+7 && ph_iso_pt/ph.Pt()<0.05) iso = true;
            // if (ph_iso_et>0.065*ph.Pt()+2 && ph_iso_et<0.065*ph.Pt()+7 && ph_iso_pt/ph.Pt()<0.05) noniso = true;
            if (params["IsoInversion"]) {
                if (ph_iso_et>ratio*ph.Pt()+params["MediumCut"] && ph_iso_pt/ph.Pt()>0.05) iso = true;
                if (ph_iso_et>ratio*ph.Pt()+params["MinCut"] && ph_iso_et<ratio*ph.Pt()+params["MediumCut"] && ph_iso_pt/ph.Pt()>0.05) noniso = true;
            }
            else {
                if (ph_iso_et>ratio*ph.Pt()+10.5 && ph_iso_pt/ph.Pt()<0.05) iso = true;
                if (ph_iso_et>ratio*ph.Pt()+2 && ph_iso_et<ratio*ph.Pt()+10.5 && ph_iso_pt/ph.Pt()<0.05) noniso = true;
            }
        }
        if (ph_isem!=0 && !params["CR2"]) { 
            unsigned int loose;
            int lloose=(int)params["Loose'"];
            switch(lloose){
                case 2: loose = 0x27fc01; break;
                case 3: loose = 0x25fc01; break;
                case 4: loose = 0x5fc01; break;
                case 5: loose = 0x1fc01; break;
            }
            if ((ph_isem & loose)==0) {

                if (!params["IsoLoose"]) {
                    if (params["CaloOnly"]) h["ph_iso_et_antitight"]->Fill(ph_iso_et-0.022*ph.Pt(),weight);
                    else if (!params["CaloOnly"]) {
                        if (ph_iso_pt/ph.Pt()<0.05) h["ph_iso_et_antitight"]->Fill(ph_iso_et-0.022*ph.Pt(),weight);
                    }
                }
                else if (params["IsoLoose"] && ph_iso_pt/ph.Pt()<0.05) h["ph_iso_et_antitight"]->Fill(ph_iso_et-0.065*ph.Pt(),weight);

                if (iso) {
                    // SumC+=koef*weight/sumw; SumCsq+=weight*weight; SumCnom+=weight; ///F
                    h["F_region_ph_eta"]->Fill(ph.Eta(),weight);

                }
                if (noniso) {
                    // SumD+=koef*weight/sumw; SumDsq+=weight*weight; SumDnom+=weight; ///D-F
                    h["D_region_ph_eta"]->Fill(ph.Eta(),weight);
                    // continue;
                }
            }
            continue;
        }

        if (!params["IsoLoose"]) {
            if (params["CaloOnly"]) h["ph_iso_et_tight"]->Fill(ph_iso_et-0.022*ph.Pt(),weight);
            else if (!params["CaloOnly"]) {
                if (ph_iso_pt/ph.Pt()<0.05) h["ph_iso_et_tight"]->Fill(ph_iso_et-0.022*ph.Pt(),weight);
            }
        }
        else if (params["IsoLoose"] && ph_iso_pt/ph.Pt()<0.05) h["ph_iso_et_tight"]->Fill(ph_iso_et-0.065*ph.Pt(),weight);

        if (iso) {
            // SumA+=koef*weight/sumw; SumAsq+=weight*weight; SumAnom+=weight; ///E
            h["E_region_ph_eta"]->Fill(ph.Eta(),weight);
        }
        if (noniso) {
            // SumB+=koef*weight/sumw; SumBsq+=weight*weight; SumBnom+=weight; ///B-E
            h["B_region_ph_eta"]->Fill(ph.Eta(),weight);
        }

        // if (n_jet!=0){	
        //     h["dPhi_jet_MET"]->Fill(fabs(met.DeltaPhi(jet)),weight);
        //     // if (fabs(met.DeltaPhi(jet))<metjet && params["angular"] && !params["CR3"]) continue; 
        //     // if (fabs(met.DeltaPhi(jet))>metjet && params["angular"] && params["CR3"]) continue; 
        //     // if (fabs(ph.DeltaPhi(jet))>2.6 && angular) continue;
        //     h["dPhi_y_jet"]->Fill(fabs(ph.DeltaPhi(jet)),weight);

        // }
    
        //     h["pT_MET"]->Fill(met.Pt(),weight);
        //     h["pT_lead_y"]->Fill(ph.Pt(),weight);
        //     h["n_el"]->Fill(n_e_looseBL,weight);
        //     h["n_ph"]->Fill(n_ph,weight);
        //     h["n_mu"]->Fill(n_mu,weight);
        //     h["n_jet"]->Fill(n_jet,weight);
        //     // h["ph_iso_et"]->Fill(ph_iso_et, weight);
        //     // h["ph_iso_pt"]->Fill(ph_iso_pt, weight);
    }
    h.includeOverflow();
    if (koef!=1 && params["norm"]) 
        h.Norm(koef/sumw);
 
    cout<<"integral="<<h["E_region_ph_eta"]->Integral()<<endl;
    return h;
    file->Close();
};