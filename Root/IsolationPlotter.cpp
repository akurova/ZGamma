#include "IsolationPlotter.h"

void IsolationPlotter::IsoPlots(TH1D* tight1, TH1D* anti1, TString name){

  gROOT->LoadMacro("$HOME/atlasrootstyle/AtlasLabels.C");
  gROOT->LoadMacro("$HOME/atlasrootstyle/AtlasUtils.C");

  SetAtlasStyle();

  gStyle->SetErrorX(0.5);
  TString names;
  Config conf("../ZGamma.cfg");
  bool nojets = conf.getBool("nojets");
  TString loose = conf.getStr("LoosePrime");

  TH1D *tight = (TH1D*)tight1->Clone("tight");
  TH1D *anti = (TH1D*)anti1->Clone("anti");
  TLine *line;

  TCanvas *c1 = new TCanvas(name.Data(),name.Data(),0.,0.,600,600);
  if (!conf.getBool("IsolationLoose")){
  	if (conf.getBool("IsoCaloOnly")) names="FixedCutTightCaloOnly iso Loose'"+conf.getStr("LoosePrime");
  	else if (!conf.getBool("IsoCaloOnly")) names="FixedCutTight iso Loose'"+conf.getStr("LoosePrime");
  }
  else if (conf.getBool("IsolationLoose")) names="FixedCutLoose iso Loose'"+conf.getStr("LoosePrime");
  c1->SetTitle(names.Data());
  TPad *pad1 = new TPad("pad1","pad1",0,0.3,1,1);
  pad1->SetBottomMargin(0.02);
     pad1->Draw();
     pad1->cd();
  anti->SetLineColor(kRed);
  anti->SetMarkerStyle(1);
  tight->SetLineColor(kBlue);
  tight->SetMarkerStyle(1);
  std::cout<<name<<" integral tight "<<tight->Integral()<<"\n";
  std::cout<<name<<" integral anti "<<anti->Integral()<<"\n";
  if (!conf.getBool("UnScaled")){
    anti->Scale(1/anti->Integral());
    tight->Scale(1/tight->Integral());
  }
  tight->GetXaxis()->SetRangeUser(-30,70);
  if (conf.getBool("RangeY")) 
    tight->GetYaxis()->SetRangeUser(0,0.25);
  tight->GetXaxis()->SetLabelOffset(0.015);
  tight->Draw("E");
  anti->Draw("same E");
  TLegend *leg = new TLegend(0.7,0.5,0.93,0.9);
  leg->SetBorderSize(0);
  leg->SetFillColor(0);
  if (name.Contains("Data")) leg->SetHeader("Z(#nu#bar{#nu})jet in Data");
  else leg->SetHeader(name.Data());
  leg->AddEntry(tight,"Tight ID", "l");
  leg->AddEntry(anti,"Loose'"+loose+" ID", "l");
  leg->Draw();

  TLatex *tex = new TLatex(0.71,0.43,"N_{jet}#geq0");
  if (nojets) tex = new TLatex(0.71,0.43,"N_{jet}=0");
  tex->SetNDC();
     tex->SetTextFont(42);
     tex->SetLineWidth(2);
     tex->Draw("same");
     tex = new TLatex(0.20,0.78,"#sqrt{s}=13 TeV, 36.2 fb^{-1}");
  	tex->SetNDC();
     tex->SetTextFont(42);
     tex->SetLineWidth(2);
     tex->Draw("same");

     ATLASLabel(0.20,0.85,"Internal");

  line = new TLine(29.45,0,29.45,0.25);
  // line = new TLine(2.45,0,2.45,0.25);
  line->SetLineStyle(2);
  // line->Draw();
    if (!conf.getBool("IsolationLoose")) anti1->GetXaxis()->SetTitle("ET_cone_40-0.022*p^{#gamma}_{T} [GeV]");
    else if (conf.getBool("IsolationLoose")) anti1->GetXaxis()->SetTitle("ET_cone_20-0.065*p^{#gamma}_{T} [GeV]");
  c1->cd();
  TPad *pad2 = new TPad("pad2","pad2",0,0,1,0.3);
  pad2->SetTopMargin(0.02);
     pad2->Draw();
     pad2->cd();
  pad2->SetBottomMargin(0.33);
  anti1->GetYaxis()->SetTitle("loose'/tight");
  if (conf.getBool("RangeY")) DrawRatioSubPlot("", anti1, tight1, anti1->Integral(), tight1->Integral(), -2.2, 3.2, -30, 70);
  else DrawRatioSubPlot("", anti1, tight1, anti1->Integral(), tight1->Integral(), 0, 0, -30, 70);

  c1->cd();
  c1->Update();

  if (conf.getBool("SavePlots")){
    TString scale="";
    if (conf.getBool("UnScaled")) scale=" UnScaled";

      if (name.Contains("Data")){
        names+="../plots/"+TString(c1->GetTitle())+" BKGSubtr "+conf.getStr("Subtraction")+scale+".png";
        c1->SaveAs(names.Data());
        names+="../plots/"+TString(c1->GetTitle())+" BKGSubtr "+conf.getStr("Subtraction")+scale+".C";
        c1->SaveAs(names.Data());
        names+="../plots/"+TString(c1->GetTitle())+" BKGSubtr "+conf.getStr("Subtraction")+scale+".pdf";
        c1->SaveAs(names.Data());
      }
      else {
        names="../plots/"+TString(c1->GetTitle())+" "+name+scale+".png";
        c1->SaveAs(names.Data());
        names="../plots/"+TString(c1->GetTitle())+" "+name+scale+".C";
        c1->SaveAs(names.Data());
        names="../plots/"+TString(c1->GetTitle())+" "+name+scale+".pdf";
        c1->SaveAs(names.Data());
      }
  }


}


void IsolationPlotter::IsoPlotsJetToE(TH1D* tight1, TH1D* anti1, TString name){

  gROOT->LoadMacro("$HOME/atlasrootstyle/AtlasLabels.C");
  gROOT->LoadMacro("$HOME/atlasrootstyle/AtlasUtils.C");

  SetAtlasStyle();

  gStyle->SetErrorX(0.5);
  TString names;
  Config conf("../ZGamma.cfg");
  bool nojets = conf.getBool("nojets");

  TH1D *tight = (TH1D*)tight1->Clone("tight");
  TH1D *anti = (TH1D*)anti1->Clone("anti");
  TLine *line;

  TCanvas *c1 = new TCanvas(name.Data(),"",0.,0.,600,600);
  TPad *pad1 = new TPad("pad1","pad1",0,0.3,1,1);
  pad1->SetBottomMargin(0.02);
     pad1->Draw();
     pad1->cd();
  anti->SetLineColor(kRed);
  anti->SetMarkerStyle(1);
  tight->SetLineColor(kBlue);
  tight->SetMarkerStyle(1);
  std::cout<<name<<" integral tight "<<tight->Integral()<<"\n";
  std::cout<<name<<" integral anti "<<anti->Integral()<<"\n";
  if (!conf.getBool("UnScaled")){
    anti->Scale(1/anti->Integral());
    tight->Scale(1/tight->Integral());
  }
  // tight->GetXaxis()->SetRangeUser(-30,70);
  // if (conf.getBool("RangeY")) 
  //   tight->GetYaxis()->SetRangeUser(0,0.25);
  tight->GetXaxis()->SetLabelOffset(0.015);
  tight->Draw("hist");
  anti->Draw("same hist");
  TLegend *leg = new TLegend(0.7,0.5,0.93,0.9);
  leg->SetBorderSize(0);
  leg->SetFillColor(0);
  if (name.Contains("Data")) leg->SetHeader("Jet#rightarrow e in Data");
  else leg->SetHeader(name.Data());
  leg->AddEntry(tight,"Tight ID", "l");
  leg->AddEntry(anti,"LooseBL ID", "l");
  leg->Draw();

  TLatex *tex; 
  // tex = new TLatex(0.71,0.43,"N_{jet}#geq0");
  // if (nojets) tex = new TLatex(0.71,0.43,"N_{jet}=0");
  // tex->SetNDC();
  //    tex->SetTextFont(42);
  //    tex->SetLineWidth(2);
  //    tex->Draw("same");
     tex = new TLatex(0.20,0.78,"#sqrt{s}=13 TeV, 36.2 fb^{-1}");
    tex->SetNDC();
     tex->SetTextFont(42);
     tex->SetLineWidth(2);
     tex->Draw("same");

     ATLASLabel(0.20,0.85,"Internal");

  line = new TLine(29.45,0,29.45,0.25);
  // line = new TLine(2.45,0,2.45,0.25);
  line->SetLineStyle(2);
  // line->Draw();
  // anti1->GetXaxis()->SetTitle("ET_cone_40-0.022*p^{#gamma}_{T} [GeV]");
  if (name.Contains("e_iso_pt")) anti1->GetXaxis()->SetTitle("ptvarcone20_TightTTVA_pt1000");
  if (name.Contains("e_iso_et")) anti1->GetXaxis()->SetTitle("topoetcone20");

  c1->cd();
  TPad *pad2 = new TPad("pad2","pad2",0,0,1,0.3);
  pad2->SetTopMargin(0.02);
     pad2->Draw();
     pad2->cd();
  pad2->SetBottomMargin(0.33);
  anti1->GetYaxis()->SetTitle("loose/tight");
  if (conf.getBool("RangeY")) DrawRatioSubPlot("", anti1, tight1, anti1->Integral(), tight1->Integral(), -2.2, 3.2, anti1->GetXaxis()->GetXmin(), anti1->GetXaxis()->GetXmax());
  else DrawRatioSubPlot("", anti1, tight1, anti1->Integral(), tight1->Integral(), 0, 0, anti1->GetXaxis()->GetXmin(), anti1->GetXaxis()->GetXmax());

  c1->cd();
  c1->Update();

  if (conf.getBool("SavePlots")){
    TString scale="";
    if (conf.getBool("UnScaled")) scale=" UnScaled";

      if (name.Contains("Data")){
        names+="../plots/"+TString(c1->GetTitle())+" BKGSubtr "+conf.getStr("Subtraction")+scale+".png";
        c1->SaveAs(names.Data());
        names+="../plots/"+TString(c1->GetTitle())+" BKGSubtr "+conf.getStr("Subtraction")+scale+".C";
        c1->SaveAs(names.Data());
        names+="../plots/"+TString(c1->GetTitle())+" BKGSubtr "+conf.getStr("Subtraction")+scale+".pdf";
        c1->SaveAs(names.Data());
      }
      else {
        names="../plots/"+TString(c1->GetTitle())+" "+name+scale+".png";
        c1->SaveAs(names.Data());
        names="../plots/"+TString(c1->GetTitle())+" "+name+scale+".C";
        c1->SaveAs(names.Data());
        names="../plots/"+TString(c1->GetTitle())+" "+name+scale+".pdf";
        c1->SaveAs(names.Data());
      }
  }


}