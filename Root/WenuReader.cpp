#include "WenuReader.h"
#include "Config.h"
#include <vector>
#include <map>
#include <TString.h>

#include <TTree.h>
#include <TFile.h>
#include <iostream>
#include <TMath.h>
#include <TH1.h>
#include <TLorentzVector.h>

using namespace std;

WenuReader::WenuReader(){

};

WenuReader::WenuReader(TString s){
    if (s.Contains("MC")) filename = s+"/user.akurova.MxAOD.root";
    else if (s.Contains("data")||s.Contains("Data")) filename=s;
    // cout<<"Opening file "<<filename<<endl;
};

void WenuReader::setParams(double lumi, Config con){
    //TODO avoid double initialization of config (now it's done both here and in main programm)
    // Config con("../ZGamma.cfg");
        params["CR"]=con.getBool("CR");
        params["CR2"]=con.getBool("CR2"); //always true for etogam data-driven background
        params["CR3"]=con.getBool("CR3");
        params["veto"]=con.getBool("veto");
        params["angular"]=con.getBool("angular");
        // params["metjet"]=par[5];
        // params["metgam"]=par[6];
        params["nojets"]=con.getBool("nojets");
        params["VBSjets"]=con.getBool("VBSjets");
        params["METsign"]=con.getNum("METSignificance");
        params["METsoft"]=con.getNum("SoftTerm");
        params["DPhiMetGam"]=con.getNum("DPhiMETGam");
        params["metjet2"]=con.getNum("DPhiSubleadJetMET");
        params["metjet"]=con.getNum("DPhiMETjet");
        params["gCent"]=con.getNum("GamCentrality");
        params["DYjj"]=con.getNum("DYjj");
        params["MET"]=con.getNum("MET");
        params["mjj"]=con.getNum("mjj");
        params["lumi"]=lumi;
        params["norm"]=con.getBool("normalize");

        // for (auto p: params){
        //     std::cout<<p.first<<":"<<p.second<<"\n";
        // }
};

HistMap WenuReader::getInitialMap(TString name){
    HistMap h;
    double N_jets[6]={-0.5,0.5,1.5,2.5,3.5,7.5};
    double pt[5]={150,250,400,600,1100}; //for e to gam CR
    double ptmiss[5]={120,250,400,600,1100}; //for e to gam C
    Double_t x[5]={150,200,250,300,1010};
    // Double_t x[3]={150,250,1010};
    h["n_ph_forw"]=new TH1D("n_ph_forw_"+name,";N_{#gamma};Events/bin",6,-0.5,5.5);
    h["n_ph_forw"]->Sumw2();

    h["ph_pT_forw"]=new TH1D("ph_pT_forw_"+name,"",4,x);
    h["ph_pT_forw"]->Sumw2();

    h["ph_eta"]=new TH1D("ph_eta_"+name,"",10,-3,3);
    h["ph_eta"]->Sumw2();

    h["n_ph_cent_lt250"]=new TH1D("n_ph_cent_lt250_"+name,";N_{#gamma};Events/bin",6,-0.5,5.5);
    h["n_ph_cent_lt250"]->Sumw2();

    h["ph_pT_cent"]=new TH1D("ph_pT_cent_"+name,"",4,x);
    h["ph_pT_cent"]->Sumw2();

    h["n_ph_cent_gt250"]=new TH1D("n_ph_cent_gt250_"+name,";N_{#gamma};Events/bin",6,-0.5,5.5);
    h["n_ph_cent_gt250"]->Sumw2();


    return h;
};

HistMap WenuReader::getHistograms(TString name){
    HistMap h; 
    TFile *file = new TFile(filename);
    h = getInitialMap(name);
    // Double_t metjet=0.4;
    Double_t metgam=TMath::Pi()*0.5;;    
    // bool ph_tight, loosePrime3, loosePrime4;
    unsigned int n_ph, n_e_looseBL, n_e_medium, n_e_Tight, n_mu, n_jet, N_f_l, N_f_h, N_c_l, N_c_h, run;//, event;
    unsigned long long event;
    double  ph_pt, ph_eta, ph_phi, ph_iso_et, ph_iso_pt, ph_zp, jet_pt, jet_eta, jet_phi, jet_E, jet2_pt, jet2_eta, jet2_phi, jet2_E, jet3_pt, jet3_eta, jet3_phi, jet3_E, metTST_pt, metTST_phi, metTST_Rho, metTST_VarL, \
    e_lead_pt, e_lead_eta, e_lead_phi, e_lead_E, weight,  jet_sum_pt, fake_weight, metTSTsignif, soft_term_pt;
    unsigned int ph_isem;
    int  mc_ph_type, mc_ph_origin, mc_el_type, mc_el_origin;
    bool jet_third_nTrkqg, jet_third_BDTqg, jet_sublead_nTrkqg, jet_sublead_BDTqg, jet_lead_nTrkqg, jet_lead_BDTqg;
    TLorentzVector met, ph, jet, jet2, el;
    Double_t sum_of_weights, sum_of_weights_bk_xAOD, koef,
    sum_of_weights_bk_DxAOD, sumw, sum1, sum2, sum3;
    Long64_t N=0;
    TTree *norm_tree = (TTree*)file->Get("norm_tree");
    if (norm_tree!=0){
        norm_tree->SetBranchAddress("koef",&koef);
        norm_tree->GetEntry(0);
    }
    if (norm_tree==0) {
        koef = 1;
        checker.initialize();
    }
    else koef*=params["lumi"]; 
    // std::cout<<"fake rates "<<params["fr_forw"]<<" "<<params["fr_cent_gt250"]<<" "<<params["fr_cent_lt250"]<<"\n";
    TTree *tree_MC_sw = (TTree*)file->Get("output_tree_sw");
    // tree_MC_sw->SetBranchAddress("sum_of_weights",&sum_of_weights);
    // tree_MC_sw->SetBranchAddress("sum_of_weights_bk_DxAOD",&sum_of_weights_bk_DxAOD);
    tree_MC_sw->SetBranchAddress("sum_of_weights_bk_xAOD",&sum_of_weights_bk_xAOD);

    Int_t entry = (Int_t)tree_MC_sw->GetEntries();
    sumw=0;sum1=0;sum2=0;sum3=0;

    for (Int_t i=0; i<entry; i++)
    { 
        tree_MC_sw->GetEntry(i);
        // sum1=sum1+sum_of_weights;
        // sum2=sum2+sum_of_weights_bk_DxAOD;
        sum3=sum3+sum_of_weights_bk_xAOD;
    }

    // sumw=sum1*sum3/sum2;
    sumw=sum3;
    if (tree_MC_sw==0 || koef==1) sumw=1;
    
    TTree *tree;
    if (!params["CR2"]) {tree=(TTree*)file->Get("output_tree");}
    if (params["CR2"]) {tree=(TTree*)file->Get("output_tree_emet");}
    
    N = tree->GetEntries();
    
tree->SetBranchAddress("n_ph", &n_ph); //number of photons in event
tree->SetBranchAddress("n_jet", &n_jet); //number of jets in event
tree->SetBranchAddress("n_e_looseBL", &n_e_looseBL);  //number of loose electrons in event
tree->SetBranchAddress("n_mu", &n_mu); //number of muons in event

if (params["CR2"]){
    tree->SetBranchAddress("el_pt", &e_lead_pt);  //leading leading electron p_x
    tree->SetBranchAddress("el_eta", &e_lead_eta);  //p_y
    tree->SetBranchAddress("el_phi", &e_lead_phi);  //p_z
    tree->SetBranchAddress("el_E", &e_lead_E);    //E
    tree->SetBranchAddress("n_e_Tight", &n_e_Tight);  //number of loose electrons in event
}

tree->SetBranchAddress("jet_sum_pt", &jet_sum_pt); 
if (!params["CR2"]){
    // tree->SetBranchAddress("ph_iso_et20", &ph_iso_et);    //E //FixedCutLoose
    tree->SetBranchAddress("ph_iso_et40", &ph_iso_et);    //E //FixedCutTight
    tree->SetBranchAddress("ph_iso_pt", &ph_iso_pt);    //E
    tree->SetBranchAddress("ph_isem", &ph_isem);  //leading ph tighness
    tree->SetBranchAddress("ph_pt", &ph_pt);  //leading ph p_x
    tree->SetBranchAddress("ph_eta", &ph_eta);  //p_y
    tree->SetBranchAddress("ph_phi", &ph_phi);  //p_z
    tree->SetBranchAddress("mc_ph_type", &mc_ph_type);  //p_z
    tree->SetBranchAddress("mc_ph_origin", &mc_ph_origin);  //p_z
}
tree->SetBranchAddress("jet_lead_pt", &jet_pt);  //leading jet p_x
tree->SetBranchAddress("jet_lead_eta", &jet_eta);  //p_y 
tree->SetBranchAddress("jet_lead_phi", &jet_phi);  //p_z
tree->SetBranchAddress("jet_lead_E", &jet_E);    //E

    tree->SetBranchAddress("jet_sublead_pt", &jet2_pt);  //leading jet p_x
    tree->SetBranchAddress("jet_sublead_eta", &jet2_eta);  //p_y 
    tree->SetBranchAddress("jet_sublead_phi", &jet2_phi);  //p_z
    tree->SetBranchAddress("jet_sublead_E", &jet2_E);    //E

tree->SetBranchAddress("metTST_pt", &metTST_pt);  //MET p_x
tree->SetBranchAddress("metTST_phi", &metTST_phi);  //p_y  
tree->SetBranchAddress("metTSTsignif", &metTSTsignif);  //MET significance  
tree->SetBranchAddress("soft_term_pt", &soft_term_pt);  //MET soft term 
if (koef==1){
    tree->SetBranchAddress("RunNumber",&run);
    tree->SetBranchAddress("EventNumber",&event);
}
if (koef!=1) tree->SetBranchAddress("weight", &weight);    //weight

if (!params["CR2"])   tree->SetBranchAddress("ph_z_point",&ph_zp);

tree->SetBranchAddress("jet_lead_BDTqg", &jet_lead_BDTqg);
tree->SetBranchAddress("jet_lead_nTrkqg", &jet_lead_nTrkqg);

tree->SetBranchAddress("jet_sublead_BDTqg", &jet_sublead_BDTqg);
tree->SetBranchAddress("jet_sublead_nTrkqg", &jet_sublead_nTrkqg);


    tree->SetBranchAddress("jet_third_pt", &jet3_pt);  //leading jet p_x
    tree->SetBranchAddress("jet_third_eta", &jet3_eta);  //p_y 
    tree->SetBranchAddress("jet_third_phi", &jet3_phi);  //p_z
    tree->SetBranchAddress("jet_third_E", &jet3_E);    //E

tree->SetBranchAddress("jet_third_BDTqg", &jet_third_BDTqg);
tree->SetBranchAddress("jet_third_nTrkqg", &jet_third_nTrkqg);

weight=1;

double forw=0; double cent_lt250=0; double cent_gt250=0; double whats_left=0;


int counter=0;
    for (Int_t i=0; i<N; i++)
    {
        tree->GetEntry(i);
        jet.SetPtEtaPhiE(jet_pt,jet_eta,jet_phi,jet_E);
        jet2.SetPtEtaPhiE(jet2_pt,jet2_eta,jet2_phi,jet2_E);
        met.SetPtEtaPhiM(metTST_pt,0,metTST_phi,0);
        if (params["CR2"]) ph.SetPtEtaPhiE(e_lead_pt,e_lead_eta,e_lead_phi,e_lead_E);
        else ph.SetPtEtaPhiM(ph_pt,ph_eta,ph_phi,0);
            // h["n_el"]->Fill(n_e_looseBL,weight);

        if (n_ph!=1 && !params["CR2"]) continue;
        if (n_ph!=0 && params["CR2"]) continue;
        if (n_e_looseBL!=1 && params["CR2"] && params["veto"]) continue;
        if (n_e_Tight!=1 && params["CR2"]) continue;
        if (n_e_looseBL<1 && params["CR2"] && !params["veto"]) continue;
        if (n_mu!=0 && params["CR2"] && params["veto"]) continue;
        if (n_jet!=0 && params["nojets"]) continue;
        if (params["VBSjets"] && n_jet<2) continue;
        if (params["VBSjets"] && (jet+jet2).M()<params["mjj"]) continue;

        if (met.Pt()<params["MET"]) continue; 
        if (ph.Pt()<150) continue;
        if (!params["CR2"] && fabs(ph_zp)>250) continue;
        if (soft_term_pt>params["METsoft"] && params["angular"]) continue;
        if (metTSTsignif<params["METsign"] && params["angular"]) continue;
        if (ph_isem!=0 && !params["CR2"]) continue; //comment for Z jet control regions
        if ((ph_iso_et>2.45+0.022*ph.Pt() || ph_iso_pt/ph.Pt()>0.05) && !params["CR2"]) continue; //comment for Z jet control regions
        if (!params["CR2"] && mc_ph_type!=2 && mc_ph_type!=15) continue;//for fake rate check
        if (!params["CR2"] && mc_ph_origin!=12 && mc_ph_origin!=40) continue;//for fake rate check
        if ((n_mu+n_e_looseBL!=2) && params["CR"] && params["veto"] && params["CR2"]) continue; //Wgam CR
        if ((n_mu+n_e_looseBL!=1) && params["CR"] && params["veto"] && !params["CR2"]) continue; //Wgam CR
        if ((n_mu+n_e_looseBL<1) && params["CR"] && !params["veto"]) continue; //Wgam CR
        if ((n_e_looseBL!=0 || n_mu!=0) && params["veto"] && !params["CR"] && !params["CR2"]) continue; //lepton veto
        if (fabs(ph.DeltaPhi(met))<params["DPhiMetGam"] && params["angular"]) continue;

        if (n_jet!=0){  
            if (fabs(met.DeltaPhi(jet))<params["metjet"] && params["angular"] && !params["CR3"]) continue; 
            if (fabs(met.DeltaPhi(jet))>params["metjet"] && params["angular"] && params["CR3"]) continue; 
            if (n_jet>1 && fabs(met.DeltaPhi(jet2))<params["metjet2"] && params["angular"]) continue;
            if (n_jet>1 && fabs((ph.Rapidity()-(jet.Rapidity()+jet2.Rapidity())*0.5)/(jet.Rapidity()-jet2.Rapidity()))>params["gCent"] && params["angular"]) continue;
            if (n_jet>1 && fabs(jet.Rapidity()-jet2.Rapidity())<params["DYjj"] && params["angular"]) continue;

        }
    // std::cout<<"basic selection passed\n";
        h["ph_eta"]->Fill(ph.Eta(),weight);

        if (fabs(ph.Eta())>1.5){

                forw+=weight;

                h["n_ph_forw"]->Fill(n_ph,weight);
                h["ph_pT_forw"]->Fill(ph.Pt(),weight);

        } else if (fabs(ph.Eta())<1.5){

            h["ph_pT_cent"]->Fill(ph.Pt(),weight);

            if (ph.Pt()>250){

                cent_gt250+=weight;

                h["n_ph_cent_gt250"]->Fill(n_ph,weight);

            } else if (ph.Pt()<=250){


                cent_lt250+=weight;

                h["n_ph_cent_lt250"]->Fill(n_ph,weight);

            }
        } else {
            whats_left+=weight;
            std::cout<<"out of range "<<ph.Eta()<<" "<<ph.Pt()<<"\n";
        }


    }

    // std::cout<<"forward "<<forw<<"\n";
    // std::cout<<"central high "<<cent_gt250<<"\n";
    // std::cout<<"central low "<<cent_lt250<<"\n";
    h.includeOverflow();
    if (koef!=1 && params["norm"]) h.Norm(koef/sumw);

    return h;
    file->Close();

};