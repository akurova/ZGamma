// STL include(s):
#include <iostream>

// ROOT include(s):
#include "TFile.h"
#include "TH1.h"
#include "TObjArray.h"
#include "TObjString.h"
#include "TSystem.h"

// Local include(s):
#include "Common.h"

  void fatal(TString msg)
  {
    printf("\nFATAL\n  %s\n\n", msg.Data());
    abort();
  }

  void warning(TString msg)
  {
    printf("\nWARNING\n  %s\n\n", msg.Data());
  }

    StrV vectorize(TString str, TString sep)
  {
    StrV result;
    TObjArray *strings = str.Tokenize(sep.Data());

    if (strings->GetEntries() == 0) { delete strings; return result; }

    TIter istr(strings);

    while (TObjString *os = (TObjString *)istr()) {
      // the number sign and everything after is treated as a comment
      if (os->GetString()[0] == '#') { break; }

      result.push_back(os->GetString());
    }

    delete strings;
    return result;
  }

  // convert a text line containing a list of numbers to a vector<double>
  NumV vectorizeNum(TString str, TString sep)
  {
    NumV result;
    StrV vecS = vectorize(str, sep);

    for (uint i = 0; i < vecS.size(); ++i)
    { result.push_back(atof(vecS[i])); }

    return result;
  }

  // checks if a given file or directory exist
  bool fileExist(TString fn)
  {
    return !(gSystem->AccessPathName(fn.Data()));
  }


