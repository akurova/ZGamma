#include "FakeRateReader.h"
#include "Config.h"
#include <vector>
#include <map>
#include <TString.h>

#include <TTree.h>
#include <TFile.h>
#include <iostream>
#include <TMath.h>
#include <TH1.h>
#include <TLorentzVector.h>

FakeRateReader::FakeRateReader(){

};

FakeRateReader::FakeRateReader(TString s){
    if (s.Contains("MC")) filename = s+"/user.akurova.MxAOD.root";
    else if (s.Contains("data")||s.Contains("Data")) filename=s;
    // cout<<"Opening file "<<filename<<endl;
};

void FakeRateReader::setParams(double lumi, Config conf){
    
    params["Central"]=conf.getBool("Central");
    params["HighPt"]=conf.getBool("HighPt");
    params["Truth"]=conf.getBool("Truth");
    params["FromMC"]=conf.getBool("FromMC");
    params["Plots"]=conf.getBool("Plots");
    params["PlotsMass"]=conf.getBool("PlotsMass");
    params["Mz"]=conf.getNum("Mz");
    params["MEL"]=conf.getNum("MEL");
    params["MER"]=conf.getNum("MER");
    params["BELL"]=conf.getNum("BELL");
    params["BELR"]=conf.getNum("BELR");
    params["BERL"]=conf.getNum("BERL");
    params["BERR"]=conf.getNum("BERR");
    params["IsoGap"]=conf.getNum("IsoGap");
    params["MinCut"]=conf.getNum("MinCut");
    params["MediumCut"]=conf.getNum("MediumCut");
    params["IsoInversion"]=conf.getBool("IsoInversion");
    params["lumi"]=lumi;
    params["IsoLoose"]=conf.getBool("IsolationLoose");
    params["Loose'"]=conf.getInt("LoosePrime");
    params["CaloOnly"]=conf.getBool("IsoCaloOnly");    
    params["CheckConversion"]=conf.getBool("CheckConversion");
    params["OldTightID"]=conf.getBool("OldTightID");
    params["Converted"]=conf.getBool("Converted");
    params["MassWindow"]=conf.getBool("MassWindow");
    params["bkgSyst"]=conf.getBool("bkgSyst");
    params["bkgFit"]=conf.getBool("bkgFit");
    paramsStr["Region"]=conf.getStr("Region");
    // std::cout<<"Fake rate configuration \n";
    // for (auto m: params){
    // 	std::cout<<m.first<<" "<<m.second<<"\n";
    // }

};

HistMap FakeRateReader::getInitialMap(TString name){
	Config conf("../ZGamma.cfg");
	HistMap h;
	Double_t x[5]={150,200,250,300,1010};
	// Double_t x[3]={150,250,1010};
	Double_t x1=1; Double_t x2=4001; Int_t n=800;
	Int_t neg=n;
	// Double_t eta_x[23]={-2.5,-2,-1.75,-1.5,-1.4,-1.2,-1,-0.8,-0.6,-0.4,-0.2,0,0.2,0.4,0.6,0.8,1,1.2,1.4,1.5,1.75,2,2.5};
	// Double_t eta_x[19]={-2.5,-2,-1.75,-1.5,-1.25,-1,-0.75,-0.5,-0.25,0,0.25,0.5,0.75,1,1.25,1.5,1.75,2,2.5};
	Double_t eta_x[15]={-2.5,-2,-1.75,-1.5,-1.25,-1,-0.5,0,0.5,1,1.25,1.5,1.75,2,2.5};
	if (!params["Central"] && paramsStr["Region"]=="A") neg=2000;
	if (conf.getBool("MassWindow")) {x1=71.2; x2=111.2; n=200; neg=200;}
    h["pT_yee"]=new TH1D("pT_yee_"+name,"",4,x);
    h["pT_yee"]->Sumw2();
    h["M_yee"]=new TH1D("M_yee_"+name,";M_{ee};Events/bin",n,x1,x2);
    h["M_yee"]->Sumw2();
    h["M_yee_b"]=new TH1D("M_yee_b_"+name,";M_{ee};Events/bin",n,x1,x2);
    h["M_yee_b"]->Sumw2();
    h["pT_yey"]=new TH1D("pT_yey_"+name,"",4,x);
    h["pT_yey"]->Sumw2();
    h["M_yey"]=new TH1D("M_yey_"+name,";M_{e#gamma};Events/bin",neg,x1,x2);
    h["M_yey"]->Sumw2();
    h["M_yey_b"]=new TH1D("M_yey_b_"+name,";M_{e#gamma};Events/bin",neg,x1,x2);
    h["M_yey_b"]->Sumw2();
    if (!params["HighPt"]) {
    	h["eta_yey"]=new TH1D("eta_yey_"+name,"",14,eta_x); h["eta_yee"]=new TH1D("eta_yee_"+name,"",14,eta_x);
    }
    else {h["eta_yey"]=new TH1D("eta_yey_"+name,"",10,-2.5,2.5); h["eta_yee"]=new TH1D("eta_yee_"+name,"",10,-2.5,2.5);}
    // h["eta_yey"]=new TH1D("eta_yey_"+name,"",10,-2.5,2.5);
    // h["eta_yey"]=new TH1D("eta_yey_"+name,"",10,-3,3);
    h["eta_yey"]->Sumw2();
    // h["eta_yee"]=new TH1D("eta_yee_"+name,"",10,-2.5,2.5);
    // h["eta_yee"]=new TH1D("eta_yee_"+name,"",10,-3,3);
    h["eta_yee"]->Sumw2();

	return h;
};

HistMap FakeRateReader::getHistograms(TString name){

	HistMap h;
	TFile *file = new TFile(filename);
	h = getInitialMap(name);
	unsigned int n_ph, n_e_looseBL, n_e_medium, n_mu, n_jet, ph_isem;
	double ph_pt, ph_eta, ph_phi, ph_iso_et, ph_iso_pt, jet_pt, jet_eta, jet_phi, jet_E, metTST_pt, metTST_phi, \
	e_lead_pt, e_lead_eta, e_lead_phi, e_lead_E, e_sublead_pt, e_sublead_eta, e_sublead_phi, e_sublead_E, weight, ph_zp;
	TLorentzVector ph, el, met;
	bool ph_tight, e_lead_iso_Tight, fr_ey, fr_ee;
	
	int mc_ph_type, mc_ph_origin, mc_el_type, mc_el_origin, e_lead_type, e_sublead_type, e_lead_origin, e_sublead_origin, ph_convFlag;
	Double_t sum_of_weights, sum_of_weights_bk_xAOD, koef,
    sum_of_weights_bk_DxAOD, sumw, sum1, sum2, sum3;
    Long64_t N=0;


    TTree *norm_tree = (TTree*)file->Get("norm_tree");
    if (norm_tree!=0){
        norm_tree->SetBranchAddress("koef",&koef);
        norm_tree->GetEntry(0);
    }
    if (norm_tree==0) koef = 1; 
    else koef*=params["lumi"]; 
	// std::cout<<"koef "<<koef<<" ";
    TTree *tree_MC_sw = (TTree*)file->Get("output_tree_sw");
    // tree_MC_sw->SetBranchAddress("sum_of_weights",&sum_of_weights);
    // tree_MC_sw->SetBranchAddress("sum_of_weights_bk_DxAOD",&sum_of_weights_bk_DxAOD);
    tree_MC_sw->SetBranchAddress("sum_of_weights_bk_xAOD",&sum_of_weights_bk_xAOD);

    Int_t entry = (Int_t)tree_MC_sw->GetEntries();
    sumw=0;sum1=0;sum2=0;sum3=0;

    for (Int_t i=0; i<entry; i++)
    { 
        tree_MC_sw->GetEntry(i);
        // sum1=sum1+sum_of_weights;
        // sum2=sum2+sum_of_weights_bk_DxAOD;
        sum3=sum3+sum_of_weights_bk_xAOD;
    }

    // sumw=sum1*sum3/sum2;
    sumw=sum3;
    // std::cout<<"sumw "<<sumw<<"\n";
    if (tree_MC_sw==0 || koef==1) sumw=1;

    TTree *ey = (TTree*)file->Get("output_tree_fr_eg");
	TTree *ee = (TTree*)file->Get("output_tree_fr_ee");


	ey->SetBranchAddress("e_lead_pt", &e_lead_pt);  
	ey->SetBranchAddress("e_lead_eta", &e_lead_eta);  
	ey->SetBranchAddress("e_lead_phi", &e_lead_phi);  
	ey->SetBranchAddress("e_lead_E", &e_lead_E);   

	ey->SetBranchAddress("ph_pt", &ph_pt); 
	ey->SetBranchAddress("ph_eta", &ph_eta);  
	ey->SetBranchAddress("ph_phi", &ph_phi); 

	ey->SetBranchAddress("metTST_pt", &metTST_pt);  
	ee->SetBranchAddress("metTST_pt", &metTST_pt);  

	ee->SetBranchAddress("e_sublead_pt", &e_sublead_pt);  
	ee->SetBranchAddress("e_sublead_eta", &e_sublead_eta);  
	ee->SetBranchAddress("e_sublead_phi", &e_sublead_phi);  
	ee->SetBranchAddress("e_sublead_E", &e_sublead_E);   

	ee->SetBranchAddress("e_lead_pt", &e_lead_pt);  
	ee->SetBranchAddress("e_lead_eta", &e_lead_eta);  
	ee->SetBranchAddress("e_lead_phi", &e_lead_phi);  
	ee->SetBranchAddress("e_lead_E", &e_lead_E);
	// ee->SetBranchAddress("e_probe_iso_FCTight", &e_lead_iso_Tight);
	ee->SetBranchAddress("n_ph", &n_ph); 

	if (params["OldTightID"]) ey->SetBranchAddress("ph_isem_old",&ph_isem);
	else ey->SetBranchAddress("ph_isem",&ph_isem);
	ey->SetBranchAddress("ph_iso_pt",&ph_iso_pt);
    if (params["IsoLoose"]) {
    // if (params["IsoLoose"] && paramsStr["Region"]!="A" && paramsStr["Region"]!="B" && paramsStr["Region"]!="C" && paramsStr["Region"]!="D") {
    // std::cout<<"using loose isolation\n";
    ey->SetBranchAddress("ph_iso_et20", &ph_iso_et);     //E //FixedCutLoose
    }
    else ey->SetBranchAddress("ph_iso_et40",&ph_iso_et); //FixedCutTight
    ey->SetBranchAddress("ph_convFlag",&ph_convFlag); //FixedCutTight

	if (koef!=1 && params["Truth"] && params["FromMC"]){
		ey->SetBranchAddress("e_lead_type", &mc_el_type);
		ey->SetBranchAddress("e_lead_origin", &mc_el_origin);
		ey->SetBranchAddress("mc_ph_type", &mc_ph_type);
		ey->SetBranchAddress("mc_ph_origin", &mc_ph_origin);
		ee->SetBranchAddress("e_lead_type", &e_lead_type); 
		ee->SetBranchAddress("e_lead_origin", &e_lead_origin); 
		ee->SetBranchAddress("e_sublead_type", &e_sublead_type);   
		ee->SetBranchAddress("e_sublead_origin", &e_sublead_origin);   
		ee->SetBranchAddress("weight", &weight); 
		ey->SetBranchAddress("weight", &weight); 
	}
	if (params["FromMC"]) {		
		ee->SetBranchAddress("weight", &weight); 
		ey->SetBranchAddress("weight", &weight); 
	}

	if (koef==1 && !params["CR2"]) ey->SetBranchAddress("ph_z_point",&ph_zp);

	weight=1;

	N=ey->GetEntries();
	// cout<<"N "<<N<<endl;
	for (Int_t i=0; i<N; i++){
	// cout<<"egam"<<endl;
	ey->GetEntry(i);
	// weight*=koef;
	// if (fabs(weight-22.5717)<0.0001) continue;
	if (fabs(weight)>100 && !(filename.Contains("410470") || filename.Contains("700") || filename.Contains("5046"))) {std::cout<<"large weight "<<weight<<"\n"; continue;/*weight =1;*/}
	if (!filename.Contains("DYee") && fabs(weight)>100) continue;
	// if (fabs(weight)>8) continue;
	if (filename.Contains("data_full_reproc-02-06-2020-MET100GeV_checked.root")) {
		e_lead_eta*=1000;
		e_lead_phi*=1000;
	}
	    el.SetPtEtaPhiE(e_lead_pt,e_lead_eta,e_lead_phi,e_lead_E);
	    ph.SetPtEtaPhiM(ph_pt,ph_eta,ph_phi,0.);
        unsigned int loose;
        int lloose=(int)params["Loose'"];
        switch(lloose){
            case 2: loose = 0x27fc01; break;
            case 3: loose = 0x25fc01; break;
            case 4: loose = 0x5fc01; break;
            case 5: loose = 0x1fc01; break;
        }
	    if (fabs(ph_zp)>250) continue;
	    if (el.Pt()<25) continue;
	    if (params["Truth"] && params["FromMC"]) {
	      if (mc_el_type!=2 && koef!=1) continue;
	      if (mc_el_origin!=13 && koef!=1) continue;
	      if (mc_ph_type!=2 && mc_ph_type!=15) continue;//for fake rate check
	      if (mc_ph_origin!=13 && mc_ph_origin!=40) continue;//for fake rate check
	    }
	    if (ph.Pt()<150) continue;
	    if (params["CheckConversion"]){
		    if (ph_convFlag==0 && params["Converted"]) continue;
		    if (ph_convFlag!=0 && !params["Converted"]) continue;
		}
	    if (!params["Truth"] /*|| !params["FromMC"]*/) {if (metTST_pt>40) continue;}
	        bool iso = false; bool noniso = false; float ratio=0; float step=0;
	        if (params["IsoLoose"]) {ratio=0.065;}
	        else {ratio=0.022; step=2.45;}
	    if (paramsStr["Region"]!="A" && paramsStr["Region"]!="B" && paramsStr["Region"]!="C" && paramsStr["Region"]!="D") {
	        if (!params["IsoLoose"]){
	            if (params["CaloOnly"]){
	                // std::cout<<"this is TightCaloOnly\n";
	                if (ph_iso_et>11.45+ratio*ph.Pt() && ph_iso_et<29.45+ratio*ph.Pt()) iso = true;
	                if (ph_iso_et>4.45+ratio*ph.Pt() && ph_iso_et<11.45+ratio*ph.Pt()) noniso = true;
	            }
	            else {
	                // std::cout<<"this is Tight\n";
	                if (ph_iso_et>11.45+ratio*ph.Pt() && ph_iso_et<ratio*ph.Pt()+29.45 && ph_iso_pt/ph.Pt()<0.05) iso = true;
	                if (ph_iso_et>4.45+ratio*ph.Pt() && ph_iso_et<11.45+ratio*ph.Pt() && ph_iso_pt/ph.Pt()<0.05) noniso = true;
	            }
	        }
	        else if (params["IsoLoose"]){
	            // std::cout<<"this is Loose\n";
	            if (params["IsoInversion"]) {
	            	if (ph_iso_et>ratio*ph.Pt()+params["MediumCut"] && ph_iso_pt/ph.Pt()>0.05) iso = true;
	            	if (ph_iso_et>ratio*ph.Pt()+params["MinCut"] && ph_iso_et<ratio*ph.Pt()+params["MediumCut"] && ph_iso_pt/ph.Pt()>0.05) noniso = true;
	            }
	            else {
	            	if (ph_iso_et>ratio*ph.Pt()+10.5 && ph_iso_pt/ph.Pt()<0.05) iso = true;
	            	if (ph_iso_et>ratio*ph.Pt()+2 && ph_iso_et<ratio*ph.Pt()+10.5 && ph_iso_pt/ph.Pt()<0.05) noniso = true;
	        	}
	        }

	        if (paramsStr["Region"]=="B-E" && (ph_isem!=0 || !noniso)) continue;
	        if (paramsStr["Region"]=="E" && (ph_isem!=0 || !iso)) continue;
	        if (paramsStr["Region"]=="D-F" && ((ph_isem & loose)!=0 || !noniso || ph_isem==0)) continue;
	        if (paramsStr["Region"]=="F" && ((ph_isem & loose)!=0 || !iso || ph_isem==0)) continue;

	    }   

	    // iso = false; noniso = false;
	    else if (paramsStr["Region"]=="A"){
	    	// std::cout<<"selection for signal region";
		    if (ph_isem!=0) continue;
			if (ph_iso_et>=step+ratio*ph.Pt()) continue;
			if (ph_iso_pt/ph.Pt()>=0.05 && !params["CaloOnly"]) continue;
		}
		else if (paramsStr["Region"]=="B"){
		    if (ph_isem!=0) continue;
		    if (params["IsoLoose"]) {if (ph_iso_et<step+params["IsoGap"]+ratio*ph.Pt()) continue;}
		    else if (ph_iso_et<step+params["IsoGap"]+ratio*ph.Pt() || ph_iso_et>ratio*ph.Pt()+29.45) continue;
		    if (params["IsoInversion"]) {if (ph_iso_pt/ph.Pt()<0.05 && !params["CaloOnly"]) continue;}
		    else if (ph_iso_pt/ph.Pt()>0.05 && !params["CaloOnly"]) continue;
		}
		else if (paramsStr["Region"]=="C"){
		    if ((ph_isem & loose)!=0 || ph_isem==0) continue;
		    // if ((ph_isem & 0x5fc01) != 0 || ph_isem==0) continue; //loose'4
		    // if ((ph_isem & 0x27fc01) != 0 || ph_isem==0) continue; //loose'2
		    // if ((ph_isem & 0x25fc01) != 0 || ph_isem==0) continue; //loose'3
		    if (ph_iso_et>step+ratio*ph.Pt()) continue;
		    if (ph_iso_pt/ph.Pt()>0.05 && !params["CaloOnly"]) continue;
		}    
		else if (paramsStr["Region"]=="D"){
		    if ((ph_isem & loose)!=0 || ph_isem==0) continue; //loose'2
		    // if ((ph_isem & 0x5fc01) != 0 || ph_isem==0) continue; //loose'4
		    // if ((ph_isem & 0x27fc01) != 0 || ph_isem==0) continue; //loose'2
		    // if ((ph_isem & 0x25fc01) != 0 || ph_isem==0) continue; //loose'3
		    if (params["IsoLoose"]) {if (ph_iso_et<step+params["IsoGap"]+ratio*ph.Pt()) continue;}
		    else if (ph_iso_et<step+params["IsoGap"]+ratio*ph.Pt() || ph_iso_et>ratio*ph.Pt()+29.45) continue;
		    if (params["IsoInversion"]) {if (ph_iso_pt/ph.Pt()<0.05 && !params["CaloOnly"]) continue;}
		    else if (ph_iso_pt/ph.Pt()>0.05 && !params["CaloOnly"]) continue;
		}
	    if (params["Central"] /*&& !params["Plots"]*/ && fabs(ph.Eta())>=1.5) continue; //central region
	    if (!params["Central"] /*&& !params["Plots"]*/ && fabs(ph.Eta())<1.5) continue; //forward region

	    // if (params["Plots"]) h["pT_yey"]->Fill(ph.Pt(), weight);

	    if (params["Central"]/* && !params["Plots"]*/) {
	      if (params["HighPt"] && ph.Pt()<250) continue;
	      if (!params["HighPt"] && ph.Pt()>250) continue;
	    }
	    // Negam+=weight;     
	    if (params["PlotsMass"] || params["Truth"] || params["MassWindow"]){
	      h["M_yey"]->Fill((ph+el).M(), weight);          
	      if (params["Plots"]) h["eta_yey"]->Fill(ph.Eta(), weight);
	      if (params["Central"] && params["Plots"] && fabs(ph.Eta())>=1.5) continue; //central region
	      if (!params["Central"] && params["Plots"] && fabs(ph.Eta())<1.5) continue; //forward region
	      h["pT_yey"]->Fill(ph.Pt(), weight);	      
	    } else {
	      if (params["bkgSyst"]||params["bkgFit"]) h["M_yey_b"]->Fill((ph+el).M(), weight);
	      if ((ph+el).M()<(params["Mz"]-params["MEL"])) {if ((ph+el).M()<(params["Mz"]-params["BELR"])&&(ph+el).M()>(params["Mz"]-params["BELL"])) {if (!params["bkgSyst"]&&!params["bkgFit"]) h["M_yey_b"]->Fill((ph+el).M(), weight);} continue;}
	      if ((ph+el).M()>(params["Mz"]+params["MER"])) {if ((ph+el).M()>(params["Mz"]+params["BERL"])&&(ph+el).M()<(params["Mz"]+params["BERR"])) {if (!params["bkgSyst"]&&!params["bkgFit"]) h["M_yey_b"]->Fill((ph+el).M(), weight);} continue;}
	      // if (params["Plots"]) h["eta_yey"]->Fill(ph.Eta(), weight);
	      h["M_yey"]->Fill((ph+el).M(), weight);	      
	      // if (params["Central"] && params["Plots"] && fabs(ph.Eta())>=1.5) continue; //central region
	      // if (!params["Central"] && params["Plots"] && fabs(ph.Eta())<1.5) continue; //forward region
	      h["pT_yey"]->Fill(ph.Pt(), weight);	      
	      // if (params["Plots"]) 
	      	h["eta_yey"]->Fill(ph.Eta(), weight);
	    }
	    // cout<<weight<<endl;
	}

    N=ee->GetEntries();
    for (Int_t i=0; i<N; i++){
      // cout<<"ee"<<endl;,
    ee->GetEntry(i);
    // if (fabs(weight)>8) continue;
    // if (fabs(weight-22.5717)<0.0001) continue;
    // if (fabs(weight)>100) continue;
    if (fabs(weight)>100 && !(filename.Contains("410470") || filename.Contains("700") || filename.Contains("5046"))) {std::cout<<"large weight "<<weight<<"\n"; continue;/*weight =1;*/}
	if (!filename.Contains("DYee") && fabs(weight)>100) continue;

        el.SetPtEtaPhiE(e_sublead_pt,e_sublead_eta,e_sublead_phi,e_sublead_E);
        ph.SetPtEtaPhiE(e_lead_pt,e_lead_eta,e_lead_phi,e_lead_E); //charge is checked in main framework
        if (el.Pt()<25) continue;
        if (ph.Pt()<150) continue; 
	    if (!params["Truth"] || !params["FromMC"]) {if (metTST_pt>40) continue;}

        if (params["Truth"]) {
	        if (e_sublead_type!=2 && koef!=1) continue;
	        if (e_sublead_origin!=13 && koef!=1) continue;
	        if (e_lead_type!=2) continue; //for fake rate check
	        if (e_lead_origin!=13) continue; //for fake rate check
        }
        // if (params["Plots"]) h["eta_yee"]->Fill(ph.Eta(), weight);

        if (params["Central"] /*&& !params["Plots"]*/ && fabs(ph.Eta())>=1.5) continue; //central region
        if (!params["Central"] /*&& !params["Plots"]*/ && fabs(ph.Eta())<=1.5) continue; //forward region

        // if (params["Plots"]) h["pT_yee"]->Fill(ph.Pt(), weight);

        if (params["Central"] /*&& !params["Plots"]*/) {
          if (params["HighPt"] && ph.Pt()<250) continue;
          if (!params["HighPt"] && ph.Pt()>250) continue;
        }
        // Nee+=weight;
        if (params["PlotsMass"] || params["Truth"] || params["MassWindow"]){
          h["M_yee"]->Fill((ph+el).M(), weight);

          if (params["Plots"]) h["eta_yee"]->Fill(ph.Eta(), weight);
	      if (params["Central"] && params["Plots"] && fabs(ph.Eta())>=1.5) continue; //central region
	      if (!params["Central"] && params["Plots"] && fabs(ph.Eta())<1.5) continue; //forward region
          h["pT_yee"]->Fill(ph.Pt(), weight);

        } else {
          if (params["bkgSyst"]||params["bkgFit"]) h["M_yee_b"]->Fill((ph+el).M(), weight);
          if ((ph+el).M()<(params["Mz"]-params["MEL"])) {if ((ph+el).M()<(params["Mz"]-params["BELR"])&&(ph+el).M()>(params["Mz"]-params["BELL"])) {if (!params["bkgSyst"]&&!params["bkgFit"]) h["M_yee_b"]->Fill((ph+el).M(), weight);} continue;}
          if ((ph+el).M()>(params["Mz"]+params["MER"])) {if ((ph+el).M()>(params["Mz"]+params["BERL"])&&(ph+el).M()<(params["Mz"]+params["BERR"])) {if (!params["bkgSyst"]&&!params["bkgFit"]) h["M_yee_b"]->Fill((ph+el).M(), weight);} continue;}
          // if (params["Plots"]) h["eta_yee"]->Fill(ph.Eta(), weight);
          h["M_yee"]->Fill((ph+el).M(), weight);
	      // if (params["Central"] && params["Plots"] && fabs(ph.Eta())>=1.5) continue; //central region
	      // if (!params["Central"] && params["Plots"] && fabs(ph.Eta())<1.5) continue; //forward region
          h["pT_yee"]->Fill(ph.Pt(), weight);
          // if (params["Plots"]) 
          	h["eta_yee"]->Fill(ph.Eta(), weight);
        }
        // cout<<weight<<endl;
      
    }
	// std::cout<<"integral "<<h["M_yee"]->Integral()<<"\n";
    h.includeOverflow();
    if (koef!=1){
    	h.Norm(koef/sumw);
	}
	// std::cout<<"integral "<<h["M_yee"]->Integral()<<"\n";

    return h;
    file->Close();

};
