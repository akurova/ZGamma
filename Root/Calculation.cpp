#include "Calculation.h"
// #include "TreeReader.h"
#include "TreeReader.h"

#include <TString.h>
#include "Common.h"
#include <iostream>

Calculation::Calculation(StrV files, TString name, double lumi, Config conf){
	filelist=files;
	// TString histname = name;
	SName = name;
	TreeReader *nominal = 0;
	hists=nominal->getInitialMap(SName, conf);
  	for (auto p: filelist){
      // std::cout<<"processing sample "<<p<<"\n";
      nominal = new TreeReader(p);
      nominal->setParams(lumi,conf);
      hists+=nominal->getHistograms(SName,conf);
      // std::cout<<"file is read. \n";
      // nominal=0;
      // std::cout<<"finished \n";
    }
};

void Calculation::operator+=(const Calculation &c2){

    this->hists+=c2.hists;
};

void Calculation::operator-=(const Calculation &c2){
  this->hists-=c2.hists;

};

void Calculation::operator=(const Calculation &c2){

  this->hists=c2.hists;

}

