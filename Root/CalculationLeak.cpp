#include "CalculationLeak.h"
#include "LeakReader.h"

#include <TString.h>
#include "Common.h"
#include <iostream>

CalculationLeak::CalculationLeak(TString files, TString name, double lumi){
	// TString histname = name;
  TString file = files;
  SName = name;
	std::cout<<name<<std::endl;
	LeakReader *nominal = 0;
	hists=nominal->getInitialMap(SName);
      std::cout<<"processing sample "<<file<<"\n";
      nominal = new LeakReader(file);
      nominal->setParams(lumi);
      hists+=nominal->getHistograms(SName);
      std::cout<<"file is read. \n";
      // nominal=0;
      std::cout<<"finished \n";
};
