#include "CalculationEToGam.h"
#include "EToGamReader.h"

CalculationEToGam::CalculationEToGam(StrV files, TString name, double lumi, Config conf){
	filelist=files;
	SName = name;
	std::cout<<"SName "<<SName<<"\n";
	EToGamReader *nominal = new EToGamReader();
    nominal->setParams(lumi, conf);
	hists=nominal->getInitialMap(SName);
  	for (auto p: filelist){
      std::cout<<"processing sample "<<p<<"\n";
      nominal = new EToGamReader(p);
      nominal->setParams(lumi, conf);
      hists+=nominal->getHistograms(SName, conf);
      // std::cout<<"file is read. \n";
      // nominal=0;
      // std::cout<<"finished \n";
    }
}