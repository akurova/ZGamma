#include "CalculationWenu.h"
#include "WenuReader.h"

CalculationWenu::CalculationWenu(StrV files, TString name, double lumi, Config con){
	filelist=files;
	SName = name;
	// std::cout<<"SName "<<SName<<"\n";
	WenuReader *nominal = 0;
	hists=nominal->getInitialMap(SName);
  	for (auto p: filelist){
      // std::cout<<"processing sample "<<p<<"\n";
      nominal = new WenuReader(p);
      nominal->setParams(lumi,con);
      hists+=nominal->getHistograms(SName);
      // std::cout<<"file is read. \n";
      // nominal=0;
      // std::cout<<"finished \n";
    }
}