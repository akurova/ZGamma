#include "JetToEReader.h"
#include "Config.h"
#include <vector>
#include <map>
#include <TString.h>

#include <TTree.h>
#include <TFile.h>
#include <iostream>
#include <TMath.h>
#include <TH1.h>
#include <TLorentzVector.h>

using namespace std;

JetToEReader::JetToEReader(TString s){
    if (s.Contains("MC")) filename = s+"/user.akurova.MxAOD.root";
    else if (s.Contains("data")) filename=s;
    // cout<<"Opening file "<<filename<<endl;
};

void JetToEReader::setParams(double lumi){
    //TODO avoid double initialization of config (now it's done both here and in main programm)
    Config con("../ZGamma.cfg");
        params["CR"]=con.getBool("CR");
        params["CR2"]=1; //always true for etogam data-driven background
        params["CR3"]=con.getBool("CR3");
        params["veto"]=con.getBool("veto");
        params["angular"]=con.getBool("angular");
        // params["metjet"]=par[5];
        // params["metgam"]=par[6];
        params["nojets"]=con.getBool("nojets");
        params["VBSjets"]=con.getBool("VBSjets");
        params["METsign"]=con.getNum("METSignificance");
        params["METsoft"]=con.getNum("SoftTerm");
        params["DPhiMetGam"]=con.getNum("DPhiMETGam");
        params["metjet2"]=con.getNum("DPhiSubleadJetMET");
        params["metjet"]=con.getNum("DPhiMETjet");
        params["gCent"]=con.getNum("GamCentrality");
        params["DYjj"]=con.getNum("DYjj");
        params["MET"]=con.getNum("MET");
        params["mjj"]=con.getNum("mjj");
        params["lumi"]=lumi;
        params["norm"]=con.getBool("normalize");
        params["Write"]=con.getBool("WriteToTree");

        // for (auto p: params){
        //     std::cout<<p.first<<":"<<p.second<<"\n";
        // }
};

HistMap JetToEReader::getInitialMap(TString name){
    HistMap h;
    double N_jets[6]={-0.5,0.5,1.5,2.5,7.5,10.5};
    double pt[5]={150,250,400,600,1100}; //for e to gam CR
    double ptmiss[5]={100,250,400,600,1100}; //for e to gam CR
    h["B_region_e_eta"]=new TH1D("B_region_e_eta_"+name,"",10,-5,5);
    h["B_region_e_eta"]->Sumw2();
    h["D_region_e_eta"]=new TH1D("D_region_e_eta_"+name,"",10,-5,5);
    h["D_region_e_eta"]->Sumw2();
    h["A_region_e_eta"]=new TH1D("A_region_e_eta_"+name,"",10,-5,5);
    h["A_region_e_eta"]->Sumw2();
    h["C_region_e_eta"]=new TH1D("C_region_e_eta_"+name,"",10,-5,5);
    h["C_region_e_eta"]->Sumw2();
    h["e_iso_et_tight"]=new TH1D("h_e_iso_et_tight_"+name,"",120,-600,600);
    h["e_iso_et_tight"]->GetXaxis()->SetTitle("topoetcone20");
    h["e_iso_et_tight"]->Sumw2();
    h["e_iso_et_antitight"]=new TH1D("h_e_iso_et_loose_"+name,"",120,-600,600);
    h["e_iso_et_antitight"]->GetXaxis()->SetTitle("topoetcone20");
    h["e_iso_et_antitight"]->Sumw2();
    h["e_iso_pt_tight"]=new TH1D("h_e_iso_pt_tight_"+name,"",55,0,110);
    h["e_iso_et_tight"]->GetXaxis()->SetTitle("ptvarcone20_TightTTVA_pt1000");
    h["e_iso_pt_tight"]->Sumw2();
    h["e_iso_pt_antitight"]=new TH1D("h_e_iso_pt_loose_"+name,"",55,0,110);
    h["e_iso_et_antitight"]->GetXaxis()->SetTitle("ptvarcone20_TightTTVA_pt1000");
    h["e_iso_pt_antitight"]->Sumw2();
    h["pT_MET_iso"]=new TH1D("pT_MET_iso_"+name,"FCTight iso;E_{T}^{miss} [GeV];Events/bin",4,ptmiss);
    h["pT_MET_noniso"]=new TH1D("pT_MET_noniso_"+name,"FCLoose iso;E_{T}^{miss} [GeV];Events/bin",4,ptmiss);
    h["pT_MET_iso"]->Sumw2();
    h["pT_MET_noniso"]->Sumw2();
    return h;
};

HistMap JetToEReader::getHistograms(TString name){
    HistMap h;
    TFile *file = new TFile(filename);
    h = getInitialMap(name);
    // Double_t metjet=0.4;
    Double_t metgam=TMath::Pi()*0.5;;    
    // bool ph_tight, loosePrime3, loosePrime4;
    unsigned int n_ph, n_e_looseBL, n_e_medium, n_e_Tight, n_mu, n_jet, n_jet20, n_jet25, n_jet30, n_jet35, n_jet40, n_jet45, N_f_l, N_f_h, N_c_l, N_c_h, run;//, event;
    unsigned long long event;
    double  jet_pt, jet_eta, jet_phi, jet_E, jet2_pt, jet2_eta, jet2_phi, jet2_E, jet3_pt, jet3_eta, jet3_phi, jet3_E, metTST_pt, metTST_phi, metTST_Rho, metTST_VarL, \
    e_lead_pt, e_lead_eta, e_lead_phi, e_lead_E, weight,  jet_sum_pt, fake_weight, metTSTsignif, soft_term_pt, topoetcone20, ptvarcone20;
    int  mc_el_type, mc_el_origin;
    bool jet_third_nTrkqg, jet_third_BDTqg, jet_sublead_nTrkqg, jet_sublead_BDTqg, jet_lead_nTrkqg, jet_lead_BDTqg, PIDTight;
    TLorentzVector met, ph, jet, jet2, el;
    Double_t sum_of_weights, sum_of_weights_bk_xAOD, koef,
    sum_of_weights_bk_DxAOD, sumw, sum1, sum2, sum3;
    Long64_t N=0;
    TTree *norm_tree = (TTree*)file->Get("norm_tree");
    if (norm_tree!=0){
        norm_tree->SetBranchAddress("koef",&koef);
        norm_tree->GetEntry(0);
    }
    if (norm_tree==0) {
        koef = 1;
        checker.initialize();
    }
    else koef*=params["lumi"]; 
    // std::cout<<"fake rates "<<params["fr_forw"]<<" "<<params["fr_cent_gt250"]<<" "<<params["fr_cent_lt250"]<<"\n";
    TTree *tree_MC_sw = (TTree*)file->Get("output_tree_sw");
    // tree_MC_sw->SetBranchAddress("sum_of_weights",&sum_of_weights);
    // tree_MC_sw->SetBranchAddress("sum_of_weights_bk_DxAOD",&sum_of_weights_bk_DxAOD);
    tree_MC_sw->SetBranchAddress("sum_of_weights_bk_xAOD",&sum_of_weights_bk_xAOD);

    Int_t entry = (Int_t)tree_MC_sw->GetEntries();
    sumw=0;sum1=0;sum2=0;sum3=0;

    for (Int_t i=0; i<entry; i++)
    { 
        tree_MC_sw->GetEntry(i);
        // sum1=sum1+sum_of_weights;
        // sum2=sum2+sum_of_weights_bk_DxAOD;
        sum3=sum3+sum_of_weights_bk_xAOD;
    }

    // sumw=sum1*sum3/sum2;
    sumw=sum3;
    if (tree_MC_sw==0 || koef==1) sumw=1;
    
    TTree *tree;
    
    tree=(TTree*)file->Get("output_tree_emet"); //std::cout<<"using emet tree\n";

    
    N = tree->GetEntries();
    
tree->SetBranchAddress("n_ph", &n_ph); //number of photons in event
tree->SetBranchAddress("n_jet", &n_jet); //number of jets in event
// tree->SetBranchAddress("n_jet20", &n_jet20); //number of jets in event
// tree->SetBranchAddress("n_jet25", &n_jet25); //number of jets in event
// tree->SetBranchAddress("n_jet30", &n_jet30); //number of jets in event
// tree->SetBranchAddress("n_jet35", &n_jet35); //number of jets in event
// tree->SetBranchAddress("n_jet40", &n_jet40); //number of jets in event
// tree->SetBranchAddress("n_jet45", &n_jet45); //number of jets in event
tree->SetBranchAddress("n_e_looseBL", &n_e_looseBL);  //number of loose electrons in event
tree->SetBranchAddress("n_mu", &n_mu); //number of muons in event
tree->SetBranchAddress("n_e_Tight", &n_e_Tight);  //number of Tight electrons in event


    tree->SetBranchAddress("el_pt", &e_lead_pt);  //leading leading electron p_x
    tree->SetBranchAddress("el_eta", &e_lead_eta);  //p_y
    tree->SetBranchAddress("el_phi", &e_lead_phi);  //p_z
    tree->SetBranchAddress("el_E", &e_lead_E);    //E
    tree->SetBranchAddress("PIDTight", &PIDTight);    //E
    tree->SetBranchAddress("topoetcone20", &topoetcone20);    //E
    tree->SetBranchAddress("ptvarcone20_TightTTVA_pt1000", &ptvarcone20);    //E


// tree->SetBranchAddress("jet_sum_pt", &jet_sum_pt); 

tree->SetBranchAddress("jet_lead_pt", &jet_pt);  //leading jet p_x
tree->SetBranchAddress("jet_lead_eta", &jet_eta);  //p_y 
tree->SetBranchAddress("jet_lead_phi", &jet_phi);  //p_z
tree->SetBranchAddress("jet_lead_E", &jet_E);    //E

tree->SetBranchAddress("jet_lead_BDTqg", &jet_lead_BDTqg);
tree->SetBranchAddress("jet_lead_nTrkqg", &jet_lead_nTrkqg);

    tree->SetBranchAddress("jet_sublead_pt", &jet2_pt);  //leading jet p_x
    tree->SetBranchAddress("jet_sublead_eta", &jet2_eta);  //p_y 
    tree->SetBranchAddress("jet_sublead_phi", &jet2_phi);  //p_z
    tree->SetBranchAddress("jet_sublead_E", &jet2_E);    //E

tree->SetBranchAddress("jet_sublead_BDTqg", &jet_sublead_BDTqg);
tree->SetBranchAddress("jet_sublead_nTrkqg", &jet_sublead_nTrkqg);


    tree->SetBranchAddress("jet_third_pt", &jet3_pt);  //leading jet p_x
    tree->SetBranchAddress("jet_third_eta", &jet3_eta);  //p_y 
    tree->SetBranchAddress("jet_third_phi", &jet3_phi);  //p_z
    tree->SetBranchAddress("jet_third_E", &jet3_E);    //E

tree->SetBranchAddress("jet_third_BDTqg", &jet_third_BDTqg);
tree->SetBranchAddress("jet_third_nTrkqg", &jet_third_nTrkqg);


tree->SetBranchAddress("metTST_pt", &metTST_pt);  //MET p_x
tree->SetBranchAddress("metTST_phi", &metTST_phi);  //p_y  
// tree->SetBranchAddress("metTST_Rho", &metTST_Rho);  //p_y  
// tree->SetBranchAddress("metTST_VarL", &metTST_VarL);  //p_y  
tree->SetBranchAddress("metTSTsignif", &metTSTsignif);  //MET significance  
tree->SetBranchAddress("soft_term_pt", &soft_term_pt);  //MET soft term 
if (koef==1){
    tree->SetBranchAddress("RunNumber",&run);
    tree->SetBranchAddress("EventNumber",&event);
}
if (koef!=1) tree->SetBranchAddress("weight", &weight);    //weight


weight=1;
// std::cout<<"starting loop\n";
    for (Int_t i=0; i<N; i++)
    {
        tree->GetEntry(i);
        if (fabs(weight)>100) continue;
        jet.SetPtEtaPhiE(jet_pt,jet_eta,jet_phi,jet_E);
        jet2.SetPtEtaPhiE(jet2_pt,jet2_eta,jet2_phi,jet2_E);
        met.SetPtEtaPhiM(metTST_pt,0,metTST_phi,0);
        ph.SetPtEtaPhiE(e_lead_pt,e_lead_eta,e_lead_phi,e_lead_E);

        if (n_ph!=0) continue;
        // if (n_e_looseBL!=1 && params["veto"]) continue;
        if (n_e_looseBL!=1 && params["veto"]) continue;
        if (n_e_looseBL<1 && !params["veto"]) continue;
        if (n_mu!=0 && params["veto"] && !params["CR"]) continue;
        if (n_jet!=0 && params["nojets"]) continue;
        if (params["VBSjets"] && n_jet<2) continue;
        if (params["VBSjets"] && (jet+jet2).M()<params["mjj"]) continue;

        if (met.Pt()<params["MET"]) continue; 
        if (ph.Pt()<150) continue;
        // if (met.Pt()/sqrt(jet_sum_pt+ph.Pt())<params["METsign"] && !params["CR3"] && params["angular"]) continue;
        // if (met.Pt()/sqrt(jet_sum_pt+ph.Pt())>=params["METsign"] && params["CR3"] && params["angular"]) continue;
        if (soft_term_pt>params["METsoft"] && params["angular"]) continue;
        if (metTSTsignif<params["METsign"] && params["angular"]) continue;
        if (fabs(ph.DeltaPhi(met))<params["DPhiMetGam"] && params["angular"]) continue;
        if (n_jet!=0){  
                if (fabs(met.DeltaPhi(jet))<params["metjet"] && params["angular"] && !params["CR3"]) continue; 
                if (fabs(met.DeltaPhi(jet))>params["metjet"] && params["angular"] && params["CR3"]) continue; 
                if (n_jet>1 && fabs(met.DeltaPhi(jet2))<params["metjet2"] && params["angular"]) continue;
                if (n_jet>1 && fabs((ph.Rapidity()-(jet.Rapidity()+jet2.Rapidity())*0.5)/(jet.Rapidity()-jet2.Rapidity()))>params["gCent"] && params["angular"]) continue;
                if (n_jet>1 && fabs(jet.Rapidity()-jet2.Rapidity())<params["DYjj"] && params["angular"]) continue;
        }

        bool iso = false; bool noniso = false;

        if (topoetcone20*0.001<0.06*ph.Pt() && ptvarcone20*0.001<0.06*ph.Pt()) iso = true;
        if (topoetcone20*0.001<0.2*ph.Pt() && ptvarcone20*0.001<0.15*ph.Pt() && (topoetcone20*0.001>0.06*ph.Pt() || ptvarcone20*0.001>0.06*ph.Pt())) noniso = true;

        if (n_e_Tight==1 && PIDTight) {
            h["e_iso_pt_tight"]->Fill(ptvarcone20*0.001,weight);
            h["e_iso_et_tight"]->Fill(topoetcone20*0.001,weight);
            if (iso) {h["A_region_e_eta"]->Fill(ph.Eta(),weight); h["pT_MET_iso"]->Fill(met.Pt(),weight);}
            else if (noniso) {h["B_region_e_eta"]->Fill(ph.Eta(),weight); h["pT_MET_noniso"]->Fill(met.Pt(),weight);}
        } else {
            h["e_iso_et_antitight"]->Fill(topoetcone20*0.001,weight);
            h["e_iso_pt_antitight"]->Fill(ptvarcone20*0.001,weight);
            if (iso) {h["C_region_e_eta"]->Fill(ph.Eta(),weight); /*h["pT_MET_iso"]->Fill(met.Pt(),weight);*/}
            else if (noniso) {h["D_region_e_eta"]->Fill(ph.Eta(),weight); /*h["pT_MET_iso"]->Fill(met.Pt(),weight);*/}
        }

    }
    h.includeOverflow();
    if (koef!=1 && params["norm"]) 
        h.Norm(koef/sumw);
 
    // cout<<"integral="<<h["A_region_e_eta"]->Integral()<<endl;
    return h;
    file->Close();
};