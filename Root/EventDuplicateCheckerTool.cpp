//
// Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
//

// Local include(s).
#include "EventDuplicateCheckerTool.h"

   void EventDuplicateCheckerTool::initialize() {

      // Clear the internal variable.
      m_processedEvents.clear();

   }


   bool EventDuplicateCheckerTool::isDuplicate( unsigned int runNumber, unsigned long long eventNumber ) {

      // Do the check in a relatively optimal way...
      return !( m_processedEvents.insert( RunEventPair_t( runNumber, eventNumber ) ).second );
   }

