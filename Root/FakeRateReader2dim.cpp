#include "FakeRateReader2dim.h"
#include "Config.h"
#include <vector>
#include <map>
#include <TString.h>

#include <TTree.h>
#include <TFile.h>
#include <iostream>
#include <TMath.h>
#include <TH1.h>
#include <TLorentzVector.h>

FakeRateReader2dim::FakeRateReader2dim(){

};

FakeRateReader2dim::FakeRateReader2dim(TString s){
    if (s.Contains("MC")) filename = s+"/user.akurova.MxAOD.root";
    else if (s.Contains("data")||s.Contains("Data")) filename=s;
    cout<<"Opening file "<<filename<<endl;
};

void FakeRateReader2dim::setParams(double lumi, Config conf){
    
    params["Central"]=conf.getBool("Central");
    params["HighPt"]=conf.getBool("HighPt");
    params["Truth"]=conf.getBool("Truth");
    params["FromMC"]=conf.getBool("FromMC");
    params["Plots"]=conf.getBool("Plots");
    params["PlotsMass"]=conf.getBool("PlotsMass");
    params["Mz"]=conf.getNum("Mz");
    params["MEL"]=conf.getNum("MEL");
    params["MER"]=conf.getNum("MER");
    params["BELL"]=conf.getNum("BELL");
    params["BELR"]=conf.getNum("BELR");
    params["BERL"]=conf.getNum("BERL");
    params["BERR"]=conf.getNum("BERR");
    params["lumi"]=lumi;
    params["IsoLoose"]=conf.getBool("IsolationLoose");
    params["Loose'"]=conf.getInt("LoosePrime");
    params["CaloOnly"]=conf.getBool("IsoCaloOnly");    
    params["CheckConversion"]=conf.getBool("CheckConversion");
    params["OldTightID"]=conf.getBool("OldTightID");
    params["Converted"]=conf.getBool("Converted");
    params["MassWindow"]=conf.getBool("MassWindow");
    paramsStr["Region"]=conf.getStr("Region");
    // std::cout<<"Fake rate configuration \n";
    // for (auto m: params){
    // 	std::cout<<m.first<<" "<<m.second<<"\n";
    // }

};

HistMap2dim FakeRateReader2dim::getInitialMap(TString name){
	Config conf("../ZGamma.cfg");
	HistMap2dim h;
	Double_t x[5]={150,200,250,300,1010};
	Double_t x1=1; Double_t x2=301; Int_t n=60;
    h["dRee_vs_Mee"]=new TH2F("dRee_vs_Mee_"+name,";M_{ee};#DeltaR_{ee};Events/bin",n,x1,x2,35,0,3.5);
    h["dRee_vs_Mee"]->Sumw2();

    h["dReg_vs_Meg"]=new TH2F("dReg_vs_Meg_"+name,";M_{e#gamma};#DeltaR_{e#gamma};Events/bin",n,x1,x2,35,0,3.5);
    h["dReg_vs_Meg"]->Sumw2();

	return h;
};

HistMap2dim FakeRateReader2dim::getHistograms(TString name){

	HistMap2dim h;
	TFile *file = new TFile(filename);
	h = getInitialMap(name);
	unsigned int n_ph, n_e_looseBL, n_e_medium, n_mu, n_jet, ph_isem;
	double ph_pt, ph_eta, ph_phi, ph_iso_et, ph_iso_pt, jet_pt, jet_eta, jet_phi, jet_E, metTST_pt, metTST_phi, \
	e_lead_pt, e_lead_eta, e_lead_phi, e_lead_E, e_sublead_pt, e_sublead_eta, e_sublead_phi, e_sublead_E, weight, ph_zp;
	TLorentzVector ph, el, met;
	bool ph_tight, fr_ey, fr_ee;
	
	int mc_ph_type, mc_ph_origin, mc_el_type, mc_el_origin, e_lead_type, e_sublead_type, e_lead_origin, e_sublead_origin, ph_convFlag;
	Double_t sum_of_weights, sum_of_weights_bk_xAOD, koef,
    sum_of_weights_bk_DxAOD, sumw, sum1, sum2, sum3;
    Long64_t N=0;


    TTree *norm_tree = (TTree*)file->Get("norm_tree");
    if (norm_tree!=0){
        norm_tree->SetBranchAddress("koef",&koef);
        norm_tree->GetEntry(0);
    }
    if (norm_tree==0) koef = 1; 
    else koef*=params["lumi"]; 

    TTree *tree_MC_sw = (TTree*)file->Get("output_tree_sw");
    // tree_MC_sw->SetBranchAddress("sum_of_weights",&sum_of_weights);
    // tree_MC_sw->SetBranchAddress("sum_of_weights_bk_DxAOD",&sum_of_weights_bk_DxAOD);
    tree_MC_sw->SetBranchAddress("sum_of_weights_bk_xAOD",&sum_of_weights_bk_xAOD);

    Int_t entry = (Int_t)tree_MC_sw->GetEntries();
    sumw=0;sum1=0;sum2=0;sum3=0;

    for (Int_t i=0; i<entry; i++)
    { 
        tree_MC_sw->GetEntry(i);
        // sum1=sum1+sum_of_weights;
        // sum2=sum2+sum_of_weights_bk_DxAOD;
        sum3=sum3+sum_of_weights_bk_xAOD;
    }

    // sumw=sum1*sum3/sum2;
    sumw=sum3;
    
    if (tree_MC_sw==0 || koef==1) sumw=1;

    TTree *ey = (TTree*)file->Get("output_tree_fr_eg");
	TTree *ee = (TTree*)file->Get("output_tree_fr_ee");


	ey->SetBranchAddress("e_lead_pt", &e_lead_pt);  
	ey->SetBranchAddress("e_lead_eta", &e_lead_eta);  
	ey->SetBranchAddress("e_lead_phi", &e_lead_phi);  
	ey->SetBranchAddress("e_lead_E", &e_lead_E);   

	ey->SetBranchAddress("ph_pt", &ph_pt); 
	ey->SetBranchAddress("ph_eta", &ph_eta);  
	ey->SetBranchAddress("ph_phi", &ph_phi); 

	ey->SetBranchAddress("metTST_pt", &metTST_pt);  
	ee->SetBranchAddress("metTST_pt", &metTST_pt);  

	ee->SetBranchAddress("e_sublead_pt", &e_sublead_pt);  
	ee->SetBranchAddress("e_sublead_eta", &e_sublead_eta);  
	ee->SetBranchAddress("e_sublead_phi", &e_sublead_phi);  
	ee->SetBranchAddress("e_sublead_E", &e_sublead_E);   

	ee->SetBranchAddress("e_lead_pt", &e_lead_pt);  
	ee->SetBranchAddress("e_lead_eta", &e_lead_eta);  
	ee->SetBranchAddress("e_lead_phi", &e_lead_phi);  
	ee->SetBranchAddress("e_lead_E", &e_lead_E);
	ee->SetBranchAddress("n_ph", &n_ph); 

	if (params["OldTightID"]) ey->SetBranchAddress("ph_isem_old",&ph_isem);
	else ey->SetBranchAddress("ph_isem",&ph_isem);
	ey->SetBranchAddress("ph_iso_pt",&ph_iso_pt);
    if (params["IsoLoose"] && paramsStr["Region"]!="A" && paramsStr["Region"]!="B" && paramsStr["Region"]!="C" && paramsStr["Region"]!="D") {
    std::cout<<"using loose isolation\n";
    ey->SetBranchAddress("ph_iso_et20", &ph_iso_et);     //E //FixedCutLoose
    }
    else ey->SetBranchAddress("ph_iso_et40",&ph_iso_et); //FixedCutTight
    ey->SetBranchAddress("ph_convFlag",&ph_convFlag); //FixedCutTight

	if (koef!=1 && params["Truth"] && params["FromMC"]){
		ey->SetBranchAddress("e_lead_type", &mc_el_type);
		ey->SetBranchAddress("e_lead_origin", &mc_el_origin);
		ey->SetBranchAddress("mc_ph_type", &mc_ph_type);
		ey->SetBranchAddress("mc_ph_origin", &mc_ph_origin);
		ee->SetBranchAddress("e_lead_type", &e_lead_type); 
		ee->SetBranchAddress("e_lead_origin", &e_lead_origin); 
		ee->SetBranchAddress("e_sublead_type", &e_sublead_type);   
		ee->SetBranchAddress("e_sublead_origin", &e_sublead_origin);   
		ee->SetBranchAddress("weight", &weight); 
		ey->SetBranchAddress("weight", &weight); 
	}
	if (params["FromMC"]) {		
		ee->SetBranchAddress("weight", &weight); 
		ey->SetBranchAddress("weight", &weight); 
	}

	if (koef==1 && !params["CR2"]) ey->SetBranchAddress("ph_z_point",&ph_zp);

	weight=1;

	N=ey->GetEntries();
	// cout<<"N "<<N<<endl;
	for (Int_t i=0; i<N; i++){
	// cout<<"egam"<<endl;
	ey->GetEntry(i);
	// weight*=koef;
	// if (fabs(weight-22.5717)<0.0001) continue;
	// if (fabs(weight)>100) continue;
	// if (fabs(weight)>8) continue;
	    el.SetPtEtaPhiE(e_lead_pt,e_lead_eta,e_lead_phi,e_lead_E);
	    ph.SetPtEtaPhiM(ph_pt,ph_eta,ph_phi,0.);
        unsigned int loose;
        int lloose=(int)params["Loose'"];
        switch(lloose){
            case 2: loose = 0x27fc01; break;
            case 3: loose = 0x25fc01; break;
            case 4: loose = 0x5fc01; break;
            case 5: loose = 0x1fc01; break;
        }
	    if (fabs(ph_zp)>250) continue;
	    if (el.Pt()<25) continue;
	    if (params["Truth"] && params["FromMC"]) {
	      if (mc_el_type!=2 && koef!=1) continue;
	      if (mc_el_origin!=13 && koef!=1) continue;
	      if (mc_ph_type!=2 && mc_ph_type!=15) continue;//for fake rate check
	      if (mc_ph_origin!=13 && mc_ph_origin!=40) continue;//for fake rate check
	    }
	    if (ph.Pt()<150) continue;
	    if (params["CheckConversion"]){
		    if (ph_convFlag==0 && params["Converted"]) continue;
		    if (ph_convFlag!=0 && !params["Converted"]) continue;
		}
	    if (!params["Truth"] || !params["FromMC"]) {if (metTST_pt>40) continue;}
	    if (paramsStr["Region"]!="A" && paramsStr["Region"]!="B" && paramsStr["Region"]!="C" && paramsStr["Region"]!="D") {
	        bool iso = false; bool noniso = false;
	        if (!params["IsoLoose"]){
	            if (params["CaloOnly"]){
	                // std::cout<<"this is TightCaloOnly\n";
	                if (ph_iso_et>11.45+0.022*ph.Pt() && ph_iso_et<29.45+0.022*ph.Pt()) iso = true;
	                if (ph_iso_et>4.45+0.022*ph.Pt() && ph_iso_et<11.45+0.022*ph.Pt()) noniso = true;
	            }
	            else {
	                // std::cout<<"this is Tight\n";
	                if (ph_iso_et>11.45+0.022*ph.Pt() && ph_iso_et<0.022*ph.Pt()+29.45 && ph_iso_pt/ph.Pt()<0.05) iso = true;
	                if (ph_iso_et>4.45+0.022*ph.Pt() && ph_iso_et<11.45+0.022*ph.Pt() && ph_iso_pt/ph.Pt()<0.05) noniso = true;
	            }
	        }
	        else if (params["IsoLoose"]){
	            // std::cout<<"this is Loose\n";
	            if (ph_iso_et>0.065*ph.Pt()+7 && ph_iso_pt/ph.Pt()<0.05) iso = true;
	            if (ph_iso_et>0.065*ph.Pt()+2 && ph_iso_et<0.065*ph.Pt()+7 && ph_iso_pt/ph.Pt()<0.05) noniso = true;
	        }

	        if (paramsStr["Region"]=="B-E" && (ph_isem!=0 || !noniso)) continue;
	        if (paramsStr["Region"]=="E" && (ph_isem!=0 || !iso)) continue;
	        if (paramsStr["Region"]=="D-F" && ((ph_isem & loose)!=0 || !noniso || ph_isem==0)) continue;
	        if (paramsStr["Region"]=="F" && ((ph_isem & loose)!=0 || !iso || ph_isem==0)) continue;

	    }
	    else if (paramsStr["Region"]=="A"){
	    	// std::cout<<"selection for signal region";
		    if (ph_isem!=0) continue;
			if (ph_iso_et>=2.45+0.022*ph.Pt()) continue;
			if (ph_iso_pt/ph.Pt()>=0.05) continue;
		}
		else if (paramsStr["Region"]=="B"){
		    if (ph_isem!=0) continue;
		    if (ph_iso_et<4.45+0.022*ph.Pt() || ph_iso_et>0.022*ph.Pt()+29.45) continue;
		    if (ph_iso_pt/ph.Pt()>0.05) continue;
		}
		else if (paramsStr["Region"]=="C"){
		    if ((ph_isem & loose)!=0 || ph_isem==0) continue;
		    // if ((ph_isem & 0x5fc01) != 0 || ph_isem==0) continue; //loose'4
		    // if ((ph_isem & 0x27fc01) != 0 || ph_isem==0) continue; //loose'2
		    // if ((ph_isem & 0x25fc01) != 0 || ph_isem==0) continue; //loose'3
		    if (ph_iso_et>2.45+0.022*ph.Pt()) continue;
		    if (ph_iso_pt/ph.Pt()>0.05) continue;
		}    
		else if (paramsStr["Region"]=="D"){
		    if ((ph_isem & loose)!=0 || ph_isem==0) continue; //loose'2
		    // if ((ph_isem & 0x5fc01) != 0 || ph_isem==0) continue; //loose'4
		    // if ((ph_isem & 0x27fc01) != 0 || ph_isem==0) continue; //loose'2
		    // if ((ph_isem & 0x25fc01) != 0 || ph_isem==0) continue; //loose'3
		    if (ph_iso_et<4.45+0.022*ph.Pt() || ph_iso_et>0.022*ph.Pt()+29.45) continue;
		    if (ph_iso_pt/ph.Pt()>0.05) continue;
		}
	    if (params["Central"] && !params["Plots"] && fabs(ph.Eta())>=1.5) continue; //central region
	    if (!params["Central"] && !params["Plots"] && fabs(ph.Eta())<1.5) continue; //forward region

	    // if (params["Plots"]) h["pT_yey"]->Fill(ph.Pt(), weight);

	    if (params["Central"] && !params["Plots"]) {
	      if (params["HighPt"] && ph.Pt()<250) continue;
	      if (!params["HighPt"] && ph.Pt()>250) continue;
	    }
	    // Negam+=weight;     
	    if (params["PlotsMass"] || params["Truth"] || params["MassWindow"]){
	      // eta_yey->Fill(ph.Eta(), weight);
	      // h["pT_yey"]->Fill(ph.Pt(), weight);
          h["dReg_vs_Meg"]->Fill((ph+el).M(), fabs(ph.DeltaR(el)), weight);
	               
	    } 
	    // else {
	    //   if ((ph+el).M()<(params["Mz"]-params["MEL"])) {if ((ph+el).M()<(params["Mz"]-params["BELR"])&&(ph+el).M()>(params["Mz"]-params["BELL"])) {/*eyBKG+=1;*/ h["M_yey_b"]->Fill((ph+el).M(), weight);} continue;}
	    //   if ((ph+el).M()>(params["Mz"]+params["MER"])) {if ((ph+el).M()>(params["Mz"]+params["BERL"])&&(ph+el).M()<(params["Mz"]+params["BERR"])) {/*eyBKG+=1;*/ h["M_yey_b"]->Fill((ph+el).M(), weight);} continue;}
	    //   if (params["Plots"]) h["eta_yey"]->Fill(ph.Eta(), weight);
	    //   h["M_yey"]->Fill((ph+el).M(), weight);	      
	    //   if (params["Central"] && params["Plots"] && fabs(ph.Eta())>=1.5) continue; //central region
	    //   if (!params["Central"] && params["Plots"] && fabs(ph.Eta())<1.5) continue; //forward region
	    //   h["pT_yey"]->Fill(ph.Pt(), weight);	      
	    // }
	    // cout<<weight<<endl;
	}

    N=ee->GetEntries();
	// cout<<"N "<<N<<endl;
    for (Int_t i=0; i<N; i++){
      // cout<<"ee"<<endl;,
    ee->GetEntry(i);
    // if (fabs(weight)>8) continue;
    // if (fabs(weight-22.5717)<0.0001) continue;
    // if (fabs(weight)>100) continue;

        el.SetPtEtaPhiE(e_sublead_pt,e_sublead_eta,e_sublead_phi,e_sublead_E);
        ph.SetPtEtaPhiE(e_lead_pt,e_lead_eta,e_lead_phi,e_lead_E); //charge is checked in main framework
        if (el.Pt()<25) continue;
        if (ph.Pt()<150) continue; 
	    if (!params["Truth"] || !params["FromMC"]) {if (metTST_pt>40) continue;}

        if (params["Truth"]) {
	        if (e_sublead_type!=2 && koef!=1) continue;
	        if (e_sublead_origin!=13 && koef!=1) continue;
	        if (e_lead_type!=2) continue; //for fake rate check
	        if (e_lead_origin!=13) continue; //for fake rate check
        }
        // if (params["Plots"]) h["eta_yee"]->Fill(ph.Eta(), weight);

        if (params["Central"] && !params["Plots"] && fabs(ph.Eta())>=1.5) continue; //central region
        if (!params["Central"] && !params["Plots"] && (fabs(ph.Eta())<=1.5 || fabs(ph.Eta())>=2.37)) continue; //forward region

        // if (params["Plots"]) h["pT_yee"]->Fill(ph.Pt(), weight);

        if (params["Central"] && !params["Plots"]) {
          if (params["HighPt"] && ph.Pt()<250) continue;
          if (!params["HighPt"] && ph.Pt()>250) continue;
        }
        // Nee+=weight;
        if (params["PlotsMass"] || params["Truth"] || params["MassWindow"]){
          // eta_yee->Fill(ph.Eta(), weight);
          // h["pT_yee"]->Fill(ph.Pt(), weight);
        	// cout<<fabs(ph.DeltaR(el))<<" "<<(ph+el).M()<<"\n";
          h["dRee_vs_Mee"]->Fill((ph+el).M(), fabs(ph.DeltaR(el)), weight);
          // h["M_yee"]->Fill((ph+el).M());
        } 
       // else {
       //    if ((ph+el).M()<(params["Mz"]-params["MEL"])) {if ((ph+el).M()<(params["Mz"]-params["BELR"])&&(ph+el).M()>(params["Mz"]-params["BELL"])) {/*eeBKG+=1;*/ h["M_yee_b"]->Fill((ph+el).M(), weight);} continue;}
       //    if ((ph+el).M()>(params["Mz"]+params["MER"])) {if ((ph+el).M()>(params["Mz"]+params["BERL"])&&(ph+el).M()<(params["Mz"]+params["BERR"])) {/*eeBKG+=1;*/ h["M_yee_b"]->Fill((ph+el).M(), weight);} continue;}
       //    if (params["Plots"]) h["eta_yee"]->Fill(ph.Eta(), weight);
       //    h["M_yee"]->Fill((ph+el).M(), weight);
	      // if (params["Central"] && params["Plots"] && fabs(ph.Eta())>=1.5) continue; //central region
	      // if (!params["Central"] && params["Plots"] && (fabs(ph.Eta())<1.5 || fabs(ph.Eta())>2.37)) continue; //forward region
       //    h["pT_yee"]->Fill(ph.Pt(), weight);
       //  }
        // cout<<weight<<endl;
      
    }
	// std::cout<<"integral "<<h["M_yee"]->Integral()<<"\n";

    if (koef!=1){
    	h.Norm(koef/sumw);
	}
	std::cout<<"integral "<<h["dRee_vs_Mee"]->Integral()<<"\n";

    return h;
    file->Close();

};
