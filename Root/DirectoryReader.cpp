#include "DirectoryReader.h"
#include <iostream>

DirectoryReader::DirectoryReader(){};

DirectoryReader::DirectoryReader(TString dirName, StrV cath){
    name = dirName;
    cathegory = cath;
    std::cout<<"Opening directory "<<name<<std::endl;
};

void DirectoryReader::SetDirectory(TString dir){
	name = dir;
};

std::map<TString,StrV> DirectoryReader::loop(TString name, StrV cath) //reads folder names in folder "name"
{
	if (!fileExist(name)) fatal("Directory "+name+" doesn't exist");
	const char* inDir = name.Data();

	char* dir = gSystem->ExpandPathName(inDir);
	void* dirp = gSystem->OpenDirectory(dir);

	const char* entry;
	std::map<TString,StrV> filename;
	Int_t n = 0;
	TString str;

	while((entry = (char*)gSystem->GetDirEntry(dirp))) {
		str = entry;
		if(str.EndsWith("MxAOD.root")){
			if (cath.size()==15) {
				if (str.Contains(cath[0]) &&!str.Contains("2jEWK") && str.Contains("Sh_2211") && cath[0]!="") filename[cath[0]].push_back(gSystem->ConcatFileName(dir, entry));
				if (str.Contains(cath[1]) && cath[1]!="") filename[cath[1]].push_back(gSystem->ConcatFileName(dir, entry));
				if (str.Contains(cath[2]) &&!str.Contains("2jEWK") && cath[2]!="") filename[cath[2]].push_back(gSystem->ConcatFileName(dir, entry));
				if (str.Contains(cath[3]) && cath[3]!="") filename[cath[3]].push_back(gSystem->ConcatFileName(dir, entry));
				if (str.Contains(cath[4]) &&!str.Contains(cath[0]) && str.Contains("Sh_2211") &&!str.Contains("2jEWK") && cath[4]!="") filename[cath[4]].push_back(gSystem->ConcatFileName(dir, entry));
				if (str.Contains(cath[5]) &&!str.Contains(cath[4]) &&!str.Contains("2jEWK") && !str.Contains("ttgamma") && !str.Contains("Zgamma2jINT") && cath[5]!="") filename[cath[5]].push_back(gSystem->ConcatFileName(dir, entry));
				if (str.Contains(cath[6]) && str.Contains("MGPy8EG") && cath[6]!="") filename[cath[6]].push_back(gSystem->ConcatFileName(dir, entry));
				if (str.Contains(cath[7]) &&!str.Contains(cath[6]) && str.Contains("MGPy8EG") && cath[7]!="") filename[cath[7]].push_back(gSystem->ConcatFileName(dir, entry));
				if (str.Contains(cath[8]) && cath[8]!="") filename[cath[8]].push_back(gSystem->ConcatFileName(dir, entry));
				if (str.Contains(cath[9]) && cath[9]!="") filename[cath[9]].push_back(gSystem->ConcatFileName(dir, entry));
				if (str.Contains(cath[10]) && cath[10]!="") filename[cath[10]].push_back(gSystem->ConcatFileName(dir, entry));
				if (str.Contains(cath[11]) && cath[11]!="" &&!str.Contains("fast") && str.Contains("410389")) filename[cath[11]].push_back(gSystem->ConcatFileName(dir, entry));
				if (str.Contains(cath[12]) && cath[12]!="") filename[cath[12]].push_back(gSystem->ConcatFileName(dir, entry));
				if (str.Contains(cath[13]) && cath[13]!="") filename[cath[13]].push_back(gSystem->ConcatFileName(dir, entry));
				if (str.Contains(cath[14]) && cath[14]!="" && !str.Contains("nunugamma")) filename[cath[14]].push_back(gSystem->ConcatFileName(dir, entry));
			} else if (cath.size()>1 && cath.size()!=15){
				if (str.Contains(cath[0]) && cath[0]!="") filename[cath[0]].push_back(gSystem->ConcatFileName(dir, entry));
				for (unsigned int i=1; i<cath.size(); i++){
					if (str.Contains(cath[i]) && cath[i]!=""){
					// std::cout<<"i"<<i<<std::endl;
						bool yes=false; unsigned int j=i;
						while (j>0){
							j--;
							// std::cout<<"j"<<j<<std::endl;
							if (str.Contains(cath[j])) yes=true;
							// std::cout<<yes<<std::endl;
						}
						if (!yes) filename[cath[i]].push_back(gSystem->ConcatFileName(dir, entry));
					} 
				}
			}
			// else if (cath.size()==4){
			// 	if (str.Contains(cath[0]) && cath[0]!="") filename[cath[0]].push_back(gSystem->ConcatFileName(dir, entry));
			// 	if (str.Contains(cath[1]) && cath[1]!="" && !str.Contains(cath[0])) filename[cath[1]].push_back(gSystem->ConcatFileName(dir, entry));
			// 	if (str.Contains(cath[2]) && cath[2]!="" && !str.Contains(cath[0]) && !str.Contains(cath[1])) filename[cath[2]].push_back(gSystem->ConcatFileName(dir, entry)); 
			// 	if (str.Contains(cath[3]) && cath[3]!="" && !str.Contains(cath[0]) && !str.Contains(cath[1]) && !str.Contains(cath[2])) filename[cath[3]].push_back(gSystem->ConcatFileName(dir, entry)); 
			// } else if (cath.size()==3){
			// 	if (str.Contains(cath[0]) && cath[0]!="") filename[cath[0]].push_back(gSystem->ConcatFileName(dir, entry));
			// 	if (str.Contains(cath[1]) && cath[1]!="" && !str.Contains(cath[0])) filename[cath[1]].push_back(gSystem->ConcatFileName(dir, entry));
			// 	if (str.Contains(cath[2]) && cath[2]!="" && !str.Contains(cath[0]) && !str.Contains(cath[1])) filename[cath[2]].push_back(gSystem->ConcatFileName(dir, entry));
			// } else if (cath.size()==2){
			// 	if (str.Contains(cath[0]) && cath[0]!="") filename[cath[0]].push_back(gSystem->ConcatFileName(dir, entry));
			// 	if (str.Contains(cath[1]) && cath[1]!="" && !str.Contains(cath[0])) filename[cath[1]].push_back(gSystem->ConcatFileName(dir, entry));
			// } 
			else if (cath.size()==1){
				if (str.Contains(cath[0]) && cath[0]!="") filename[cath[0]].push_back(gSystem->ConcatFileName(dir, entry));
			}
		}
	}
	return filename;
};

std::map<TString,StrV> DirectoryReader::GetFilesList(){
	std::map<TString,StrV> list = loop(name, cathegory);
	return list;
};