#include "TreeReader.h"
// #include "Config.h"
#include <vector>
#include <map>
#include <TString.h>

#include <TTree.h>
#include <TFile.h>
#include <iostream>
#include <TMath.h>
#include <TH1.h>
#include <TLorentzVector.h>

using namespace std;

TreeReader::TreeReader(){

};

TreeReader::TreeReader(TString s){
    if (s.Contains("MC")) filename = s+"/user.akurova.MxAOD.root";
    else if (s.Contains("data")) filename=s;
    cout<<"Opening file "<<filename<<endl;
};

void TreeReader::setParams(double lumi, Config con){
    //TODO avoid double initialization of config (now it's done both here and in main programm)
    // Config conf("../ZGamma.cfg");
        params["CR"]=con.getBool("CR");
        params["CR2"]=con.getBool("CR2");
        params["CR3"]=con.getBool("CR3");
        params["veto"]=con.getBool("veto");
        params["tauveto"]=con.getBool("tauveto");
        params["angular"]=con.getBool("angular");
        // params["metjet"]=par[5];
        // params["metgam"]=par[6];
        params["nojets"]=con.getBool("nojets");
        params["VBSjets"]=con.getBool("VBSjets");
        params["METsign"]=con.getNum("METSignificance");
        params["METsoft"]=con.getNum("SoftTerm");
        params["DPhiMetGam"]=con.getNum("DPhiMETGam");
        params["metjet2"]=con.getNum("DPhiSubleadJetMET");
        params["metjet"]=con.getNum("DPhiMETjet");
        params["gCent"]=con.getNum("GamCentrality");
        params["DYjj"]=con.getNum("DYjj");
        params["MET"]=con.getNum("MET");
        params["mjj"]=con.getNum("mjj");
        params["lumi"]=lumi;
        params["norm"]=con.getBool("normalize");
        params["IsoLoose"]=con.getBool("IsolationLoose");
        params["CaloOnly"]=con.getBool("IsoCaloOnly");    
        // for (auto p: params){
        //     std::cout<<p.first<<":"<<p.second<<"\n";
        // }
};

HistMap TreeReader::getInitialMap(TString name, Config con){
    HistMap h;
    // Config conf("../ZGamma.cfg");
    double N_jets[5]={-0.5,0.5,1.5,2.5,7.5};
    // double N_jets[6]={-0.5,0.5,1.5,2.5,3.5,7.5};
    // double pt[6]={150,200,250,350,450,600}; //for e to gam CR
    // double pt[6]={150,200,250,350,600,1100}; //for e to gam CR
    // double pt[6]={150,220,300,420,600,1100}; //for e to gam CR
    // double pt[11]={150,180,210,240,270,300,340,380,430,510,600}; //for e to gam CR
    double pt[7]={150,200,250,350,450,600,1100}; //for e to gam CR
    double pt_j[8]={50,100,150,250,350,450,600,1100}; //for e to gam CR
    // double pt[5]={150,250,400,600,1100}; //for e to gam CR
    double ptZg[5]={0,100,250,550,1100}; //for e to gam CR
    double mtZg[6]={250,500,750,1000,1500,2500}; //for e to gam CR
    // double ptZg[5]={0,150,400,600,1100}; //for e to gam CR
    double ptmiss[7]={130,200,250,350,450,600,1100}; //for e to gam CR
    // double ptmiss[5]={130,300,500,800,1100}; //for e to gam CR
    double mjj[7]={0, 150, 300, 500, 850, 1600, 3500}; 
    // double ptmiss[5]={120,250,400,600,1100}; //for e to gam CR
    double etah[5]={-2.4,-1,0,1,2.4};
    h["n_el"]=new TH1D("n_el_"+name,";N_{e};Events/bin",6,-0.5,5.5);
    h["n_el"]->Sumw2();
    h["n_lep"]=new TH1D("n_lep_"+name,";N_{leptons};Events/bin",6,-0.5,5.5);
    h["n_lep"]->Sumw2();
    // if (conf.getBool("CR2")) {
    // if (conf.getNum("mjj")>0) h["e_eta"]=new TH1D("e_eta_"+name,";#eta_{e-probe};Events/bin",4,etah);
    // else 
    h["e_eta"]=new TH1D("e_eta_"+name,";#eta_{e-probe};Events/bin",6,-3,3);
    h["e_eta"]->Sumw2();
    h["y_eta"]=new TH1D("gamma_eta_"+name,";#eta_{#gamma};Events/bin",10,-2.5,2.5);
    h["y_eta"]->Sumw2();
    // h["e_eta"]=new TH1D("e_eta_"+name,";#eta_{e-probe};Events/bin",10,-3,3);
    // }
    h["n_mu"]=new TH1D("n_mu_"+name,";N_{#mu};Events/bin",6,-0.5,5.5);
    h["n_mu"]->Sumw2();
    h["n_ph"]=new TH1D("n_ph_"+name,";N_{#gamma};Events/bin",6,-0.5,5.5);
    h["n_ph"]->Sumw2();
    h["pT_lead_y"]=new TH1D("pT_lead_y_"+name,";E_{T}^{#gamma} [GeV];Events/bin",6,pt);//33 ,0,1650);//,237,150,4890);
    h["pT_lead_y"]->Sumw2();
    h["pT_j1"]=new TH1D("pT_j1_"+name,";p_{T}^{j_{1}} [GeV];Events/bin",7,pt_j);//33 ,0,1650);//,237,150,4890);
    h["pT_j1"]->Sumw2();
    h["pT_j2"]=new TH1D("pT_j2_"+name,";p_{T}^{j_{2}} [GeV];Events/bin",7,pt_j);//33 ,0,1650);//,237,150,4890);
    h["pT_j2"]->Sumw2();
    h["pT_Zy"]=new TH1D("pT_Zy_"+name,";p_{T}^{p^{miss}_{T}#gamma} [GeV];Events/bin",4,ptZg);//33 ,0,1650);//,237,150,4890);
    h["pT_Zy"]->Sumw2();  
    h["mT_Zy"]=new TH1D("mT_Zy_"+name,";m_{T}^{p^{miss}_{T}#gamma} [GeV];Events/bin",5,mtZg);//33 ,0,1650);//,237,150,4890);
    h["mT_Zy"]->Sumw2();     
    h["n_jet"]=new TH1D("n_jet_"+name,";N_{jets};Events/bin",4,N_jets);
    h["n_jet"]->Sumw2();
    h["pT_MET"]=new TH1D("pT_MET_"+name,";E_{T}^{miss} [GeV];Events/bin",6,ptmiss);
    h["pT_MET"]->Sumw2();
    h["gCent"]=new TH1D("gCent_"+name,";#gamma-centrality;Events/bin",12,0,6);
    h["gCent"]->Sumw2();
    h["dPhi_jet_MET"]=new TH1D("dPhi_jet_MET_"+name,";#Delta#phi(jet,p^{miss}_{T}) [rad];Events/bin",7,0.4,3.2);
    h["dPhi_jet_MET"]->Sumw2();
    h["dPhi_y_MET"]=new TH1D("dPhi_y_MET_"+name,";#Delta#phi(#gamma,p^{miss}_{T}) [rad];Events/bin",13,0.6,3.2);
    h["dPhi_y_MET"]->Sumw2();
    h["dPhi_y_jet"]=new TH1D("dPhi_y_jet_"+name,";#Delta#phi(#gamma,jet) [rad];Events/bin",13,0.6,3.2);
    h["dPhi_y_jet"]->Sumw2();
    h["dPhi_jj"]=new TH1D("dPhi_jj_"+name,";|#Delta#phi(j,j)| [rad];Events/bin",13,0,3.25);
    h["dPhi_jj"]->Sumw2();
    h["dPhi_Zj"]=new TH1D("dPhi_Zj_"+name,";|#Delta#phi(p^{miss}_{T},j)| [rad];Events/bin",12,0.25,3.25);
    h["dPhi_Zj"]->Sumw2();
    h["dR_jy"]=new TH1D("dR_jy_"+name,";#Delta R(j,#gamma) [rad];Events/bin",6,0,6);
    h["dR_jy"]->Sumw2();
    h["mjj"]=new TH1D("mjj_"+name,";m_{jj};Events/bin",6,mjj);
    // h["mjj"]=new TH1D("mjj_"+name,";m_{jj};Events/bin",100,0,3500);
    // h["mjj"]=new TH1D("mjj_"+name,";m_{jj};Events/bin",7,0,1400);
    h["mjj"]->Sumw2();
    if (con.getBool("CR2")){
        h["pT_lead_y"]->GetXaxis()->SetTitle("E_{T}^{e-probe} [GeV]");
        h["dPhi_y_MET"]->GetXaxis()->SetTitle("#Delta#phi(e-probe,p^{miss}_{T}) [rad]");
        h["dPhi_y_jet"]->GetXaxis()->SetTitle("#Delta#phi(e-probe,jet) [rad]");
        h["y_eta"]->GetXaxis()->SetTitle("#eta_{e-probe}");
        h["pT_Zy"]->GetXaxis()->SetTitle("p_{T}^{Ze-probe}");
    }
    return h;
};

HistMap TreeReader::getHistograms(TString name, Config con){
    HistMap h;
    // Config conf("../ZGamma.cfg");
    TFile *file = new TFile(filename);
    h = getInitialMap(name,con);
    // Double_t metjet=0.4;
    Double_t metgam=TMath::Pi()*0.5;;    
    // bool ph_tight, loosePrime3, loosePrime4;
    unsigned int n_ph, n_e_looseBL, n_e_Tight, n_e_medium, n_mu, n_jet, N_f_l, N_f_h, N_c_l, N_c_h, run, n_top_lep;
    unsigned long long event;
    double ph_pt, ph_eta, ph_phi, ph_iso_et, ph_iso_pt, jet_pt, jet_eta, jet_phi, jet_E, jet2_pt, jet2_eta, jet2_phi, jet2_E, jet3_pt, jet3_eta, jet3_phi, jet3_E, metTST_pt, metTST_phi, metTSTsignif, \
    e_lead_pt, e_lead_eta, e_lead_phi, e_lead_E, weight, ph_zp, jet_sum_pt, soft_term_pt, topoetcone20, ptvarcone20;
    unsigned int ph_isem;
    int mc_ph_type, mc_ph_origin, mc_el_type, mc_el_origin, n_tau_loose;
    bool PIDTight;
    TLorentzVector met, ph, jet, jet2, jet3, el;
    Double_t sum_of_weights, sum_of_weights_bk_xAOD, koef, sum_of_weights_bk_DxAOD, sumw, sum1, sum2, sum3;
    Long64_t N=0;
    TTree *norm_tree = (TTree*)file->Get("norm_tree");
    if (norm_tree!=0){
        norm_tree->SetBranchAddress("koef",&koef);
        norm_tree->GetEntry(0);
    }
    if (filename.Contains("545839")) koef=0.27410;
    cout<<"koef="<<koef<<endl;
    if (norm_tree==0) {
        koef = 1; 
        checker.initialize();
    }
    else koef*=params["lumi"]; 
    // if (filename.Contains("jets_JZ8")) {
    //     koef*=1000; 
    //     // std::cout<<"\n\n\n\ncorrecting jets_JZ8 cross section\n\n\n\n";
    // }

    TTree *tree_MC_sw = (TTree*)file->Get("output_tree_sw");
    // tree_MC_sw->SetBranchAddress("sum_of_weights",&sum_of_weights);
    // tree_MC_sw->SetBranchAddress("sum_of_weights_bk_DxAOD",&sum_of_weights_bk_DxAOD);
    tree_MC_sw->SetBranchAddress("sum_of_weights_bk_xAOD",&sum_of_weights_bk_xAOD);

    Int_t entry = (Int_t)tree_MC_sw->GetEntries();
    sumw=0;sum1=0;sum2=0;sum3=0;

    for (Int_t i=0; i<entry; i++)
    { 
        tree_MC_sw->GetEntry(i);
        // sum1=sum1+sum_of_weights;
        // sum2=sum2+sum_of_weights_bk_DxAOD;
        sum3=sum3+sum_of_weights_bk_xAOD;
    }
    // std::cout<<"sum1 "<<sum1<<endl;
    // std::cout<<"sum2 "<<sum2<<endl;
    // std::cout<<"sum3 "<<sum3<<endl;
    // sumw=sum1*sum3/sum2;
    sumw=sum3;
    if (tree_MC_sw==0 || koef==1) sumw=1;
    
    TTree *tree;
    if (!params["CR2"]) {tree=(TTree*)file->Get("output_tree");}
    if (params["CR2"]) {tree=(TTree*)file->Get("output_tree_emet");}

    
    N = tree->GetEntries();
    
tree->SetBranchAddress("n_ph", &n_ph); //number of photons in event
tree->SetBranchAddress("n_jet", &n_jet); //number of jets in event
// tree->SetBranchAddress("n_e_looseBL", &n_e_looseBL);  //number of loose electrons in event
tree->SetBranchAddress("n_tau_loose", &n_tau_loose);  //number of loose tau in event
// tree->SetBranchAddress("n_mu", &n_mu); //number of muons in event
tree->SetBranchAddress(con.getStr("N_Electrons").Data(), &n_e_looseBL);  //number of loose electrons in event
tree->SetBranchAddress(con.getStr("N_Muons").Data(), &n_mu); //number of muons in event

if (params["CR2"]){
    tree->SetBranchAddress("el_pt", &e_lead_pt);  //leading leading electron p_x
    tree->SetBranchAddress("el_eta", &e_lead_eta);  //p_y
    tree->SetBranchAddress("el_phi", &e_lead_phi);  //p_z
    tree->SetBranchAddress("el_E", &e_lead_E);    //E
    tree->SetBranchAddress("n_e_Tight", &n_e_Tight);  //number of Tight electrons in event
    if (!filename.Contains("pTV_mjj_merging") && !filename.Contains("old_merging") && !filename.Contains("12-08-2020") && !filename.Contains("qg_tagger")) tree->SetBranchAddress("PIDTight", &PIDTight);    //E
    if (!filename.Contains("pTV_mjj_merging") && !filename.Contains("old_merging") && !filename.Contains("12-08-2020") && !filename.Contains("qg_tagger")) tree->SetBranchAddress("topoetcone20", &topoetcone20);    //E
    if (!filename.Contains("pTV_mjj_merging") && !filename.Contains("old_merging") && !filename.Contains("12-08-2020") && !filename.Contains("qg_tagger")) tree->SetBranchAddress("ptvarcone20_TightTTVA_pt1000", &ptvarcone20);    //E
}

// tree->SetBranchAddress("jet_sum_pt", &jet_sum_pt); 
if (!params["CR2"]){
    // tree->SetBranchAddress("ph_iso_et20", &ph_iso_et);    //E //FixedCutLoose
    if (params["IsoLoose"]) {
    tree->SetBranchAddress("ph_iso_et20", &ph_iso_et);     //E //FixedCutLoose
    }
    else tree->SetBranchAddress("ph_iso_et40",&ph_iso_et); //FixedCutTight
    // tree->SetBranchAddress("ph_iso_et40", &ph_iso_et);    //E //FixedCutTight
    tree->SetBranchAddress("ph_iso_pt", &ph_iso_pt);    //E
    tree->SetBranchAddress("ph_isem", &ph_isem);  //leading ph tighness
    tree->SetBranchAddress("ph_pt", &ph_pt);  //leading ph p_x
    tree->SetBranchAddress("ph_eta", &ph_eta);  //p_y
    tree->SetBranchAddress("ph_phi", &ph_phi);  //p_z
    tree->SetBranchAddress("mc_ph_type", &mc_ph_type);  //p_z
    tree->SetBranchAddress("mc_ph_origin", &mc_ph_origin);  //p_z
}
tree->SetBranchAddress("jet_lead_pt", &jet_pt);  //leading jet p_x
tree->SetBranchAddress("jet_lead_eta", &jet_eta);  //p_y 
tree->SetBranchAddress("jet_lead_phi", &jet_phi);  //p_z
tree->SetBranchAddress("jet_lead_E", &jet_E);    //E

    tree->SetBranchAddress("jet_sublead_pt", &jet2_pt);  //leading jet p_x
    tree->SetBranchAddress("jet_sublead_eta", &jet2_eta);  //p_y 
    tree->SetBranchAddress("jet_sublead_phi", &jet2_phi);  //p_z
    tree->SetBranchAddress("jet_sublead_E", &jet2_E);    //E

tree->SetBranchAddress("metTST_pt", &metTST_pt);  //MET p_x
tree->SetBranchAddress("metTST_phi", &metTST_phi);  //p_y  
tree->SetBranchAddress("metTSTsignif", &metTSTsignif);  //MET significance  
tree->SetBranchAddress("soft_term_pt", &soft_term_pt);  //MET soft term 
// if (name.Contains("ttgamma")) tree->SetBranchAddress("n_top_lep", &n_top_lep);
// if (koef==1){
    tree->SetBranchAddress("RunNumber",&run);
    tree->SetBranchAddress("EventNumber",&event);
// }
if (koef!=1) tree->SetBranchAddress("weight", &weight);    //weight

if (!params["CR2"])   tree->SetBranchAddress("ph_z_point",&ph_zp);
weight=1;
int counter=0;
    for (Int_t i=0; i<N; i++)
    {
        tree->GetEntry(i);
        // if (koef==1 && (run==280673||run==332303||run==333380||run==334413||run==349263)) {
        //     // std::cout<<"run found\n";
        //     if (checker.isDuplicate(run,event)) {std::cout<<"duplicated: "<<run<<" "<<event<<"\n"; counter++;}
        // }
        // if (event==789228) continue;
        jet.SetPtEtaPhiE(jet_pt,jet_eta,jet_phi,jet_E);
        jet2.SetPtEtaPhiE(jet2_pt,jet2_eta,jet2_phi,jet2_E);
        met.SetPtEtaPhiM(metTST_pt,0,metTST_phi,0);
        if (params["CR2"]) ph.SetPtEtaPhiE(e_lead_pt,e_lead_eta,e_lead_phi,e_lead_E);
        else ph.SetPtEtaPhiM(ph_pt,ph_eta,ph_phi,0);
            // h["n_el"]->Fill(n_e_looseBL,weight);
        // if (n_top_lep!=1 && name.Contains("tta_1lep")) continue;
        // if (n_top_lep<2 && name.Contains("tta_gt1lep")) continue;
            // h["n_lep"]->Fill(n_mu+n_e_looseBL,weight);

        
        if (n_ph!=1 && !params["CR2"]) continue;
        if (n_ph!=0 && params["CR2"]) continue;
        if (n_e_looseBL!=1 && params["CR2"] && !params["CR"] && params["veto"]) continue;
        if (n_e_Tight!=1 && params["CR2"] && !params["CR"] && params["veto"]) continue;
        if (!PIDTight && params["CR2"] && (!filename.Contains("pTV_mjj_merging") && !filename.Contains("old_merging") && !filename.Contains("12-08-2020") && !filename.Contains("qg_tagger"))) continue;
        // if ((topoetcone20*0.001>0.06*ph.Pt() || ptvarcone20*0.001>0.06*ph.Pt()) && params["CR2"] && (!filename.Contains("pTV_mjj_merging") && !filename.Contains("old_merging"))) continue;
        if (n_e_looseBL<1 && params["CR2"] && !params["veto"]) continue;
        if (n_mu!=0 && params["CR2"] && params["veto"]) continue;
        if (n_jet!=0 && params["nojets"]) continue;
        if (params["VBSjets"] && n_jet<2) continue;
        if (params["VBSjets"] && (jet+jet2).M()<params["mjj"]) continue;

        if (met.Pt()<params["MET"]) continue; 
        if (ph.Pt()<150) continue;
        if (!params["CR2"] && fabs(ph_zp)>250) continue;
        // if (met.Pt()/sqrt(jet_sum_pt+ph.Pt())<params["METsign"] && !params["CR3"] && params["angular"]) continue;
        // if (met.Pt()/sqrt(jet_sum_pt+ph.Pt())>=params["METsign"] && params["CR3"] && params["angular"]) continue;
        if (soft_term_pt>params["METsoft"] && params["angular"]) continue;
        if (metTSTsignif<params["METsign"] && params["angular"] && !params["CR3"]) continue;
        if (metTSTsignif>params["METsign"] && params["angular"] && params["CR3"]) continue;
        if (ph_isem!=0 && !params["CR2"]) continue; //comment for Z jet control regions
        float ratio=0; float step=0;
        if (params["IsoLoose"]) {ratio=0.065;}
        else {ratio=0.022; step=2.45;}
        if (ph_iso_et>=step+ratio*ph.Pt() && !params["CR2"]) continue;
        if (ph_iso_pt/ph.Pt()>=0.05 && !params["CaloOnly"] && !params["CR2"]) continue;
        // if ((ph_iso_et>2.45+0.022*ph.Pt()) && !params["CR2"]) continue; //comment for Z jet control regions
        // if ((ph_iso_et>2.45+0.022*ph.Pt() || ph_iso_pt/ph.Pt()>0.05) && !params["CR2"]) continue; //comment for Z jet control regions
        // if ((n_mu+n_e_looseBL!=1) && params["CR"] && params["veto"] && !params["CR2"]) continue; //Wgam CR
        if ((n_mu+n_e_looseBL<1) && params["CR"] && !params["CR2"]) continue; //Wgam CR
        // if ((n_mu+n_e_looseBL!=2) && params["CR"] && params["veto"] && params["CR2"]) continue; //Wgam CR
        if ((n_mu+n_e_looseBL<2) && params["CR"] && params["CR2"]) continue; //Wgam CR
        if ((n_e_looseBL!=0 || n_mu!=0) && params["veto"] && !params["CR"] && !params["CR2"]) continue; //lepton veto
        if (n_tau_loose!=0 && params["tauveto"]) continue;//tau veto
        if (fabs(ph.DeltaPhi(met))<params["DPhiMetGam"] && params["angular"]) continue;

        if (n_jet!=0){	
            if (fabs(met.DeltaPhi(jet))<params["metjet"] && params["angular"]) continue; 
            if (n_jet>1 && fabs(met.DeltaPhi(jet2))<params["metjet2"] && params["angular"]) continue;
            if (n_jet>1 && fabs((ph.Rapidity()-(jet.Rapidity()+jet2.Rapidity())*0.5)/(jet.Rapidity()-jet2.Rapidity()))>params["gCent"] && params["angular"]) continue;
            if (n_jet>1 && fabs(jet.Rapidity()-jet2.Rapidity())<params["DYjj"] && params["angular"]) continue;
            // if (fabs(jet.Eta())<2.5 || fabs(jet2.Eta())<2.5) continue;

        }
        if (fabs(weight)>100 && !(filename.Contains("410470") || filename.Contains("700") || filename.Contains("5046"))) {std::cout<<"large weight "<<weight<<"\n"; continue;/*weight =1;*/}
        
            counter++;
            h["dPhi_y_MET"]->Fill(ph.DeltaPhi(met),weight);
            if (n_jet!=0){  
                h["dPhi_jet_MET"]->Fill(fabs(met.DeltaPhi(jet)),weight);
                h["dPhi_y_jet"]->Fill(fabs(ph.DeltaPhi(jet)),weight);
                h["dPhi_Zj"]->Fill(fabs(met.DeltaPhi(jet)),weight);
                h["pT_j1"]->Fill(jet.Pt(),weight);
                h["dR_jy"]->Fill(ph.DeltaR(jet),weight);
            }
            h["pT_Zy"]->Fill((ph+met).Pt(),weight);
            h["mT_Zy"]->Fill((ph+met).Mt(),weight);
            h["y_eta"]->Fill(ph.Eta(),weight);
            h["pT_MET"]->Fill(met.Pt(),weight);
            h["pT_lead_y"]->Fill(ph.Pt(),weight);
            h["n_el"]->Fill(n_e_looseBL,weight);
            h["n_ph"]->Fill(n_ph,weight);
            h["n_mu"]->Fill(n_mu,weight);
            h["n_jet"]->Fill(n_jet,weight);
            if (params["CR2"]) h["e_eta"]->Fill(ph.Eta(),weight);
            if (n_jet>1) {
                h["mjj"]->Fill((jet+jet2).M(),weight);
                h["gCent"]->Fill(fabs((ph.Rapidity()-(jet.Rapidity()+jet2.Rapidity())*0.5)/(jet.Rapidity()-jet2.Rapidity())),weight);
                h["pT_j2"]->Fill(jet2.Pt(),weight);
                h["dPhi_jj"]->Fill(fabs(jet.DeltaPhi(jet2)),weight);
            }

    }
    std::cout<<"entries "<<counter<<" ";
    h.includeOverflow();
    cout<<"integral="<<h["n_el"]->Integral();
    std::cout<<" sumw "<<sumw<<std::endl;

    if (koef!=1 && params["norm"]) h.Norm(koef/sumw);
    Double_t err=0;
    cout<<" after normalization "<<h["n_el"]->IntegralAndError(1,h["n_el"]->GetNbinsX(),err)<<"±"<<err<<endl;
    return h;
    file->Close();
};