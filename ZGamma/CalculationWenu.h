#pragma once
#include "Calculation.h"
#include "Config.h"

class CalculationWenu: public Calculation{
public:
	CalculationWenu(){};
	CalculationWenu(StrV files, TString name, double lumi, Config con);
};