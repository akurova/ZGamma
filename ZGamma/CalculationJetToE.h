#pragma once
#include <TString.h>
#include "Common.h"
#include "Calculation.h"

class CalculationJetToE: public Calculation{
	public:
		CalculationJetToE(){};
		CalculationJetToE(StrV files, TString name, double lumi);
};