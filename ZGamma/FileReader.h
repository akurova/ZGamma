#ifndef __FILE_READER_H
#define __FILE_READER_H

#include <TString.h>
#include <map>
#include <TH1F.h>

#include "HistMap.h"
#include "EventDuplicateCheckerTool.h"

using namespace std; 

class FileReader{
public:
	FileReader(){

	};

	TString filename;
	// void setFilename();
	HistMap getHistograms(TString name);
	HistMap getInitialMap(TString name);
	map<TString, double> params;
	map<TString, TString> paramsStr;
	// virtual void setParams() = 0;
	EventDuplicateCheckerTool checker;
};

#endif // __FILE_READER_H
