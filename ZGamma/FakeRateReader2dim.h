#pragma once

#include <TString.h>

#include "FileReader2dim.h"
#include "HistMap.h"
#include "Common.h"
#include "Config.h"

class FakeRateReader2dim: public FileReader2dim{
public:
    FakeRateReader2dim();
    FakeRateReader2dim(TString s);
    HistMap2dim getInitialMap(TString name);
    HistMap2dim getHistograms(TString name);
    void setParams(double lumi, Config conf);
};
