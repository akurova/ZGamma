#include "Calculation2dim.h"
#include "Config.h"

class CalculationFakeRate2dim: public Calculation2dim{
public:
	CalculationFakeRate2dim(){};
	CalculationFakeRate2dim(StrV files, TString name, double lumi, Config conf);
};