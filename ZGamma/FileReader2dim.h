#ifndef __FILE_READER_H
#define __FILE_READER_H

#include <TString.h>
#include <map>
#include <TH2F.h>

#include "HistMap2dim.h"
#include "EventDuplicateCheckerTool.h"

using namespace std; 

class FileReader2dim{
public:
	FileReader2dim(){

	};

	TString filename;
	// void setFilename();
	HistMap2dim getHistograms(TString name);
	HistMap2dim getInitialMap(TString name);
	map<TString, double> params;
	map<TString, TString> paramsStr;
	// virtual void setParams() = 0;
	EventDuplicateCheckerTool checker;
};

#endif // __FILE_READER_H
