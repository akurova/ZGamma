#ifndef __TREE_READER_H
#define __TREE_READER_H

#include <TString.h>

#include "FileReader.h"
#include "Config.h"
#include "HistMap.h"

class TreeReader: public FileReader{
public:
    TreeReader();
    TreeReader(TString s);
    HistMap getInitialMap(TString name, Config conf);
    HistMap getHistograms(TString name, Config conf);
    void setParams(double lumi, Config conf);
};

#endif // __TREE_READER_H
