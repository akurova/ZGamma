#pragma once
#include <TString.h>
#include "Common.h"
#include "Calculation.h"

class CalculationLeak: public Calculation{
	public:
		CalculationLeak(){};
		CalculationLeak(TString files, TString name, double lumi);
};