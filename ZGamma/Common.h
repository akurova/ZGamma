#pragma once

#include "TH1.h"
#include "TLorentzVector.h"
#include "TObject.h"
#include "TPRegexp.h"
#include <iostream>
//! \name   A few general helper methods and definitions
//! \author Dag Gillberg

  //! \brief method to abort program with error message
  void fatal(TString msg);
  void warning(TString msg);

  //! \brief typedef for a vector of doubles (to save some typing)
  using NumV=std::vector<double>;

  //! \brief typedef for a vector of ints (to save some typing)
  using IntV=std::vector<int>;

  //! \brief typedef for a vector of TStrings (to save some typing)
  using StrV=std::vector<TString>;

  //! \brief typedef for a vector of std::strings (to save some typing)
  using StdStrV=std::vector<std::string>;

  //! \brief Converts a text line to a vector of words
  //  \param str input string with words
  //  \param sep separator to define where a word ends or starts
  StrV vectorize(TString str, TString sep = " ");

  //! \brief Converts string of separated numbers to vector<double>
  //  \param str input string with numbers
  //  \param sep separator to define where a number ends or starts
  NumV vectorizeNum(TString str, TString sep = " ");

  // 1*GeV = 1000*MeV
  static const double GeV(1000), invGeV(1.0 / GeV);


  bool fileExist(TString fn);

// template<typename Container>
// void printContainer(const Container& container)
// {
// using Type = Container::value_type;
// std::copy(std::begin(container), std::end(container), std::ostream_iterator<Type>(std::cout, " "));
// };

template<typename Array>
void printArrSq(const Array* arr, int size){
  std::cout<<"array content:\n";
  for (int i=0; i<size; arr++){
    std::cout<<(*arr)*(*arr)<<" ";
    i++;
  } std::cout<<"\n";
}

template<typename Array>
void printArr(const Array* arr, int size){
  std::cout<<"array contNOent:\n";
  for (int i=0; i<size; arr++){
    std::cout<<*arr<<" ";
    i++;
  } std::cout<<"\n";
}
