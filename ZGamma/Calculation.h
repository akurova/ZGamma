#pragma once

#include <map>
#include <TString.h>
#include <TH1F.h>
// #include "FileReader.h"
#include "Common.h"
#include "Config.h"
#include "TreeReader.h"

//
//	Represents a histogram set for input vector of filenames
//
class Calculation{
public:
	Calculation(){};

	// void read(TString filename);
	// FileReader * reader;
	Calculation(StrV files, TString name, double lumi, Config conf);
	HistMap hists;
	StrV filelist;
	TString SName;
	void operator+=(const Calculation &c2);
	void operator-=(const Calculation &c2);
	void operator=(const Calculation &c2);

	using CalcV=std::vector<Calculation>;
};


