#ifndef __TREE_READER_H
#define __TREE_READER_H

#include <TString.h>

#include "FileReader2dim.h"
#include "HistMap2dim.h"

class TreeReader2dim: public FileReader2dim{
public:
    TreeReader2dim();
    TreeReader2dim(TString s);
    HistMap2dim getInitialMap(TString name);
    HistMap2dim getHistograms(TString name);
    void setParams(double lumi);
};

#endif // __TREE_READER_H