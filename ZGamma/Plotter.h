#pragma once

#include "Calculation.h"
#include <TGraphErrors.h>
#include "Config.h"
#include <TROOT.h>
#include <TH1D.h>
#include <TH2F.h>
#include <TProfile.h>
#include <TCanvas.h>
#include <TLine.h>
#include <TLegend.h>
#include <TLatex.h>
#include <TStyle.h>
#include "AtlasLabels.h"
#include "AtlasUtils.h"
#include "AtlasStyle.h"
#include <TROOT.h>
#include <TH1.h>
#include <TF1.h>



class Plotter{
	public:
		Plotter(){};
		template <typename T>
		void hist_plotter(T* hist)
		{
		gROOT->LoadMacro("$HOME/atlasrootstyle/AtlasLabels.C");
		gROOT->LoadMacro("$HOME/atlasrootstyle/AtlasUtils.C");


		SetAtlasStyle();
		gStyle->SetErrorX(0.5);
		  // TApplication theApp("tapp", &argc, argv);
			cout<<hist->GetName()<<"\n";
		  TCanvas *c1=new TCanvas(hist->GetName(),hist->GetName());
		  hist->Draw();
		  TLatex *   tex;
	  		tex = new TLatex(0.43,0.45,"#sqrt{s}=13 TeV, 140 fb^{-1}");
			tex->SetNDC();
			tex->SetTextFont(42);
			tex->SetLineWidth(2);
			tex->Draw("same");
			ATLASLabel(0.45,0.35,"Internal");
		  c1->UseCurrentStyle();
		  c1->Update();
		  // theApp.Run();
		};

		template <typename T, typename T2>
		void hist_plotter(T* hist, T2* hist2)
		{
		gROOT->LoadMacro("$HOME/atlasrootstyle/AtlasLabels.C");
		gROOT->LoadMacro("$HOME/atlasrootstyle/AtlasUtils.C");


		SetAtlasStyle();
		gStyle->SetErrorX(0.5);
			  // TApplication theApp("tapp", &argc, argv);
		  TCanvas *c1=new TCanvas(hist->GetName(),hist->GetName());
		  hist2->Draw("hist");
		  hist->Draw("hist same");
		  TLegend *leg5=new TLegend(0.196,0.59,0.467,0.901);
		  leg5->AddEntry(hist,hist->GetTitle(),"lp");
		  leg5->AddEntry(hist2,hist2->GetTitle(),"lp");
		  leg5->Draw("same");
		  c1->UseCurrentStyle();
		  hist2->SetLineColor(kRed);
		  hist2->SetMarkerColor(kRed);
		  c1->Update();
		  // theApp.Run();
		};

		void fit_plotter(TH1D* hist, TF1* hist2, TF1* hist3, Config conf);
		void fit_plotter(TH1D* hist, TF1* hist2, Config conf);

		TGraphErrors* DrawStatErrors(TH1* hist);
		void DrawRatioSubPlot(TString errorsType, TH1* histData, TH1* histMC, float integralData, float integralMC, float setRangeA_Y, float setRangeB_Y, float setRangeA_X, float setRangeB_X);
};
