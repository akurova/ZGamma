#pragma once

#include "Plotter.h"
#include "HistMap.h"
#include "Config.h"

class FakeRatePlotter: public Plotter{
	public:
		FakeRatePlotter(){};
		FakeRatePlotter(HistMap map, Config conf);
		void FakeRatePlotterWenu(HistMap SR, HistMap CR2, Config conf);
		void FakeRateComp(HistMap Zee_cent, HistMap Zee_forw, HistMap SR, HistMap CR2, Config conf);
};