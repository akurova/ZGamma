#pragma once 

#include <map>
#include <TH1.h>
#include <TString.h>
#include <iostream>
#include "Common.h"
#include <TFile.h>
#include <TKey.h>
#include <TROOT.h>

using namespace std;


class HistMap{
public: 
	HistMap(){};

	HistMap& operator+=(const HistMap & M){

		for (auto k: M.content){
				if (!this->operator[](k.first)) fatal("there is no key " + k.first + " in this map");
				this->operator[](k.first)->Add(k.second);
		}
		return *this;
	};

	HistMap& operator-=(const HistMap & M){

		for (auto k: M.content){
				if (!this->operator[](k.first)) fatal("there is no key " + k.first + " in this map");
				this->operator[](k.first)->Add(k.second,-1);
		}
		return *this;
	};

	TH1D *& operator[](TString k){
		// if (!content[k]) fatal("there is no key " + k + " in this map");
		return content[k];
	};

	HistMap Norm(double norm){
		
		for (auto k: this->content){
				k.second->Scale(norm); //content of each bin is scaled to norm, sumw2 in each bin is multiplied on norm^2
		}
		return *this;
	};

	//scales uncertainty in each bin to the derr value
	HistMap ScaleError(double derr){ 
		
		for (auto k: this->content){
			double in, rel, err, binerr;
			in=k.second->IntegralAndError(1,k.second->GetNbinsX(),err);
			rel=in/err;
			std::cout<<"in "<<in<<" err "<<err<<" new abs err "<<err*derr*rel<<"\n";
			for (int i=1; i<=k.second->GetNbinsX(); i++){
				if (k.second->GetBinContent(i)!=0){
					binerr = k.second->GetBinError(i);
					// bincontent = k.second->GetBinContent(i);
					k.second->SetBinError(i,binerr*derr*rel);
				} else k.second->SetBinError(i,0);
			} 
			k.second->IntegralAndError(1,k.second->GetNbinsX(),err);
			std::cout<<"unc "<<err<<"\n";
		}
		return *this;
	};

	//addes additional relative errror to the one which exists in hist
	HistMap AddError(double derr){ 
		
		for (auto k: this->content){
			double bincontent, binerr, err;
			k.second->IntegralAndError(1,k.second->GetNbinsX(),err);
			std::cout<<k.first<<" unc "<<err<<"\n";
			for (int i=1; i<=k.second->GetNbinsX(); i++){
				if (k.second->GetBinContent(i)!=0){
					binerr = k.second->GetBinError(i);
					bincontent = k.second->GetBinContent(i);
					k.second->SetBinError(i,sqrt(derr*derr+binerr*binerr/bincontent/bincontent)*bincontent);
				} else k.second->SetBinError(i,0);
			} 
			k.second->IntegralAndError(1,k.second->GetNbinsX(),err);
			std::cout<<k.first<<" unc "<<err<<"\n";
		}
		return *this;
	};

	//include overflow and underflow
	HistMap includeOverflow(){ 
		
		for (auto k: this->content){	
			k.second->SetBinContent(k.second->GetNbinsX(),k.second->GetBinContent(k.second->GetNbinsX())+k.second->GetBinContent(k.second->GetNbinsX()+1));	
			k.second->SetBinError(k.second->GetNbinsX(),sqrt(k.second->GetBinError(k.second->GetNbinsX())*k.second->GetBinError(k.second->GetNbinsX())+k.second->GetBinError(k.second->GetNbinsX()+1)*k.second->GetBinError(k.second->GetNbinsX()+1)));	
			k.second->SetBinContent(1,k.second->GetBinContent(1)+k.second->GetBinContent(0));
			k.second->SetBinError(1,sqrt(k.second->GetBinError(1)*k.second->GetBinError(1)+k.second->GetBinError(0)*k.second->GetBinError(0)));
		}
		return *this;
	};

	HistMap& operator=(const HistMap & M){

		for (auto k: M.content){
			TString name = (TString)k.second->GetName()+"_clone";
				this->operator[](k.first)=(TH1D*)k.second->Clone(name);
		}
		return *this;
	};

	bool find(TString key){
		if (content.find(key)==content.end()) fatal("there is no key " + key + " in this map");
		return true;
	};

	HistMap fillFromFile(TString pathToFile){
		std::cout<<"reading histograms from file\n";
		TFile *file = new TFile(pathToFile, "READ");
		for(auto k : *file->GetListOfKeys()) {
	      TKey *key = static_cast<TKey*>(k);
	      TClass *cl = gROOT->GetClass(key->GetClassName());
	      if (!cl->InheritsFrom("TH1")) continue;
	      TH1D *h = key->ReadObject<TH1D>();
	      // std::cout<<"hist "<<h->GetName()<<"\n";
	      TString name = h->GetName();
	      // if (name=="n_jet") {
	      	// std::cout<<"found n_jet\n";
	      	TString name2 = name+"_clone";
	      	this->operator[](name)=(TH1D*)h->Clone("name2");
	      	double err;
	      	std::cout<<this->operator[](name)->IntegralAndError(1,this->operator[](name)->GetNbinsX(),err)<<"±"<<err<<"\n";

	      // }
	      // h->Draw();
	      // c1.Print("hsimple11.ps");
	   }
	   return *this;
	};

	HistMap setErr(double value){
		for (auto k: this->content){
			for (int i=1; i<=k.second->GetNbinsX(); i++){
				k.second->SetBinError(i,k.second->GetBinContent(i)*value);
			} 
		// k.second->IntegralAndError(1,k.second->GetNbinsX(),err);
		// std::cout<<"unc "<<err<<"\n";
		}
		return *this;
	};
	
private:
	std::map<TString, TH1D *> content;

};