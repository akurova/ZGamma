#pragma once
#include <TString.h>
#include "Common.h"
#include "Calculation.h"

class CalculationRData: public Calculation{
	public:
		CalculationRData(){};
		CalculationRData(StrV files, TString name, double lumi);
};