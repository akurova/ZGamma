#pragma once

#include <map>
#include <TString.h>
#include <TH2F.h>
// #include "FileReader.h"
#include "Common.h"
#include "HistMap2dim.h"
#include "TreeReader2dim.h"

//
//	Represents a histogram set for input vector of filenames
//
class Calculation2dim{
public:
	Calculation2dim(){};

	// void read(TString filename);
	// FileReader * reader;
	Calculation2dim(StrV files, TString name, double lumi);
	HistMap2dim hists;
	StrV filelist;
	TString SName;

	void operator+=(const Calculation2dim &c2);
	void operator-=(const Calculation2dim &c2);
	void operator=(const Calculation2dim &c2);

	typedef std::vector<Calculation2dim> CalcV2;
};


