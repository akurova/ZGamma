#include <TString.h>

#include "FileReader.h"
#include "HistMap.h"

class JetToEReader: public FileReader{
public:
    JetToEReader(){};
    JetToEReader(TString s);
    HistMap getInitialMap(TString name);
    HistMap getHistograms(TString name);
    void setParams(double lumi);
};
