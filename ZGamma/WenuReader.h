#pragma once

#include <TString.h>

#include "FileReader.h"
#include "HistMap.h"
#include "Common.h"
#include "Config.h"


class WenuReader: public FileReader{
public:
    WenuReader();
    WenuReader(TString s);
    HistMap getInitialMap(TString name);
    HistMap getHistograms(TString name);
    void setParams(double lumi, Config con);
    void getFakeRate(StrV input, double lumi);
    void getFakeRateSyst();
};
