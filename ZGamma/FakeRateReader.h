#pragma once

#include <TString.h>

#include "FileReader.h"
#include "HistMap.h"
#include "Common.h"
#include "Config.h"

class FakeRateReader: public FileReader{
public:
    FakeRateReader();
    FakeRateReader(TString s);
    HistMap getInitialMap(TString name);
    HistMap getHistograms(TString name);
    void setParams(double lumi, Config conf);
};
