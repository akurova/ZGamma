#include <TString.h>

#include "FileReader.h"
#include "HistMap.h"

class RDataReader: public FileReader{
public:
    RDataReader(){};
    RDataReader(TString s);
    HistMap getInitialMap(TString name);
    HistMap getHistograms(TString name);
    void setParams(double lumi);
};
