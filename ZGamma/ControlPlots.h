#ifndef CONTROLPLOTS_H
#define CONTROLPLOTS_H
#include "Plotter.h"

class ControlPlots: public Plotter{
	public:
		ControlPlots(){};
		void ControlPlot(Calculation::CalcV vec, TString histname);
};

#endif