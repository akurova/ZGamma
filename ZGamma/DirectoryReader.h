#pragma once
#include "TSystem.h"
#include <TString.h>
#include <Common.h>
#include <map>

//this class is created for reading the system directories

class DirectoryReader {

public:
	DirectoryReader();
	DirectoryReader(TString dirName, StrV cath);
	void SetDirectory(TString dir);
	std::map<TString,StrV> GetFilesList();
private:
	TString name;
	StrV cathegory;
	std::map<TString,StrV> loop(TString name, StrV cathegory);
};