// Dear emacs, this is -*- c++ -*-
//
// Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
//
#ifndef EVENTDUPLICATECHECKER_EVENTDUPLICATECHECKERTOOL_H
#define EVENTDUPLICATECHECKER_EVENTDUPLICATECHECKERTOOL_H


// System include(s).
#include <set>
#include <string>
#include <utility>


   /// Tool checking for event duplicates in a job
   ///
   /// This tool is mainly meant as an example of how to do this sort of a
   /// check in the best way in the last steps of an analysis. The tool can
   /// either be used directly from this repository, or it could just be
   /// copied into a user's own repository.
   ///
   /// @author Attila Krasznahorkay <Attila.Krasznahorkay@cern.ch>
   ///
   class EventDuplicateCheckerTool {

   public:
      //constructor
      EventDuplicateCheckerTool(){};

      /// Function initialising the tool
      virtual void initialize();

      /// Check whether the current event has already been seen in the job
      ///
      /// @param runNumber The run number of the current event
      /// @param eventNumber The event number of the current event
      /// @return @c true if the event was already seen, @c false if not
      ///
      virtual bool isDuplicate( unsigned int runNumber, unsigned long long eventNumber );

      /// @}

   private:

      /// Type used to indetify an event
      typedef std::pair< unsigned int, unsigned long long > RunEventPair_t;
      /// Internal variable keeping track of all processed events
      std::set< RunEventPair_t > m_processedEvents;

      /// @}

   }; // class EventDuplicateCheckerTool



#endif // EVENTDUPLICATECHECKER_EVENTDUPLICATECHECKERTOOL_H
