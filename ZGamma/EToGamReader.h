#pragma once

#include <TString.h>

#include "FileReader.h"
#include "HistMap.h"
#include "Common.h"
#include "Config.h"
#include <TF1.h>

class EToGamReader: public FileReader{
public:
    EToGamReader();
    EToGamReader(TString s);
    HistMap getInitialMap(TString name);
    HistMap getHistograms(TString name, Config conf);
    void setParams(double lumi, Config conf);
    void getFakeRate(StrV input, double lumi);
    void getFakeRateSyst();
    NumV bkgFitSyst(HistMap h, Config conf);

};
