#ifndef ISOLATIONPLOTTER_H
#define ISOLATIONPLOTTER_H
#include "Plotter.h"

class IsolationPlotter: public Plotter{
	public:
		IsolationPlotter(){};
		void IsoPlots(TH1D* tight1, TH1D* anti1, TString name);
		void IsoPlotsJetToE(TH1D* tight1, TH1D* anti1, TString name);
};

#endif