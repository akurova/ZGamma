#pragma once 

#include <map>
#include <TH2.h>
#include <TString.h>
#include <iostream>
#include "Common.h"

using namespace std;


class HistMap2dim{
public: 
	HistMap2dim(){};
	// HistMap operator+(HistMap & M1, HistMap & M2);

	HistMap2dim& operator+=(const HistMap2dim & M){

		for (auto k: M.content){
				if (!this->operator[](k.first)) fatal("there is no key " + k.first + " in this map");
				this->operator[](k.first)->Add(k.second);
		}
		return *this;
	};

	HistMap2dim& operator-=(const HistMap2dim & M){

		for (auto k: M.content){
				if (!this->operator[](k.first)) fatal("there is no key " + k.first + " in this map");
				this->operator[](k.first)->Add(k.second,-1);
		}
		return *this;
	};

	TH2F *& operator[](TString k){
		// if (!content[k]) fatal("there is no key " + k + " in this map");
		return content[k];
	};

	HistMap2dim Norm(double norm){
		
		for (auto k: this->content){
				k.second->Scale(norm);
		}
		return *this;
	};

	//addes additional relative errror to the one which exists in hist
	// HistMap AddError(double derr){ 
		
	// 	for (auto k: this->content){
	// 		double bincontent, binerr, err;
	// 		for (int i=1; i<=k.second->GetNbinsX(); i++){
	// 			if (k.second->GetBinContent(i)!=0){
	// 				binerr = k.second->GetBinError(i);
	// 				bincontent = k.second->GetBinContent(i);
	// 				k.second->SetBinError(i,sqrt(derr*derr+binerr*binerr/bincontent/bincontent)*bincontent);
	// 			} else k.second->SetBinError(i,0);
	// 		} 
	// 		// k.second->IntegralAndError(1,k.second->GetNbinsX(),err);
	// 		// std::cout<<"unc "<<err<<"\n";
	// 	}
	// 	return *this;
	// };

	HistMap2dim& operator=(const HistMap2dim & M){

		for (auto k: M.content){
			TString name = (TString)k.second->GetName()+"_clone";
				this->operator[](k.first)=(TH2F*)k.second->Clone(name);
		}
		return *this;
	};

	bool find(TString key){
		if (content.find(key)==content.end()) fatal("there is no key " + key + " in this map");
		return true;
	};

	private:
		std::map<TString, TH2F *> content;

};