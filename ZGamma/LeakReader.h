#include <TString.h>

#include "FileReader.h"
#include "HistMap.h"

class LeakReader: public FileReader{
public:
    LeakReader(){};
    LeakReader(TString s);
    HistMap getInitialMap(TString name);
    HistMap getHistograms(TString name);
    void setParams(double lumi);
};
