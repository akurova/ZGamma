#include "Calculation.h"
#include "Config.h"

class CalculationFakeRate: public Calculation{
public:
	CalculationFakeRate(){};
	CalculationFakeRate(StrV files, TString name, double lumi, Config conf);
};