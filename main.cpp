//#include "boost/algorithm/string/split.hpp"
//#include <boost/algorithm/string.hpp>
// #include <iostream>
#include <TRandom1.h>
#include <TH1D.h>
#include <TProfile.h>
#include <TApplication.h>
#include <TFile.h>
#include <map>
#include <TCanvas.h>
#include "Calculation.h"
#include "Calculation2dim.h"
#include "CalculationRData.h"
#include "CalculationJetToE.h"
#include "CalculationLeak.h"
#include "CalculationEToGam.h"
#include "CalculationWenu.h"
#include "CalculationFakeRate.h"
#include "CalculationFakeRate2dim.h"
#include "Common.h"
#include "Config.h"
#include "DirectoryReader.h"
#include "FakeRateReader.h"
#include "EToGamReader.h"
#include "HistMap.h"
#include "HistMap2dim.h"
#include "Plotter.h"
#include "ControlPlots.h"
#include "IsolationPlotter.h"
#include "FakeRatePlotter.h"
#include <TApplication.h>


void execute(int argc, char **argv){
  Config conf("../ZGamma.cfg");
  Calculation::CalcV vec;
  DirectoryReader *dir1=0;
  DirectoryReader *dir2=0;
  map<TString,StrV> filename;
  ControlPlots drawing;
  TApplication theApp("tapp", &argc, argv);
  StrV cath = {"nunugamma","Znunu_","Wenu","SinglePhotonPt","nugamma","gamma_pty_140", "Znunugamma2jEWK", "nugamma2jEWK", "jets_JZ", "ttbar", "singletop","ttgamma","Wtaunu_H","Wtaunu_L","nugamma.",};

  if (conf.isDefined("MC16aDir")) {
    dir1 = new DirectoryReader(conf.getStr("MC16aDir"),cath);
    filename = dir1->GetFilesList();
  }
  

  std::cout<<"making sample cathegorization for MC16a\n\n";
  for (auto p: filename){
    std::cout<<p.first<<":\n";
    for (auto p2: p.second){
      std::cout<<p2<<"\n";
    }
  }
  std::cout<<"\n";
  double err;
  double lumi = conf.getNum("Lumi2015")+conf.getNum("Lumi2016");

  StrV data;
  if (conf.isDefined("Data")) {
  data = {conf.getStr("Data")};
  }
  else fatal("Data directory is not set");

  // printf("%.6f", conf.getNum("Lumi2015")+conf.getNum("Lumi2016")+conf.getNum("Lumi2017")+conf.getNum("Lumi2018"));
  Calculation WGamma = Calculation(filename["nugamma"],"Wgam_16a",lumi,conf);

  std::cout<<WGamma.SName<<" Total: "<<WGamma.hists["n_el"]->IntegralAndError(1,WGamma.hists["n_el"]->GetNbinsX(),err)<<"+-"<<err<<std::endl;

  Calculation ZllGamma = Calculation(filename["gamma_pty_140"],"llgam_16a",lumi,conf);

  std::cout<<ZllGamma.SName<<" Total: "<<ZllGamma.hists["n_el"]->IntegralAndError(1,ZllGamma.hists["n_el"]->GetNbinsX(),err)<<"+-"<<err<<std::endl;

  // WGamma+=ZllGamma;

  Calculation JetToMET = Calculation(filename["SinglePhotonPt"],"SP_16a",lumi,conf);
 
  std::cout<<JetToMET.SName<<" Total: "<<JetToMET.hists["n_el"]->IntegralAndError(1,JetToMET.hists["n_el"]->GetNbinsX(),err)<<"+-"<<err<<std::endl;

  Calculation EToGam = Calculation(filename["Wenu"],"Wenu_16a",lumi,conf);

  std::cout<<EToGam.SName<<" Total: "<<EToGam.hists["n_el"]->IntegralAndError(1,EToGam.hists["n_el"]->GetNbinsX(),err)<<"+-"<<err<<std::endl;

  Calculation JetToGam = Calculation(filename["Znunu_"],"Znunu_16a",lumi,conf);
  
  std::cout<<JetToGam.SName<<" Total: "<<JetToGam.hists["n_el"]->IntegralAndError(1,JetToGam.hists["n_el"]->GetNbinsX(),err)<<"+-"<<err<<std::endl;
  Calculation nunugamma = Calculation(filename["nunugamma"],"nunugam_16a",lumi,conf);
  
  std::cout<<nunugamma.SName<<" Total: "<<nunugamma.hists["n_el"]->IntegralAndError(1,nunugamma.hists["n_el"]->GetNbinsX(),err)<<"+-"<<err<<std::endl;

  Calculation Wgamma2jEWK = Calculation(filename["nugamma2jEWK"],"Wgamma2jEWK_16a",lumi,conf);
  
  std::cout<<Wgamma2jEWK.SName<<" Total: "<<Wgamma2jEWK.hists["n_el"]->IntegralAndError(1,Wgamma2jEWK.hists["n_el"]->GetNbinsX(),err)<<"+-"<<err<<std::endl;

  Calculation Znunugamma2jEWK = Calculation(filename["Znunugamma2jEWK"],"nunugamma2jEWK_16a",lumi,conf);
  
  std::cout<<Znunugamma2jEWK.SName<<" Total: "<<Znunugamma2jEWK.hists["n_el"]->IntegralAndError(1,Znunugamma2jEWK.hists["n_el"]->GetNbinsX(),err)<<"+-"<<err<<std::endl;

  Calculation multijet = Calculation(filename["jets_JZ"],"multijet_16a",lumi,conf);
  
  std::cout<<multijet.SName<<" Total: "<<multijet.hists["n_el"]->IntegralAndError(1,multijet.hists["n_el"]->GetNbinsX(),err)<<"+-"<<err<<std::endl;

  Calculation singletop = Calculation(filename["singletop"],"singletop_16a",lumi,conf);
  
  std::cout<<singletop.SName<<" Total: "<<singletop.hists["n_el"]->IntegralAndError(1,singletop.hists["n_el"]->GetNbinsX(),err)<<"+-"<<err<<std::endl;

  Calculation tops = Calculation(filename["ttbar"],"tops_16a",lumi,conf);
  
  std::cout<<tops.SName<<" Total: "<<tops.hists["n_el"]->IntegralAndError(1,tops.hists["n_el"]->GetNbinsX(),err)<<"+-"<<err<<std::endl;
  tops+=singletop;

  Calculation tta_1lep = Calculation(filename["ttgamma"],"tta_1lep_16a",lumi,conf);
  
  std::cout<<tta_1lep.SName<<" Total: "<<tta_1lep.hists["n_el"]->IntegralAndError(1,tta_1lep.hists["n_el"]->GetNbinsX(),err)<<"+-"<<err<<std::endl;

  // Calculation tta_gt1lep = Calculation(filename["ttgamma"],"tta_gt1lep_16a",lumi);
  
  // std::cout<<tta_gt1lep.SName<<" Total: "<<tta_gt1lep.hists["n_el"]->IntegralAndError(1,tta_gt1lep.hists["n_el"]->GetNbinsX(),err)<<"+-"<<err<<std::endl;

  Calculation Wtaunu_H = Calculation(filename["Wtaunu_H"],"Wtaunu_H_16a",lumi,conf);
  
  std::cout<<Wtaunu_H.SName<<" Total: "<<Wtaunu_H.hists["n_el"]->IntegralAndError(1,Wtaunu_H.hists["n_el"]->GetNbinsX(),err)<<"+-"<<err<<std::endl;

  Calculation Wtaunu_L = Calculation(filename["Wtaunu_L"],"Wtaunu_L_16a",lumi,conf);
  
  std::cout<<Wtaunu_L.SName<<" Total: "<<Wtaunu_L.hists["n_el"]->IntegralAndError(1,Wtaunu_L.hists["n_el"]->GetNbinsX(),err)<<"+-"<<err<<std::endl;

  if (conf.isDefined("MC16dDir")){
    dir1->SetDirectory(conf.getStr("MC16dDir"));
    filename = dir1->GetFilesList();
  }

  std::cout<<"\n making sample cathegorization for MC16d\n\n";
  for (auto p: filename){
    std::cout<<p.first<<":\n";
    for (auto p2: p.second){
      std::cout<<p2<<"\n";
    }
  }

  lumi = conf.getNum("Lumi2017");

  Calculation WGammaD = Calculation(filename["nugamma"],"Wgam_16d",lumi,conf);
  std::cout<<WGammaD.SName<<" Total: "<<WGammaD.hists["n_el"]->IntegralAndError(1,WGammaD.hists["n_el"]->GetNbinsX(),err)<<"+-"<<err<<std::endl;

  Calculation ZllGammaD = Calculation(filename["gamma_pty_140"],"llgam_16d",lumi,conf);
  std::cout<<ZllGammaD.SName<<" Total: "<<ZllGammaD.hists["n_el"]->IntegralAndError(1,ZllGammaD.hists["n_el"]->GetNbinsX(),err)<<"+-"<<err<<std::endl;

  ZllGamma+=ZllGammaD;
  WGamma+=WGammaD;

  Calculation JetToMETD = Calculation(filename["SinglePhotonPt"],"SP_16d",lumi,conf);
  JetToMET+=JetToMETD;
  std::cout<<JetToMETD.SName<<" Total: "<<JetToMETD.hists["n_el"]->IntegralAndError(1,JetToMETD.hists["n_el"]->GetNbinsX(),err)<<"+-"<<err<<std::endl;

  Calculation EToGamD = Calculation(filename["Wenu"],"Wenu_16d",lumi,conf);
  EToGam+=EToGamD;
  std::cout<<EToGamD.SName<<" Total: "<<EToGamD.hists["n_el"]->IntegralAndError(1,EToGamD.hists["n_el"]->GetNbinsX(),err)<<"+-"<<err<<std::endl;
  
  Calculation JetToGamD = Calculation(filename["Znunu_"],"Znunu_16d",lumi,conf);
  JetToGam+=JetToGamD;
  std::cout<<JetToGamD.SName<<" Total: "<<JetToGamD.hists["n_el"]->IntegralAndError(1,JetToGamD.hists["n_el"]->GetNbinsX(),err)<<"+-"<<err<<std::endl;

  Calculation nunugammaD = Calculation(filename["nunugamma"],"nunugam_16d",lumi,conf);
  nunugamma+=nunugammaD;
  std::cout<<nunugammaD.SName<<" Total: "<<nunugammaD.hists["n_el"]->IntegralAndError(1,nunugammaD.hists["n_el"]->GetNbinsX(),err)<<"+-"<<err<<std::endl;

  Calculation Wgamma2jEWKD = Calculation(filename["nugamma2jEWK"],"Wgamma2jEWK_16d",lumi,conf);
  Wgamma2jEWK+=Wgamma2jEWKD;
  std::cout<<Wgamma2jEWKD.SName<<" Total: "<<Wgamma2jEWKD.hists["n_el"]->IntegralAndError(1,Wgamma2jEWKD.hists["n_el"]->GetNbinsX(),err)<<"+-"<<err<<std::endl;

  Calculation Znunugamma2jEWKD = Calculation(filename["Znunugamma2jEWK"],"nunugamma2jEWK_16d",lumi,conf);
  Znunugamma2jEWK+=Znunugamma2jEWKD;
  std::cout<<Znunugamma2jEWKD.SName<<" Total: "<<Znunugamma2jEWKD.hists["n_el"]->IntegralAndError(1,Znunugamma2jEWKD.hists["n_el"]->GetNbinsX(),err)<<"+-"<<err<<std::endl;

  Calculation multijetD = Calculation(filename["jets_JZ"],"multijet_16d",lumi,conf);
  multijet+=multijetD;
  std::cout<<multijetD.SName<<" Total: "<<multijetD.hists["n_el"]->IntegralAndError(1,multijetD.hists["n_el"]->GetNbinsX(),err)<<"+-"<<err<<std::endl;

  Calculation singletopD = Calculation(filename["singletop"],"singletop_16d",lumi,conf);
  std::cout<<singletopD.SName<<" Total: "<<singletopD.hists["n_el"]->IntegralAndError(1,singletopD.hists["n_el"]->GetNbinsX(),err)<<"+-"<<err<<std::endl;

  Calculation topsD = Calculation(filename["ttbar"],"tops_16d",lumi,conf);
  std::cout<<topsD.SName<<" Total: "<<topsD.hists["n_el"]->IntegralAndError(1,topsD.hists["n_el"]->GetNbinsX(),err)<<"+-"<<err<<std::endl;
  
  topsD+=singletopD;
  tops+=topsD;

  Calculation ttaD_1lep = Calculation(filename["ttgamma"],"tta_1lep_16d",lumi,conf);
  
  std::cout<<ttaD_1lep.SName<<" Total: "<<ttaD_1lep.hists["n_el"]->IntegralAndError(1,ttaD_1lep.hists["n_el"]->GetNbinsX(),err)<<"+-"<<err<<std::endl;

  tta_1lep+=ttaD_1lep;

  // Calculation ttaD_gt1lep = Calculation(filename["ttgamma"],"tta_gt1lep_16d",lumi);
  
  // std::cout<<ttaD_gt1lep.SName<<" Total: "<<ttaD_gt1lep.hists["n_el"]->IntegralAndError(1,ttaD_gt1lep.hists["n_el"]->GetNbinsX(),err)<<"+-"<<err<<std::endl;

  // tta_gt1lep+=ttaD_gt1lep;

  Calculation WtaunuD_H = Calculation(filename["Wtaunu_H"],"Wtaunu_H_16d",lumi,conf);
  
  std::cout<<WtaunuD_H.SName<<" Total: "<<WtaunuD_H.hists["n_el"]->IntegralAndError(1,WtaunuD_H.hists["n_el"]->GetNbinsX(),err)<<"+-"<<err<<std::endl;

  Calculation WtaunuD_L = Calculation(filename["Wtaunu_L"],"Wtaunu_L_16d",lumi,conf);
  
  std::cout<<WtaunuD_L.SName<<" Total: "<<WtaunuD_L.hists["n_el"]->IntegralAndError(1,WtaunuD_L.hists["n_el"]->GetNbinsX(),err)<<"+-"<<err<<std::endl;
  Wtaunu_L+=WtaunuD_L;
  Wtaunu_H+=WtaunuD_H;

  // cath = {"nunugamma","Znunu_","Wenu","SinglePhoton","nugamma","gamma_pty_140", "Znunugamma2jEWK", "nugamma2jEWK", "jets_JZ", "ttbar", "singletop"};


  if (conf.isDefined("MC16eDir")){
    dir2 = new DirectoryReader(conf.getStr("MC16eDir"),cath);
    filename = dir2->GetFilesList();
  }

  std::cout<<"\n making sample cathegorization for MC16e\n\n";
  for (auto p: filename){
    std::cout<<p.first<<":\n";
    for (auto p2: p.second){
      std::cout<<p2<<"\n";
    }
  }

  lumi = conf.getNum("Lumi2018");

  Calculation WGammaE = Calculation(filename["nugamma"],"Wgam_16e",lumi,conf);
  std::cout<<WGammaE.SName<<" Total: "<<WGammaE.hists["n_el"]->IntegralAndError(1,WGammaE.hists["n_el"]->GetNbinsX(),err)<<"+-"<<err<<std::endl;

  Calculation ZllGammaE = Calculation(filename["gamma_pty_140"],"llgam_16e",lumi,conf);
  std::cout<<ZllGammaE.SName<<" Total: "<<ZllGammaE.hists["n_el"]->IntegralAndError(1,ZllGammaE.hists["n_el"]->GetNbinsX(),err)<<"+-"<<err<<std::endl;

  ZllGamma+=ZllGammaE;
  WGamma+=WGammaE;
  // vec.push_back(WGamma);

  Calculation JetToMETE = Calculation(filename["SinglePhotonPt"],"SP_16e",lumi,conf);
  JetToMET+=JetToMETE;
  // vec.push_back(JetToMET);
  std::cout<<JetToMETE.SName<<" Total: "<<JetToMETE.hists["n_el"]->IntegralAndError(1,JetToMETE.hists["n_el"]->GetNbinsX(),err)<<"+-"<<err<<std::endl;

  Calculation EToGamE = Calculation(filename["Wenu"],"Wenu_16e",lumi,conf);
  EToGam+=EToGamE;
  std::cout<<EToGamE.SName<<" Total: "<<EToGamE.hists["n_el"]->IntegralAndError(1,EToGamE.hists["n_el"]->GetNbinsX(),err)<<"+-"<<err<<std::endl;
  
  Calculation JetToGamE = Calculation(filename["Znunu_"],"Znunu_16e",lumi,conf);
  JetToGam+=JetToGamE;
  // vec.push_back(JetToGam);
  std::cout<<JetToGamE.SName<<" Total: "<<JetToGamE.hists["n_el"]->IntegralAndError(1,JetToGamE.hists["n_el"]->GetNbinsX(),err)<<"+-"<<err<<std::endl;

  Calculation nunugammaE = Calculation(filename["nunugamma"],"nunugam_16e",lumi,conf);
  nunugamma+=nunugammaE;
  // vec.push_back(nunugamma);
  std::cout<<nunugammaE.SName<<" Total: "<<nunugammaE.hists["n_el"]->IntegralAndError(1,nunugammaE.hists["n_el"]->GetNbinsX(),err)<<"+-"<<err<<std::endl;

  Calculation Wgamma2jEWKE = Calculation(filename["nugamma2jEWK"],"Wgamma2jEWK_16e",lumi,conf);
  Wgamma2jEWK+=Wgamma2jEWKE;
  // vec.push_back(Wgamma2jEWK);
  std::cout<<Wgamma2jEWKE.SName<<" Total: "<<Wgamma2jEWKE.hists["n_el"]->IntegralAndError(1,Wgamma2jEWKE.hists["n_el"]->GetNbinsX(),err)<<"+-"<<err<<std::endl;

  Calculation Znunugamma2jEWKE = Calculation(filename["Znunugamma2jEWK"],"nunugamma2jEWK_16e",lumi,conf);
  Znunugamma2jEWK+=Znunugamma2jEWKE;
  // vec.push_back(Znunugamma2jEWK);
  std::cout<<Znunugamma2jEWKE.SName<<" Total: "<<Znunugamma2jEWKE.hists["n_el"]->IntegralAndError(1,Znunugamma2jEWKE.hists["n_el"]->GetNbinsX(),err)<<"+-"<<err<<std::endl;

  Calculation multijetE = Calculation(filename["jets_JZ"],"multijet_16e",lumi,conf);
  multijet+=multijetE;
  // vec.push_back(multijet);
  std::cout<<multijetE.SName<<" Total: "<<multijetE.hists["n_el"]->IntegralAndError(1,multijetE.hists["n_el"]->GetNbinsX(),err)<<"+-"<<err<<std::endl;

  Calculation singletopE = Calculation(filename["singletop"],"singletop_16e",lumi,conf);
  std::cout<<singletopE.SName<<" Total: "<<singletopE.hists["n_el"]->IntegralAndError(1,singletopE.hists["n_el"]->GetNbinsX(),err)<<"+-"<<err<<std::endl;

  Calculation topsE = Calculation(filename["ttbar"],"tops_16d",lumi,conf);
  std::cout<<topsE.SName<<" Total: "<<topsE.hists["n_el"]->IntegralAndError(1,topsE.hists["n_el"]->GetNbinsX(),err)<<"+-"<<err<<std::endl;
  
  topsE+=singletopE;
  tops+=topsE;
  // vec.push_back(tops);
  Calculation ttaE_1lep = Calculation(filename["ttgamma"],"tta_1lep_16e",lumi,conf);
  
  std::cout<<ttaE_1lep.SName<<" Total: "<<ttaE_1lep.hists["n_el"]->IntegralAndError(1,ttaE_1lep.hists["n_el"]->GetNbinsX(),err)<<"+-"<<err<<std::endl;

  tta_1lep+=ttaE_1lep;

  // Calculation ttaE_gt1lep = Calculation(filename["ttgamma"],"tta_gt1lep_16e",lumi);
  
  // std::cout<<ttaE_gt1lep.SName<<" Total: "<<ttaE_gt1lep.hists["n_el"]->IntegralAndError(1,ttaE_gt1lep.hists["n_el"]->GetNbinsX(),err)<<"+-"<<err<<std::endl;

  // tta_gt1lep+=ttaE_gt1lep;

  Calculation WtaunuE_H = Calculation(filename["Wtaunu_H"],"Wtaunu_H_16e",lumi,conf);
  
  std::cout<<WtaunuE_H.SName<<" Total: "<<WtaunuE_H.hists["n_el"]->IntegralAndError(1,WtaunuE_H.hists["n_el"]->GetNbinsX(),err)<<"+-"<<err<<std::endl;

  Calculation WtaunuE_L = Calculation(filename["Wtaunu_L"],"Wtaunu_L_16e",lumi,conf);
  
  std::cout<<WtaunuE_L.SName<<" Total: "<<WtaunuE_L.hists["n_el"]->IntegralAndError(1,WtaunuE_L.hists["n_el"]->GetNbinsX(),err)<<"+-"<<err<<std::endl;
  Wtaunu_L+=WtaunuE_L;
  Wtaunu_H+=WtaunuE_H;

  Calculation JetToGamDD = Calculation();
  JetToGamDD.SName="Znunu";
  TreeReader *nominal = 0;
  JetToGamDD.hists=nominal->getInitialMap(JetToGamDD.SName, conf);
  if (conf.getBool("ReadJetToGam") && !conf.getBool("CR") && !conf.getBool("CR3")){
    JetToGamDD.hists.fillFromFile(conf.getStr("inputHist.JetToGam"));
  }
  // JetToGamDD.hists.setErr(0);

  std::cout<<"Total background contributions:\n";

  std::cout<<WGamma.SName<<" Total: "<<WGamma.hists["n_el"]->IntegralAndError(1,WGamma.hists["n_el"]->GetNbinsX(),err)<<"+-"<<err<<std::endl;

  std::cout<<ZllGamma.SName<<" Total: "<<ZllGamma.hists["n_el"]->IntegralAndError(1,ZllGamma.hists["n_el"]->GetNbinsX(),err)<<"+-"<<err<<std::endl;
 
  std::cout<<JetToMET.SName<<" Total: "<<JetToMET.hists["n_el"]->IntegralAndError(1,JetToMET.hists["n_el"]->GetNbinsX(),err)<<"+-"<<err<<std::endl;

  std::cout<<EToGam.SName<<" Total: "<<EToGam.hists["n_el"]->IntegralAndError(1,EToGam.hists["n_el"]->GetNbinsX(),err)<<"+-"<<err<<std::endl;
  
  std::cout<<JetToGam.SName<<" Total: "<<JetToGam.hists["n_jet"]->IntegralAndError(1,JetToGam.hists["n_jet"]->GetNbinsX(),err)<<"+-"<<err<<std::endl;
  
  // std::cout<<tta_gt1lep.SName<<" Total: "<<tta_gt1lep.hists["n_el"]->IntegralAndError(1,tta_gt1lep.hists["n_el"]->GetNbinsX(),err)<<"+-"<<err<<std::endl;

  std::cout<<tta_1lep.SName<<" Total: "<<tta_1lep.hists["n_el"]->IntegralAndError(1,tta_1lep.hists["n_el"]->GetNbinsX(),err)<<"+-"<<err<<std::endl;
  
  std::cout<<nunugamma.SName<<" Total: "<<nunugamma.hists["n_ph"]->IntegralAndError(1,nunugamma.hists["n_ph"]->GetNbinsX(),err)<<"+-"<<err<<std::endl;
  
  std::cout<<Wgamma2jEWK.SName<<" Total: "<<Wgamma2jEWK.hists["n_el"]->IntegralAndError(1,Wgamma2jEWK.hists["n_el"]->GetNbinsX(),err)<<"+-"<<err<<std::endl;
  
  std::cout<<Znunugamma2jEWK.SName<<" Total: "<<Znunugamma2jEWK.hists["n_el"]->IntegralAndError(1,Znunugamma2jEWK.hists["n_el"]->GetNbinsX(),err)<<"+-"<<err<<std::endl;

  std::cout<<multijet.SName<<" Total: "<<multijet.hists["n_el"]->IntegralAndError(1,multijet.hists["n_el"]->GetNbinsX(),err)<<"+-"<<err<<std::endl;
  
  std::cout<<tops.SName<<" Total: "<<tops.hists["n_el"]->IntegralAndError(1,tops.hists["n_el"]->GetNbinsX(),err)<<"+-"<<err<<std::endl;
  
  std::cout<<Wtaunu_L.SName<<" Total: "<<Wtaunu_L.hists["n_el"]->IntegralAndError(1,Wtaunu_L.hists["n_el"]->GetNbinsX(),err)<<"+-"<<err<<std::endl;
  std::cout<<Wtaunu_H.SName<<" Total: "<<Wtaunu_H.hists["n_el"]->IntegralAndError(1,Wtaunu_H.hists["n_el"]->GetNbinsX(),err)<<"+-"<<err<<std::endl;

  CalculationEToGam EToGamDD;

  if (conf.getBool("DataDriven") && !conf.getBool("CR2")) {
    EToGamDD = CalculationEToGam(data,"Wenu_15-18",conf.getNum("TotalLumi"),conf);
    EToGamDD.SName="Wenu_15-18";
    std::cout<<EToGamDD.SName<<" Total: "<<EToGamDD.hists["n_el"]->IntegralAndError(1,EToGamDD.hists["n_el"]->GetNbinsX(),err)<<"+-"<<err<<std::endl;
    // if (conf.getBool("DrawPlots")) vec.push_back(EToGamDD);
  }
  // else if (conf.getBool("DrawPlots")) vec.push_back(EToGam);
  StrV plots = conf.getStrV("VariableToPlot");
  if (conf.getBool("DrawPlots")) {
    EToGam+=tops; EToGam+=Wtaunu_L; 
    // JetToGam+=multijet; 
    // JetToGam+=Wtaunu_H;
    WGamma+=Wgamma2jEWK;
    nunugamma+=Znunugamma2jEWK;

    // if (!conf.getBool("DataDriven") || conf.getBool("CR2")) {EToGam+=tta_1lep; EToGam+=Wgamma2jEWK; EToGam+=WGamma; vec.push_back(EToGam);}
    if (conf.getBool("CR2")) {
      JetToGam+=multijet; 
      JetToGam+=Wtaunu_H;
      EToGam+=tta_1lep; EToGam+=Wgamma2jEWK; EToGam+=WGamma; vec.push_back(EToGam);
      Calculation other = ZllGamma;
      other+=JetToGam;
      other+=JetToMET;
      // other+=tta_1lep;
      // other+=Wgamma2jEWK;
      // other+=WGamma;
      other+=nunugamma;
      other+=Znunugamma2jEWK;
      other.SName="other";
      std::cout<<"name of other backgrounds "<<other.SName<<std::endl;
      vec.push_back(other);
    }
      else {
        if (conf.getBool("DataDriven") && !conf.getBool("CR2") /*&& !conf.getBool("CR") && !conf.getBool("CR3")*/) {
          JetToMET.hists.Norm(0.74); 
          JetToMET.hists.AddError(0.11/0.74);
          // JetToGam=nunugamma; 
          // double sc=0;
          // // JetToGam.hists.Norm(1700/JetToGam.hists["n_ph"]->IntegralAndError(1,JetToGam.hists["n_ph"]->GetNbinsX(),err)); 
          // // JetToGam.hists.ScaleError(135/err);
          if (!conf.getBool("ReadJetToGam")&& !conf.getBool("CR") && !conf.getBool("CR3")){
            JetToGam.hists.Norm(1765/JetToGam.hists["n_ph"]->IntegralAndError(1,JetToGam.hists["n_ph"]->GetNbinsX(),err)); 
            JetToGam.hists.ScaleError(348/1765.0);
            JetToGamDD=JetToGam;
          } else if (conf.getBool("CR")||conf.getBool("CR3")){
              JetToGam+=Wtaunu_H;
              JetToGam.hists.Norm(2.36); //for Njet>=0 and Njet>0
              // JetToGam.hists.Norm(2.61); //for Njet>1
              JetToGam.hists.AddError(348/1765.0);
              JetToGamDD=JetToGam;
          }
          // for (auto name: plots){
          //   if (!name.Contains("j")||name.Contains("n_jet")){
          //     sc=4730/JetToMET.hists[name]->IntegralAndError(1,JetToMET.hists[name]->GetNbinsX(),err);
          //     JetToMET.hists[name]->Scale(sc);
          //     std::cout<<name<<" Njets>=0 "<<sc<<" int "<<JetToMET.hists[name]->IntegralAndError(1,JetToMET.hists[name]->GetNbinsX(),err)<<"±"<<err<<"\n";
          //   }
          //   else if (!name.Contains("j2")&&!name.Contains("jj")){
          //     sc=JetToMET.hists["n_jet"]->Integral(2,JetToMET.hists["n_jet"]->GetNbinsX())/JetToMET.hists["n_jet"]->Integral();
          //     JetToMET.hists[name]->Scale(4730*sc/JetToMET.hists[name]->Integral());
          //     JetToMET.hists[name]->IntegralAndError(1,JetToMET.hists[name]->GetNbinsX(),err);
          //     std::cout<<name<<" Njets>0 "<<sc<<" unc "<<err<<"\n";            
          //   }
          //   else {
          //     sc=JetToMET.hists["n_jet"]->Integral(3,JetToMET.hists["n_jet"]->GetNbinsX())/JetToMET.hists["n_jet"]->Integral();
          //     JetToMET.hists[name]->Scale(4730*sc/JetToMET.hists[name]->Integral());
          //     JetToMET.hists[name]->IntegralAndError(1,JetToMET.hists[name]->GetNbinsX(),err);
          //     std::cout<<name<<" Njets>1 "<<sc<<" unc "<<err<<"\n";
          //   }
          // }
          // sc=sqrt(940*940+690*690)/4730.0;
          // std::cout<<" sc "<<sc<<"\n";
          // JetToMET.hists.AddError(sc);
          WGamma+=tta_1lep;
          WGamma.hists.Norm(0.94);
          WGamma.hists.AddError(0.13/0.94);
        }

        // JetToMET+=tta;
        // vec.push_back(tta_gt1lep);
        // vec.push_back(tta_1lep);
        vec.push_back(ZllGamma);
        if (conf.getBool("DataDriven")){
          vec.push_back(JetToGamDD); 
          EToGamDD.hists.AddError(0.07); vec.push_back(EToGamDD); 
        }
        else {vec.push_back(tta_1lep); vec.push_back(EToGam);}
        if (!conf.getBool("CR")) vec.push_back(WGamma);
        if (!conf.getBool("CR3")) vec.push_back(JetToMET);
        vec.push_back(nunugamma);
        if (conf.getBool("CR")) vec.push_back(WGamma);
        if (conf.getBool("CR3")) vec.push_back(JetToMET);
      }
  }
    Calculation Data = Calculation(data,"Data_15-18",lumi,conf);
  // std::cout<< "calculation objects created.\n";
  if (conf.getBool("CR2")||conf.getBool("CR")||conf.getBool("CR3")) {
  std::cout<<Data.SName<<" Total: "<<Data.hists["n_ph"]->IntegralAndError(1,Data.hists["n_ph"]->GetNbinsX(),err)<<"+-"<<err<<std::endl;
  vec.push_back(Data); //data should always be the last for plotter to work properly
}
  // // std::cout<<"adding hists: \n";
  // // std::cout<<"initial EToGam: "<<EToGam->hists["n_jet"]->Integral()<<"\n";
  // // std::cout<<"initial JetToGam: "<<JetToGam->hists["n_jet"]->Integral()<<"\n";
  // // // (*EToGam)+=(*JetToGam);
  // // std::cout<<"result: "<<EToGam->hists["n_jet"]->Integral()<<"\n";
  // // Plotter();
  // std::cout<<"Sample name "<<Data.SName<<std::endl;
  // TString name = conf.getStr("VariableToPlot");
  if (conf.getBool("DrawPlots")){
    std::cout<<"Drawing plots"<<std::endl;
    for (auto name: plots){
      std::cout<<"plotting "<<name<<std::endl;
      drawing.ControlPlot(vec, name);
    }
  }
  // drawing.hist_plotter(Data.hists["n_jet"]);
  
  for (int i=1; i<=JetToGamDD.hists["pT_lead_y"]->GetNbinsX(); i++){
    if (conf.getBool("DataDriven")) std::cout<<"JetToGamDD "<<i<<" bin "<<JetToGamDD.hists["pT_lead_y"]->GetBinContent(i)<<"±"<<JetToGamDD.hists["pT_lead_y"]->GetBinError(i);
    else std::cout<<"JetToGam "<<i<<" bin "<<JetToGam.hists["pT_lead_y"]->GetBinContent(i)<<"±"<<JetToGam.hists["pT_lead_y"]->GetBinError(i);
    std::cout<<" JetToMET "<<i<<" bin "<<JetToMET.hists["pT_lead_y"]->GetBinContent(i)<<"±"<<JetToMET.hists["pT_lead_y"]->GetBinError(i);
    if (conf.getBool("DataDriven")) std::cout<<" EToGamDD "<<i<<" bin "<<EToGamDD.hists["pT_lead_y"]->GetBinContent(i)<<"±"<<EToGamDD.hists["pT_lead_y"]->GetBinError(i);
    else std::cout<<" EToGam "<<i<<" bin "<<EToGam.hists["pT_lead_y"]->GetBinContent(i)<<"±"<<EToGam.hists["pT_lead_y"]->GetBinError(i);
    std::cout<<" nunugamma "<<i<<" bin "<<nunugamma.hists["pT_lead_y"]->GetBinContent(i)<<"±"<<nunugamma.hists["pT_lead_y"]->GetBinError(i)\
    <<" WGamma "<<i<<" bin "<<WGamma.hists["pT_lead_y"]->GetBinContent(i)<<"±"<<WGamma.hists["pT_lead_y"]->GetBinError(i)\
    <<" tta_1lep "<<i<<" bin "<<tta_1lep.hists["pT_lead_y"]->GetBinContent(i)<<"±"<<tta_1lep.hists["pT_lead_y"]->GetBinError(i)\
    <<" ZllGamma "<<i<<" bin "<<ZllGamma.hists["pT_lead_y"]->GetBinContent(i)<<"±"<<ZllGamma.hists["pT_lead_y"]->GetBinError(i)\
    <<"\n";
  }
  theApp.Run();
  // StdStrV str_vec;
  // split(str_vec, str, boost::algorithm::is_any_of("."), boost::token_compress_on);
  // std::cout<<str_vec[0]<<" "<<str_vec[1]<<" "<<str_vec[2]<<"\n";
}

void RfromMC(int argc, char **argv){
  Config conf("../ZGamma.cfg");
  Calculation::CalcV vec;
  DirectoryReader *dir1=0;
  double lumi1, lumi2, lumi; 
  lumi1 = conf.getNum("Lumi2015");
  lumi2 = conf.getNum("Lumi2016");
  lumi = lumi1+lumi2;
  double errB, errD, errE, errF;
  double B,D,E,F;
  double R, errR;

  StrV cath = {"nunugamma","Znunu_","Wenu","SinglePhotonPt","nugamma","gamma_pty_140", "Znunugamma2jEWK", "nugamma2jEWK", "jets_JZ", "ttbar", "singletop","ttgamma","Wtaunu_H","Wtaunu_L","nugamma.",};

  if (conf.isDefined("MC16aDir")) {
    dir1 = new DirectoryReader(conf.getStr("MC16aDir"),cath);
  }
  map<TString,StrV> filename = dir1->GetFilesList();

  std::cout<<"making sample cathegorization\n\n";
  for (auto p: filename){
    std::cout<<p.first<<":\n";
    for (auto p2: p.second){
      std::cout<<p2<<"\n";
    }
  }

  auto Data = CalculationRData(filename["Znunu_"],"Data_15-16",lumi);
  auto Wtaunu_H = CalculationRData(filename["Wtaunu_H"],"Wtaunu_H_15-16",lumi);
  Data+=Wtaunu_H;

  if (conf.isDefined("MC16dDir")){
    dir1->SetDirectory(conf.getStr("MC16dDir"));
    filename = dir1->GetFilesList();
  }

  std::cout<<"\n making sample cathegorization for MC16d\n\n";
  for (auto p: filename){
    std::cout<<p.first<<":\n";
    for (auto p2: p.second){
      std::cout<<p2<<"\n";
    }
  }

  lumi = conf.getNum("Lumi2017");
  auto DataD = CalculationRData(filename["Znunu_"],"Data_17",lumi);
  auto Wtaunu_HD = CalculationRData(filename["Wtaunu_H"],"Wtaunu_H_17",lumi);
  Data+=DataD;
  Data+=Wtaunu_HD;


  if (conf.isDefined("MC16eDir")){
    dir1->SetDirectory(conf.getStr("MC16dDir"));
    filename = dir1->GetFilesList();
  }

  std::cout<<"\n making sample cathegorization for MC16e\n\n";
  for (auto p: filename){
    std::cout<<p.first<<":\n";
    for (auto p2: p.second){
      std::cout<<p2<<"\n";
    }
  }

  lumi = conf.getNum("Lumi2018");
  auto DataE = CalculationRData(filename["Znunu_"],"Data_18",lumi);
  auto Wtaunu_HE = CalculationRData(filename["Wtaunu_H"],"Wtaunu_H_18",lumi);
  Data+=DataE;
  Data+=Wtaunu_HE;


  B=Data.hists["B_region_ph_eta"]->IntegralAndError(1,Data.hists["B_region_ph_eta"]->GetNbinsX(),errB);
  D=Data.hists["D_region_ph_eta"]->IntegralAndError(1,Data.hists["D_region_ph_eta"]->GetNbinsX(),errD);
  E=Data.hists["E_region_ph_eta"]->IntegralAndError(1,Data.hists["E_region_ph_eta"]->GetNbinsX(),errE);
  F=Data.hists["F_region_ph_eta"]->IntegralAndError(1,Data.hists["F_region_ph_eta"]->GetNbinsX(),errF);
  std::cout<<"Regions: \n";
  std::cout<<"B-E "<<B<<"+-"<<errB<<"\n";
  std::cout<<"D-F "<<D<<"+-"<<errD<<"\n";
  std::cout<<"E "<<E<<"+-"<<errE<<"\n";
  std::cout<<"F "<<F<<"+-"<<errF<<"\n";
  errR=sqrt(errB*errB*F*F/D/D/E/E+errF*errF*B*B/D/D/E/E+errD*errD*F*F*B*B/D/D/D/D/E/E+errE*errE*F*F*B*B/D/D/E/E/E/E);
  cout<<"R factor "<<B*F/D/E<<"+-"<<errR<<endl;

}

void RData(int argc, char **argv){

  Config conf("../ZGamma.cfg");
  Calculation::CalcV vec;
  IsolationPlotter drawing;
  // Plotter drawing;
  DirectoryReader *dir1=0;
  TApplication theApp("tapp", &argc, argv);
  double lumi1, lumi2, lumi; 
  lumi1 = conf.getNum("Lumi2015");
  lumi2 = conf.getNum("Lumi2016");
  lumi = lumi1+lumi2;
  // printf("%.6f %.6f %.6f", lumi1, lumi2, lumi) ;
  std::cout<<"calculated luminosity: "<<lumi<<std::endl;

  StrV cath = {"nunugamma","Znunu","Wenu","SinglePhotonPt","nugamma","gamma"};

  if (conf.isDefined("MC16aDir")) {
    dir1 = new DirectoryReader(conf.getStr("MC16aDir"),cath);
  }
  map<TString,StrV> filename = dir1->GetFilesList();

  std::cout<<"making sample cathegorization\n\n";
  for (auto p: filename){
    std::cout<<p.first<<":\n";
    for (auto p2: p.second){
      std::cout<<p2<<"\n";
    }
  }

  double err;


  StrV data;
  if (conf.isDefined("Data")) {
  data = {conf.getStr("Data")};
  }
  else fatal("Data directory is not set");
  auto Data = CalculationRData(data,"Data_15-16",lumi);
  std::cout<< "calculation objects created.\n";
  vec.push_back(Data);

  double errB, errD, errE, errF;
  double B,D,E,F;
  double R, errR;

  B=Data.hists["B_region_ph_eta"]->IntegralAndError(1,Data.hists["B_region_ph_eta"]->GetNbinsX(),errB);
  D=Data.hists["D_region_ph_eta"]->IntegralAndError(1,Data.hists["D_region_ph_eta"]->GetNbinsX(),errD);
  E=Data.hists["E_region_ph_eta"]->IntegralAndError(1,Data.hists["E_region_ph_eta"]->GetNbinsX(),errE);
  F=Data.hists["F_region_ph_eta"]->IntegralAndError(1,Data.hists["F_region_ph_eta"]->GetNbinsX(),errF);
  std::cout<<"Regions before subtraction: \n";
  std::cout<<"B-E "<<B<<"+-"<<errB<<"\n";
  std::cout<<"D-F "<<D<<"+-"<<errD<<"\n";
  std::cout<<"E "<<E<<"+-"<<errE<<"\n";
  std::cout<<"F "<<F<<"+-"<<errF<<"\n";
  errR=sqrt(errB*errB*F*F/D/D/E/E+errF*errF*B*B/D/D/E/E+errD*errD*F*F*B*B/D/D/D/D/E/E+errE*errE*F*F*B*B/D/D/E/E/E/E);
  cout<<"R factor data "<<B*F/D/E<<"+-"<<errR<<endl;

  if (conf.getBool("Subtraction")||conf.getBool("BKGPlots")){
    auto EToGam = CalculationRData(filename["Wenu"],"Wenu_16a",lumi);
    std::cout<<"Total B: "<<EToGam.hists["E_region_ph_eta"]->IntegralAndError(1,EToGam.hists["E_region_ph_eta"]->GetNbinsX(),err)<<"+-"<<err<<std::endl;
    vec.push_back(EToGam);
    auto JetToGam = CalculationRData(filename["SinglePhotonPt"],"SinglePhoton_16a",lumi);
    vec.push_back(JetToGam);
    std::cout<<"Total B: "<<JetToGam.hists["E_region_ph_eta"]->IntegralAndError(1,JetToGam.hists["E_region_ph_eta"]->GetNbinsX(),err)<<"+-"<<err<<std::endl;
    auto GamToMET = CalculationRData(filename["Znunu"],"Znunu_16a",lumi);
    vec.push_back(GamToMET);
    std::cout<<"Total B: "<<GamToMET.hists["E_region_ph_eta"]->IntegralAndError(1,GamToMET.hists["E_region_ph_eta"]->GetNbinsX(),err)<<"+-"<<err<<std::endl;
    auto WGamma = CalculationRData(filename["nugamma"],"Wgam_16a",lumi);
    vec.push_back(WGamma);
    std::cout<<"Total B: "<<WGamma.hists["E_region_ph_eta"]->IntegralAndError(1,WGamma.hists["E_region_ph_eta"]->GetNbinsX(),err)<<"+-"<<err<<std::endl;
    auto ZllGamma = CalculationRData(filename["gamma"],"llgam_16a",lumi);
    vec.push_back(ZllGamma);
    std::cout<<"Total B: "<<ZllGamma.hists["E_region_ph_eta"]->IntegralAndError(1,ZllGamma.hists["E_region_ph_eta"]->GetNbinsX(),err)<<"+-"<<err<<std::endl;
    auto nunugamma = CalculationRData(filename["nunugamma"],"nunugam_16a",lumi);
    vec.push_back(nunugamma);
    std::cout<<"Total B: "<<nunugamma.hists["E_region_ph_eta"]->IntegralAndError(1,nunugamma.hists["E_region_ph_eta"]->GetNbinsX(),err)<<"+-"<<err<<std::endl;
    Data-=EToGam;
    std::cout<<"-etogam="<<Data.hists["B_region_ph_eta"]->IntegralAndError(1,Data.hists["B_region_ph_eta"]->GetNbinsX(),errB)<<std::endl;
    Data-=JetToGam;
    std::cout<<"-JetToGam="<<Data.hists["B_region_ph_eta"]->IntegralAndError(1,Data.hists["B_region_ph_eta"]->GetNbinsX(),errB)<<std::endl;
    Data-=WGamma;
    std::cout<<"-Wgamma="<<Data.hists["B_region_ph_eta"]->IntegralAndError(1,Data.hists["B_region_ph_eta"]->GetNbinsX(),errB)<<std::endl;
    Data-=ZllGamma;
    std::cout<<"-Zllgam="<<Data.hists["B_region_ph_eta"]->IntegralAndError(1,Data.hists["B_region_ph_eta"]->GetNbinsX(),errB)<<std::endl;
    Data-=nunugamma;
    std::cout<<"-nunugam="<<Data.hists["B_region_ph_eta"]->IntegralAndError(1,Data.hists["B_region_ph_eta"]->GetNbinsX(),errB)<<std::endl;
    std::cout<<"backgrounds subtracted\n";
    B=Data.hists["B_region_ph_eta"]->IntegralAndError(1,Data.hists["B_region_ph_eta"]->GetNbinsX(),errB);
    D=Data.hists["D_region_ph_eta"]->IntegralAndError(1,Data.hists["D_region_ph_eta"]->GetNbinsX(),errD);
    E=Data.hists["E_region_ph_eta"]->IntegralAndError(1,Data.hists["E_region_ph_eta"]->GetNbinsX(),errE);
    F=Data.hists["F_region_ph_eta"]->IntegralAndError(1,Data.hists["F_region_ph_eta"]->GetNbinsX(),errF);
    std::cout<<"Regions after subtraction: \n";
    std::cout<<"B-E "<<B<<"+-"<<errB<<"\n";
    std::cout<<"D-F "<<D<<"+-"<<errD<<"\n";
    std::cout<<"E "<<E<<"+-"<<errE<<"\n";
    std::cout<<"F "<<F<<"+-"<<errF<<"\n";
    errR=sqrt(errB*errB*F*F/D/D/E/E+errF*errF*B*B/D/D/E/E+errD*errD*F*F*B*B/D/D/D/D/E/E+errE*errE*F*F*B*B/D/D/E/E/E/E);
    cout<<"R factor data "<<B*F/D/E<<"+-"<<errR<<endl;
    if (conf.getBool("BKGPlots")){
      std::cout<<"BKG plots\n";
      drawing.IsoPlots(EToGam.hists["ph_iso_et_tight"], EToGam.hists["ph_iso_et_antitight"], EToGam.SName);
      drawing.IsoPlots(JetToGam.hists["ph_iso_et_tight"], JetToGam.hists["ph_iso_et_antitight"], JetToGam.SName);
      drawing.IsoPlots(WGamma.hists["ph_iso_et_tight"], WGamma.hists["ph_iso_et_antitight"], WGamma.SName);
      drawing.IsoPlots(ZllGamma.hists["ph_iso_et_tight"], ZllGamma.hists["ph_iso_et_antitight"], ZllGamma.SName);
      drawing.IsoPlots(nunugamma.hists["ph_iso_et_tight"], nunugamma.hists["ph_iso_et_antitight"], nunugamma.SName);
    }
  }

  // if (Data.hists.find("ph_iso_et_tight")) std::cout<<"key found\n";
  // TApplication theApp("tapp", &argc, argv);
  // TCanvas *c1=new TCanvas("c1");
  // Data.hists["ph_iso_et_tight"]->Draw();
  // c1->Update();
  // theApp.Run();
  // drawing.hist_plotter(Data.hists["ph_iso_et_tight"]);
  if (!conf.getBool("BKGPlots")) drawing.IsoPlots(Data.hists["ph_iso_et_tight"], Data.hists["ph_iso_et_antitight"], Data.SName);
  theApp.Run();
}

void leakage(int argc, char **argv){
  Config conf("../ZGamma.cfg");
  Calculation::CalcV vec;
  DirectoryReader *dir1=0;
  double lumi1, lumi2, lumi; 
  lumi1 = conf.getNum("Lumi2015");
  lumi2 = conf.getNum("Lumi2016");
  lumi = lumi1+lumi2;
  // if (!conf.isDefined("Sherpa21")) fatal("sample Sherpa 2.1 is not defined");
  // if (!conf.isDefined("Sherpa222")) fatal("sample Sherpa 2.2.2 is not defined");
  // if (!conf.isDefined("MadGraph")) fatal("sample MadGraph is not defined");
  // auto sherpa21 = CalculationLeak(conf.getStr("Sherpa21"),"Sherpa21",lumi);
  // auto sherpa222 = CalculationLeak(conf.getStr("Sherpa222"),"Sherpa222",lumi);
  // auto madgraph = CalculationLeak(conf.getStr("MadGraph"),"madgraph",lumi);
  // if (!conf.isDefined("Sherpa21")) fatal("sample Sherpa 2.1 is not defined");
  // if (!conf.isDefined("Sherpa222")) fatal("sample Sherpa 2.2.2 is not defined");
  if (!conf.isDefined("MadGraphPy")) fatal("sample MadGraph+Pythia is not defined");
  if (!conf.isDefined("MadGraphHg")) fatal("sample MadGraph+Herwig is not defined");
  // auto sherpa21 = CalculationLeak(conf.getStr("Sherpa21"),"Sherpa21",lumi);
  // auto sherpa222 = CalculationLeak(conf.getStr("Sherpa222"),"Sherpa222",lumi);
  auto madgraphPy = CalculationLeak(conf.getStr("MadGraphPy"),"madgraphPy",lumi);
  auto madgraphHg = CalculationLeak(conf.getStr("MadGraphHg"),"madgraphHg",lumi);
  double A,B,C,D,errA,errB,errC,errD;
  // A=sherpa21.hists["A_region_ph_eta"]->IntegralAndError(1,sherpa21.hists["A_region_ph_eta"]->GetNbinsX(),errA);
  // B=sherpa21.hists["B_region_ph_eta"]->IntegralAndError(1,sherpa21.hists["B_region_ph_eta"]->GetNbinsX(),errB);
  // C=sherpa21.hists["C_region_ph_eta"]->IntegralAndError(1,sherpa21.hists["C_region_ph_eta"]->GetNbinsX(),errC);
  // D=sherpa21.hists["D_region_ph_eta"]->IntegralAndError(1,sherpa21.hists["D_region_ph_eta"]->GetNbinsX(),errD);
  // std::cout<<"Sherpa 2.1\n"<<"c_B="<<B/A<<"+-"<<sqrt((errA*errA/A/A)+(errB*errB/B/B))*B/A<<"("<<sqrt((errA*errA/A/A)+(errB*errB/B/B))*100<<"%)\n";
  // std::cout<<"c_C="<<C/A<<"+-"<<sqrt((errA*errA/A/A)+(errC*errC/C/C))*C/A<<"("<<sqrt((errA*errA/A/A)+(errC*errC/C/C))*100<<"%)\n";
  // std::cout<<"c_D="<<D/A<<"+-"<<sqrt((errA*errA/A/A)+(errD*errD/D/D))*D/A<<"("<<sqrt((errA*errA/A/A)+(errD*errD/D/D))*100<<"%)\n";
  // A=sherpa222.hists["A_region_ph_eta"]->IntegralAndError(1,sherpa222.hists["A_region_ph_eta"]->GetNbinsX(),errA);
  // B=sherpa222.hists["B_region_ph_eta"]->IntegralAndError(1,sherpa222.hists["B_region_ph_eta"]->GetNbinsX(),errB);
  // C=sherpa222.hists["C_region_ph_eta"]->IntegralAndError(1,sherpa222.hists["C_region_ph_eta"]->GetNbinsX(),errC);
  // D=sherpa222.hists["D_region_ph_eta"]->IntegralAndError(1,sherpa222.hists["D_region_ph_eta"]->GetNbinsX(),errD);
  // std::cout<<"Sherpa 2.2.2\n"<<"c_B="<<B/A<<"+-"<<sqrt((errA*errA/A/A)+(errB*errB/B/B))*B/A<<"("<<sqrt((errA*errA/A/A)+(errB*errB/B/B))*100<<"%)\n";
  // std::cout<<"c_C="<<C/A<<"+-"<<sqrt((errA*errA/A/A)+(errC*errC/C/C))*C/A<<"("<<sqrt((errA*errA/A/A)+(errC*errC/C/C))*100<<"%)\n";
  // std::cout<<"c_D="<<D/A<<"+-"<<sqrt((errA*errA/A/A)+(errD*errD/D/D))*D/A<<"("<<sqrt((errA*errA/A/A)+(errD*errD/D/D))*100<<"%)\n";
  // A=madgraph.hists["A_region_ph_eta"]->IntegralAndError(1,madgraph.hists["A_region_ph_eta"]->GetNbinsX(),errA);
  // B=madgraph.hists["B_region_ph_eta"]->IntegralAndError(1,madgraph.hists["B_region_ph_eta"]->GetNbinsX(),errB);
  // C=madgraph.hists["C_region_ph_eta"]->IntegralAndError(1,madgraph.hists["C_region_ph_eta"]->GetNbinsX(),errC);
  // D=madgraph.hists["D_region_ph_eta"]->IntegralAndError(1,madgraph.hists["D_region_ph_eta"]->GetNbinsX(),errD);
  // std::cout<<"MadGraph\n"<<"c_B="<<B/A<<"+-"<<sqrt((errA*errA/A/A)+(errB*errB/B/B))*B/A<<"("<<sqrt((errA*errA/A/A)+(errB*errB/B/B))*100<<"%)\n";
  // std::cout<<"c_C="<<C/A<<"+-"<<sqrt((errA*errA/A/A)+(errC*errC/C/C))*C/A<<"("<<sqrt((errA*errA/A/A)+(errC*errC/C/C))*100<<"%)\n";
  // std::cout<<"c_D="<<D/A<<"+-"<<sqrt((errA*errA/A/A)+(errD*errD/D/D))*D/A<<"("<<sqrt((errA*errA/A/A)+(errD*errD/D/D))*100<<"%)\n";

  A=madgraphPy.hists["A_region_ph_eta"]->IntegralAndError(1,madgraphPy.hists["A_region_ph_eta"]->GetNbinsX(),errA);
  B=madgraphPy.hists["B_region_ph_eta"]->IntegralAndError(1,madgraphPy.hists["B_region_ph_eta"]->GetNbinsX(),errB);
  C=madgraphPy.hists["C_region_ph_eta"]->IntegralAndError(1,madgraphPy.hists["C_region_ph_eta"]->GetNbinsX(),errC);
  D=madgraphPy.hists["D_region_ph_eta"]->IntegralAndError(1,madgraphPy.hists["D_region_ph_eta"]->GetNbinsX(),errD);
  std::cout<<"MadGraph+Pythia\n"<<"c_B="<<B/A<<"+-"<<sqrt((errA*errA/A/A)+(errB*errB/B/B))*B/A<<"("<<sqrt((errA*errA/A/A)+(errB*errB/B/B))*100<<"%)\n";
  std::cout<<"c_C="<<C/A<<"+-"<<sqrt((errA*errA/A/A)+(errC*errC/C/C))*C/A<<"("<<sqrt((errA*errA/A/A)+(errC*errC/C/C))*100<<"%)\n";
  std::cout<<"c_D="<<D/A<<"+-"<<sqrt((errA*errA/A/A)+(errD*errD/D/D))*D/A<<"("<<sqrt((errA*errA/A/A)+(errD*errD/D/D))*100<<"%)\n";
  A=madgraphHg.hists["A_region_ph_eta"]->IntegralAndError(1,madgraphHg.hists["A_region_ph_eta"]->GetNbinsX(),errA);
  B=madgraphHg.hists["B_region_ph_eta"]->IntegralAndError(1,madgraphHg.hists["B_region_ph_eta"]->GetNbinsX(),errB);
  C=madgraphHg.hists["C_region_ph_eta"]->IntegralAndError(1,madgraphHg.hists["C_region_ph_eta"]->GetNbinsX(),errC);
  D=madgraphHg.hists["D_region_ph_eta"]->IntegralAndError(1,madgraphHg.hists["D_region_ph_eta"]->GetNbinsX(),errD);
  std::cout<<"MadGraph+Herwig\n"<<"c_B="<<B/A<<"+-"<<sqrt((errA*errA/A/A)+(errB*errB/B/B))*B/A<<"("<<sqrt((errA*errA/A/A)+(errB*errB/B/B))*100<<"%)\n";
  std::cout<<"c_C="<<C/A<<"+-"<<sqrt((errA*errA/A/A)+(errC*errC/C/C))*C/A<<"("<<sqrt((errA*errA/A/A)+(errC*errC/C/C))*100<<"%)\n";
  std::cout<<"c_D="<<D/A<<"+-"<<sqrt((errA*errA/A/A)+(errD*errD/D/D))*D/A<<"("<<sqrt((errA*errA/A/A)+(errD*errD/D/D))*100<<"%)\n";
}

void etogam(int argc, char **argv){
  Config conf("../ZGamma.cfg");
  TApplication theApp("tapp", &argc, argv);
  Plotter drawing;
  double lumi1, lumi2, lumi3, lumi4, lumi; 
  lumi1 = conf.getNum("Lumi2015");
  lumi2 = conf.getNum("Lumi2016");
  lumi3 = conf.getNum("Lumi2017");
  lumi4 = conf.getNum("Lumi2018");
  lumi = lumi1+lumi2+lumi3+lumi4;  
  printf("%.6f",lumi);
  StrV data;
  DirectoryReader *dir1=0;
  map<TString,StrV> mc16a,mc16d,mc16e;
  StrV cath = {"enugamma"};
  Double_t err=0; Double_t unc=0;

  if (conf.getBool("FromMC")){

    if (conf.isDefined("MC16aDir")) {
      dir1 = new DirectoryReader(conf.getStr("MC16aDir"),cath);
      mc16a = dir1->GetFilesList();
    }
    
    std::cout<<"making sample cathegorization for MC16a\n\n";
    for (auto p: mc16a){
      std::cout<<p.first<<":\n";
      for (auto p2: p.second){
        std::cout<<p2<<"\n";
      }
    }


    if (conf.isDefined("MC16dDir")) {
      dir1 = new DirectoryReader(conf.getStr("MC16dDir"),cath);
      mc16d = dir1->GetFilesList();
    }
    
    std::cout<<"making sample cathegorization for MC16d\n\n";
    for (auto p: mc16d){
      std::cout<<p.first<<":\n";
      for (auto p2: p.second){
        std::cout<<p2<<"\n";
      }
    }



    if (conf.isDefined("MC16eDir")) {
      dir1 = new DirectoryReader(conf.getStr("MC16eDir"),cath);
      mc16e = dir1->GetFilesList();
    }
    
    std::cout<<"making sample cathegorization for MC16e\n\n";
    for (auto p: mc16e){
      std::cout<<p.first<<":\n";
      for (auto p2: p.second){
        std::cout<<p2<<"\n";
      }
    }

      Double_t Nee=0;
      Double_t Negam=0;
      Double_t eeBKG=0;
      Double_t eyBKG=0;
      Double_t dNee=0;
      Double_t dNegam=0;
      Double_t deeBKG=0;
      Double_t deyBKG=0;
      Double_t err=0;

      conf.setValue("Central", "YES");
      conf.setValue("HighPt", "NO");
      auto FakeRateenugamma_CentLE = CalculationFakeRate(mc16a["enugamma"],"enugamma_CentLE",lumi1+lumi2,conf);
      auto FakeRateenugamma_mc16d_CentLE = CalculationFakeRate(mc16d["enugamma"],"enugamma_mc16d_CentLE",lumi3,conf);
      FakeRateenugamma_CentLE+=FakeRateenugamma_mc16d_CentLE;
      auto FakeRateenugamma_mc16e_CentLE = CalculationFakeRate(mc16e["enugamma"],"enugamma_mc16e_CentLE",lumi4,conf);
      FakeRateenugamma_CentLE+=FakeRateenugamma_mc16e_CentLE;


      Nee=FakeRateenugamma_CentLE.hists["M_yee"]->IntegralAndError(1,FakeRateenugamma_CentLE.hists["M_yee"]->GetNbinsX(),dNee);
      Negam=FakeRateenugamma_CentLE.hists["M_yey"]->IntegralAndError(1,FakeRateenugamma_CentLE.hists["M_yey"]->GetNbinsX(),dNegam);
      eeBKG=FakeRateenugamma_CentLE.hists["M_yee_b"]->IntegralAndError(1,FakeRateenugamma_CentLE.hists["M_yee_b"]->GetNbinsX(),deeBKG);
      eyBKG=FakeRateenugamma_CentLE.hists["M_yey_b"]->IntegralAndError(1,FakeRateenugamma_CentLE.hists["M_yey_b"]->GetNbinsX(),deyBKG);

      cout<<"central LowPt\n";
      cout<<"N ee in the peak="<<Nee<<"±"<<dNee<<endl;
      cout<<"N e gam in the peak="<<Negam<<"±"<<dNegam<<endl;
      cout<<"N ee all="<<eeBKG<<"±"<<deeBKG<<endl;
      cout<<"N e gam all="<<eyBKG<<"±"<<deyBKG<<endl;

      if (conf.getBool("Plots")||conf.getBool("PlotsMass")) {
        FakeRatePlotter p_cent_le = FakeRatePlotter(FakeRateenugamma_CentLE.hists, conf);
      }

      conf.setValue("Central", "YES");
      conf.setValue("HighPt", "YES");
      auto FakeRateenugamma_CentHE = CalculationFakeRate(mc16a["enugamma"],"enugamma_CentHE",lumi1+lumi2,conf);
      auto FakeRateenugamma_mc16d_CentHE = CalculationFakeRate(mc16d["enugamma"],"enugamma_mc16d_CentHE",lumi3,conf);
      FakeRateenugamma_CentHE+=FakeRateenugamma_mc16d_CentHE;
      auto FakeRateenugamma_mc16e_CentHE = CalculationFakeRate(mc16e["enugamma"],"enugamma_mc16e_CentHE",lumi4,conf);
      FakeRateenugamma_CentHE+=FakeRateenugamma_mc16e_CentHE;

      Nee=FakeRateenugamma_CentHE.hists["M_yee"]->IntegralAndError(1,FakeRateenugamma_CentHE.hists["M_yee"]->GetNbinsX(),dNee);
      Negam=FakeRateenugamma_CentHE.hists["M_yey"]->IntegralAndError(1,FakeRateenugamma_CentHE.hists["M_yey"]->GetNbinsX(),dNegam);
      eeBKG=FakeRateenugamma_CentHE.hists["M_yee_b"]->IntegralAndError(1,FakeRateenugamma_CentHE.hists["M_yee_b"]->GetNbinsX(),deeBKG);
      eyBKG=FakeRateenugamma_CentHE.hists["M_yey_b"]->IntegralAndError(1,FakeRateenugamma_CentHE.hists["M_yey_b"]->GetNbinsX(),deyBKG);

      cout<<"central HighPt\n";
      cout<<"N ee in the peak="<<Nee<<"±"<<dNee<<endl;
      cout<<"N e gam in the peak="<<Negam<<"±"<<dNegam<<endl;
      cout<<"N ee all="<<eeBKG<<"±"<<deeBKG<<endl;
      cout<<"N e gam all="<<eyBKG<<"±"<<deyBKG<<endl;


      if (conf.getBool("PlotsMass")) {
        FakeRatePlotter p_cent_he = FakeRatePlotter(FakeRateenugamma_CentHE.hists, conf);
      }
      conf.setValue("Central", "NO");
      conf.setValue("HighPt", "NO");
      auto FakeRateenugamma_forw = CalculationFakeRate(mc16a["enugamma"],"enugamma_forw",lumi1+lumi2,conf);
      auto FakeRateenugamma_mc16d_forw = CalculationFakeRate(mc16d["enugamma"],"enugamma_mc16d_forw",lumi3,conf);
      FakeRateenugamma_forw+=FakeRateenugamma_mc16d_forw;
      auto FakeRateenugamma_mc16e_forw = CalculationFakeRate(mc16e["enugamma"],"enugamma_mc16e_forw",lumi4,conf);
      FakeRateenugamma_forw+=FakeRateenugamma_mc16e_forw;

      Nee=FakeRateenugamma_forw.hists["M_yee"]->IntegralAndError(1,FakeRateenugamma_forw.hists["M_yee"]->GetNbinsX(),dNee);
      Negam=FakeRateenugamma_forw.hists["M_yey"]->IntegralAndError(1,FakeRateenugamma_forw.hists["M_yey"]->GetNbinsX(),dNegam);
      eeBKG=FakeRateenugamma_forw.hists["M_yee_b"]->IntegralAndError(1,FakeRateenugamma_forw.hists["M_yee_b"]->GetNbinsX(),deeBKG);
      eyBKG=FakeRateenugamma_forw.hists["M_yey_b"]->IntegralAndError(1,FakeRateenugamma_forw.hists["M_yey_b"]->GetNbinsX(),deyBKG);

      cout<<"forward\n";
      cout<<"N ee in the peak="<<Nee<<"±"<<dNee<<endl;
      cout<<"N e gam in the peak="<<Negam<<"±"<<dNegam<<endl;
      cout<<"N ee all="<<eeBKG<<"±"<<deeBKG<<endl;
      cout<<"N e gam all="<<eyBKG<<"±"<<deyBKG<<endl;

      if (conf.getBool("Plots")||conf.getBool("PlotsMass")) {
        FakeRatePlotter p_forw = FakeRatePlotter(FakeRateenugamma_forw.hists, conf);
      }



      //   conf.setValue("mjj","0");
      //   conf.setValue("GamCentrality","0");
      //   conf.setValue("angular","NO");
      //   conf.setValue("VBSjets","NO");
      //   conf.setValue("CR2","NO");
      //   auto WenuSR = CalculationWenu(mc16a["Wenu_"],"WenuSR",lumi1+lumi2,conf);
      //   auto WenuSR_mc16d = CalculationWenu(mc16d["Wenu_"],"WenuSR_mc16d",lumi3,conf);
      //   WenuSR+=WenuSR_mc16d;
      //   auto WenuSR_mc16e = CalculationWenu(mc16e["Wenu_"],"WenuSR_mc16e",lumi4,conf);
      //   WenuSR+=WenuSR_mc16e;     

      //   conf.setValue("CR2","YES");
      //   auto WenuCR2 = CalculationWenu(mc16a["Wenu_"],"WenuCR2",lumi1+lumi2,conf);
      //   auto WenuCR2_mc16d = CalculationWenu(mc16d["Wenu_"],"WenuCR2_mc16d",lumi3,conf);
      //   WenuCR2+=WenuCR2_mc16d;
      //   auto WenuCR2_mc16e = CalculationWenu(mc16e["Wenu_"],"WenuCR2_mc16e",lumi4,conf);
      //   WenuCR2+=WenuCR2_mc16e;     

      // if (conf.getBool("Plots")){
      //   FakeRatePlotter *Wenu_plots = 0;
      //   Wenu_plots->FakeRateComp(FakeRateZee_CentLE.hists, FakeRateZee_forw.hists, WenuSR.hists, WenuCR2.hists, conf);
      // }
      // conf.setValue("Central", "YES");
      // conf.setValue("HighPt", "NO");
      // auto FakeRateDYee_CentLE = CalculationFakeRate(mc16a["DYee"],"DYee_CentLE",lumi1+lumi2,conf);
      // auto FakeRateDYee_mc16d_CentLE = CalculationFakeRate(mc16d["DYee"],"DYee_mc16d_CentLE",lumi3,conf);
      // FakeRateDYee_CentLE+=FakeRateDYee_mc16d_CentLE;
      // auto FakeRateDYee_mc16e_CentLE = CalculationFakeRate(mc16e["DYee"],"DYee_mc16e_CentLE",lumi4,conf);
      // FakeRateDYee_CentLE+=FakeRateDYee_mc16e_CentLE;

      // auto FakeRateDYee_CentLE2dim = CalculationFakeRate2dim(mc16a["DYee"],"DYee_CentLE",lumi1+lumi2,conf);
      // auto FakeRateDYee_mc16d_CentLE2dim = CalculationFakeRate2dim(mc16d["DYee"],"DYee_mc16d_CentLE",lumi3,conf);
      // FakeRateDYee_CentLE2dim+=FakeRateDYee_mc16d_CentLE2dim;
      // auto FakeRateDYee_mc16e_CentLE2dim = CalculationFakeRate2dim(mc16e["DYee"],"DYee_mc16e_CentLE",lumi4,conf);
      // FakeRateDYee_CentLE2dim+=FakeRateDYee_mc16e_CentLE2dim;

      // Plotter *drawing=0;
      // if (conf.getBool("PlotsMass")) {
      //   FakeRatePlotter p_cent_le = FakeRatePlotter(FakeRateDYee_CentLE.hists, conf);
      //   drawing->hist_plotter<TH2F>(FakeRateDYee_CentLE2dim.hists["dRee_vs_Mee"]);
      // }

      // conf.setValue("Central", "YES");
      // conf.setValue("HighPt", "YES");
      // auto FakeRateDYee_CentHE = CalculationFakeRate(mc16a["DYee"],"DYee_CentHE",lumi1+lumi2,conf);
      // auto FakeRateDYee_mc16d_CentHE = CalculationFakeRate(mc16d["DYee"],"DYee_mc16d_CentHE",lumi3,conf);
      // FakeRateDYee_CentHE+=FakeRateDYee_mc16d_CentHE;
      // auto FakeRateDYee_mc16e_CentHE = CalculationFakeRate(mc16e["DYee"],"DYee_mc16e_CentHE",lumi4,conf);
      // FakeRateDYee_CentHE+=FakeRateDYee_mc16e_CentHE;

      // auto FakeRateDYee_CentHE2dim = CalculationFakeRate2dim(mc16a["DYee"],"DYee_CentHE",lumi1+lumi2,conf);
      // auto FakeRateDYee_mc16d_CentHE2dim = CalculationFakeRate2dim(mc16d["DYee"],"DYee_mc16d_CentHE",lumi3,conf);
      // FakeRateDYee_CentHE2dim+=FakeRateDYee_mc16d_CentHE2dim;
      // auto FakeRateDYee_mc16e_CentHE2dim = CalculationFakeRate2dim(mc16e["DYee"],"DYee_mc16e_CentHE",lumi4,conf);
      // FakeRateDYee_CentHE2dim+=FakeRateDYee_mc16e_CentHE2dim;

      // if (conf.getBool("PlotsMass")) {
      //   FakeRatePlotter p_cent_he = FakeRatePlotter(FakeRateDYee_CentHE.hists, conf);
      //   drawing->hist_plotter<TH2F>(FakeRateDYee_CentHE2dim.hists["dRee_vs_Mee"]);
      // }
      // conf.setValue("Central", "NO");
      // conf.setValue("HighPt", "NO");
      // auto FakeRateDYee_forw = CalculationFakeRate(mc16a["DYee"],"DYee_forw",lumi1+lumi2,conf);
      // auto FakeRateDYee_mc16d_forw = CalculationFakeRate(mc16d["DYee"],"DYee_mc16d_forw",lumi3,conf);
      // FakeRateDYee_forw+=FakeRateDYee_mc16d_forw;
      // auto FakeRateDYee_mc16e_forw = CalculationFakeRate(mc16e["DYee"],"DYee_mc16e_forw",lumi4,conf);
      // FakeRateDYee_forw+=FakeRateDYee_mc16e_forw;

      // auto FakeRateDYee_forw2dim = CalculationFakeRate2dim(mc16a["DYee"],"DYee_forw",lumi1+lumi2,conf);
      // auto FakeRateDYee_mc16d_forw2dim = CalculationFakeRate2dim(mc16d["DYee"],"DYee_mc16d_forw",lumi3,conf);
      // FakeRateDYee_forw2dim+=FakeRateDYee_mc16d_forw2dim;
      // auto FakeRateDYee_mc16e_forw2dim = CalculationFakeRate2dim(mc16e["DYee"],"DYee_mc16e_forw",lumi4,conf);
      // FakeRateDYee_forw2dim+=FakeRateDYee_mc16e_forw2dim;

      // if (conf.getBool("PlotsMass")) {
      //   FakeRatePlotter p_forw = FakeRatePlotter(FakeRateDYee_forw.hists, conf);
      //   drawing->hist_plotter<TH2F>(FakeRateDYee_forw2dim.hists["dRee_vs_Mee"]);
      // }

  }
  else {

    std::cout<<"calculating fake rate from data\n";
    if (conf.isDefined("Data")) {
    data = {conf.getStr("Data")};
    }
    else fatal("Data directory is not set");
    if (conf.getBool("bkgSyst")) {
      auto Data = EToGamReader();
      Data.getFakeRate(data,lumi);
    }
    else{
      if (conf.getBool("EtaSyst")){
        Double_t Nee, Negam, BkgNoSub, BkgEta, EtaUnc;
        Nee=0; Negam=0; BkgEta=0; BkgNoSub=0; EtaUnc=0;
        conf.setValue("Central", "NO");
        conf.setValue("HighPt", "NO");
        auto FakeRateForw = CalculationFakeRate(data,"data_forw",lumi,conf);
        auto emetForw = CalculationEToGam(data,"emetForw",lumi,conf);
        std::cout<<"forward emet "<<emetForw.hists["y_eta"]->IntegralAndError(1,emetForw.hists["y_eta"]->GetNbinsX(),err)<<"+-"<<err<<"\n";
        Nee=FakeRateForw.hists["M_yee"]->Integral(); Negam=FakeRateForw.hists["M_yey"]->Integral();
        printf("\nNegam %f Nee %f fake rate w/o subt %.6f±%.6f\n",Negam,Nee,Negam/Nee,Negam/Nee*sqrt(1/Negam+1/Nee));
        BkgNoSub=Negam/Nee*emetForw.hists["y_eta"]->Integral(); unc+=Negam/Nee*Negam/Nee*emetForw.hists["y_eta"]->Integral(); EtaUnc=emetForw.hists["y_eta"]->Integral();
        TH1D *rate_eta_f = (TH1D*)FakeRateForw.hists["eta_yey"]->Clone();
        rate_eta_f->Divide(FakeRateForw.hists["eta_yee"]);
        emetForw.hists["eta_yey"]->Multiply(rate_eta_f);
        BkgEta=emetForw.hists["eta_yey"]->Integral();
        if (conf.getBool("Plots")||conf.getBool("PlotsMass")) {
          FakeRatePlotter p_forw = FakeRatePlotter(FakeRateForw.hists, conf);
          drawing.hist_plotter<TH1D>(emetForw.hists["eta_yey"]);
        }

        conf.setValue("Central", "YES");
        conf.setValue("HighPt", "NO");
        auto FakeRateCentLE = CalculationFakeRate(data,"data_cent_le",lumi,conf);
        auto emetCentLe = CalculationEToGam(data,"emetCentLe",lumi,conf);
        std::cout<<"central pT<250 GeV emet "<<emetCentLe.hists["y_eta"]->IntegralAndError(1,emetCentLe.hists["y_eta"]->GetNbinsX(),err)<<"+-"<<err<<"\n";
        Nee=FakeRateCentLE.hists["M_yee"]->Integral(); Negam=FakeRateCentLE.hists["M_yey"]->Integral();
        printf("\nNegam %f Nee %f fake rate w/o subt %.6f±%.6f\n",Negam,Nee,Negam/Nee,Negam/Nee*sqrt(1/Negam+1/Nee));
        BkgNoSub+=Negam/Nee*emetCentLe.hists["y_eta"]->Integral(); unc+=Negam/Nee*Negam/Nee*emetCentLe.hists["y_eta"]->Integral(); EtaUnc+=emetCentLe.hists["y_eta"]->Integral();
        TH1D *rate_eta_le = (TH1D*)FakeRateCentLE.hists["eta_yey"]->Clone();
        rate_eta_le->Divide(FakeRateCentLE.hists["eta_yee"]);
        emetCentLe.hists["eta_yey"]->Multiply(rate_eta_le);  
        BkgEta+=emetCentLe.hists["eta_yey"]->Integral();      
        if (conf.getBool("Plots")||conf.getBool("PlotsMass")) {
          FakeRatePlotter p_cent_le = FakeRatePlotter(FakeRateCentLE.hists, conf);
          drawing.hist_plotter<TH1D>(emetCentLe.hists["eta_yey"]);
        }

        conf.setValue("Central", "YES");
        conf.setValue("HighPt", "YES");
        auto FakeRateCentHE = CalculationFakeRate(data,"data_cent_he",lumi,conf);
        auto emetCentHe = CalculationEToGam(data,"emetCentHe",lumi,conf);
        std::cout<<"central pT>250 GeV emet "<<emetCentHe.hists["y_eta"]->IntegralAndError(1,emetCentHe.hists["y_eta"]->GetNbinsX(),err)<<"+-"<<err<<"\n";
        Nee=FakeRateCentHE.hists["M_yee"]->Integral(); Negam=FakeRateCentHE.hists["M_yey"]->Integral();
        printf("\nNegam %f Nee %f fake rate w/o subt %.6f±%.6f\n",Negam,Nee,Negam/Nee,Negam/Nee*sqrt(1/Negam+1/Nee));
        BkgNoSub+=Negam/Nee*emetCentHe.hists["y_eta"]->Integral(); unc+=Negam/Nee*Negam/Nee*emetCentLe.hists["y_eta"]->Integral(); EtaUnc+=emetCentHe.hists["y_eta"]->Integral();
        TH1D *rate_eta_he = (TH1D*)FakeRateCentHE.hists["eta_yey"]->Clone();
        rate_eta_he->Divide(FakeRateCentHE.hists["eta_yee"]);
        emetCentHe.hists["eta_yey"]->Multiply(rate_eta_he);
        BkgEta+=emetCentHe.hists["eta_yey"]->Integral();      
        if (conf.getBool("Plots")||conf.getBool("PlotsMass")) {
          FakeRatePlotter p_cent_he = FakeRatePlotter(FakeRateCentHE.hists, conf);
          drawing.hist_plotter<TH1D>(emetCentHe.hists["eta_yey"]);
        }
        cout<<"total w/o subt "<<BkgNoSub<<"±"<<sqrt(unc)<<endl; 
        cout<<"total w eta binning "<<BkgEta<<"±"<<sqrt(EtaUnc)/EtaUnc*BkgEta<<endl; 
        cout<<"diff "<<fabs(BkgEta-BkgNoSub)/min(BkgEta,BkgNoSub)<<endl; 

      } 
      else {
        auto Data = CalculationEToGam(data,"Data",lumi,conf);
        std::cout<<"etogam bkg data-driven "<<Data.hists["n_el"]->IntegralAndError(1,Data.hists["n_el"]->GetNbinsX(),err)<<"+-"<<err<<"\n";
      }
    }
  }
  if (conf.getBool("Plots")||conf.getBool("PlotsMass")||conf.getBool("bkgSyst")||conf.getBool("bkgFit")) theApp.Run();

}

void check(int argc, char **argv){
  Config conf("../ZGamma.cfg");
  // double lumi = conf.getNum("TotalLumi");
  // StrV data;
  // if (conf.isDefined("Data")) {
  // data = {conf.getStr("Data")};
  // }
  // else fatal("Data directory is not set");
  // Calculation Data = Calculation(data,"Data_15-17",lumi,conf);
  Calculation JetToGam = Calculation();
  JetToGam.SName="Znunu";
  TreeReader *nominal = 0;
  JetToGam.hists=nominal->getInitialMap(JetToGam.SName, conf);
  JetToGam.hists.fillFromFile(conf.getStr("inputHist.JetToGam"));
}

void isocheck(int argc, char **argv){
  Config conf("../ZGamma.cfg");
  StrV cath = {"364525","504678","545839","700404"}; 
  TApplication theApp("tapp", &argc, argv);
  map<TString,StrV> mc16a,mc16d,mc16e;
  DirectoryReader *dir1=0;
  double lumi;
  lumi = conf.getNum("Lumi2015")+conf.getNum("Lumi2016");
  if (conf.isDefined("MC16aDir")) {
    dir1 = new DirectoryReader(conf.getStr("MC16aDir"),cath);
    mc16a = dir1->GetFilesList();
  } 
  std::cout<<"making sample cathegorization for MC16a\n\n";
  for (auto p: mc16a){
    std::cout<<p.first<<":\n";
    for (auto p2: p.second){
      std::cout<<p2<<"\n";
    }
  }
  // std::cout<<"mc16a"<<":\n";
  // for (auto p2: mc16a["nugamma_pty_140"]){
  //   std::cout<<p2<<"\n";
  // }
  // Calculation WgamNominalA = Calculation(mc16a["364525"],"WgamNominalA",lumi,conf);
  // Calculation WgamOldA = Calculation(mc16a["504678"],"WgamOldA",lumi,conf);
  // Calculation WgamNewA = Calculation(mc16a["545839"],"WgamNewA",lumi,conf);
  Calculation Wgam11A = Calculation(mc16a["700404"],"Wgam11A",lumi,conf);

  lumi = conf.getNum("Lumi2017");
  if (conf.isDefined("MC16dDir")) {
    dir1 = new DirectoryReader(conf.getStr("MC16dDir"),cath);
    mc16d = dir1->GetFilesList();
  }   
  for (auto p: mc16d){
    std::cout<<p.first<<":\n";
    for (auto p2: p.second){
      std::cout<<p2<<"\n";
    }
  }
  // std::cout<<"mc16d"<<":\n";
  // for (auto p2: mc16d["nugamma_pty_140"]){
  //   std::cout<<p2<<"\n";
  // }
  // Calculation WgamNominalD = Calculation(mc16d["364525"],"WgamNominalD",lumi,conf);
  // Calculation WgamOldD = Calculation(mc16d["504678"],"WgamOldD",lumi,conf);
  // Calculation WgamNewD = Calculation(mc16d["545839"],"WgamNewD",lumi,conf);
  Calculation Wgam11D = Calculation(mc16d["700404"],"Wgam11D",lumi,conf);

  // WgamNominalA+=WgamNominalD;
  // WgamOldA+=WgamOldD;
  // WgamNewA+=WgamNewD;
  Wgam11A+=Wgam11D;

  lumi = conf.getNum("Lumi2018");
  if (conf.isDefined("MC16eDir")) {
    dir1 = new DirectoryReader(conf.getStr("MC16eDir"),cath);
    mc16e = dir1->GetFilesList();
  }   
  for (auto p: mc16e){
    std::cout<<p.first<<":\n";
    for (auto p2: p.second){
      std::cout<<p2<<"\n";
    }
  }
  // std::cout<<"mc16e"<<":\n";
  // for (auto p2: mc16e["nugamma_pty_140"]){
  //   std::cout<<p2<<"\n";
  // }
  // Calculation WgamNominalE = Calculation(mc16e["364525"],"WgamNominalE",lumi,conf);
  // Calculation WgamOldE = Calculation(mc16e["504678"],"WgamOldE",lumi,conf);
  // Calculation WgamNewE = Calculation(mc16e["545839"],"WgamNewE",lumi,conf);
  Calculation Wgam11E = Calculation(mc16e["700404"],"Wgam11E",lumi,conf);
  // WgamNominalA+=WgamNominalE;
  // WgamOldA+=WgamOldE;
  // WgamNewA+=WgamNewE;
  Wgam11A+=Wgam11E;
  
  double err=0;
  // std::cout<<"Total WgamNominalA "<<WgamNominalA.hists["n_ph"]->IntegralAndError(1,WgamNominalA.hists["n_ph"]->GetNbinsX(),err)<<"+-"<<err<<"\n";
  // std::cout<<"Total WgamOldA "<<WgamOldA.hists["n_ph"]->IntegralAndError(1,WgamOldA.hists["n_ph"]->GetNbinsX(),err)<<"+-"<<err<<"\n";
  // std::cout<<"Total WgamNewA "<<mWgamNewA.hists["n_ph"]->IntegralAndError(1,WgamNewA.hists["n_ph"]->GetNbinsX(),err)<<"+-"<<err<<"\n";
  std::cout<<"Total Wgam11A "<<Wgam11A.hists["n_ph"]->IntegralAndError(1,Wgam11A.hists["n_ph"]->GetNbinsX(),err)<<"+-"<<err<<"\n";
  // Plotter drawing;
  // drawing.hist_plotter<TH1D,TH1D>(WgamOldA.hists["pT_lead_y"],WgamNewA.hists["pT_lead_y"]);
  // theApp.Run();

}

void hist2dim(int argc, char **argv){
  Config conf("../ZGamma.cfg");
  TApplication theApp("tapp", &argc, argv);
  Calculation::CalcV vec;
  DirectoryReader *dir1=0;
  DirectoryReader *dir2=0;
  map<TString,StrV> filename;
  // TApplication theApp("tapp", &argc, argv);
  StrV cath = {"nunugamma","Znunu_","Wenu","SinglePhotonPt","nugamma","gamma", "Znunugamma2jEWK", "nugamma2jEWK", "jets_JZ", "ttbar", "singletop"};

  if (conf.isDefined("MC16aDir")) {
    dir1 = new DirectoryReader(conf.getStr("MC16aDir"),cath);
    filename = dir1->GetFilesList();
  }
  std::cout<<"making sample cathegorization for MC16a\n\n";

    std::cout<<"Wenu"<<":\n";
    for (auto p2: filename["Wenu"]){
      std::cout<<p2<<"\n";
    }
  std::cout<<"\n";
  double err;
  double lumi = conf.getNum("Lumi2015")+conf.getNum("Lumi2016");

  auto EToGam = Calculation2dim(filename["Wenu"],"Wenu_16a",lumi);
  // auto EToGam1 = Calculation(filename["Wenu"],"Wenu_16a_1dim",lumi);

  std::cout<<EToGam.SName<<" Total: "<<EToGam.hists["e_pT_vs_n_jet"]->IntegralAndError(1,EToGam.hists["e_pT_vs_n_jet"]->GetNbinsX(),1,EToGam.hists["e_pT_vs_n_jet"]->GetNbinsY(),err)<<"+-"<<err<<std::endl;
  // std::cout<<EToGam1.SName<<" Total: "<<EToGam1.hists["n_el"]->IntegralAndError(1,EToGam1.hists["n_el"]->GetNbinsX(),err)<<"+-"<<err<<std::endl;

  if (conf.isDefined("MC16dDir")){
    dir1->SetDirectory(conf.getStr("MC16dDir"));
    filename = dir1->GetFilesList();
  }

  std::cout<<"\n making sample cathegorization for MC16d\n\n";
   std::cout<<"Wenu"<<":\n";
    for (auto p2: filename["Wenu"]){
      std::cout<<p2<<"\n";
    }
  std::cout<<"\n";
  lumi = conf.getNum("Lumi2017");

  auto EToGamD = Calculation2dim(filename["Wenu"],"Wenu_16d",lumi);
  EToGam+=EToGamD;
  // auto EToGamD1 = Calculation(filename["Wenu"],"Wenu_16d_1dim",lumi);
  // EToGam1+=EToGamD1;
  std::cout<<EToGamD.SName<<" Total: "<<EToGamD.hists["e_pT_vs_n_jet"]->IntegralAndError(1,EToGamD.hists["e_pT_vs_n_jet"]->GetNbinsX(),1,EToGam.hists["e_pT_vs_n_jet"]->GetNbinsY(),err)<<"+-"<<err<<std::endl;
  // std::cout<<EToGamD1.SName<<" Total: "<<EToGamD1.hists["n_el"]->IntegralAndError(1,EToGamD1.hists["n_el"]->GetNbinsX(),err)<<"+-"<<err<<std::endl;

  cath = {"nunugamma","Znunu_","Wenu","SinglePhoton","nugamma","gamma", "Znunugamma2jEWK", "nugamma2jEWK", "jets_JZ", "ttbar", "singletop"};


  if (conf.isDefined("MC16eDir")){
    dir2 = new DirectoryReader(conf.getStr("MC16eDir"),cath);
    filename = dir2->GetFilesList();
  }

  std::cout<<"\n making sample cathegorization for MC16e\n\n";
   std::cout<<"Wenu"<<":\n";
    for (auto p2: filename["Wenu"]){
      std::cout<<p2<<"\n";
    }
  std::cout<<"\n";

  lumi = conf.getNum("Lumi2018");

  auto EToGamE = Calculation2dim(filename["Wenu"],"Wenu_16e",lumi);
  EToGam+=EToGamE;
  // auto EToGamE1 = Calculation(filename["Wenu"],"Wenu_16e_1dim",lumi);
  // EToGam1+=EToGamE1;
  std::cout<<EToGamE.SName<<" Total: "<<EToGamE.hists["e_pT_vs_n_jet"]->IntegralAndError(1,EToGamE.hists["e_pT_vs_n_jet"]->GetNbinsX(),1,EToGam.hists["e_pT_vs_n_jet"]->GetNbinsY(),err)<<"+-"<<err<<std::endl;
  // std::cout<<EToGamE1.SName<<" Total: "<<EToGamE1.hists["n_el"]->IntegralAndError(1,EToGamE1.hists["n_el"]->GetNbinsX(),err)<<"+-"<<err<<std::endl;


  std::cout<<EToGam.SName<<" Total: "<<EToGam.hists["e_pT_vs_n_jet"]->IntegralAndError(1,EToGam.hists["e_pT_vs_n_jet"]->GetNbinsX(),1,EToGam.hists["e_pT_vs_n_jet"]->GetNbinsY(),err)<<"+-"<<err<<std::endl;
  // std::cout<<EToGam1.SName<<" Total: "<<EToGam1.hists["n_el"]->IntegralAndError(1,EToGam1.hists["n_el"]->GetNbinsX(),err)<<"+-"<<err<<std::endl;
  EToGam.SName="Wenu";
  Plotter drawing;
  drawing.hist_plotter<TH2F, TProfile>(EToGam.hists["e_pT_vs_n_jet"], EToGam.hists["e_pT_vs_n_jet"]->ProfileX());
  // drawing.hist_plotter<TProfile>(EToGam.hists["e_pT_vs_n_jet"]->ProfileX());
  theApp.Run();
}

void bugfixing(int argc, char **argv){
  Config conf("../ZGamma.cfg");
  TApplication theApp("tapp", &argc, argv);
  StrV Wgam;
  double err=0;
  double lumi = conf.getNum("Lumi2015")+conf.getNum("Lumi2016");

  if (conf.isDefined("WgamOld")) {
    Wgam = {conf.getStr("WgamOld")};
  }
  Calculation Wgamma = Calculation(Wgam,"Wgamma_old_16a",lumi,conf);
  std::cout<<"Wtaunugamma old "<<Wgamma.hists["n_el"]->IntegralAndError(1,Wgamma.hists["n_el"]->GetNbinsX(),err)<<"+-"<<err<<"\n";

  if (conf.isDefined("WgamOldCross")) {
    Wgam = {conf.getStr("WgamOldCross")};
  }
  Calculation WgammaCross = Calculation(Wgam,"Wgamma_oldCross_16a",lumi,conf);
  std::cout<<"Wtaunugamma old "<<WgammaCross.hists["n_el"]->IntegralAndError(1,WgammaCross.hists["n_el"]->GetNbinsX(),err)<<"+-"<<err<<"\n";

  if (conf.isDefined("WgamNewRel")) {
    Wgam = {conf.getStr("WgamNewRel")};
  }
  Calculation WgammaNewRel = Calculation(Wgam,"Wgamma_NewRel_16a",lumi,conf);
  std::cout<<"Wtaunugamma NewRel "<<WgammaNewRel.hists["n_el"]->IntegralAndError(1,WgammaNewRel.hists["n_el"]->GetNbinsX(),err)<<"+-"<<err<<"\n";

  if (conf.isDefined("WgamNewHGam")) {
    Wgam = {conf.getStr("WgamNewHGam")};
  }
  Calculation WgammaNewHGam = Calculation(Wgam,"Wgamma_NewHGam_16a",lumi,conf);
  std::cout<<"Wtaunugamma NewHGam "<<WgammaNewHGam.hists["n_el"]->IntegralAndError(1,WgammaNewHGam.hists["n_el"]->GetNbinsX(),err)<<"+-"<<err<<"\n";

  if (conf.isDefined("WgamNewFramework")) {
    Wgam = {conf.getStr("WgamNewFramework")};
  }
  Calculation WgammaNewFramework = Calculation(Wgam,"Wgamma_NewFramework_16a",lumi,conf);
  std::cout<<"Wtaunugamma NewFramework "<<WgammaNewFramework.hists["n_el"]->IntegralAndError(1,WgammaNewFramework.hists["n_el"]->GetNbinsX(),err)<<"+-"<<err<<"\n";

  if (conf.isDefined("WgamNewFrameworkOldIso")) {
    Wgam = {conf.getStr("WgamNewFrameworkOldIso")};
  }
  Calculation WgammaNewFrameworkOldIso = Calculation(Wgam,"Wgamma_NewFrameworkOldIso_16a",lumi,conf);
  std::cout<<"Wtaunugamma NewFrameworkOldIso "<<WgammaNewFrameworkOldIso.hists["n_el"]->IntegralAndError(1,WgammaNewFrameworkOldIso.hists["n_el"]->GetNbinsX(),err)<<"+-"<<err<<"\n";

  Plotter drawing;
  // drawing.hist_plotter<TH1D, TH1D>(WgammaCross.hists["pT_lead_y"], WgammaNewFramework.hists["pT_lead_y"]);
  drawing.hist_plotter<TH1D, TH1D>(WgammaCross.hists["pT_MET"], WgammaNewFramework.hists["pT_MET"]);
  // drawing.hist_plotter<TH1D, TH1D>(WgammaCross.hists["n_mu"], WgammaNewFramework.hists["n_mu"]);
  // drawing.hist_plotter<TH1D, TH1D>(WgammaCross.hists["n_ph"], WgammaNewFramework.hists["n_ph"]);
  // drawing.hist_plotter<TH1D, TH1D>(WgammaCross.hists["n_el"], WgammaNewFramework.hists["n_el"]);
  theApp.Run();

}

void sumcheck(int argc, char **argv){
  Config conf("../ZGamma.cfg");
  TApplication theApp("tapp", &argc, argv);
  double lumi = conf.getNum("Lumi2018");
  StrV data;
  if (conf.isDefined("sumcheckFile")) {
  data = {conf.getStr("sumcheckFile")};
  }
  else fatal("Directory is not set");
  Calculation file = Calculation(data,"file",lumi,conf);
  Plotter drawing;
  drawing.hist_plotter<TH1D>(file.hists["n_lep"]);
  theApp.Run();
}


void leptonv(int argc, char **argv){
  Config conf("../ZGamma.cfg");
  TApplication theApp("tapp", &argc, argv);
  map<TString,StrV> filename;
  double lumi = conf.getNum("Lumi2018");
  StrV samples, muons, elecs;
  DirectoryReader *dir1=0;
  double err;

  samples = {"nunugamma","nugamma"};
    dir1 = new DirectoryReader("../../lepton_WPs",samples);
    filename = dir1->GetFilesList();

  // muons = {"n_mu_Medium_pT7_d0z0_no_iso","n_mu_Loose_pT4_d0z0_no_iso","n_mu_Loose_pT4_d0z0_iso","n_mu_Medium_pT4_d0z0_no_iso"};
  // elecs = {"n_e_looseBL_pT45_no_d0z0_no_iso","n_e_looseBL_pT45_no_d0z0_iso","n_e_looseBL_pT45_d0z0_no_iso","n_e_looseBL_pT7_d0z0_no_iso"};
  Calculation Zgam;
  Calculation Wgam;
  double S, B, Nom_S, Nom_B;
  // muons = {"n_mu_Medium_pT7_d0z0_iso"};
  // elecs = {"n_e_looseBL_pT45_no_d0z0_no_iso","n_e_looseBL_pT45_no_d0z0_iso","n_e_looseBL_pT45_d0z0_no_iso","n_e_looseBL_pT7_d0z0_no_iso"};
  elecs = {"n_e_looseBL_pT7_d0z0_iso"};
  muons = {"n_mu_Medium_pT7_d0z0_no_iso","n_mu_Loose_pT4_d0z0_no_iso","n_mu_Loose_pT4_d0z0_iso","n_mu_Medium_pT4_d0z0_no_iso","n_mu_Medium_pT7_d0z0_iso"};
  conf.setValue("veto","NO");

      conf.setValue("N_Electrons","n_e_looseBL_pT7_d0z0_iso");
      conf.setValue("N_Muons","n_mu_Medium_pT7_d0z0_iso");
      Zgam = Calculation(filename["nunugamma"],"Zgam"+conf.getStr("N_Electrons")+conf.getStr("N_Muons"),lumi,conf);
      Wgam = Calculation(filename["nugamma"],"Wgam"+conf.getStr("N_Electrons")+conf.getStr("N_Muons"),lumi,conf);
      std::cout<<"lepton veto "<<conf.getBool("veto")<<conf.getStr("N_Electrons")<<" "<<conf.getStr("N_Muons")<<"\n";
      Nom_S=Zgam.hists["n_el"]->IntegralAndError(1,Zgam.hists["n_el"]->GetNbinsX(),err);
      std::cout<<"Zgam "<<Nom_S<<"±"<<err<<"\n";
      Nom_B=Wgam.hists["n_el"]->IntegralAndError(1,Wgam.hists["n_el"]->GetNbinsX(),err);
      std::cout<<"Wgam "<<Nom_B<<"±"<<err<<"\n";

  for (auto mu: muons){
    for (auto el: elecs){
      conf.setValue("veto","YES");
      conf.setValue("N_Electrons",el);
      conf.setValue("N_Muons",mu);
      Zgam = Calculation(filename["nunugamma"],"Zgam"+conf.getStr("N_Electrons")+conf.getStr("N_Muons"),lumi,conf);
      Wgam = Calculation(filename["nugamma"],"Wgam"+conf.getStr("N_Electrons")+conf.getStr("N_Muons"),lumi,conf);
      std::cout<<"lepton veto "<<conf.getBool("veto")<<conf.getStr("N_Electrons")<<" "<<conf.getStr("N_Muons")<<"\n";
      S=Zgam.hists["n_el"]->IntegralAndError(1,Zgam.hists["n_el"]->GetNbinsX(),err);
      std::cout<<"Zgam "<<S<<"±"<<err<<" efficiency "<<S/Nom_S<<"\n";
      B=Wgam.hists["n_el"]->IntegralAndError(1,Wgam.hists["n_el"]->GetNbinsX(),err);
      std::cout<<B<<"±"<<err<<" efficiency "<<B/Nom_B<<"\n";
      std::cout<<"significance "<<S/sqrt(S+B)<<"\n";
    }
  }

}



void jettoe(int argc, char **argv){
  Config conf("../ZGamma.cfg");
  Calculation::CalcV vec;
  DirectoryReader *dir1=0;
  DirectoryReader *dir2=0;
  map<TString,StrV> filename;
  IsolationPlotter drawing;
  Plotter drawing2;
  TApplication theApp("tapp", &argc, argv);
  StrV cath = {"nunugamma","Znunu_","Wenu","SinglePhotonPt","nugamma","gamma", "Znunugamma2jEWK", "nugamma2jEWK", "jets_JZ", "ttbar", "singletop","ttgamma","Wtaunu_"};

  double errA, errB, errC, errD;
  double B,D,C,A;
  double R, errR;

  if (conf.isDefined("MC16aDir")) {
    dir1 = new DirectoryReader(conf.getStr("MC16aDir"),cath);
    filename = dir1->GetFilesList();
  }
  
  std::cout<<"making sample cathegorization for MC16a\n\n";
  for (auto p: filename){
    std::cout<<p.first<<":\n";
    for (auto p2: p.second){
      std::cout<<p2<<"\n";
    }
  }
  std::cout<<"\n";
  double err;
  double lumi = conf.getNum("Lumi2015")+conf.getNum("Lumi2016");

  CalculationJetToE WGamma = CalculationJetToE(filename["nugamma"],"Wgam_16a",lumi);

  CalculationJetToE ZllGamma = CalculationJetToE(filename["gamma"],"llgam_16a",lumi);

  CalculationJetToE JetToMET = CalculationJetToE(filename["SinglePhotonPt"],"SP_16a",lumi);

  CalculationJetToE EToGam = CalculationJetToE(filename["Wenu"],"Wenu_16a",lumi);

  CalculationJetToE JetToGam = CalculationJetToE(filename["Znunu_"],"Znunu_16a",lumi);

  CalculationJetToE nunugamma = CalculationJetToE(filename["nunugamma"],"nunugam_16a",lumi);

  CalculationJetToE Wgamma2jEWK = CalculationJetToE(filename["nugamma2jEWK"],"Wgamma2jEWK_16a",lumi);

  CalculationJetToE Znunugamma2jEWK = CalculationJetToE(filename["Znunugamma2jEWK"],"nunugamma2jEWK_16a",lumi);

  CalculationJetToE multijet = CalculationJetToE(filename["jets_JZ"],"multijet_16a",lumi);

  CalculationJetToE singletop = CalculationJetToE(filename["singletop"],"singletop_16a",lumi);

  CalculationJetToE tops = CalculationJetToE(filename["ttbar"],"tops_16a",lumi);

  CalculationJetToE tta_1lep = CalculationJetToE(filename["tta"],"tta_1lep_16a",lumi);

  CalculationJetToE Wtaunu = CalculationJetToE(filename["Wtaunu_"],"Wtaunu_16a",lumi);
  

  if (conf.isDefined("MC16dDir")){
    dir1->SetDirectory(conf.getStr("MC16dDir"));
    filename = dir1->GetFilesList();
  }

  std::cout<<"\n making sample cathegorization for MC16d\n\n";
  for (auto p: filename){
    std::cout<<p.first<<":\n";
    for (auto p2: p.second){
      std::cout<<p2<<"\n";
    }
  }

  lumi = conf.getNum("Lumi2017");

  CalculationJetToE WGammaD = CalculationJetToE(filename["nugamma"],"Wgam_16d",lumi);

  CalculationJetToE ZllGammaD = CalculationJetToE(filename["gamma"],"llgam_16d",lumi);

  ZllGamma+=ZllGammaD;
  WGamma+=WGammaD;

  CalculationJetToE JetToMETD = CalculationJetToE(filename["SinglePhotonPt"],"SP_16d",lumi);
  JetToMET+=JetToMETD;

  CalculationJetToE EToGamD = CalculationJetToE(filename["Wenu"],"Wenu_16d",lumi);
  EToGam+=EToGamD;
  
  CalculationJetToE JetToGamD = CalculationJetToE(filename["Znunu_"],"Znunu_16d",lumi);
  JetToGam+=JetToGamD;

  CalculationJetToE nunugammaD = CalculationJetToE(filename["nunugamma"],"nunugam_16d",lumi);
  nunugamma+=nunugammaD;

  CalculationJetToE Wgamma2jEWKD = CalculationJetToE(filename["nugamma2jEWK"],"Wgamma2jEWK_16d",lumi);
  Wgamma2jEWK+=Wgamma2jEWKD;

  CalculationJetToE Znunugamma2jEWKD = CalculationJetToE(filename["Znunugamma2jEWK"],"nunugamma2jEWK_16d",lumi);
  Znunugamma2jEWK+=Znunugamma2jEWKD;

  CalculationJetToE multijetD = CalculationJetToE(filename["jets_JZ"],"multijet_16d",lumi);
  multijet+=multijetD;

  CalculationJetToE singletopD = CalculationJetToE(filename["singletop"],"singletop_16d",lumi);

  CalculationJetToE topsD = CalculationJetToE(filename["ttbar"],"tops_16d",lumi);
  
  topsD+=singletopD;
  tops+=topsD;

  CalculationJetToE ttaD_1lep = CalculationJetToE(filename["tta"],"tta_1lep_16d",lumi);

  tta_1lep+=ttaD_1lep;

  CalculationJetToE WtaunuD = CalculationJetToE(filename["Wtaunu_"],"Wtaunu_16d",lumi);

  Wtaunu+=WtaunuD;

  if (conf.isDefined("MC16eDir")){
    dir2 = new DirectoryReader(conf.getStr("MC16eDir"),cath);
    filename = dir2->GetFilesList();
  }

  std::cout<<"\n making sample cathegorization for MC16e\n\n";
  for (auto p: filename){
    std::cout<<p.first<<":\n";
    for (auto p2: p.second){
      std::cout<<p2<<"\n";
    }
  }

  lumi = conf.getNum("Lumi2018");

  CalculationJetToE WGammaE = CalculationJetToE(filename["nugamma"],"Wgam_16e",lumi);

  CalculationJetToE ZllGammaE = CalculationJetToE(filename["gamma"],"llgam_16e",lumi);

  ZllGamma+=ZllGammaE;
  WGamma+=WGammaE;

  CalculationJetToE JetToMETE = CalculationJetToE(filename["SinglePhotonPt"],"SP_16e",lumi);
  JetToMET+=JetToMETE;

  CalculationJetToE EToGamE = CalculationJetToE(filename["Wenu"],"Wenu_16e",lumi);
  EToGam+=EToGamE;
  
  CalculationJetToE JetToGamE = CalculationJetToE(filename["Znunu_"],"Znunu_16e",lumi);
  JetToGam+=JetToGamE;

  CalculationJetToE nunugammaE = CalculationJetToE(filename["nunugamma"],"nunugam_16e",lumi);
  nunugamma+=nunugammaE;

  CalculationJetToE Wgamma2jEWKE = CalculationJetToE(filename["nugamma2jEWK"],"Wgamma2jEWK_16e",lumi);
  Wgamma2jEWK+=Wgamma2jEWKE;

  CalculationJetToE Znunugamma2jEWKE = CalculationJetToE(filename["Znunugamma2jEWK"],"nunugamma2jEWK_16e",lumi);
  Znunugamma2jEWK+=Znunugamma2jEWKE;

  CalculationJetToE multijetE = CalculationJetToE(filename["jets_JZ"],"multijet_16e",lumi);
  multijet+=multijetE;

  CalculationJetToE singletopE = CalculationJetToE(filename["singletop"],"singletop_16e",lumi);

  CalculationJetToE topsE = CalculationJetToE(filename["ttbar"],"tops_16d",lumi);
  
  topsE+=singletopE;
  tops+=topsE;

  CalculationJetToE ttaE_1lep = CalculationJetToE(filename["tta"],"tta_1lep_16e",lumi);

  tta_1lep+=ttaE_1lep;

  CalculationJetToE WtaunuE = CalculationJetToE(filename["Wtaunu_"],"Wtaunu_16e",lumi);

  Wtaunu+=WtaunuE;

  StrV data;
  if (conf.isDefined("Data")) {
  data = {conf.getStr("Data")};
  }
  else fatal("Data directory is not set");
  auto Data = CalculationJetToE(data,"Data_15-18",conf.getNum("TotalLumi"));

  A=Data.hists["A_region_e_eta"]->IntegralAndError(1,Data.hists["A_region_e_eta"]->GetNbinsX(),errA);
  B=Data.hists["B_region_e_eta"]->IntegralAndError(1,Data.hists["B_region_e_eta"]->GetNbinsX(),errB);
  C=Data.hists["C_region_e_eta"]->IntegralAndError(1,Data.hists["C_region_e_eta"]->GetNbinsX(),errC);
  D=Data.hists["D_region_e_eta"]->IntegralAndError(1,Data.hists["D_region_e_eta"]->GetNbinsX(),errD);
  std::cout<<"Regions before subtraction: \n";
  std::cout<<"A "<<A<<"+-"<<errA<<"\n";
  std::cout<<"B "<<B<<"+-"<<errB<<"\n";
  std::cout<<"C "<<C<<"+-"<<errC<<"\n";
  std::cout<<"D "<<D<<"+-"<<errD<<"\n";

  drawing.IsoPlotsJetToE(Data.hists["e_iso_et_tight"], Data.hists["e_iso_et_antitight"], "e_iso_et");
  drawing.IsoPlotsJetToE(Data.hists["e_iso_pt_tight"], Data.hists["e_iso_pt_antitight"], "e_iso_pt");
  // drawing2.hist_plotter(Data.hists["pT_MET_iso"], Data.hists["pT_MET_noniso"]);

  std::cout<<"Total background contributions in CR A:\n";

  std::cout<<WGamma.SName<<" Total: "<<WGamma.hists["A_region_e_eta"]->IntegralAndError(1,WGamma.hists["A_region_e_eta"]->GetNbinsX(),err)<<"+-"<<err<<std::endl;

  std::cout<<ZllGamma.SName<<" Total: "<<ZllGamma.hists["A_region_e_eta"]->IntegralAndError(1,ZllGamma.hists["A_region_e_eta"]->GetNbinsX(),err)<<"+-"<<err<<std::endl;
 
  std::cout<<JetToMET.SName<<" Total: "<<JetToMET.hists["A_region_e_eta"]->IntegralAndError(1,JetToMET.hists["A_region_e_eta"]->GetNbinsX(),err)<<"+-"<<err<<std::endl;

  std::cout<<EToGam.SName<<" Total: "<<EToGam.hists["A_region_e_eta"]->IntegralAndError(1,EToGam.hists["A_region_e_eta"]->GetNbinsX(),err)<<"+-"<<err<<std::endl;
  
  std::cout<<JetToGam.SName<<" Total: "<<JetToGam.hists["A_region_e_eta"]->IntegralAndError(1,JetToGam.hists["A_region_e_eta"]->GetNbinsX(),err)<<"+-"<<err<<std::endl;

  std::cout<<tta_1lep.SName<<" Total: "<<tta_1lep.hists["A_region_e_eta"]->IntegralAndError(1,tta_1lep.hists["A_region_e_eta"]->GetNbinsX(),err)<<"+-"<<err<<std::endl;
  
  std::cout<<nunugamma.SName<<" Total: "<<nunugamma.hists["A_region_e_eta"]->IntegralAndError(1,nunugamma.hists["A_region_e_eta"]->GetNbinsX(),err)<<"+-"<<err<<std::endl;
  
  std::cout<<Wgamma2jEWK.SName<<" Total: "<<Wgamma2jEWK.hists["A_region_e_eta"]->IntegralAndError(1,Wgamma2jEWK.hists["A_region_e_eta"]->GetNbinsX(),err)<<"+-"<<err<<std::endl;
  
  std::cout<<Znunugamma2jEWK.SName<<" Total: "<<Znunugamma2jEWK.hists["A_region_e_eta"]->IntegralAndError(1,Znunugamma2jEWK.hists["A_region_e_eta"]->GetNbinsX(),err)<<"+-"<<err<<std::endl;

  std::cout<<multijet.SName<<" Total: "<<multijet.hists["A_region_e_eta"]->IntegralAndError(1,multijet.hists["A_region_e_eta"]->GetNbinsX(),err)<<"+-"<<err<<std::endl;
  
  std::cout<<tops.SName<<" Total: "<<tops.hists["A_region_e_eta"]->IntegralAndError(1,tops.hists["A_region_e_eta"]->GetNbinsX(),err)<<"+-"<<err<<std::endl;

  std::cout<<Wtaunu.SName<<" Total: "<<Wtaunu.hists["A_region_e_eta"]->IntegralAndError(1,Wtaunu.hists["A_region_e_eta"]->GetNbinsX(),err)<<"+-"<<err<<std::endl;

  std::cout<<"Total background contributions in CR B:\n";

  std::cout<<WGamma.SName<<" Total: "<<WGamma.hists["B_region_e_eta"]->IntegralAndError(1,WGamma.hists["B_region_e_eta"]->GetNbinsX(),err)<<"+-"<<err<<std::endl;

  std::cout<<ZllGamma.SName<<" Total: "<<ZllGamma.hists["B_region_e_eta"]->IntegralAndError(1,ZllGamma.hists["B_region_e_eta"]->GetNbinsX(),err)<<"+-"<<err<<std::endl;
 
  std::cout<<JetToMET.SName<<" Total: "<<JetToMET.hists["B_region_e_eta"]->IntegralAndError(1,JetToMET.hists["B_region_e_eta"]->GetNbinsX(),err)<<"+-"<<err<<std::endl;

  std::cout<<EToGam.SName<<" Total: "<<EToGam.hists["B_region_e_eta"]->IntegralAndError(1,EToGam.hists["B_region_e_eta"]->GetNbinsX(),err)<<"+-"<<err<<std::endl;
  
  std::cout<<JetToGam.SName<<" Total: "<<JetToGam.hists["B_region_e_eta"]->IntegralAndError(1,JetToGam.hists["B_region_e_eta"]->GetNbinsX(),err)<<"+-"<<err<<std::endl;

  std::cout<<tta_1lep.SName<<" Total: "<<tta_1lep.hists["B_region_e_eta"]->IntegralAndError(1,tta_1lep.hists["B_region_e_eta"]->GetNbinsX(),err)<<"+-"<<err<<std::endl;
  
  std::cout<<nunugamma.SName<<" Total: "<<nunugamma.hists["B_region_e_eta"]->IntegralAndError(1,nunugamma.hists["B_region_e_eta"]->GetNbinsX(),err)<<"+-"<<err<<std::endl;
  
  std::cout<<Wgamma2jEWK.SName<<" Total: "<<Wgamma2jEWK.hists["B_region_e_eta"]->IntegralAndError(1,Wgamma2jEWK.hists["B_region_e_eta"]->GetNbinsX(),err)<<"+-"<<err<<std::endl;
  
  std::cout<<Znunugamma2jEWK.SName<<" Total: "<<Znunugamma2jEWK.hists["B_region_e_eta"]->IntegralAndError(1,Znunugamma2jEWK.hists["B_region_e_eta"]->GetNbinsX(),err)<<"+-"<<err<<std::endl;

  std::cout<<multijet.SName<<" Total: "<<multijet.hists["B_region_e_eta"]->IntegralAndError(1,multijet.hists["B_region_e_eta"]->GetNbinsX(),err)<<"+-"<<err<<std::endl;
  
  std::cout<<tops.SName<<" Total: "<<tops.hists["B_region_e_eta"]->IntegralAndError(1,tops.hists["B_region_e_eta"]->GetNbinsX(),err)<<"+-"<<err<<std::endl;

  std::cout<<Wtaunu.SName<<" Total: "<<Wtaunu.hists["B_region_e_eta"]->IntegralAndError(1,Wtaunu.hists["B_region_e_eta"]->GetNbinsX(),err)<<"+-"<<err<<std::endl;

  std::cout<<"Total background contributions in CR C:\n";

  std::cout<<WGamma.SName<<" Total: "<<WGamma.hists["C_region_e_eta"]->IntegralAndError(1,WGamma.hists["C_region_e_eta"]->GetNbinsX(),err)<<"+-"<<err<<std::endl;

  std::cout<<ZllGamma.SName<<" Total: "<<ZllGamma.hists["C_region_e_eta"]->IntegralAndError(1,ZllGamma.hists["C_region_e_eta"]->GetNbinsX(),err)<<"+-"<<err<<std::endl;
 
  std::cout<<JetToMET.SName<<" Total: "<<JetToMET.hists["C_region_e_eta"]->IntegralAndError(1,JetToMET.hists["C_region_e_eta"]->GetNbinsX(),err)<<"+-"<<err<<std::endl;

  std::cout<<EToGam.SName<<" Total: "<<EToGam.hists["C_region_e_eta"]->IntegralAndError(1,EToGam.hists["C_region_e_eta"]->GetNbinsX(),err)<<"+-"<<err<<std::endl;
  
  std::cout<<JetToGam.SName<<" Total: "<<JetToGam.hists["C_region_e_eta"]->IntegralAndError(1,JetToGam.hists["C_region_e_eta"]->GetNbinsX(),err)<<"+-"<<err<<std::endl;

  std::cout<<tta_1lep.SName<<" Total: "<<tta_1lep.hists["C_region_e_eta"]->IntegralAndError(1,tta_1lep.hists["C_region_e_eta"]->GetNbinsX(),err)<<"+-"<<err<<std::endl;
  
  std::cout<<nunugamma.SName<<" Total: "<<nunugamma.hists["C_region_e_eta"]->IntegralAndError(1,nunugamma.hists["C_region_e_eta"]->GetNbinsX(),err)<<"+-"<<err<<std::endl;
  
  std::cout<<Wgamma2jEWK.SName<<" Total: "<<Wgamma2jEWK.hists["C_region_e_eta"]->IntegralAndError(1,Wgamma2jEWK.hists["C_region_e_eta"]->GetNbinsX(),err)<<"+-"<<err<<std::endl;
  
  std::cout<<Znunugamma2jEWK.SName<<" Total: "<<Znunugamma2jEWK.hists["C_region_e_eta"]->IntegralAndError(1,Znunugamma2jEWK.hists["C_region_e_eta"]->GetNbinsX(),err)<<"+-"<<err<<std::endl;

  std::cout<<multijet.SName<<" Total: "<<multijet.hists["C_region_e_eta"]->IntegralAndError(1,multijet.hists["C_region_e_eta"]->GetNbinsX(),err)<<"+-"<<err<<std::endl;
  
  std::cout<<tops.SName<<" Total: "<<tops.hists["C_region_e_eta"]->IntegralAndError(1,tops.hists["C_region_e_eta"]->GetNbinsX(),err)<<"+-"<<err<<std::endl;

  std::cout<<Wtaunu.SName<<" Total: "<<Wtaunu.hists["C_region_e_eta"]->IntegralAndError(1,Wtaunu.hists["C_region_e_eta"]->GetNbinsX(),err)<<"+-"<<err<<std::endl;

  std::cout<<"Total background contributions in CR D:\n";

  std::cout<<WGamma.SName<<" Total: "<<WGamma.hists["D_region_e_eta"]->IntegralAndError(1,WGamma.hists["D_region_e_eta"]->GetNbinsX(),err)<<"+-"<<err<<std::endl;

  std::cout<<ZllGamma.SName<<" Total: "<<ZllGamma.hists["D_region_e_eta"]->IntegralAndError(1,ZllGamma.hists["D_region_e_eta"]->GetNbinsX(),err)<<"+-"<<err<<std::endl;
 
  std::cout<<JetToMET.SName<<" Total: "<<JetToMET.hists["D_region_e_eta"]->IntegralAndError(1,JetToMET.hists["D_region_e_eta"]->GetNbinsX(),err)<<"+-"<<err<<std::endl;

  std::cout<<EToGam.SName<<" Total: "<<EToGam.hists["D_region_e_eta"]->IntegralAndError(1,EToGam.hists["D_region_e_eta"]->GetNbinsX(),err)<<"+-"<<err<<std::endl;
  
  std::cout<<JetToGam.SName<<" Total: "<<JetToGam.hists["D_region_e_eta"]->IntegralAndError(1,JetToGam.hists["D_region_e_eta"]->GetNbinsX(),err)<<"+-"<<err<<std::endl;

  std::cout<<tta_1lep.SName<<" Total: "<<tta_1lep.hists["D_region_e_eta"]->IntegralAndError(1,tta_1lep.hists["D_region_e_eta"]->GetNbinsX(),err)<<"+-"<<err<<std::endl;
  
  std::cout<<nunugamma.SName<<" Total: "<<nunugamma.hists["D_region_e_eta"]->IntegralAndError(1,nunugamma.hists["D_region_e_eta"]->GetNbinsX(),err)<<"+-"<<err<<std::endl;
  
  std::cout<<Wgamma2jEWK.SName<<" Total: "<<Wgamma2jEWK.hists["D_region_e_eta"]->IntegralAndError(1,Wgamma2jEWK.hists["D_region_e_eta"]->GetNbinsX(),err)<<"+-"<<err<<std::endl;
  
  std::cout<<Znunugamma2jEWK.SName<<" Total: "<<Znunugamma2jEWK.hists["D_region_e_eta"]->IntegralAndError(1,Znunugamma2jEWK.hists["D_region_e_eta"]->GetNbinsX(),err)<<"+-"<<err<<std::endl;

  std::cout<<multijet.SName<<" Total: "<<multijet.hists["D_region_e_eta"]->IntegralAndError(1,multijet.hists["D_region_e_eta"]->GetNbinsX(),err)<<"+-"<<err<<std::endl;
  
  std::cout<<tops.SName<<" Total: "<<tops.hists["D_region_e_eta"]->IntegralAndError(1,tops.hists["D_region_e_eta"]->GetNbinsX(),err)<<"+-"<<err<<std::endl;

  std::cout<<Wtaunu.SName<<" Total: "<<Wtaunu.hists["D_region_e_eta"]->IntegralAndError(1,Wtaunu.hists["D_region_e_eta"]->GetNbinsX(),err)<<"+-"<<err<<std::endl;

Data-=WGamma;
Data-=ZllGamma;
Data-=JetToMET;
Data-=EToGam;
Data-=tta_1lep;
Data-=nunugamma;
Data-=Wgamma2jEWK;
Data-=Znunugamma2jEWK;
Data-=tops;
Data-=Wtaunu;

  A=Data.hists["A_region_e_eta"]->IntegralAndError(1,Data.hists["A_region_e_eta"]->GetNbinsX(),errA);
  B=Data.hists["B_region_e_eta"]->IntegralAndError(1,Data.hists["B_region_e_eta"]->GetNbinsX(),errB);
  C=Data.hists["C_region_e_eta"]->IntegralAndError(1,Data.hists["C_region_e_eta"]->GetNbinsX(),errC);
  D=Data.hists["D_region_e_eta"]->IntegralAndError(1,Data.hists["D_region_e_eta"]->GetNbinsX(),errD);
  std::cout<<"Regions after subtraction: \n";
  std::cout<<"A "<<A<<"+-"<<errA<<"\n";
  std::cout<<"B "<<B<<"+-"<<errB<<"\n";
  std::cout<<"C "<<C<<"+-"<<errC<<"\n";
  std::cout<<"D "<<D<<"+-"<<errD<<"\n";
  std::cout<<"rough estimation: "<<B*C/D<<"±"<<sqrt(errB*errB*C*C/D/D+errC*errC*B*B/D/D+errD*errD*B*B*C*C/D/D/D/D)<<"\n";

  multijet+=JetToGam;

  A=multijet.hists["A_region_e_eta"]->IntegralAndError(1,multijet.hists["A_region_e_eta"]->GetNbinsX(),errA);
  B=multijet.hists["B_region_e_eta"]->IntegralAndError(1,multijet.hists["B_region_e_eta"]->GetNbinsX(),errB);
  C=multijet.hists["C_region_e_eta"]->IntegralAndError(1,multijet.hists["C_region_e_eta"]->GetNbinsX(),errC);
  D=multijet.hists["D_region_e_eta"]->IntegralAndError(1,multijet.hists["D_region_e_eta"]->GetNbinsX(),errD);
  std::cout<<"MC jet to e prediction: \n";
  std::cout<<"A "<<A<<"+-"<<errA<<"\n";
  std::cout<<"B "<<B<<"+-"<<errB<<"\n";
  std::cout<<"C "<<C<<"+-"<<errC<<"\n";
  std::cout<<"D "<<D<<"+-"<<errD<<"\n";

  std::cout<<"R factor MC: "<<A*D/B/C<<"±"<<sqrt(errA*errA*D*D/B/B/C/C+errD*errD*A*A/B/B/C/C+errB*errB*D*D*A*A/B/B/B/B/C/C+errC*errC*D*D*A*A/B/B/C/C/C/C)<<"\n";

  A=JetToGam.hists["A_region_e_eta"]->IntegralAndError(1,JetToGam.hists["A_region_e_eta"]->GetNbinsX(),errA);
  B=JetToGam.hists["B_region_e_eta"]->IntegralAndError(1,JetToGam.hists["B_region_e_eta"]->GetNbinsX(),errB);
  C=JetToGam.hists["C_region_e_eta"]->IntegralAndError(1,JetToGam.hists["C_region_e_eta"]->GetNbinsX(),errC);
  D=JetToGam.hists["D_region_e_eta"]->IntegralAndError(1,JetToGam.hists["D_region_e_eta"]->GetNbinsX(),errD);
  std::cout<<"MC Znunu prediction: \n";
  std::cout<<"A "<<A<<"+-"<<errA<<"\n";
  std::cout<<"B "<<B<<"+-"<<errB<<"\n";
  std::cout<<"C "<<C<<"+-"<<errC<<"\n";
  std::cout<<"D "<<D<<"+-"<<errD<<"\n";
  std::cout<<"R factor MC Znunu: "<<A*D/B/C<<"±"<<sqrt(errA*errA*D*D/B/B/C/C+errD*errD*A*A/B/B/C/C+errB*errB*D*D*A*A/B/B/B/B/C/C+errC*errC*D*D*A*A/B/B/C/C/C/C)<<"\n";


  drawing.IsoPlotsJetToE(multijet.hists["e_iso_et_tight"], multijet.hists["e_iso_et_antitight"], "mc_e_iso_et");
  drawing.IsoPlotsJetToE(multijet.hists["e_iso_pt_tight"], multijet.hists["e_iso_pt_antitight"], "mc_e_iso_pt");
  Data.hists["pT_MET_iso"]->Scale(1/Data.hists["pT_MET_iso"]->Integral());
  Data.hists["pT_MET_noniso"]->Scale(1/Data.hists["pT_MET_noniso"]->Integral());
  drawing2.hist_plotter(Data.hists["pT_MET_iso"], Data.hists["pT_MET_noniso"]);
  // drawing.IsoPlotsJetToE(Data.hists["e_iso_et_tight"], Data.hists["e_iso_et_antitight"], "e_iso_et");
  // drawing.IsoPlotsJetToE(Data.hists["e_iso_pt_tight"], Data.hists["e_iso_pt_antitight"], "e_iso_pt");

  theApp.Run();

}

void spectra(int argc, char **argv){
  Config conf("../ZGamma.cfg");
  TApplication theApp("tapp", &argc, argv);
  Plotter drawing;
  StrV data;
  DirectoryReader *dir1=0;
      Double_t Nee=0;
      Double_t Negam=0;
      Double_t eeBKG=0;
      Double_t eyBKG=0;
      Double_t dNee=0;
      Double_t dNegam=0;
      Double_t deeBKG=0;
      Double_t deyBKG=0;

  map<TString,StrV> mc16a,mc16d,mc16e;
  StrV cath = {"enugamma","enu","Zee","top","ttg","eeg", "jets", "SinglePhoton"};
  Double_t err=0; Double_t unc=0;
  // conf.setValue("bkgFit", "NO");
  conf.setValue("FromMC", "YES");
  conf.setValue("Central", "YES");
  conf.setValue("HighPt", "YES");
  conf.setValue("PlotsMass", "NO"); //to use all spectra: yes, peak: no
  std::cout<<"region:"<<std::endl<<"Central: "<<conf.getBool("Central")<<std::endl<<"HighPt: "<<conf.getBool("HighPt")<<"PlotsMass: "<<conf.getBool("PlotsMass")<<std::endl;
  
  double lumi = conf.getNum("Lumi2015")+conf.getNum("Lumi2016");
    if (conf.isDefined("MC16aDir_etogam")) {
      dir1 = new DirectoryReader(conf.getStr("MC16aDir_etogam"),cath);
      mc16a = dir1->GetFilesList();
    }
    
    std::cout<<"making sample cathegorization for MC16a\n\n";
    for (auto p: mc16a){
      std::cout<<p.first<<":\n";
      for (auto p2: p.second){
        std::cout<<p2<<"\n";
      }
    }
    auto Zee = CalculationFakeRate(mc16a["Zee"],"Zee_mc16a",lumi,conf);
    auto enug = CalculationFakeRate(mc16a["enugamma"],"enug_mc16a",lumi,conf);
    auto enu = CalculationFakeRate(mc16a["enu"],"enu_mc16a",lumi,conf);
    auto top = CalculationFakeRate(mc16a["top"],"top_mc16a",lumi,conf);
    auto ttg = CalculationFakeRate(mc16a["ttg"],"ttg_mc16a",lumi,conf);
    auto eeg = CalculationFakeRate(mc16a["eeg"],"eeg_mc16a",lumi,conf);
    auto jets = CalculationFakeRate(mc16a["jets"],"jets_mc16a",lumi,conf);
    auto gj = CalculationFakeRate(mc16a["SinglePhoton"],"gj_mc16a",lumi,conf);

  lumi = conf.getNum("Lumi2017");
    if (conf.isDefined("MC16dDir_etogam")) {
      dir1 = new DirectoryReader(conf.getStr("MC16dDir_etogam"),cath);
      mc16d = dir1->GetFilesList();
    }
    
    std::cout<<"making sample cathegorization for MC16d\n\n";
    for (auto p: mc16d){
      std::cout<<p.first<<":\n";
      for (auto p2: p.second){
        std::cout<<p2<<"\n";
      }
    }
    auto ZeeD = CalculationFakeRate(mc16d["Zee"],"Zee_mc16d",lumi,conf);
    auto enugD = CalculationFakeRate(mc16d["enugamma"],"enug_mc16a",lumi,conf);
    auto enuD = CalculationFakeRate(mc16d["enu"],"enu_mc16d",lumi,conf);
    auto topD = CalculationFakeRate(mc16d["top"],"top_mc16d",lumi,conf);
    auto ttgD = CalculationFakeRate(mc16d["ttg"],"ttg_mc16d",lumi,conf);
    auto eegD = CalculationFakeRate(mc16d["eeg"],"eeg_mc16d",lumi,conf);
    auto jetsD = CalculationFakeRate(mc16d["jets"],"jets_mc16d",lumi,conf);
    auto gjD = CalculationFakeRate(mc16d["SinglePhoton"],"gj_mc16d",lumi,conf);
    Zee+=ZeeD;
    enug+=enugD;
    enu+=enuD;
    top+=topD;
    ttg+=ttgD;
    eeg+=eegD;
    jets+=jetsD;
    gj+=gjD;

  lumi = conf.getNum("Lumi2018");
    if (conf.isDefined("MC16eDir_etogam")) {
      dir1 = new DirectoryReader(conf.getStr("MC16eDir_etogam"),cath);
      mc16e = dir1->GetFilesList();
    }
    
    std::cout<<"making sample cathegorization for MC16e\n\n";
    for (auto p: mc16e){
      std::cout<<p.first<<":\n";
      for (auto p2: p.second){
        std::cout<<p2<<"\n";
      }
    }
    auto ZeeE = CalculationFakeRate(mc16e["Zee"],"Zee_mc16e",lumi,conf);
    auto enugE = CalculationFakeRate(mc16e["enugamma"],"enug_mc16e",lumi,conf);
    auto enuE = CalculationFakeRate(mc16e["enu"],"enu_mc16e",lumi,conf);
    auto topE = CalculationFakeRate(mc16e["top"],"top_mc16e",lumi,conf);
    auto ttgE = CalculationFakeRate(mc16e["ttg"],"ttg_mc16e",lumi,conf);
    auto eegE = CalculationFakeRate(mc16e["eeg"],"eeg_mc16e",lumi,conf);
    auto jetsE = CalculationFakeRate(mc16e["jets"],"jets_mc16e",lumi,conf);
    auto gjE = CalculationFakeRate(mc16e["SinglePhoton"],"gj_mc16e",lumi,conf);
    Zee+=ZeeE;
    enug+=enugE;
    enu+=enuE;
    top+=topE;
    ttg+=ttgE;
    eeg+=eegE;
    jets+=jetsE;
    gj+=gjE;

    Nee=Zee.hists["M_yee"]->IntegralAndError(1,Zee.hists["M_yee"]->GetNbinsX(),dNee);
    Negam=Zee.hists["M_yey"]->IntegralAndError(1,Zee.hists["M_yey"]->GetNbinsX(),dNegam);
    printf("\nZee Negam %.0f±%.0f Nee %.0f±%.0f\n",Negam,dNegam,Nee,dNee);
    Nee=enug.hists["M_yee"]->IntegralAndError(1,enug.hists["M_yee"]->GetNbinsX(),dNee);
    Negam=enug.hists["M_yey"]->IntegralAndError(1,enug.hists["M_yey"]->GetNbinsX(),dNegam);
    printf("\nenug Negam %.0f±%.0f Nee %.0f±%.0f\n",Negam,dNegam,Nee,dNee);
    Nee=enu.hists["M_yee"]->IntegralAndError(1,enu.hists["M_yee"]->GetNbinsX(),dNee);
    Negam=enu.hists["M_yey"]->IntegralAndError(1,enu.hists["M_yey"]->GetNbinsX(),dNegam);
    printf("\nenu Negam %.0f±%.0f Nee %.0f±%.0f\n",Negam,dNegam,Nee,dNee);
    Nee=top.hists["M_yee"]->IntegralAndError(1,top.hists["M_yee"]->GetNbinsX(),dNee);
    Negam=top.hists["M_yey"]->IntegralAndError(1,top.hists["M_yey"]->GetNbinsX(),dNegam);
    printf("\ntop Negam %.0f±%.0f Nee %.0f±%.0f\n",Negam,dNegam,Nee,dNee);
    Nee=ttg.hists["M_yee"]->IntegralAndError(1,ttg.hists["M_yee"]->GetNbinsX(),dNee);
    Negam=ttg.hists["M_yey"]->IntegralAndError(1,ttg.hists["M_yey"]->GetNbinsX(),dNegam);
    printf("\nttg Negam %.0f±%.0f Nee %.0f±%.0f\n",Negam,dNegam,Nee,dNee);
    Nee=eeg.hists["M_yee"]->IntegralAndError(1,eeg.hists["M_yee"]->GetNbinsX(),dNee);
    Negam=eeg.hists["M_yey"]->IntegralAndError(1,eeg.hists["M_yey"]->GetNbinsX(),dNegam);
    printf("\neeg Negam %.0f±%.0f Nee %.0f±%.0f\n",Negam,dNegam,Nee,dNee);
    Nee=jets.hists["M_yee"]->IntegralAndError(1,jets.hists["M_yee"]->GetNbinsX(),dNee);
    Negam=jets.hists["M_yey"]->IntegralAndError(1,jets.hists["M_yey"]->GetNbinsX(),dNegam);
    printf("\njets Negam %.0f±%.0f Nee %.0f±%.0f\n",Negam,dNegam,Nee,dNee);
    Nee=gj.hists["M_yee"]->IntegralAndError(1,gj.hists["M_yee"]->GetNbinsX(),dNee);
    Negam=gj.hists["M_yey"]->IntegralAndError(1,gj.hists["M_yey"]->GetNbinsX(),dNegam);
    printf("\ngj Negam %.0f±%.0f Nee %.0f±%.0f\n",Negam,dNegam,Nee,dNee);
}

int main(int argc, char **argv) {
  unsigned int key;
cout<<"enter the number of the function you want to call:\n\n";
cout<<" etogam: 1\n duplicheck: 2\n execute: 3\n sumcheck: 4\n RData: 5\n leakage: 6\n isocheck: 7\n hist2dim: 8\n spectra: 9\n JetToE: 10\n";
// cout<<" etogam: 1\n duplicheck: 2\n execute: 3\n sumcheck: 4\n RData: 5\n leakage: 6\n isocheck: 7\n hist2dim: 8\n leptonv: 9\n JetToE: 10\n";
cin>>key;
    switch(key){
            case 1: etogam(argc, argv); break;
            case 2: check(argc,argv); break;
            case 3: execute(argc, argv); break;
            case 4: sumcheck(argc, argv); break;
            case 5: RfromMC(argc, argv); break;
            // case 5: RData(argc, argv); break;
            case 6: leakage(argc, argv); break;
            case 7: isocheck(argc,argv); break;
            case 8: hist2dim(argc,argv); break;
            // case 9: bugfixing(argc,argv); break;
            // case 9: leptonv(argc,argv); break;
            case 9: spectra(argc,argv); break;
            case 10: jettoe(argc,argv); break;
            default: return 0;
    }


  // etogam(argc, argv);
  // duplicheck(argc,argv);
  // execute(argc, argv);
  // sumcheck(argc, argv);
  // RData(argc, argv);
  // leakage(argc, argv);
  // isocheck(argc,argv);
  // hist2dim(argc,argv);
  // bugfixing(argc,argv);
  return 0;
}
